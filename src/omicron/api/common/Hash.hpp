/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_COMMON_HASH_HPP_
#define OMICRON_API_COMMON_HASH_HPP_

#include <cstdint>
#include <ostream>
#include <string>


namespace omi
{

/*!
 * \brief Simple data structure that stores the values of a 128-bit hash.
 *
 * \note This object does not provide functionality for hashing data, it is
 *       purely used for storing and checking the results of hashing
 *       algorithms.
 */
class Hash
{
public:

    //--------------------------------------------------------------------------
    //                             PUBLIC ATTRIBUTES
    //--------------------------------------------------------------------------

    /*!
     * \brief The first 64-bits of the hash.
     */
    uint64_t part1;
    /*!
     * \brief The second 64-bits of the hash.
     */
    uint64_t part2;

    //--------------------------------------------------------------------------
    //                                CONSTRUCTORS
    //--------------------------------------------------------------------------

    /*!
     * \brief Creates a new zero'd hash.
     */
    Hash()
        : part1(0)
        , part2(0)
    {
    }

    /*!
     * \brief Creates a new 128-bit hash from the 2 64-bit values.
     */
    Hash(uint64_t in_part1, uint64_t in_part2)
        : part1(in_part1)
        , part2(in_part2)
    {
    }

    /*!
     * \brief Copy constructor.
     */
    Hash(const Hash& other)
        : part1(other.part1)
        , part2(other.part2)
    {
    }

    /*!
     * \brief Move constructor.
     */
    Hash(Hash&& other)
        : part1(other.part1)
        , part2(other.part2)
    {
        other.part1 = 0;
        other.part2 = 0;
    }

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    ~Hash()
    {
    }

    //--------------------------------------------------------------------------
    //                                 OPERATORS
    //--------------------------------------------------------------------------

    /*!
     * \brief Assignment operator.
     */
    Hash& operator=(const Hash& other)
    {
        part1 = other.part1;
        part2 = other.part2;
        return *this;
    }

    /*!
     * \brief Move assignment operator.
     */
    Hash& operator=(Hash&& other)
    {
        part1 = other.part1;
        part2 = other.part2;
        other.part1 = 0;
        other.part2 = 0;
        return *this;
    }

    /*!
     * \brief Equality operator.
     */
    bool operator==(const Hash& other) const
    {
        return part1 == other.part1 && part2 == other.part2;
    }

    /*!
     * \brief Inequality operator.
     */
    bool operator!=(const Hash& other) const
    {
        return !((*this) == other);
    }
};

} // namespace omi

//------------------------------------------------------------------------------
//                               EXTERNAL OPERATORS
//------------------------------------------------------------------------------

inline std::ostream& operator<<(std::ostream& s, omi::Hash const& h)
{
    s
        << "(" << std::to_string(h.part1) << ", " << std::to_string(h.part2)
        << ")";
    return s;
}

#endif
