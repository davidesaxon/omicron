/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#include <cstring>

#include "omicron/api/API.h"

#include "omicron/api/common/attribute/_codegen/AttributeCSymbols.h"


#ifdef __cplusplus
extern "C" {
#endif

OMI_API_EXPORT int OMI_Attribute_BINDING_IMPL(char const* func_def, void** func_ptr)
{
    if(strcmp(func_def, OMI_Attribute_default_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::default_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_increase_reference_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::increase_reference;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_decrease_reference_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::decrease_reference;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_equal_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::equal;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_delete_str_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::delete_str;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_get_type_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::get_type;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_is_valid_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::is_valid;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_is_immutable_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::is_immutable;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_get_hash_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::get_hash;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_string_repr_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::string_repr;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_data_size_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::data_size;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_data_tuple_size_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::data_tuple_size;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_byte_value_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::byte_value_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_byte_values_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::byte_values_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_byte_get_values_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::byte_get_values;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_byte_set_value_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::byte_set_value;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_byte_set_values_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::byte_set_values;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_int16_value_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::int16_value_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_int16_values_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::int16_values_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_int16_get_values_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::int16_get_values;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_int16_set_value_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::int16_set_value;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_int16_set_values_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::int16_set_values;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_int32_value_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::int32_value_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_int32_values_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::int32_values_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_int32_get_values_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::int32_get_values;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_int32_set_value_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::int32_set_value;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_int32_set_values_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::int32_set_values;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_int64_value_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::int64_value_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_int64_values_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::int64_values_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_int64_get_values_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::int64_get_values;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_int64_set_value_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::int64_set_value;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_int64_set_values_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::int64_set_values;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_float32_value_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::float32_value_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_float32_values_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::float32_values_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_float32_get_values_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::float32_get_values;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_float32_set_value_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::float32_set_value;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_float32_set_values_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::float32_set_values;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_float64_value_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::float64_value_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_float64_values_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::float64_values_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_float64_get_values_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::float64_get_values;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_float64_set_value_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::float64_set_value;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_float64_set_values_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::float64_set_values;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_string_value_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::string_value_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_string_values_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::string_values_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_string_get_values_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::string_get_values;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_string_set_value_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::string_set_value;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_string_set_values_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::string_set_values;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_array_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::array_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_array_size_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::array_size;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_array_get_values_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::array_get_values;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_map_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::map_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_map_size_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::map_size;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_map_get_keys_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::map_get_keys;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_map_get_values_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::map_get_values;
        return 0;
    }
    if(strcmp(func_def, OMI_Attribute_map_find_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::map_find;
        return 0;
    }

    return 1;
}

#ifdef __cplusplus
} // extern "C"
#endif

