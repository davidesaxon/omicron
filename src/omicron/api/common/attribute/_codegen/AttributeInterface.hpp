/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_CODEGEN_ATTRIBUTEINTERFACE_HPP_
#define OMI_CODEGEN_ATTRIBUTEINTERFACE_HPP_

#include <cassert>
#include <string>

#include <arcanecore/base/dl/DLOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>
#include <arcanecore/base/lang/Restrictors.hpp>

#include "omicron/api/common/attribute/_codegen/AttributeCSymbols.h"


namespace omi
{

class AttributeInterface
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                              TYPE DEFINITIONS
    //--------------------------------------------------------------------------

    typedef OMI_Bool (BindingFunc)(char const*, void**);

    //--------------------------------------------------------------------------
    //                             PUBLIC ATTRIBUTES
    //--------------------------------------------------------------------------

    // default_constructor
    OMI_AttributePtr (*default_constructor)();
    // increase_reference
    void (*increase_reference)(OMI_AttributePtr);
    // decrease_reference
    void (*decrease_reference)(OMI_AttributePtr);
    // equal
    OMI_Bool (*equal)(OMI_AttributePtrConst, OMI_AttributePtrConst);
    // delete_str
    void (*delete_str)(char const*);
    // get_type
    OMI_Attribute_Type (*get_type)(OMI_AttributePtrConst);
    // is_valid
    OMI_Bool (*is_valid)(OMI_AttributePtrConst, OMI_Attribute_Type);
    // is_immutable
    OMI_Bool (*is_immutable)(OMI_AttributePtrConst);
    // get_hash
    void (*get_hash)(OMI_AttributePtrConst, uint64_t*, uint64_t*);
    // string_repr
    char const* (*string_repr)(OMI_AttributePtrConst, OMI_Size);
    // data_size
    OMI_Size (*data_size)(OMI_AttributePtrConst);
    // data_tuple_size
    OMI_Size (*data_tuple_size)(OMI_AttributePtrConst);
    // byte_value_constructor
    OMI_AttributePtr (*byte_value_constructor)(char);
    // byte_values_constructor
    OMI_Attribute_Error (*byte_values_constructor)(char const*, OMI_Size, OMI_Size, OMI_Attribute_DataDeleteFunc*, OMI_AttributePtr*);
    // byte_get_values
    OMI_Size (*byte_get_values)(OMI_AttributePtrConst, char const**);
    // byte_set_value
    OMI_Attribute_Error (*byte_set_value)(OMI_AttributePtr, char, OMI_Size);
    // byte_set_values
    OMI_Attribute_Error (*byte_set_values)(OMI_AttributePtr, char const*, OMI_Size);
    // int16_value_constructor
    OMI_AttributePtr (*int16_value_constructor)(int16_t);
    // int16_values_constructor
    OMI_Attribute_Error (*int16_values_constructor)(int16_t const*, OMI_Size, OMI_Size, OMI_Attribute_DataDeleteFunc*, OMI_AttributePtr*);
    // int16_get_values
    OMI_Size (*int16_get_values)(OMI_AttributePtrConst, int16_t const**);
    // int16_set_value
    OMI_Attribute_Error (*int16_set_value)(OMI_AttributePtr, int16_t, OMI_Size);
    // int16_set_values
    OMI_Attribute_Error (*int16_set_values)(OMI_AttributePtr, int16_t const*, OMI_Size);
    // int32_value_constructor
    OMI_AttributePtr (*int32_value_constructor)(int32_t);
    // int32_values_constructor
    OMI_Attribute_Error (*int32_values_constructor)(int32_t const*, OMI_Size, OMI_Size, OMI_Attribute_DataDeleteFunc*, OMI_AttributePtr*);
    // int32_get_values
    OMI_Size (*int32_get_values)(OMI_AttributePtrConst, int32_t const**);
    // int32_set_value
    OMI_Attribute_Error (*int32_set_value)(OMI_AttributePtr, int32_t, OMI_Size);
    // int32_set_values
    OMI_Attribute_Error (*int32_set_values)(OMI_AttributePtr, int32_t const*, OMI_Size);
    // int64_value_constructor
    OMI_AttributePtr (*int64_value_constructor)(int64_t);
    // int64_values_constructor
    OMI_Attribute_Error (*int64_values_constructor)(int64_t const*, OMI_Size, OMI_Size, OMI_Attribute_DataDeleteFunc*, OMI_AttributePtr*);
    // int64_get_values
    OMI_Size (*int64_get_values)(OMI_AttributePtrConst, int64_t const**);
    // int64_set_value
    OMI_Attribute_Error (*int64_set_value)(OMI_AttributePtr, int64_t, OMI_Size);
    // int64_set_values
    OMI_Attribute_Error (*int64_set_values)(OMI_AttributePtr, int64_t const*, OMI_Size);
    // float32_value_constructor
    OMI_AttributePtr (*float32_value_constructor)(float);
    // float32_values_constructor
    OMI_Attribute_Error (*float32_values_constructor)(float const*, OMI_Size, OMI_Size, OMI_Attribute_DataDeleteFunc*, OMI_AttributePtr*);
    // float32_get_values
    OMI_Size (*float32_get_values)(OMI_AttributePtrConst, float const**);
    // float32_set_value
    OMI_Attribute_Error (*float32_set_value)(OMI_AttributePtr, float, OMI_Size);
    // float32_set_values
    OMI_Attribute_Error (*float32_set_values)(OMI_AttributePtr, float const*, OMI_Size);
    // float64_value_constructor
    OMI_AttributePtr (*float64_value_constructor)(double);
    // float64_values_constructor
    OMI_Attribute_Error (*float64_values_constructor)(double const*, OMI_Size, OMI_Size, OMI_Attribute_DataDeleteFunc*, OMI_AttributePtr*);
    // float64_get_values
    OMI_Size (*float64_get_values)(OMI_AttributePtrConst, double const**);
    // float64_set_value
    OMI_Attribute_Error (*float64_set_value)(OMI_AttributePtr, double, OMI_Size);
    // float64_set_values
    OMI_Attribute_Error (*float64_set_values)(OMI_AttributePtr, double const*, OMI_Size);
    // string_value_constructor
    OMI_AttributePtr (*string_value_constructor)(char const*);
    // string_values_constructor
    OMI_Attribute_Error (*string_values_constructor)(char const* const*, OMI_Size, OMI_Size, OMI_Attribute_DataDeleteFunc*, OMI_AttributePtr*);
    // string_get_values
    OMI_Size (*string_get_values)(OMI_AttributePtrConst, char const* const**);
    // string_set_value
    OMI_Attribute_Error (*string_set_value)(OMI_AttributePtr, char const*, OMI_Size);
    // string_set_values
    OMI_Attribute_Error (*string_set_values)(OMI_AttributePtr, char const* const*, OMI_Size);
    // array_constructor
    OMI_AttributePtr (*array_constructor)(OMI_AttributePtr const*, OMI_Size, OMI_Attribute_ArrayDeleteFunc*);
    // array_size
    OMI_Size (*array_size)(OMI_AttributePtrConst);
    // array_get_values
    OMI_Size (*array_get_values)(OMI_AttributePtrConst, OMI_AttributePtr const**);
    // map_constructor
    OMI_AttributePtr (*map_constructor)(char const* const*, OMI_AttributePtr const*, OMI_Size, OMI_Attribute_MapDeleteFunc*);
    // map_size
    OMI_Size (*map_size)(OMI_AttributePtrConst);
    // map_get_keys
    OMI_Size (*map_get_keys)(OMI_AttributePtrConst, char const* const**);
    // map_get_values
    OMI_Size (*map_get_values)(OMI_AttributePtrConst, OMI_AttributePtr const**);
    // map_find
    OMI_Attribute_Error (*map_find)(OMI_AttributePtrConst, char const*, OMI_AttributePtr*);

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    AttributeInterface(std::string const& libname)
        : default_constructor(nullptr)
        , increase_reference(nullptr)
        , decrease_reference(nullptr)
        , equal(nullptr)
        , delete_str(nullptr)
        , get_type(nullptr)
        , is_valid(nullptr)
        , is_immutable(nullptr)
        , get_hash(nullptr)
        , string_repr(nullptr)
        , data_size(nullptr)
        , data_tuple_size(nullptr)
        , byte_value_constructor(nullptr)
        , byte_values_constructor(nullptr)
        , byte_get_values(nullptr)
        , byte_set_value(nullptr)
        , byte_set_values(nullptr)
        , int16_value_constructor(nullptr)
        , int16_values_constructor(nullptr)
        , int16_get_values(nullptr)
        , int16_set_value(nullptr)
        , int16_set_values(nullptr)
        , int32_value_constructor(nullptr)
        , int32_values_constructor(nullptr)
        , int32_get_values(nullptr)
        , int32_set_value(nullptr)
        , int32_set_values(nullptr)
        , int64_value_constructor(nullptr)
        , int64_values_constructor(nullptr)
        , int64_get_values(nullptr)
        , int64_set_value(nullptr)
        , int64_set_values(nullptr)
        , float32_value_constructor(nullptr)
        , float32_values_constructor(nullptr)
        , float32_get_values(nullptr)
        , float32_set_value(nullptr)
        , float32_set_values(nullptr)
        , float64_value_constructor(nullptr)
        , float64_values_constructor(nullptr)
        , float64_get_values(nullptr)
        , float64_set_value(nullptr)
        , float64_set_values(nullptr)
        , string_value_constructor(nullptr)
        , string_values_constructor(nullptr)
        , string_get_values(nullptr)
        , string_set_value(nullptr)
        , string_set_values(nullptr)
        , array_constructor(nullptr)
        , array_size(nullptr)
        , array_get_values(nullptr)
        , map_constructor(nullptr)
        , map_size(nullptr)
        , map_get_keys(nullptr)
        , map_get_values(nullptr)
        , map_find(nullptr)
    {
        #ifdef ARC_OS_WINDOWS
            arc::fsys::Path libpath({
                "build",
                "windows",
                "Release",
                libname + "_impl.dll"
            });
        #elif defined(ARC_OS_UNIX)
            arc::fsys::Path libpath({
                "build",
                "linux",
                "lib" + libname + "_impl.so"
            });
        #else
            assert(false);
        #endif

        arc::dl::Handle lib_handle = arc::dl::open_library(libpath);
        BindingFunc* binding_func = arc::dl::bind_symbol<BindingFunc>(lib_handle, "OMI_Attribute_BINDING_IMPL");

        if(binding_func(OMI_Attribute_default_constructor_SYMBOL, (void**) &default_constructor) != 0)
        {
        }
        if(binding_func(OMI_Attribute_increase_reference_SYMBOL, (void**) &increase_reference) != 0)
        {
        }
        if(binding_func(OMI_Attribute_decrease_reference_SYMBOL, (void**) &decrease_reference) != 0)
        {
        }
        if(binding_func(OMI_Attribute_equal_SYMBOL, (void**) &equal) != 0)
        {
        }
        if(binding_func(OMI_Attribute_delete_str_SYMBOL, (void**) &delete_str) != 0)
        {
        }
        if(binding_func(OMI_Attribute_get_type_SYMBOL, (void**) &get_type) != 0)
        {
        }
        if(binding_func(OMI_Attribute_is_valid_SYMBOL, (void**) &is_valid) != 0)
        {
        }
        if(binding_func(OMI_Attribute_is_immutable_SYMBOL, (void**) &is_immutable) != 0)
        {
        }
        if(binding_func(OMI_Attribute_get_hash_SYMBOL, (void**) &get_hash) != 0)
        {
        }
        if(binding_func(OMI_Attribute_string_repr_SYMBOL, (void**) &string_repr) != 0)
        {
        }
        if(binding_func(OMI_Attribute_data_size_SYMBOL, (void**) &data_size) != 0)
        {
        }
        if(binding_func(OMI_Attribute_data_tuple_size_SYMBOL, (void**) &data_tuple_size) != 0)
        {
        }
        if(binding_func(OMI_Attribute_byte_value_constructor_SYMBOL, (void**) &byte_value_constructor) != 0)
        {
        }
        if(binding_func(OMI_Attribute_byte_values_constructor_SYMBOL, (void**) &byte_values_constructor) != 0)
        {
        }
        if(binding_func(OMI_Attribute_byte_get_values_SYMBOL, (void**) &byte_get_values) != 0)
        {
        }
        if(binding_func(OMI_Attribute_byte_set_value_SYMBOL, (void**) &byte_set_value) != 0)
        {
        }
        if(binding_func(OMI_Attribute_byte_set_values_SYMBOL, (void**) &byte_set_values) != 0)
        {
        }
        if(binding_func(OMI_Attribute_int16_value_constructor_SYMBOL, (void**) &int16_value_constructor) != 0)
        {
        }
        if(binding_func(OMI_Attribute_int16_values_constructor_SYMBOL, (void**) &int16_values_constructor) != 0)
        {
        }
        if(binding_func(OMI_Attribute_int16_get_values_SYMBOL, (void**) &int16_get_values) != 0)
        {
        }
        if(binding_func(OMI_Attribute_int16_set_value_SYMBOL, (void**) &int16_set_value) != 0)
        {
        }
        if(binding_func(OMI_Attribute_int16_set_values_SYMBOL, (void**) &int16_set_values) != 0)
        {
        }
        if(binding_func(OMI_Attribute_int32_value_constructor_SYMBOL, (void**) &int32_value_constructor) != 0)
        {
        }
        if(binding_func(OMI_Attribute_int32_values_constructor_SYMBOL, (void**) &int32_values_constructor) != 0)
        {
        }
        if(binding_func(OMI_Attribute_int32_get_values_SYMBOL, (void**) &int32_get_values) != 0)
        {
        }
        if(binding_func(OMI_Attribute_int32_set_value_SYMBOL, (void**) &int32_set_value) != 0)
        {
        }
        if(binding_func(OMI_Attribute_int32_set_values_SYMBOL, (void**) &int32_set_values) != 0)
        {
        }
        if(binding_func(OMI_Attribute_int64_value_constructor_SYMBOL, (void**) &int64_value_constructor) != 0)
        {
        }
        if(binding_func(OMI_Attribute_int64_values_constructor_SYMBOL, (void**) &int64_values_constructor) != 0)
        {
        }
        if(binding_func(OMI_Attribute_int64_get_values_SYMBOL, (void**) &int64_get_values) != 0)
        {
        }
        if(binding_func(OMI_Attribute_int64_set_value_SYMBOL, (void**) &int64_set_value) != 0)
        {
        }
        if(binding_func(OMI_Attribute_int64_set_values_SYMBOL, (void**) &int64_set_values) != 0)
        {
        }
        if(binding_func(OMI_Attribute_float32_value_constructor_SYMBOL, (void**) &float32_value_constructor) != 0)
        {
        }
        if(binding_func(OMI_Attribute_float32_values_constructor_SYMBOL, (void**) &float32_values_constructor) != 0)
        {
        }
        if(binding_func(OMI_Attribute_float32_get_values_SYMBOL, (void**) &float32_get_values) != 0)
        {
        }
        if(binding_func(OMI_Attribute_float32_set_value_SYMBOL, (void**) &float32_set_value) != 0)
        {
        }
        if(binding_func(OMI_Attribute_float32_set_values_SYMBOL, (void**) &float32_set_values) != 0)
        {
        }
        if(binding_func(OMI_Attribute_float64_value_constructor_SYMBOL, (void**) &float64_value_constructor) != 0)
        {
        }
        if(binding_func(OMI_Attribute_float64_values_constructor_SYMBOL, (void**) &float64_values_constructor) != 0)
        {
        }
        if(binding_func(OMI_Attribute_float64_get_values_SYMBOL, (void**) &float64_get_values) != 0)
        {
        }
        if(binding_func(OMI_Attribute_float64_set_value_SYMBOL, (void**) &float64_set_value) != 0)
        {
        }
        if(binding_func(OMI_Attribute_float64_set_values_SYMBOL, (void**) &float64_set_values) != 0)
        {
        }
        if(binding_func(OMI_Attribute_string_value_constructor_SYMBOL, (void**) &string_value_constructor) != 0)
        {
        }
        if(binding_func(OMI_Attribute_string_values_constructor_SYMBOL, (void**) &string_values_constructor) != 0)
        {
        }
        if(binding_func(OMI_Attribute_string_get_values_SYMBOL, (void**) &string_get_values) != 0)
        {
        }
        if(binding_func(OMI_Attribute_string_set_value_SYMBOL, (void**) &string_set_value) != 0)
        {
        }
        if(binding_func(OMI_Attribute_string_set_values_SYMBOL, (void**) &string_set_values) != 0)
        {
        }
        if(binding_func(OMI_Attribute_array_constructor_SYMBOL, (void**) &array_constructor) != 0)
        {
        }
        if(binding_func(OMI_Attribute_array_size_SYMBOL, (void**) &array_size) != 0)
        {
        }
        if(binding_func(OMI_Attribute_array_get_values_SYMBOL, (void**) &array_get_values) != 0)
        {
        }
        if(binding_func(OMI_Attribute_map_constructor_SYMBOL, (void**) &map_constructor) != 0)
        {
        }
        if(binding_func(OMI_Attribute_map_size_SYMBOL, (void**) &map_size) != 0)
        {
        }
        if(binding_func(OMI_Attribute_map_get_keys_SYMBOL, (void**) &map_get_keys) != 0)
        {
        }
        if(binding_func(OMI_Attribute_map_get_values_SYMBOL, (void**) &map_get_values) != 0)
        {
        }
        if(binding_func(OMI_Attribute_map_find_SYMBOL, (void**) &map_find) != 0)
        {
        }
    }
};

} // namespace omi


#endif