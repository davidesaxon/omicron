/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_CODEGEN_ATTRIBUTECSMYBOLS_H_
#define OMI_CODEGEN_ATTRIBUTECSMYBOLS_H_


#ifdef __cplusplus
extern "C" {
#endif

// OMI_default_constructor
static char const* OMI_Attribute_default_constructor_SYMBOL = "OMI_AttributePtr OMI_Attribute_default_constructor()";

// OMI_increase_reference
static char const* OMI_Attribute_increase_reference_SYMBOL = "void OMI_Attribute_increase_reference(OMI_AttributePtr)";

// OMI_decrease_reference
static char const* OMI_Attribute_decrease_reference_SYMBOL = "void OMI_Attribute_decrease_reference(OMI_AttributePtr)";

// OMI_equal
static char const* OMI_Attribute_equal_SYMBOL = "OMI_Bool OMI_Attribute_equal(OMI_AttributePtrConst, OMI_AttributePtrConst)";

// OMI_delete_str
static char const* OMI_Attribute_delete_str_SYMBOL = "void OMI_Attribute_delete_str(char const*)";

// OMI_get_type
static char const* OMI_Attribute_get_type_SYMBOL = "OMI_Attribute_Type OMI_Attribute_get_type(OMI_AttributePtrConst)";

// OMI_is_valid
static char const* OMI_Attribute_is_valid_SYMBOL = "OMI_Bool OMI_Attribute_is_valid(OMI_AttributePtrConst, OMI_Attribute_Type)";

// OMI_is_immutable
static char const* OMI_Attribute_is_immutable_SYMBOL = "OMI_Bool OMI_Attribute_is_immutable(OMI_AttributePtrConst)";

// OMI_get_hash
static char const* OMI_Attribute_get_hash_SYMBOL = "void OMI_Attribute_get_hash(OMI_AttributePtrConst, uint64_t*, uint64_t*)";

// OMI_string_repr
static char const* OMI_Attribute_string_repr_SYMBOL = "char const* OMI_Attribute_string_repr(OMI_AttributePtrConst, OMI_Size)";

// OMI_data_size
static char const* OMI_Attribute_data_size_SYMBOL = "OMI_Size OMI_Attribute_data_size(OMI_AttributePtrConst)";

// OMI_data_tuple_size
static char const* OMI_Attribute_data_tuple_size_SYMBOL = "OMI_Size OMI_Attribute_data_tuple_size(OMI_AttributePtrConst)";

// OMI_byte_value_constructor
static char const* OMI_Attribute_byte_value_constructor_SYMBOL = "OMI_AttributePtr OMI_Attribute_byte_value_constructor(char)";

// OMI_byte_values_constructor
static char const* OMI_Attribute_byte_values_constructor_SYMBOL = "OMI_Attribute_Error OMI_Attribute_byte_values_constructor(char const*, OMI_Size, OMI_Size, OMI_Attribute_DataDeleteFunc*, OMI_AttributePtr*)";

// OMI_byte_get_values
static char const* OMI_Attribute_byte_get_values_SYMBOL = "OMI_Size OMI_Attribute_byte_get_values(OMI_AttributePtrConst, char const**)";

// OMI_byte_set_value
static char const* OMI_Attribute_byte_set_value_SYMBOL = "OMI_Attribute_Error OMI_Attribute_byte_set_value(OMI_AttributePtr, char, OMI_Size)";

// OMI_byte_set_values
static char const* OMI_Attribute_byte_set_values_SYMBOL = "OMI_Attribute_Error OMI_Attribute_byte_set_values(OMI_AttributePtr, char const*, OMI_Size)";

// OMI_int16_value_constructor
static char const* OMI_Attribute_int16_value_constructor_SYMBOL = "OMI_AttributePtr OMI_Attribute_int16_value_constructor(int16_t)";

// OMI_int16_values_constructor
static char const* OMI_Attribute_int16_values_constructor_SYMBOL = "OMI_Attribute_Error OMI_Attribute_int16_values_constructor(int16_t const*, OMI_Size, OMI_Size, OMI_Attribute_DataDeleteFunc*, OMI_AttributePtr*)";

// OMI_int16_get_values
static char const* OMI_Attribute_int16_get_values_SYMBOL = "OMI_Size OMI_Attribute_int16_get_values(OMI_AttributePtrConst, int16_t const**)";

// OMI_int16_set_value
static char const* OMI_Attribute_int16_set_value_SYMBOL = "OMI_Attribute_Error OMI_Attribute_int16_set_value(OMI_AttributePtr, int16_t, OMI_Size)";

// OMI_int16_set_values
static char const* OMI_Attribute_int16_set_values_SYMBOL = "OMI_Attribute_Error OMI_Attribute_int16_set_values(OMI_AttributePtr, int16_t const*, OMI_Size)";

// OMI_int32_value_constructor
static char const* OMI_Attribute_int32_value_constructor_SYMBOL = "OMI_AttributePtr OMI_Attribute_int32_value_constructor(int32_t)";

// OMI_int32_values_constructor
static char const* OMI_Attribute_int32_values_constructor_SYMBOL = "OMI_Attribute_Error OMI_Attribute_int32_values_constructor(int32_t const*, OMI_Size, OMI_Size, OMI_Attribute_DataDeleteFunc*, OMI_AttributePtr*)";

// OMI_int32_get_values
static char const* OMI_Attribute_int32_get_values_SYMBOL = "OMI_Size OMI_Attribute_int32_get_values(OMI_AttributePtrConst, int32_t const**)";

// OMI_int32_set_value
static char const* OMI_Attribute_int32_set_value_SYMBOL = "OMI_Attribute_Error OMI_Attribute_int32_set_value(OMI_AttributePtr, int32_t, OMI_Size)";

// OMI_int32_set_values
static char const* OMI_Attribute_int32_set_values_SYMBOL = "OMI_Attribute_Error OMI_Attribute_int32_set_values(OMI_AttributePtr, int32_t const*, OMI_Size)";

// OMI_int64_value_constructor
static char const* OMI_Attribute_int64_value_constructor_SYMBOL = "OMI_AttributePtr OMI_Attribute_int64_value_constructor(int64_t)";

// OMI_int64_values_constructor
static char const* OMI_Attribute_int64_values_constructor_SYMBOL = "OMI_Attribute_Error OMI_Attribute_int64_values_constructor(int64_t const*, OMI_Size, OMI_Size, OMI_Attribute_DataDeleteFunc*, OMI_AttributePtr*)";

// OMI_int64_get_values
static char const* OMI_Attribute_int64_get_values_SYMBOL = "OMI_Size OMI_Attribute_int64_get_values(OMI_AttributePtrConst, int64_t const**)";

// OMI_int64_set_value
static char const* OMI_Attribute_int64_set_value_SYMBOL = "OMI_Attribute_Error OMI_Attribute_int64_set_value(OMI_AttributePtr, int64_t, OMI_Size)";

// OMI_int64_set_values
static char const* OMI_Attribute_int64_set_values_SYMBOL = "OMI_Attribute_Error OMI_Attribute_int64_set_values(OMI_AttributePtr, int64_t const*, OMI_Size)";

// OMI_float32_value_constructor
static char const* OMI_Attribute_float32_value_constructor_SYMBOL = "OMI_AttributePtr OMI_Attribute_float32_value_constructor(float)";

// OMI_float32_values_constructor
static char const* OMI_Attribute_float32_values_constructor_SYMBOL = "OMI_Attribute_Error OMI_Attribute_float32_values_constructor(float const*, OMI_Size, OMI_Size, OMI_Attribute_DataDeleteFunc*, OMI_AttributePtr*)";

// OMI_float32_get_values
static char const* OMI_Attribute_float32_get_values_SYMBOL = "OMI_Size OMI_Attribute_float32_get_values(OMI_AttributePtrConst, float const**)";

// OMI_float32_set_value
static char const* OMI_Attribute_float32_set_value_SYMBOL = "OMI_Attribute_Error OMI_Attribute_float32_set_value(OMI_AttributePtr, float, OMI_Size)";

// OMI_float32_set_values
static char const* OMI_Attribute_float32_set_values_SYMBOL = "OMI_Attribute_Error OMI_Attribute_float32_set_values(OMI_AttributePtr, float const*, OMI_Size)";

// OMI_float64_value_constructor
static char const* OMI_Attribute_float64_value_constructor_SYMBOL = "OMI_AttributePtr OMI_Attribute_float64_value_constructor(double)";

// OMI_float64_values_constructor
static char const* OMI_Attribute_float64_values_constructor_SYMBOL = "OMI_Attribute_Error OMI_Attribute_float64_values_constructor(double const*, OMI_Size, OMI_Size, OMI_Attribute_DataDeleteFunc*, OMI_AttributePtr*)";

// OMI_float64_get_values
static char const* OMI_Attribute_float64_get_values_SYMBOL = "OMI_Size OMI_Attribute_float64_get_values(OMI_AttributePtrConst, double const**)";

// OMI_float64_set_value
static char const* OMI_Attribute_float64_set_value_SYMBOL = "OMI_Attribute_Error OMI_Attribute_float64_set_value(OMI_AttributePtr, double, OMI_Size)";

// OMI_float64_set_values
static char const* OMI_Attribute_float64_set_values_SYMBOL = "OMI_Attribute_Error OMI_Attribute_float64_set_values(OMI_AttributePtr, double const*, OMI_Size)";

// OMI_string_value_constructor
static char const* OMI_Attribute_string_value_constructor_SYMBOL = "OMI_AttributePtr OMI_Attribute_string_value_constructor(char const*)";

// OMI_string_values_constructor
static char const* OMI_Attribute_string_values_constructor_SYMBOL = "OMI_Attribute_Error OMI_Attribute_string_values_constructor(char const* const*, OMI_Size, OMI_Size, OMI_Attribute_DataDeleteFunc*, OMI_AttributePtr*)";

// OMI_string_get_values
static char const* OMI_Attribute_string_get_values_SYMBOL = "OMI_Size OMI_Attribute_string_get_values(OMI_AttributePtrConst, char const* const**)";

// OMI_string_set_value
static char const* OMI_Attribute_string_set_value_SYMBOL = "OMI_Attribute_Error OMI_Attribute_string_set_value(OMI_AttributePtr, char const*, OMI_Size)";

// OMI_string_set_values
static char const* OMI_Attribute_string_set_values_SYMBOL = "OMI_Attribute_Error OMI_Attribute_string_set_values(OMI_AttributePtr, char const* const*, OMI_Size)";

// OMI_array_constructor
static char const* OMI_Attribute_array_constructor_SYMBOL = "OMI_AttributePtr OMI_Attribute_array_constructor(OMI_AttributePtr const*, OMI_Size, OMI_Attribute_ArrayDeleteFunc*)";

// OMI_array_size
static char const* OMI_Attribute_array_size_SYMBOL = "OMI_Size OMI_Attribute_array_size(OMI_AttributePtrConst)";

// OMI_array_get_values
static char const* OMI_Attribute_array_get_values_SYMBOL = "OMI_Size OMI_Attribute_array_get_values(OMI_AttributePtrConst, OMI_AttributePtr const**)";

// OMI_map_constructor
static char const* OMI_Attribute_map_constructor_SYMBOL = "OMI_AttributePtr OMI_Attribute_map_constructor(char const* const*, OMI_AttributePtr const*, OMI_Size, OMI_Attribute_MapDeleteFunc*)";

// OMI_map_size
static char const* OMI_Attribute_map_size_SYMBOL = "OMI_Size OMI_Attribute_map_size(OMI_AttributePtrConst)";

// OMI_map_get_keys
static char const* OMI_Attribute_map_get_keys_SYMBOL = "OMI_Size OMI_Attribute_map_get_keys(OMI_AttributePtrConst, char const* const**)";

// OMI_map_get_values
static char const* OMI_Attribute_map_get_values_SYMBOL = "OMI_Size OMI_Attribute_map_get_values(OMI_AttributePtrConst, OMI_AttributePtr const**)";

// OMI_map_find
static char const* OMI_Attribute_map_find_SYMBOL = "OMI_Attribute_Error OMI_Attribute_map_find(OMI_AttributePtrConst, char const*, OMI_AttributePtr*)";

#ifdef __cplusplus
} // extern "C"
#endif

#endif