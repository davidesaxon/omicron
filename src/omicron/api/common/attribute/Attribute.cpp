/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/common/attribute/Attribute.hpp"


namespace omi
{

//------------------------------------------------------------------------------
//                                  CONSTRUTORS
//------------------------------------------------------------------------------

Attribute::Attribute()
    : m_ptr(get_interface().default_constructor())
{
}

Attribute::Attribute(Attribute const& other)
    : m_ptr(other.m_ptr)
{
    get_interface().increase_reference(m_ptr);
}

Attribute::Attribute(Attribute&& other)
    : m_ptr(other.m_ptr)
{
    other.m_ptr = nullptr;
}

Attribute::Attribute(OMI_AttributePtr ptr, bool increase_ref)
    : m_ptr(ptr)
{
    if(increase_ref)
    {
        get_interface().increase_reference(m_ptr);
    }
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

Attribute::~Attribute()
{
    get_interface().decrease_reference(m_ptr);
}

//------------------------------------------------------------------------------
//                                   OPERATORS
//------------------------------------------------------------------------------

Attribute& Attribute::operator=(Attribute const& other)
{
    if(m_ptr != other.m_ptr)
    {
        get_interface().decrease_reference(m_ptr);
        m_ptr = other.m_ptr;
        get_interface().increase_reference(m_ptr);
    }
    return *this;
}

Attribute& Attribute::operator=(Attribute&& other)
{
    get_interface().decrease_reference(m_ptr);
    m_ptr = other.m_ptr;
    other.m_ptr = nullptr;
    return *this;
}

bool Attribute::operator==(Attribute const& other) const
{
    return get_interface().equal(m_ptr, other.m_ptr) != 0;
}

bool Attribute::operator!=(Attribute const& other) const
{
    return !((*this) == other);
}

//------------------------------------------------------------------------------
//                            PUBLIC STATIC FUNCTIONS
//------------------------------------------------------------------------------

AttributeInterface& Attribute::get_interface()
{
    static AttributeInterface inst("omicron_api");
    return inst;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

OMI_AttributePtr Attribute::get_ptr() const
{
    return m_ptr;
}

Attribute::Type Attribute::get_type() const
{
    return static_cast<Type>(get_interface().get_type(m_ptr));
}

bool Attribute::is_valid() const
{
    return get_interface().is_valid(
        m_ptr,
        OMI_Attribute_Type_kAll
    ) != 0;
}

bool Attribute::is_immutable() const
{
    return get_interface().is_immutable(m_ptr) != 0;
}

Hash Attribute::get_hash() const
{
    Hash ret;
    get_interface().get_hash(
        m_ptr,
        &ret.part1,
        &ret.part2
    );
    return ret;
}

void Attribute::string_repr(std::string& s, std::size_t indentation) const
{
    char const* str = get_interface().string_repr(m_ptr, indentation);
    s += str;
    get_interface().delete_str(str);
}

} // namespace omi

//------------------------------------------------------------------------------
//                               EXTERNAL OPERATORS
//------------------------------------------------------------------------------

std::ostream& operator<<(std::ostream& s, omi::Attribute const& a)
{
    std::string str;
    a.string_repr(str);
    s << str;
    return s;
}
