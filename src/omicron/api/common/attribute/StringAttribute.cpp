/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstring>

#include "omicron/api/common/attribute/StringAttribute.hpp"


namespace omi
{

//------------------------------------------------------------------------------
//                                   UTILITIES
//------------------------------------------------------------------------------

namespace
{

static void delete_c_str_func(void const* data, OMI_Size size)
{
    char const* const* strs = (char const* const*) data;
    for(OMI_Size i = 0; i < size; ++i)
    {
        delete[] strs[i];
    }
    delete[] strs;
}

} // namespace anonymous

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

StringAttribute::StringAttribute()
    : DataAttribute(
        get_interface().string_value_constructor(""),
        false
    )
{
}

StringAttribute::StringAttribute(DataType value)
    : DataAttribute(
        get_interface().string_value_constructor(value),
        false
    )
{
}

StringAttribute::StringAttribute(std::string const& value)
    : DataAttribute(
        get_interface().string_value_constructor(value.c_str()),
        false
    )
{
}

StringAttribute::StringAttribute(
            DataType* values,
            std::size_t size,
            std::size_t tuple_size,
            DataDeleteFunc* data_delete_func)
    : DataAttribute(nullptr, false)
{
    OMI_Attribute_Error ec = get_interface().string_values_constructor(
        values,
        size,
        tuple_size,
        data_delete_func,
        &m_ptr
    );

    if(ec == OMI_Attribute_Error_kInvalidSize)
    {
        throw arc::ex::ValueError(
            "Invalid attribute size: " + std::to_string(size)
        );
    }
    if(ec == OMI_Attribute_Error_kInvalidTupleSize)
    {
        throw arc::ex::ValueError(
            "Invalid attribute tuple size: " + std::to_string(tuple_size)
        );
    }
}

StringAttribute::StringAttribute(
        std::vector<std::string> const& values,
        std::size_t tuple_size)
    : DataAttribute(nullptr, false)
{
    char** strs = new char*[values.size()];
    for(std::size_t i = 0; i < values.size(); ++i)
    {
        std::size_t const str_size = values[i].length() + 1;
        char* str = new char[str_size];
        std::memcpy(str, values[i].c_str(), str_size);
        strs[i] = str;
    }

    OMI_Attribute_Error ec = get_interface().string_values_constructor(
        strs,
        values.size(),
        tuple_size,
        &delete_c_str_func,
        &m_ptr
    );

    if(ec == OMI_Attribute_Error_kInvalidSize)
    {
        throw arc::ex::ValueError(
            "Invalid attribute size: " + std::to_string(values.size())
        );
    }
    if(ec == OMI_Attribute_Error_kInvalidTupleSize)
    {
        throw arc::ex::ValueError(
            "Invalid attribute tuple size: " + std::to_string(tuple_size)
        );
    }
}

StringAttribute::StringAttribute(Attribute const& other)
    : DataAttribute(other)
{
}

StringAttribute::StringAttribute(Attribute&& other)
    : DataAttribute(std::move(other))
{
}

//------------------------------------------------------------------------------
//                                  DESTRUCTORS
//------------------------------------------------------------------------------

StringAttribute::~StringAttribute()
{
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

bool StringAttribute::is_valid() const
{
    return get_interface().is_valid(
        m_ptr,
        OMI_Attribute_Type_kString
    ) != 0;
}

StringAttribute::DataType StringAttribute::get_value(
        DataType default_value) const
{
    DataType const* values = nullptr;
    OMI_Size size = get_interface().string_get_values(m_ptr, &values);
    if(values == nullptr || size == 0)
    {
        return default_value;
    }
    return values[0];
}

StringAttribute::ArrayType StringAttribute::get_values() const
{
    DataType const* values = nullptr;
    OMI_Size size = get_interface().string_get_values(m_ptr, &values);
    if(values == nullptr || size == 0)
    {
        throw arc::ex::ValueError(
            "Cannot retrieve values from invalid StringAttribute"
        );
    }
    return ArrayType(values, size);
}

void StringAttribute::set_value(DataType value, std::size_t index)
{
    OMI_Attribute_Error ec = get_interface().string_set_value(
        m_ptr,
        value,
        index
    );

    if(ec == OMI_Attribute_Error_kInvalidAttr)
    {
        throw arc::ex::ValueError(
            "Cannot set value on invalid StringAttribute"
        );
    }
    if(ec == OMI_Attribute_Error_kImmutable)
    {
        throw arc::ex::ValueError(
            "Cannot set value on immutable StringAttribute"
        );
    }
    if(ec == OMI_Attribute_Error_kIndex)
    {
        throw arc::ex::IndexError(
            "Cannot set value at index " + std::to_string(index) + " on "
            "StringAttribute as it is out of range of the attribute's size"
        );
    }
}

void StringAttribute::set_value(std::string const& value, std::size_t index)
{
    set_value(value.c_str(), index);
}

void StringAttribute::set_values(DataType* values, std::size_t size)
{
    OMI_Attribute_Error ec = get_interface().string_set_values(
        m_ptr,
        values,
        size
    );

    if(ec == OMI_Attribute_Error_kInvalidAttr)
    {
        throw arc::ex::ValueError(
            "Cannot set value on invalid StringAttribute"
        );
    }
    if(ec == OMI_Attribute_Error_kImmutable)
    {
        throw arc::ex::ValueError(
            "Cannot set value on immutable StringAttribute"
        );
    }
}

} // namespace omi
