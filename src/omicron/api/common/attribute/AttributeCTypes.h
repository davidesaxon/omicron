/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_COMMON_ATTRIBUTE_ATTRIBUTECTYPES_H_
#define OMICRON_API_COMMON_ATTRIBUTE_ATTRIBUTECTYPES_H_

#include <stdint.h>

#include "omicron/api/API.h"


#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
//                                ATTRIBUTE STRUCT
//------------------------------------------------------------------------------

typedef struct OMI_AttributeStruct* OMI_AttributePtr;
typedef struct OMI_AttributeStruct const* OMI_AttributePtrConst;

//------------------------------------------------------------------------------
//                             ATTRIBUTE ERROR CODES
//------------------------------------------------------------------------------

typedef uint32_t OMI_Attribute_Error;
#define OMI_Attribute_Error_kNone             0
#define OMI_Attribute_Error_kInvalidSize      1
#define OMI_Attribute_Error_kInvalidTupleSize 2
#define OMI_Attribute_Error_kInvalidAttr      3
#define OMI_Attribute_Error_kKey              4
#define OMI_Attribute_Error_kImmutable        5
#define OMI_Attribute_Error_kIndex            6

//------------------------------------------------------------------------------
//                                 ATTRIBUTE TYPE
//------------------------------------------------------------------------------

typedef uint32_t OMI_Attribute_Type;
#define OMI_Attribute_Type_kNull    0
#define OMI_Attribute_Type_kByte    1
#define OMI_Attribute_Type_kInt16   2
#define OMI_Attribute_Type_kInt32   4
#define OMI_Attribute_Type_kInt64   8
#define OMI_Attribute_Type_kFloat32 16
#define OMI_Attribute_Type_kFloat64 32
#define OMI_Attribute_Type_kString  64
#define OMI_Attribute_Type_kArray   128
#define OMI_Attribute_Type_kMap     256
#define OMI_Attribute_Type_kAll     0xFFFFFFFFU
#define OMI_Attribute_Type_kData  \
    (OMI_Attribute_Type_kByte    | \
     OMI_Attribute_Type_kInt16   | \
     OMI_Attribute_Type_kInt32   | \
     OMI_Attribute_Type_kInt64   | \
     OMI_Attribute_Type_kFloat32 | \
     OMI_Attribute_Type_kFloat64 | \
     OMI_Attribute_Type_kString)

//------------------------------------------------------------------------------
//                                DELETE FUNCTIONS
//------------------------------------------------------------------------------

typedef void (OMI_Attribute_DataDeleteFunc)(void const*, OMI_Size);
typedef void (OMI_Attribute_ArrayDeleteFunc)(
        OMI_AttributePtr const*,
        OMI_Size);
typedef void (OMI_Attribute_MapDeleteFunc)(
        char const* const*,
        OMI_AttributePtr const*,
        OMI_Size);

#ifdef __cplusplus
}
#endif

#endif
