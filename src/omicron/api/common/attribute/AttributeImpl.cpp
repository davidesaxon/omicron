/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstddef>
#include <cstring>
#include <string>

#include <arcanecore/base/hash/Spooky.hpp>
#include <arcanecore/base/str/StringOperations.hpp>

#include "omicron/api/API.h"
#include "omicron/api/common/attribute/AttributeCTypes.h"

// TODO: REMOVE ME
#include <iostream>


//------------------------------------------------------------------------------
//                                    C STRUCT
//------------------------------------------------------------------------------

struct OMI_AttributeStruct
{
    //---------------------------A T T R I B U T E S----------------------------

    std::size_t m_ref_count;
    OMI_Attribute_Type m_type;
    mutable uint64_t m_hash1;
    mutable uint64_t m_hash2;
    // TODO: can this be done without calling new?
    void* m_internals;

    //--------------------------C O N S T R U C T O R---------------------------

    OMI_AttributeStruct(OMI_Attribute_Type type, void* internals)
        : m_ref_count(1)
        , m_type     (type)
        , m_hash1    (0)
        , m_hash2    (0)
        , m_internals(internals)
    {
    }
};


OMI_CODEGEN_NAMESPACE namespace omi
{
namespace
{

//------------------------------------------------------------------------------
//                       ATTRIBUTE INTERNAL DATA STRUCTURES
//------------------------------------------------------------------------------

struct AttributeInternalData
{
    //---------------------------A T T R I B U T E S----------------------------

    std::size_t m_size;
    std::size_t m_tuple_size;
    std::size_t m_data_stride;
    void* m_data;
    OMI_Attribute_DataDeleteFunc* m_delete_func;

    //--------------------------C O N S T R U C T O R---------------------------

    AttributeInternalData(
            std::size_t size,
            std::size_t tuple_size,
            std::size_t data_stride,
            void* data,
            OMI_Attribute_DataDeleteFunc* delete_func)
        : m_size       (size)
        , m_tuple_size (tuple_size)
        , m_data_stride(data_stride)
        , m_data       (data)
        , m_delete_func(delete_func)
    {
    }
};

struct AttributeInternalArray
{
    //---------------------------A T T R I B U T E S----------------------------

    std::size_t m_size;
    OMI_AttributePtr const* m_array;
    OMI_Attribute_ArrayDeleteFunc* m_delete_func;

    //--------------------------C O N S T R U C T O R---------------------------

    AttributeInternalArray(
            std::size_t size,
            OMI_AttributePtr const* array,
            OMI_Attribute_ArrayDeleteFunc* delete_func)
        : m_size       (size)
        , m_array      (array)
        , m_delete_func(delete_func)
    {
    }
};

struct AttributeInternalMap
{
    //---------------------------A T T R I B U T E S----------------------------

    std::size_t m_size;
    char const* const* m_keys;
    OMI_AttributePtr const* m_values;
    OMI_Attribute_MapDeleteFunc* m_delete_func;

    //--------------------------C O N S T R U C T O R---------------------------

    AttributeInternalMap(
            std::size_t size,
            char const* const* keys,
            OMI_AttributePtr const* values,
            OMI_Attribute_MapDeleteFunc* delete_func)
        : m_size       (size)
        , m_keys       (keys)
        , m_values     (values)
        , m_delete_func(delete_func)
    {
    }
};

//------------------------------------------------------------------------------
//                              FORWARD DECLARATIONS
//------------------------------------------------------------------------------

static void get_hash(
        OMI_AttributePtrConst ptr,
        uint64_t* part1,
        uint64_t* part2);

//------------------------------------------------------------------------------
//                              DEFAULT CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_AttributePtr default_constructor()
{
    return new OMI_AttributeStruct(OMI_Attribute_Type_kNull, nullptr);
}

//------------------------------------------------------------------------------
//                               INCREASE REFERENCE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void increase_reference(OMI_AttributePtr ptr)
{
    ++ptr->m_ref_count;
}

//------------------------------------------------------------------------------
//                               DECREASE REFERENCE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void decrease_reference(OMI_AttributePtr ptr)
{
    if(ptr == nullptr)
    {
        return;
    }

    if(ptr->m_ref_count == 1)
    {
        // handle based on type
        if(ptr->m_type & OMI_Attribute_Type_kData)
        {
            // get internals
            AttributeInternalData* internals =
                (AttributeInternalData*) ptr->m_internals;
            // no null data
            if(internals->m_data != nullptr)
            {
                // has delete function?
                if(internals->m_delete_func != nullptr)
                {
                    internals->m_delete_func(
                        internals->m_data,
                        internals->m_size
                    );
                }
                else
                {
                    switch(ptr->m_type)
                    {
                        case OMI_Attribute_Type_kByte:
                        {
                            delete[] (char const*) internals->m_data;
                            break;
                        }
                        case OMI_Attribute_Type_kInt16:
                        {
                            delete[] (int16_t const*) internals->m_data;
                            break;
                        }
                        case OMI_Attribute_Type_kInt32:
                        {
                            delete[] (int32_t const*) internals->m_data;
                            break;
                        }
                        case OMI_Attribute_Type_kInt64:
                        {
                            delete[] (int64_t const*) internals->m_data;
                            break;
                        }
                        case OMI_Attribute_Type_kFloat32:
                        {
                            delete[] (float const*) internals->m_data;
                            break;
                        }
                        case OMI_Attribute_Type_kFloat64:
                        {
                            delete[] (double const*) internals->m_data;
                            break;
                        }
                        case OMI_Attribute_Type_kString:
                        {
                            char const* const* data =
                                (char const* const*) internals->m_data;
                            for(std::size_t i = 0; i < internals->m_size; ++i)
                            {
                                delete[] data[i];
                            }
                            delete[] data;
                            break;
                        }
                    }
                }
            }
            delete internals;
        }
        else if(ptr->m_type == OMI_Attribute_Type_kArray)
        {
            // get internals
            AttributeInternalArray* internals =
                (AttributeInternalArray*) ptr->m_internals;
            if(internals != nullptr && internals->m_delete_func != nullptr)
            {
                internals->m_delete_func(
                    internals->m_array,
                    internals->m_size
                );
            }
            delete internals;
        }
        else if(ptr->m_type == OMI_Attribute_Type_kMap)
        {
            // get internals
            AttributeInternalMap* internals =
                (AttributeInternalMap*) ptr->m_internals;
            if(internals != nullptr && internals->m_delete_func != nullptr)
            {
                internals->m_delete_func(
                    internals->m_keys,
                    internals->m_values,
                    internals->m_size
                );
            }
            delete internals;
        }
        delete ptr;
    }
    else
    {
        --ptr->m_ref_count;
    }
}

//------------------------------------------------------------------------------
//                                     EQUAL
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool equal(OMI_AttributePtrConst a, OMI_AttributePtrConst b)
{
    // different type?
    if(a->m_type != b->m_type)
    {
        return 0;
    }

    // compare hashes (need to get via get_hash to ensure the hash has been
    // computed)
    uint64_t hash_a_1 = 0;
    uint64_t hash_a_2 = 0;
    get_hash(a, &hash_a_1, &hash_a_2);
    uint64_t hash_b_1 = 0;
    uint64_t hash_b_2 = 0;
    get_hash(b, &hash_b_1, &hash_b_2);

    return hash_a_1 == hash_b_1 && hash_a_2 == hash_b_2;
}

//------------------------------------------------------------------------------
//                                   DELETE STR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void delete_str(char const* str)
{
    delete[] str;
}

//------------------------------------------------------------------------------
//                                    GET TYPE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Type get_type(OMI_AttributePtrConst ptr)
{
    return ptr->m_type;
}

//------------------------------------------------------------------------------
//                                    IS VALID
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool is_valid(
        OMI_AttributePtrConst ptr,
        OMI_Attribute_Type expected_type)
{
    if((expected_type & ptr->m_type) == 0)
    {
        return 0;
    }
    return 1;
}

//------------------------------------------------------------------------------
//                                  IS IMMUTABLE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool is_immutable(OMI_AttributePtrConst ptr)
{
    if(ptr->m_type & OMI_Attribute_Type_kData)
    {
        // get internals
        AttributeInternalData* internals =
            (AttributeInternalData*) ptr->m_internals;

        // mutable - unless the attribute has a delete function
        return internals->m_delete_func != nullptr;
    }

    // all non-primitive attributes are immutable
    return 0;
}

//------------------------------------------------------------------------------
//                                    GET HASH
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void get_hash(
        OMI_AttributePtrConst ptr,
        uint64_t* part1,
        uint64_t* part2)
{
    // compute hash?
    if(ptr->m_hash1 == 0 && ptr->m_hash2 == 0)
    {
        // handle based on type
        switch(ptr->m_type)
        {
            case OMI_Attribute_Type_kNull:
            {
                ptr->m_hash1 = 0;
                ptr->m_hash2 = 0;
                break;
            }
            case OMI_Attribute_Type_kByte:
            case OMI_Attribute_Type_kInt16:
            case OMI_Attribute_Type_kInt32:
            case OMI_Attribute_Type_kInt64:
            case OMI_Attribute_Type_kFloat32:
            case OMI_Attribute_Type_kFloat64:
            case OMI_Attribute_Type_kString:
            {
                // get internals
                AttributeInternalData* internals =
                    (AttributeInternalData*) ptr->m_internals;

                arc::hash::spooky_128(
                    internals->m_data,
                    internals->m_size * internals->m_data_stride,
                    ptr->m_hash1,
                    ptr->m_hash2,
                    0,
                    0
                );
                break;
            }
            // TODO: other types
        }
    }

    *part1 = ptr->m_hash1;
    *part2 = ptr->m_hash2;
    return;
}

//------------------------------------------------------------------------------
//                                  STRING REPR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static char const* string_repr(OMI_AttributePtrConst ptr, OMI_Size indentation)
{
    std::string str;
    // indent?
    std::string indent_pad;
    if(indentation > 0)
    {
        indent_pad = arc::str::repeat(" ", indentation);
        str += indent_pad;
    }

    // handle based on type
    if(ptr->m_type == OMI_Attribute_Type_kNull)
    {
        str += "NullAttribute";
    }
    else if(ptr->m_type & OMI_Attribute_Type_kData)
    {
        // get internals
        AttributeInternalData* internals =
            (AttributeInternalData*) ptr->m_internals;
        if(internals->m_data != nullptr)
        {
            str += "[";
            switch(ptr->m_type)
            {
                case OMI_Attribute_Type_kByte:
                {
                    for(std::size_t i = 0; i < internals->m_size; ++i)
                    {
                        str += std::to_string(
                            ((char const*) internals->m_data)[i]
                        );
                        if(i < internals->m_size - 1)
                        {
                            str += ", ";
                        }
                    }
                    break;
                }
                case OMI_Attribute_Type_kInt16:
                {
                    for(std::size_t i = 0; i < internals->m_size; ++i)
                    {
                        str += std::to_string(
                            ((int16_t const*) internals->m_data)[i]
                        );
                        if(i < internals->m_size - 1)
                        {
                            str += ", ";
                        }
                    }
                    break;
                }
                case OMI_Attribute_Type_kInt32:
                {
                    for(std::size_t i = 0; i < internals->m_size; ++i)
                    {
                        str += std::to_string(
                            ((int32_t const*) internals->m_data)[i]
                        );
                        if(i < internals->m_size - 1)
                        {
                            str += ", ";
                        }
                    }
                    break;
                }
                case OMI_Attribute_Type_kInt64:
                {
                    for(std::size_t i = 0; i < internals->m_size; ++i)
                    {
                        str += std::to_string(
                            ((int64_t const*) internals->m_data)[i]
                        );
                        if(i < internals->m_size - 1)
                        {
                            str += ", ";
                        }
                    }
                    break;
                }
                case OMI_Attribute_Type_kFloat32:
                {
                    for(std::size_t i = 0; i < internals->m_size; ++i)
                    {
                        str += std::to_string(
                            ((float const*) internals->m_data)[i]
                        );
                        if(i < internals->m_size - 1)
                        {
                            str += ", ";
                        }
                    }
                    break;
                }
                case OMI_Attribute_Type_kFloat64:
                {
                    for(std::size_t i = 0; i < internals->m_size; ++i)
                    {
                        str += std::to_string(
                            ((double const*) internals->m_data)[i]
                        );
                        if(i < internals->m_size - 1)
                        {
                            str += ", ";
                        }
                    }
                    break;
                }
                case OMI_Attribute_Type_kString:
                {
                    for(std::size_t i = 0; i < internals->m_size; ++i)
                    {
                        str += "\"";
                        str += ((char const* const*) internals->m_data)[i];
                        str += "\"";
                        if(i < internals->m_size - 1)
                        {
                            str += ", ";
                        }
                    }
                    break;
                }
            }
            str += "]";
        }
    }
    else if(ptr->m_type == OMI_Attribute_Type_kArray)
    {
        // get internals
        AttributeInternalArray* internals =
            (AttributeInternalArray*) ptr->m_internals;

        str += "[";

        std::size_t const next_indent = indentation + 4;
        for(std::size_t i = 0; i < internals->m_size; ++i)
        {
            str += "\n";
            str += string_repr(internals->m_array[i], next_indent);
            if(i < internals->m_size - 1)
            {
                str += ",";
            }
        }
        if(internals->m_size != 0)
        {
            str += "\n";
        }
        str += indent_pad + "]";
    }
    else if(ptr->m_type == OMI_Attribute_Type_kMap)
    {
        // get internals
        AttributeInternalMap* internals =
            (AttributeInternalMap*) ptr->m_internals;

        str += "{";

        std::size_t const next_indent = indentation + 4;
        std::string const next_pad = arc::str::repeat(" ", next_indent);
        for(std::size_t i = 0; i < internals->m_size; ++i)
        {
            str += "\n";
            str +=
                next_pad + "\"" + std::string(internals->m_keys[i]) + "\":\n";
            str += string_repr(internals->m_values[i], next_indent + 4);
            if(i < internals->m_size - 1)
            {
                str += ",";
            }
        }
        if(internals->m_size != 0)
        {
            str += "\n";
        }
        str += indent_pad + "}";
    }

    // return
    char* ret = new char[str.length() + 1];
    std::memcpy(ret, str.c_str(), str.length() + 1);
    return ret;
}

//------------------------------------------------------------------------------
//                                 DATA GET SIZE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size data_size(OMI_AttributePtrConst ptr)
{
    // ensure this is a data attribute
    if((OMI_Attribute_Type_kData & ptr->m_type) == 0 ||
       ptr->m_internals == nullptr)
    {
        return 0;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;

    return internals->m_size;
}

//------------------------------------------------------------------------------
//                              DATA GET TUPLE SIZE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size data_tuple_size(OMI_AttributePtrConst ptr)
{
    // ensure this is a data attribute
    if((OMI_Attribute_Type_kData & ptr->m_type) == 0 ||
       ptr->m_internals == nullptr)
    {
        return 0;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;

    return internals->m_tuple_size;
}

//------------------------------------------------------------------------------
//                             BYTE VALUE CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_AttributePtr byte_value_constructor(char value)
{
    // take a copy of the value
    char* copy = new char[1];
    copy[0] = value;
    return new OMI_AttributeStruct(
        OMI_Attribute_Type_kByte,
        new AttributeInternalData(
            1,
            1,
            sizeof(*copy),
            copy,
            nullptr
        )
    );
}

//------------------------------------------------------------------------------
//                            BYTE VALUES CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error byte_values_constructor(
        char const* values,
        OMI_Size size,
        OMI_Size tuple_size,
        OMI_Attribute_DataDeleteFunc* data_delete_func,
        OMI_AttributePtr* out_ptr)
{
    if(size == 0)
    {
        return OMI_Attribute_Error_kInvalidSize;
    }
    if(size == 0)
    {
        return OMI_Attribute_Error_kInvalidTupleSize;
    }

    char const* values_ptr = values;
    if(data_delete_func == nullptr)
    {
        // take a copy of the values?
        char* copy = new char[size];
        for(OMI_Size i = 0; i < size; ++i)
        {
            copy[i] = values[i];
        }
        values_ptr = copy;
    }


    // return
    *out_ptr = new OMI_AttributeStruct(
        OMI_Attribute_Type_kByte,
        new AttributeInternalData(
            size,
            tuple_size,
            sizeof(*values_ptr),
            (char*) values_ptr,
            data_delete_func
        )
    );
    return OMI_Attribute_Error_kNone;
}

//------------------------------------------------------------------------------
//                                BYTE GET VALUES
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size byte_get_values(
        OMI_AttributePtrConst ptr,
        char const** out_values)
{
    if(ptr->m_type != OMI_Attribute_Type_kByte || ptr->m_internals == nullptr)
    {
        *out_values = nullptr;
        return 0;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;
    *out_values = (char const*) internals->m_data;
    return internals->m_size;
}

//------------------------------------------------------------------------------
//                                 BYTE SET VALUE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error byte_set_value(
        OMI_AttributePtr ptr,
        char value,
        OMI_Size index)
{
    // invalid?
    if(ptr->m_type != OMI_Attribute_Type_kByte || ptr->m_internals == nullptr)
    {
        return OMI_Attribute_Error_kInvalidAttr;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;

    // immutable?
    if(internals->m_delete_func != nullptr)
    {
        return OMI_Attribute_Error_kImmutable;
    }

    // bad index?
    if(index >= internals->m_size)
    {
        return OMI_Attribute_Error_kIndex;
    }

    // set
    ((char*) internals->m_data)[index] = value;

    return OMI_Attribute_Error_kNone;
}

//------------------------------------------------------------------------------
//                                BYTE SET VALUES
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error byte_set_values(
        OMI_AttributePtr ptr,
        char const* values,
        OMI_Size size)
{
    // invalid?
    if(ptr->m_type != OMI_Attribute_Type_kByte || ptr->m_internals == nullptr)
    {
        return OMI_Attribute_Error_kInvalidAttr;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;

    // immutable?
    if(internals->m_delete_func != nullptr)
    {
        return OMI_Attribute_Error_kImmutable;
    }

    // same size?
    if(size == internals->m_size)
    {
        for(OMI_Size i = 0; i < size; ++i)
        {
            ((char*) internals->m_data)[i] = values[i];
        }
    }
    else
    {
        internals->m_size = size;
        // allocate new data
        char* new_data = new char[size];
        for(OMI_Size i = 0; i < size; ++i)
        {
            new_data[i] = values[i];
        }

        // delete
        delete[] (char*) internals->m_data;
        // assign
        internals->m_data = (void*) new_data;
    }

    return OMI_Attribute_Error_kNone;
}

//------------------------------------------------------------------------------
//                            INT16 VALUE CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_AttributePtr int16_value_constructor(int16_t value)
{
    // take a copy of the value
    int16_t* copy = new int16_t[1];
    copy[0] = value;
    return new OMI_AttributeStruct(
        OMI_Attribute_Type_kInt16,
        new AttributeInternalData(
            1,
            1,
            sizeof(*copy),
            copy,
            nullptr
        )
    );
}

//------------------------------------------------------------------------------
//                            INT16 VALUES CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error int16_values_constructor(
        int16_t const* values,
        OMI_Size size,
        OMI_Size tuple_size,
        OMI_Attribute_DataDeleteFunc* data_delete_func,
        OMI_AttributePtr* out_ptr)
{
    if(size == 0)
    {
        return OMI_Attribute_Error_kInvalidSize;
    }
    if(size == 0)
    {
        return OMI_Attribute_Error_kInvalidTupleSize;
    }

    int16_t const* values_ptr = values;
    // take a copy of the values?
    if(data_delete_func == nullptr)
    {
        int16_t* copy = new int16_t[size];
        for(OMI_Size i = 0; i < size; ++i)
        {
            copy[i] = values[i];
        }
        values_ptr = copy;
    }

    // return
    *out_ptr = new OMI_AttributeStruct(
        OMI_Attribute_Type_kInt16,
        new AttributeInternalData(
            size,
            tuple_size,
            sizeof(*values_ptr),
            (int16_t*) values_ptr,
            data_delete_func
        )
    );
    return OMI_Attribute_Error_kNone;
}

//------------------------------------------------------------------------------
//                                INT16 GET VALUES
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size int16_get_values(
        OMI_AttributePtrConst ptr,
        int16_t const** out_values)
{
    if(ptr->m_type != OMI_Attribute_Type_kInt16 || ptr->m_internals == nullptr)
    {
        *out_values = nullptr;
        return 0;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;
    *out_values = (int16_t const*) internals->m_data;
    return internals->m_size;
}

//------------------------------------------------------------------------------
//                                INT16 SET VALUE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error int16_set_value(
        OMI_AttributePtr ptr,
        int16_t value,
        OMI_Size index)
{
    // invalid?
    if(ptr->m_type != OMI_Attribute_Type_kInt16 ||
       ptr->m_internals == nullptr)
    {
        return OMI_Attribute_Error_kInvalidAttr;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;

    // immutable?
    if(internals->m_delete_func != nullptr)
    {
        return OMI_Attribute_Error_kImmutable;
    }

    // bad index?
    if(index >= internals->m_size)
    {
        return OMI_Attribute_Error_kIndex;
    }

    // set
    ((int16_t*) internals->m_data)[index] = value;

    return OMI_Attribute_Error_kNone;
}

//------------------------------------------------------------------------------
//                                INT16 SET VALUES
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error int16_set_values(
        OMI_AttributePtr ptr,
        int16_t const* values,
        OMI_Size size)
{
    // invalid?
    if(ptr->m_type != OMI_Attribute_Type_kInt16 ||
       ptr->m_internals == nullptr)
    {
        return OMI_Attribute_Error_kInvalidAttr;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;

    // immutable?
    if(internals->m_delete_func != nullptr)
    {
        return OMI_Attribute_Error_kImmutable;
    }

    // same size?
    if(size == internals->m_size)
    {
        for(OMI_Size i = 0; i < size; ++i)
        {
            ((int16_t*) internals->m_data)[i] = values[i];
        }
    }
    else
    {
        internals->m_size = size;
        // allocate new data
        int16_t* new_data = new int16_t[size];
        for(OMI_Size i = 0; i < size; ++i)
        {
            new_data[i] = values[i];
        }

        // delete
        delete[] (int16_t*) internals->m_data;
        // assign
        internals->m_data = (void*) new_data;
    }

    return OMI_Attribute_Error_kNone;
}

//------------------------------------------------------------------------------
//                            INT32 VALUE CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_AttributePtr int32_value_constructor(int32_t value)
{
    // take a copy of the value
    int32_t* copy = new int32_t[1];
    copy[0] = value;
    return new OMI_AttributeStruct(
        OMI_Attribute_Type_kInt32,
        new AttributeInternalData(
            1,
            1,
            sizeof(*copy),
            copy,
            nullptr
        )
    );
}

//------------------------------------------------------------------------------
//                            INT32 VALUES CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error int32_values_constructor(
        int32_t const* values,
        OMI_Size size,
        OMI_Size tuple_size,
        OMI_Attribute_DataDeleteFunc* data_delete_func,
        OMI_AttributePtr* out_ptr)
{
    if(size == 0)
    {
        return OMI_Attribute_Error_kInvalidSize;
    }
    if(size == 0)
    {
        return OMI_Attribute_Error_kInvalidTupleSize;
    }

    int32_t const* values_ptr = values;
    // take a copy of the values?
    if(data_delete_func == nullptr)
    {
        int32_t* copy = new int32_t[size];
        for(OMI_Size i = 0; i < size; ++i)
        {
            copy[i] = values[i];
        }
        values_ptr = copy;
    }

    // return
    *out_ptr = new OMI_AttributeStruct(
        OMI_Attribute_Type_kInt32,
        new AttributeInternalData(
            size,
            tuple_size,
            sizeof(*values_ptr),
            (int32_t*) values_ptr,
            data_delete_func
        )
    );
    return OMI_Attribute_Error_kNone;
}

//------------------------------------------------------------------------------
//                                INT32 GET VALUES
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size int32_get_values(
        OMI_AttributePtrConst ptr,
        int32_t const** out_values)
{
    if(ptr->m_type != OMI_Attribute_Type_kInt32 || ptr->m_internals == nullptr)
    {
        *out_values = nullptr;
        return 0;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;
    *out_values = (int32_t const*) internals->m_data;
    return internals->m_size;
}

//------------------------------------------------------------------------------
//                                INT32 SET VALUE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error int32_set_value(
        OMI_AttributePtr ptr,
        int32_t value,
        OMI_Size index)
{
    // invalid?
    if(ptr->m_type != OMI_Attribute_Type_kInt32 ||
       ptr->m_internals == nullptr)
    {
        return OMI_Attribute_Error_kInvalidAttr;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;

    // immutable?
    if(internals->m_delete_func != nullptr)
    {
        return OMI_Attribute_Error_kImmutable;
    }

    // bad index?
    if(index >= internals->m_size)
    {
        return OMI_Attribute_Error_kIndex;
    }

    // set
    ((int32_t*) internals->m_data)[index] = value;

    return OMI_Attribute_Error_kNone;
}

//------------------------------------------------------------------------------
//                                INT32 SET VALUES
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error int32_set_values(
        OMI_AttributePtr ptr,
        int32_t const* values,
        OMI_Size size)
{
    // invalid?
    if(ptr->m_type != OMI_Attribute_Type_kInt32 ||
       ptr->m_internals == nullptr)
    {
        return OMI_Attribute_Error_kInvalidAttr;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;

    // immutable?
    if(internals->m_delete_func != nullptr)
    {
        return OMI_Attribute_Error_kImmutable;
    }

    // same size?
    if(size == internals->m_size)
    {
        for(OMI_Size i = 0; i < size; ++i)
        {
            ((int32_t*) internals->m_data)[i] = values[i];
        }
    }
    else
    {
        internals->m_size = size;
        // allocate new data
        int32_t* new_data = new int32_t[size];
        for(OMI_Size i = 0; i < size; ++i)
        {
            new_data[i] = values[i];
        }

        // delete
        delete[] (int32_t*) internals->m_data;
        // assign
        internals->m_data = (void*) new_data;
    }

    return OMI_Attribute_Error_kNone;
}

//------------------------------------------------------------------------------
//                            INT64 VALUE CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_AttributePtr int64_value_constructor(int64_t value)
{
    // take a copy of the value
    int64_t* copy = new int64_t[1];
    copy[0] = value;
    return new OMI_AttributeStruct(
        OMI_Attribute_Type_kInt64,
        new AttributeInternalData(
            1,
            1,
            sizeof(*copy),
            copy,
            nullptr
        )
    );
}

//------------------------------------------------------------------------------
//                            INT64 VALUES CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error int64_values_constructor(
        int64_t const* values,
        OMI_Size size,
        OMI_Size tuple_size,
        OMI_Attribute_DataDeleteFunc* data_delete_func,
        OMI_AttributePtr* out_ptr)
{
    if(size == 0)
    {
        return OMI_Attribute_Error_kInvalidSize;
    }
    if(size == 0)
    {
        return OMI_Attribute_Error_kInvalidTupleSize;
    }

    int64_t const* values_ptr = values;
    // take a copy of the values?
    if(data_delete_func == nullptr)
    {
        int64_t* copy = new int64_t[size];
        for(OMI_Size i = 0; i < size; ++i)
        {
            copy[i] = values[i];
        }
        values_ptr = copy;
    }

    // return
    *out_ptr = new OMI_AttributeStruct(
        OMI_Attribute_Type_kInt64,
        new AttributeInternalData(
            size,
            tuple_size,
            sizeof(*values_ptr),
            (int64_t*) values_ptr,
            data_delete_func
        )
    );
    return OMI_Attribute_Error_kNone;
}

//------------------------------------------------------------------------------
//                                INT64 GET VALUES
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size int64_get_values(
        OMI_AttributePtrConst ptr,
        int64_t const** out_values)
{
    if(ptr->m_type != OMI_Attribute_Type_kInt64 || ptr->m_internals == nullptr)
    {
        *out_values = nullptr;
        return 0;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;
    *out_values = (int64_t const*) internals->m_data;
    return internals->m_size;
}

//------------------------------------------------------------------------------
//                                INT64 SET VALUE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error int64_set_value(
        OMI_AttributePtr ptr,
        int64_t value,
        OMI_Size index)
{
    // invalid?
    if(ptr->m_type != OMI_Attribute_Type_kInt64 || ptr->m_internals == nullptr)
    {
        return OMI_Attribute_Error_kInvalidAttr;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;

    // immutable?
    if(internals->m_delete_func != nullptr)
    {
        return OMI_Attribute_Error_kImmutable;
    }

    // bad index?
    if(index >= internals->m_size)
    {
        return OMI_Attribute_Error_kIndex;
    }

    // set
    ((int64_t*) internals->m_data)[index] = value;

    return OMI_Attribute_Error_kNone;
}

//------------------------------------------------------------------------------
//                                INT64 SET VALUES
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error int64_set_values(
        OMI_AttributePtr ptr,
        int64_t const* values,
        OMI_Size size)
{
    // invalid?
    if(ptr->m_type != OMI_Attribute_Type_kInt64 || ptr->m_internals == nullptr)
    {
        return OMI_Attribute_Error_kInvalidAttr;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;

    // immutable?
    if(internals->m_delete_func != nullptr)
    {
        return OMI_Attribute_Error_kImmutable;
    }

    // same size?
    if(size == internals->m_size)
    {
        for(OMI_Size i = 0; i < size; ++i)
        {
            ((int64_t*) internals->m_data)[i] = values[i];
        }
    }
    else
    {
        internals->m_size = size;
        // allocate new data
        int64_t* new_data = new int64_t[size];
        for(OMI_Size i = 0; i < size; ++i)
        {
            new_data[i] = values[i];
        }

        // delete
        delete[] (int64_t*) internals->m_data;
        // assign
        internals->m_data = (void*) new_data;
    }

    return OMI_Attribute_Error_kNone;
}

//------------------------------------------------------------------------------
//                           FLOAT32 VALUE CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_AttributePtr float32_value_constructor(float value)
{
    // take a copy of the value
    float* copy = new float[1];
    copy[0] = value;
    return new OMI_AttributeStruct(
        OMI_Attribute_Type_kFloat32,
        new AttributeInternalData(
            1,
            1,
            sizeof(*copy),
            copy,
            nullptr
        )
    );
}

//------------------------------------------------------------------------------
//                           FLOAT32 VALUES CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error float32_values_constructor(
        float const* values,
        OMI_Size size,
        OMI_Size tuple_size,
        OMI_Attribute_DataDeleteFunc* data_delete_func,
        OMI_AttributePtr* out_ptr)
{
    if(size == 0)
    {
        return OMI_Attribute_Error_kInvalidSize;
    }
    if(size == 0)
    {
        return OMI_Attribute_Error_kInvalidTupleSize;
    }

    float const* values_ptr = values;
    // take a copy of the values?
    if(data_delete_func == nullptr)
    {
        float* copy = new float[size];
        for(OMI_Size i = 0; i < size; ++i)
        {
            copy[i] = values[i];
        }
        values_ptr = copy;
    }

    // return
    *out_ptr = new OMI_AttributeStruct(
        OMI_Attribute_Type_kFloat32,
        new AttributeInternalData(
            size,
            tuple_size,
            sizeof(*values_ptr),
            (float*) values_ptr,
            data_delete_func
        )
    );
    return OMI_Attribute_Error_kNone;
}

//------------------------------------------------------------------------------
//                               FLOAT32 GET VALUES
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size float32_get_values(
        OMI_AttributePtrConst ptr,
        float const** out_values)
{
    if(ptr->m_type != OMI_Attribute_Type_kFloat32 ||
       ptr->m_internals == nullptr)
    {
        *out_values = nullptr;
        return 0;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;
    *out_values = (float const*) internals->m_data;
    return internals->m_size;
}

//------------------------------------------------------------------------------
//                               FLOAT32 SET VALUE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error float32_set_value(
        OMI_AttributePtr ptr,
        float value,
        OMI_Size index)
{
    // invalid?
    if(ptr->m_type != OMI_Attribute_Type_kFloat32 ||
       ptr->m_internals == nullptr)
    {
        return OMI_Attribute_Error_kInvalidAttr;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;

    // immutable?
    if(internals->m_delete_func != nullptr)
    {
        return OMI_Attribute_Error_kImmutable;
    }

    // bad index?
    if(index >= internals->m_size)
    {
        return OMI_Attribute_Error_kIndex;
    }

    // set
    ((float*) internals->m_data)[index] = value;

    return OMI_Attribute_Error_kNone;
}

//------------------------------------------------------------------------------
//                               FLOAT32 SET VALUES
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error float32_set_values(
        OMI_AttributePtr ptr,
        float const* values,
        OMI_Size size)
{
    // invalid?
    if(ptr->m_type != OMI_Attribute_Type_kFloat32 ||
       ptr->m_internals == nullptr)
    {
        return OMI_Attribute_Error_kInvalidAttr;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;

    // immutable?
    if(internals->m_delete_func != nullptr)
    {
        return OMI_Attribute_Error_kImmutable;
    }

    // same size?
    if(size == internals->m_size)
    {
        for(OMI_Size i = 0; i < size; ++i)
        {
            ((float*) internals->m_data)[i] = values[i];
        }
    }
    else
    {
        internals->m_size = size;
        // allocate new data
        float* new_data = new float[size];
        for(OMI_Size i = 0; i < size; ++i)
        {
            new_data[i] = values[i];
        }

        // delete
        delete[] (float*) internals->m_data;
        // assign
        internals->m_data = (void*) new_data;
    }

    return OMI_Attribute_Error_kNone;
}

//------------------------------------------------------------------------------
//                           FLOAT64 VALUE CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_AttributePtr float64_value_constructor(double value)
{
    // take a copy of the value
    double* copy = new double[1];
    copy[0] = value;
    return new OMI_AttributeStruct(
        OMI_Attribute_Type_kFloat64,
        new AttributeInternalData(
            1,
            1,
            sizeof(*copy),
            copy,
            nullptr
        )
    );
}

//------------------------------------------------------------------------------
//                           FLOAT64 VALUES CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error float64_values_constructor(
        double const* values,
        OMI_Size size,
        OMI_Size tuple_size,
        OMI_Attribute_DataDeleteFunc* data_delete_func,
        OMI_AttributePtr* out_ptr)
{
    if(size == 0)
    {
        return OMI_Attribute_Error_kInvalidSize;
    }
    if(size == 0)
    {
        return OMI_Attribute_Error_kInvalidTupleSize;
    }

    double const* values_ptr = values;
    // take a copy of the values?
    if(data_delete_func == nullptr)
    {
        double* copy = new double[size];
        for(OMI_Size i = 0; i < size; ++i)
        {
            copy[i] = values[i];
        }
        values_ptr = copy;
    }

    // return
    *out_ptr = new OMI_AttributeStruct(
        OMI_Attribute_Type_kFloat64,
        new AttributeInternalData(
            size,
            tuple_size,
            sizeof(*values_ptr),
            (double*) values_ptr,
            data_delete_func
        )
    );
    return OMI_Attribute_Error_kNone;
}

//------------------------------------------------------------------------------
//                               FLOAT64 GET VALUES
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size float64_get_values(
        OMI_AttributePtrConst ptr,
        double const** out_values)
{
    if(ptr->m_type != OMI_Attribute_Type_kFloat64 ||
       ptr->m_internals == nullptr)
    {
        *out_values = nullptr;
        return 0;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;
    *out_values = (double const*) internals->m_data;
    return internals->m_size;
}

//------------------------------------------------------------------------------
//                               FLOAT64 SET VALUE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error float64_set_value(
        OMI_AttributePtr ptr,
        double value,
        OMI_Size index)
{
    // invalid?
    if(ptr->m_type != OMI_Attribute_Type_kFloat64 ||
       ptr->m_internals == nullptr)
    {
        return OMI_Attribute_Error_kInvalidAttr;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;

    // immutable?
    if(internals->m_delete_func != nullptr)
    {
        return OMI_Attribute_Error_kImmutable;
    }

    // bad index?
    if(index >= internals->m_size)
    {
        return OMI_Attribute_Error_kIndex;
    }

    // set
    ((double*) internals->m_data)[index] = value;

    return OMI_Attribute_Error_kNone;
}

//------------------------------------------------------------------------------
//                               FLOAT64 SET VALUES
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error float64_set_values(
        OMI_AttributePtr ptr,
        double const* values,
        OMI_Size size)
{
    // invalid?
    if(ptr->m_type != OMI_Attribute_Type_kFloat64 ||
       ptr->m_internals == nullptr)
    {
        return OMI_Attribute_Error_kInvalidAttr;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;

    // immutable?
    if(internals->m_delete_func != nullptr)
    {
        return OMI_Attribute_Error_kImmutable;
    }

    // same size?
    if(size == internals->m_size)
    {
        for(OMI_Size i = 0; i < size; ++i)
        {
            ((double*) internals->m_data)[i] = values[i];
        }
    }
    else
    {
        internals->m_size = size;
        // allocate new data
        double* new_data = new double[size];
        for(OMI_Size i = 0; i < size; ++i)
        {
            new_data[i] = values[i];
        }

        // delete
        delete[] (double*) internals->m_data;
        // assign
        internals->m_data = (void*) new_data;
    }

    return OMI_Attribute_Error_kNone;
}

//------------------------------------------------------------------------------
//                            STRING VALUE CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_AttributePtr string_value_constructor(char const* value)
{
    // take a copy of the value
    std::size_t const str_size = std::strlen(value) + 1;
    char* str = new char[str_size];
    std::memcpy(str, value, str_size);
    char** copy = new char*[1];
    copy[0] = str;
    return new OMI_AttributeStruct(
        OMI_Attribute_Type_kString,
        new AttributeInternalData(
            1,
            1,
            sizeof(*copy),
            copy,
            nullptr
        )
    );
}

//------------------------------------------------------------------------------
//                           STRING VALUES CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error string_values_constructor(
        char const* const* values,
        OMI_Size size,
        OMI_Size tuple_size,
        OMI_Attribute_DataDeleteFunc* data_delete_func,
        OMI_AttributePtr* out_ptr)
{
    if(size == 0)
    {
        return OMI_Attribute_Error_kInvalidSize;
    }
    if(size == 0)
    {
        return OMI_Attribute_Error_kInvalidTupleSize;
    }

    char const* const* values_ptr = values;
    if(data_delete_func == nullptr)
    {
        // take a copy of the values
        char** copy = new char*[size];
        for(OMI_Size i = 0; i < size; ++i)
        {
            std::size_t const str_size = std::strlen(values[i]) + 1;
            char* str = new char[str_size];
            std::memcpy(str, values[i], str_size);
            copy[i] = str;
        }
        values_ptr = copy;
    }

    // return
    *out_ptr = new OMI_AttributeStruct(
        OMI_Attribute_Type_kString,
        new AttributeInternalData(
            size,
            tuple_size,
            sizeof(*values_ptr),
            (char const**) values_ptr,
            data_delete_func
        )
    );
    return OMI_Attribute_Error_kNone;
}

//------------------------------------------------------------------------------
//                               STRING GET VALUES
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size string_get_values(
        OMI_AttributePtrConst ptr,
        char const* const** out_values)
{
    if(ptr->m_type != OMI_Attribute_Type_kString ||
       ptr->m_internals == nullptr)
    {
        *out_values = nullptr;
        return 0;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;
    *out_values = (char const* const*) internals->m_data;
    return internals->m_size;
}

//------------------------------------------------------------------------------
//                                STRING SET VALUE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error string_set_value(
        OMI_AttributePtr ptr,
        char const* value,
        OMI_Size index)
{
    // invalid?
    if(ptr->m_type != OMI_Attribute_Type_kString ||
       ptr->m_internals == nullptr)
    {
        return OMI_Attribute_Error_kInvalidAttr;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;

    // immutable?
    if(internals->m_delete_func != nullptr)
    {
        return OMI_Attribute_Error_kImmutable;
    }

    // bad index?
    if(index >= internals->m_size)
    {
        return OMI_Attribute_Error_kIndex;
    }

    delete[] ((char const**) internals->m_data)[index];

    // new value
    std::size_t const str_size = std::strlen(value) + 1;
    char* copy_str = new char[str_size];
    std::memcpy(copy_str, value, str_size);
    // set
    ((char const**) internals->m_data)[index] = copy_str;

    return OMI_Attribute_Error_kNone;
}

//------------------------------------------------------------------------------
//                               STRING SET VALUES
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error string_set_values(
        OMI_AttributePtr ptr,
        char const* const* values,
        OMI_Size size)
{
    // invalid?
    if(ptr->m_type != OMI_Attribute_Type_kString ||
       ptr->m_internals == nullptr)
    {
        return OMI_Attribute_Error_kInvalidAttr;
    }

    // get internals
    AttributeInternalData* internals =
        (AttributeInternalData*) ptr->m_internals;

    // immutable?
    if(internals->m_delete_func != nullptr)
    {
        return OMI_Attribute_Error_kImmutable;
    }

    // same size?
    if(size == internals->m_size)
    {
        for(OMI_Size i = 0; i < size; ++i)
        {
            delete[] ((char const**) internals->m_data)[i];
            // new value
            std::size_t const str_size = std::strlen(values[i]) + 1;
            char* copy_str = new char[str_size];
            std::memcpy(copy_str, values[i], str_size);
            // set
            ((char const**) internals->m_data)[i] = copy_str;
        }
    }
    else
    {
        // delete
        for(OMI_Size i = 0; i < internals->m_size; ++i)
        {
            delete[] ((char const**) internals->m_data)[i];
        }
        delete[] (char const**) internals->m_data;
        internals->m_size = size;
        // allocate new data
        char const** new_data = new char const*[size];
        for(OMI_Size i = 0; i < size; ++i)
        {
            std::size_t const str_size = std::strlen(values[i]) + 1;
            char* copy_str = new char[str_size];
            std::memcpy(copy_str, values[i], str_size);

            new_data[i] = values[i];
        }
        // assign
        internals->m_data = (void*) new_data;
    }

    return OMI_Attribute_Error_kNone;
}

//------------------------------------------------------------------------------
//                               ARRAY CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_AttributePtr array_constructor(
        OMI_AttributePtr const* array,
        OMI_Size size,
        OMI_Attribute_ArrayDeleteFunc* delete_func)
{
    return new OMI_AttributeStruct(
        OMI_Attribute_Type_kArray,
        new AttributeInternalArray(
            size,
            array,
            delete_func
        )
    );
}

//------------------------------------------------------------------------------
//                                   ARRAY SIZE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size array_size(OMI_AttributePtrConst ptr)
{
    // ensure this is an array attribute
    if(ptr->m_type != OMI_Attribute_Type_kArray || ptr->m_internals == nullptr)
    {
        return 0;
    }

    // get internals
    AttributeInternalArray* internals =
        (AttributeInternalArray*) ptr->m_internals;

    return internals->m_size;
}

//------------------------------------------------------------------------------
//                                ARRAY GET VALUES
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size array_get_values(
        OMI_AttributePtrConst ptr,
        OMI_AttributePtr const** out_values)
{
    // ensure this is an array attribute
    if(ptr->m_type != OMI_Attribute_Type_kArray || ptr->m_internals == nullptr)
    {
        return 0;
    }

    // get internals
    AttributeInternalArray* internals =
        (AttributeInternalArray*) ptr->m_internals;

    *out_values = internals->m_array;

    return internals->m_size;
}

//------------------------------------------------------------------------------
//                                MAP CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_AttributePtr map_constructor(
        char const* const* keys,
        OMI_AttributePtr const* values,
        OMI_Size size,
        OMI_Attribute_MapDeleteFunc* delete_func)
{
    return new OMI_AttributeStruct(
        OMI_Attribute_Type_kMap,
        new AttributeInternalMap(
            size,
            keys,
            values,
            delete_func
        )
    );
}

//------------------------------------------------------------------------------
//                                    MAP SIZE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size map_size(OMI_AttributePtrConst ptr)
{
    // ensure this is an map attribute
    if(ptr->m_type != OMI_Attribute_Type_kMap || ptr->m_internals == nullptr)
    {
        return 0;
    }

    // get internals
    AttributeInternalMap* internals =
        (AttributeInternalMap*) ptr->m_internals;

    return internals->m_size;
}

//------------------------------------------------------------------------------
//                                  MAP GET KEYS
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size map_get_keys(
        OMI_AttributePtrConst ptr,
        char const* const** out_keys)
{
    // ensure this is an map attribute
    if(ptr->m_type != OMI_Attribute_Type_kMap || ptr->m_internals == nullptr)
    {
        return 0;
    }

    // get internals
    AttributeInternalMap* internals =
        (AttributeInternalMap*) ptr->m_internals;

    *out_keys = internals->m_keys;

    return internals->m_size;
}

//------------------------------------------------------------------------------
//                                 MAP GET VALUES
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size map_get_values(
        OMI_AttributePtrConst ptr,
        OMI_AttributePtr const** out_values)
{
    // ensure this is an map attribute
    if(ptr->m_type != OMI_Attribute_Type_kMap || ptr->m_internals == nullptr)
    {
        return 0;
    }

    // get internals
    AttributeInternalMap* internals =
        (AttributeInternalMap*) ptr->m_internals;

    *out_values = internals->m_values;

    return internals->m_size;
}

//------------------------------------------------------------------------------
//                                    MAP FIND
//------------------------------------------------------------------------------

static bool map_find_rec(
        OMI_AttributePtrConst ptr,
        std::vector<std::string> const& keys,
        std::size_t offset,
        OMI_AttributePtr* out_value)
{
    // get internals
    AttributeInternalMap* internals =
        (AttributeInternalMap*) ptr->m_internals;

    std::string const& key = keys[offset];
    // TODO: we should be using a proper map here to find the key...
    for(OMI_Size i = 0; i < internals->m_size; ++i)
    {
        if(std::strcmp(internals->m_keys[i], key.c_str()) == 0)
        {
            if(offset >= keys.size() - 1)
            {
                *out_value = internals->m_values[i];
                return true;
            }
            OMI_AttributePtrConst next_ptr = internals->m_values[i];
            if(next_ptr->m_type != OMI_Attribute_Type_kMap)
            {
                return false;
            }
            return map_find_rec(next_ptr, keys, offset + 1, out_value);
        }
    }
    return false;
}

OMI_CODEGEN_FUNCTION
static OMI_Attribute_Error map_find(
        OMI_AttributePtrConst ptr,
        char const* key,
        OMI_AttributePtr* out_value)
{
    // ensure this is an map attribute
    if(ptr->m_type != OMI_Attribute_Type_kMap || ptr->m_internals == nullptr)
    {
        return 0;
    }

    // split on .
    std::vector<std::string> const keys = arc::str::split(key, ".");
    if(!map_find_rec(ptr, keys, 0, out_value))
    {
        return OMI_Attribute_Error_kKey;
    }
    return OMI_Attribute_Error_kNone;
}

} // namespace anonymous
} // namespace omi

#include "omicron/api/common/attribute/_codegen/AttributeBinding.inl"
