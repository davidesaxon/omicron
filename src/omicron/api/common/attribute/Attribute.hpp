/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_COMMON_ATTRIBUTE_ATTRUBUTE_HPP_
#define OMICRON_API_COMMON_ATTRIBUTE_ATTRUBUTE_HPP_

#include "omicron/api/API.h"
#include "omicron/api/common/Hash.hpp"
#include "omicron/api/common/attribute/AttributeCTypes.h"
#include "omicron/api/common/attribute/_codegen/AttributeInterface.hpp"


namespace omi
{

class Attribute
{
public:

    //--------------------------------------------------------------------------
    //                                   ENUMS
    //--------------------------------------------------------------------------

    enum class Type : OMI_Attribute_Type
    {
        kNull    = OMI_Attribute_Type_kNull,
        kByte    = OMI_Attribute_Type_kByte,
        kInt16   = OMI_Attribute_Type_kInt16,
        kInt32   = OMI_Attribute_Type_kInt32,
        kInt64   = OMI_Attribute_Type_kInt64,
        kFloat32 = OMI_Attribute_Type_kFloat32,
        kFloat64 = OMI_Attribute_Type_kFloat64,
        kString  = OMI_Attribute_Type_kString,
        kArray   = OMI_Attribute_Type_kArray,
        kMap     = OMI_Attribute_Type_kMap,
        kAll     = OMI_Attribute_Type_kAll,
        kData    = OMI_Attribute_Type_kData
    };

    //--------------------------------------------------------------------------
    //                                CONSTRUCTORS
    //--------------------------------------------------------------------------

    Attribute();

    Attribute(Attribute const& other);

    Attribute(Attribute&& other);

    Attribute(OMI_AttributePtr ptr, bool increase_ref=true);

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    virtual ~Attribute();

    //--------------------------------------------------------------------------
    //                                 OPERATORS
    //--------------------------------------------------------------------------

    Attribute& operator=(Attribute const& other);

    Attribute& operator=(Attribute&& other);

    bool operator==(Attribute const& other) const;

    bool operator!=(Attribute const& other) const;

    //--------------------------------------------------------------------------
    //                          PUBLIC STATIC FUNCTIONS
    //--------------------------------------------------------------------------

    static AttributeInterface& get_interface();

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    OMI_AttributePtr get_ptr() const;

    Type get_type() const;

    virtual bool is_valid() const;

    bool is_immutable() const;

    Hash get_hash() const;

    void string_repr(std::string& s, std::size_t indentation = 0) const;

    // TODO: copy

protected:

    //--------------------------------------------------------------------------
    //                            PROTECTED ATTRIBUTES
    //--------------------------------------------------------------------------

    OMI_AttributePtr m_ptr;
};

} // namespace omi

//------------------------------------------------------------------------------
//                               EXTERNAL OPERATORS
//------------------------------------------------------------------------------

std::ostream& operator<<(std::ostream& s, omi::Attribute const& a);

#endif
