/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstring>

#include "omicron/api/common/attribute/MapAttribute.hpp"


namespace omi
{

//------------------------------------------------------------------------------
//                                    UTILITY
//------------------------------------------------------------------------------

namespace
{

static void map_delete_func(
        char const* const* keys,
        OMI_AttributePtr const* values,
        OMI_Size size)
{
    for(OMI_Size i = 0; i < size; ++i)
    {
        delete[] keys[i];
        Attribute::get_interface().decrease_reference(values[i]);
    }
    if(keys != nullptr)
    {
        delete[] keys;
    }
    if(values != nullptr)
    {
        delete[] values;
    }
}

} // namespace anonymous

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

MapAttribute::MapAttribute()
    : Attribute(
        get_interface().map_constructor(
            nullptr,
            nullptr,
            0,
            nullptr
        ),
        false
    )
{
}

MapAttribute::MapAttribute(
        std::string const* keys,
        Attribute const* values,
        std::size_t size)
    : Attribute(nullptr, false)
{
    // create c-style map
    char const** c_keys = new char const*[size];
    OMI_AttributePtr* c_values = new OMI_AttributePtr[size];
    for(std::size_t i = 0; i < size; ++i)
    {
        std::size_t const key_length = keys[i].length() + 1;
        char* c_key = new char[key_length];
        std::memcpy(c_key, keys[i].c_str(), key_length);
        c_keys[i] = c_key;

        c_values[i] = values[i].get_ptr();
        get_interface().increase_reference(c_values[i]);
    }

    m_ptr = get_interface().map_constructor(
        c_keys,
        c_values,
        size,
        &map_delete_func
    );
}

MapAttribute::MapAttribute(
        char const* const* keys,
        OMI_AttributePtr const* values,
        std::size_t size,
        OMI_Attribute_MapDeleteFunc* delete_func)
    : Attribute(
        get_interface().map_constructor(
            keys,
            values,
            size,
            delete_func
        ),
        false
    )
{
}

MapAttribute::MapAttribute(Attribute const& other)
    : Attribute(other)
{
}

MapAttribute::MapAttribute(Attribute&& other)
    : Attribute(std::move(other))
{
}

MapAttribute::MapAttribute(OMI_AttributePtr ptr, bool increase_ref)
    : Attribute(ptr, increase_ref)
{
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

MapAttribute::~MapAttribute()
{
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

bool MapAttribute::is_valid() const
{
    return get_interface().is_valid(
        m_ptr,
        OMI_Attribute_Type_kMap
    ) != 0;
}

std::size_t MapAttribute::size() const
{
    return static_cast<std::size_t>(get_interface().map_size(m_ptr));
}

MapAttribute::KeyArray MapAttribute::get_keys() const
{
    char const* const* keys = nullptr;
    OMI_Size size = get_interface().map_get_keys(m_ptr, &keys);

    if(keys == nullptr || size == 0)
    {
        throw arc::ex::ValueError(
            "Cannot retrieve keys from invalid MapAttribute"
        );
    }

    return KeyArray(keys, size);
}

char const* MapAttribute::get_key(std::size_t index) const
{
    char const* const* keys = nullptr;
    OMI_Size size = get_interface().map_get_keys(m_ptr, &keys);

    if(keys == nullptr || size == 0)
    {
        throw arc::ex::ValueError(
            "Cannot retrieve key from invalid MapAttribute"
        );
    }
    if(index <= size)
    {
        throw arc::ex::IndexError(
            "Cannot get key in map at index " + std::to_string(index) +
            " since map has size " + std::to_string(size)
        );
    }

    return keys[index];
}

Attribute MapAttribute::at(std::size_t index) const
{
    OMI_AttributePtr const* values = nullptr;
    OMI_Size size = get_interface().map_get_values(m_ptr, &values);

    if(values == nullptr || size == 0)
    {
        throw arc::ex::ValueError(
            "Cannot retrieve value from invalid MapAttribute"
        );
    }
    if(index <= size)
    {
        throw arc::ex::IndexError(
            "Cannot get value in map at index " + std::to_string(index) +
            " since map has size " + std::to_string(size)
        );
    }

    return Attribute(values[index], true);
}

Attribute MapAttribute::at(std::string const& key) const
{
    OMI_AttributePtr value = nullptr;
    OMI_Attribute_Error ec = get_interface().map_find(
        m_ptr,
        key.c_str(),
        &value
    );

    if(ec == OMI_Attribute_Error_kInvalidAttr)
    {
        throw arc::ex::ValueError(
            "Cannot retrieve value from invalid MapAttribute"
        );
    }
    if(ec == OMI_Attribute_Error_kKey)
    {
        return Attribute();
    }

    return Attribute(value, true);
}

} // namespace omi
