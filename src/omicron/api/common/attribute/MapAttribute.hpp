/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_COMMON_ATTRIBUTE_MAPATTRIBUTE_HPP_
#define OMICRON_API_COMMON_ATTRIBUTE_MAPATTRIBUTE_HPP_

#include <arcanecore/base/collection/ConstWeakArray.hpp>

#include "omicron/api/common/attribute/Attribute.hpp"


namespace omi
{

class MapAttribute
    : public Attribute
{
public:

    //--------------------------------------------------------------------------
    //                              TYPE DEFINITIONS
    //--------------------------------------------------------------------------

    typedef arc::collection::ConstWeakArray<char const*> KeyArray;

    //--------------------------------------------------------------------------
    //                                CONSTRUCTORS
    //--------------------------------------------------------------------------

    MapAttribute();

    MapAttribute(
            std::string const* keys,
            Attribute const* values,
            std::size_t size);

    MapAttribute(
            char const* const* keys,
            OMI_AttributePtr const* values,
            std::size_t size,
            OMI_Attribute_MapDeleteFunc* delete_func);

    MapAttribute(Attribute const& other);

    MapAttribute(Attribute&& other);

    MapAttribute(OMI_AttributePtr ptr, bool increase_ref);

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    virtual ~MapAttribute();

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    virtual bool is_valid() const override;

    std::size_t size() const;

    KeyArray get_keys() const;

    char const* get_key(std::size_t index) const;

    // TODO: need omi::ConstWeakPtrArray<omi::Attribute, OMI_AttributePtr>;
    // std::vector<omi::Attribute> get_attrs() const;

    Attribute at(std::size_t index) const;

    Attribute at(std::string const& key) const;
};

} // namespace omi

#endif
