/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/common/attribute/Int32Attribute.hpp"


namespace omi
{

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

Int32Attribute::Int32Attribute()
    : DataAttribute(
        get_interface().int32_value_constructor(0),
        false
    )
{
}

Int32Attribute::Int32Attribute(DataType value)
    : DataAttribute(
        get_interface().int32_value_constructor(value),
        false
    )
{
}

Int32Attribute::Int32Attribute(
            DataType* values,
            std::size_t size,
            std::size_t tuple_size,
            DataDeleteFunc* data_delete_func)
    : DataAttribute(nullptr, false)
{
    OMI_Attribute_Error ec = get_interface().int32_values_constructor(
        values,
        size,
        tuple_size,
        data_delete_func,
        &m_ptr
    );

    if(ec == OMI_Attribute_Error_kInvalidSize)
    {
        throw arc::ex::ValueError(
            "Invalid attribute size: " + std::to_string(size)
        );
    }
    if(ec == OMI_Attribute_Error_kInvalidTupleSize)
    {
        throw arc::ex::ValueError(
            "Invalid attribute tuple size: " + std::to_string(tuple_size)
        );
    }
}

Int32Attribute::Int32Attribute(Attribute const& other)
    : DataAttribute(other)
{
}

Int32Attribute::Int32Attribute(Attribute&& other)
    : DataAttribute(std::move(other))
{
}

//------------------------------------------------------------------------------
//                                  DESTRUCTORS
//------------------------------------------------------------------------------

Int32Attribute::~Int32Attribute()
{
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

bool Int32Attribute::is_valid() const
{
    return get_interface().is_valid(
        m_ptr,
        OMI_Attribute_Type_kInt32
    ) != 0;
}

Int32Attribute::DataType Int32Attribute::get_value(DataType default_value) const
{
    DataType const* values = nullptr;
    OMI_Size size = get_interface().int32_get_values(m_ptr, &values);
    if(values == nullptr || size == 0)
    {
        return default_value;
    }
    return values[0];
}

Int32Attribute::ArrayType Int32Attribute::get_values() const
{
    DataType const* values = nullptr;
    OMI_Size size = get_interface().int32_get_values(m_ptr, &values);
    if(values == nullptr || size == 0)
    {
        throw arc::ex::ValueError(
            "Cannot retrieve values from invalid Int32Attribute"
        );
    }
    return ArrayType(values, size);
}

void Int32Attribute::set_value(DataType value, std::size_t index)
{
    OMI_Attribute_Error ec = get_interface().int32_set_value(
        m_ptr,
        value,
        index
    );

    if(ec == OMI_Attribute_Error_kInvalidAttr)
    {
        throw arc::ex::ValueError(
            "Cannot set value on invalid Int32Attribute"
        );
    }
    if(ec == OMI_Attribute_Error_kImmutable)
    {
        throw arc::ex::ValueError(
            "Cannot set value on immutable Int32Attribute"
        );
    }
    if(ec == OMI_Attribute_Error_kIndex)
    {
        throw arc::ex::IndexError(
            "Cannot set value at index " + std::to_string(index) + " on "
            "Int32Attribute as it is out of range of the attribute's size"
        );
    }
}

void Int32Attribute::set_values(DataType* values, std::size_t size)
{
    OMI_Attribute_Error ec = get_interface().int32_set_values(
        m_ptr,
        values,
        size
    );

    if(ec == OMI_Attribute_Error_kInvalidAttr)
    {
        throw arc::ex::ValueError(
            "Cannot set value on invalid Int32Attribute"
        );
    }
    if(ec == OMI_Attribute_Error_kImmutable)
    {
        throw arc::ex::ValueError(
            "Cannot set value on immutable Int32Attribute"
        );
    }
}

} // namespace omi
