/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/common/attribute/ArrayAttribute.hpp"


namespace omi
{

//------------------------------------------------------------------------------
//                                    UTILITY
//------------------------------------------------------------------------------

namespace
{

static void array_delete_func(
        OMI_AttributePtr const* array,
        OMI_Size size)
{
    // decrease references
    for(OMI_Size i = 0; i < size; ++i)
    {
        Attribute::get_interface().decrease_reference(array[i]);
    }
    if(array != nullptr)
    {
        delete[] array;
    }
}

} // namespace anonymous

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

ArrayAttribute::ArrayAttribute()
    : Attribute(
        get_interface().array_constructor(
            nullptr,
            0,
            nullptr
        ),
        false
    )
{
}

ArrayAttribute::ArrayAttribute(Attribute const* values, std::size_t size)
    : Attribute(nullptr, false)
{
    // create c-style array and increase references
    OMI_AttributePtr* c_array = new OMI_AttributePtr[size];
    for(std::size_t i = 0; i < size; ++i)
    {
        c_array[i] = values[i].get_ptr();
        get_interface().increase_reference(c_array[i]);
    }

    m_ptr = get_interface().array_constructor(
        c_array,
        size,
        &array_delete_func
    );
}

ArrayAttribute::ArrayAttribute(
        OMI_AttributePtr const* values,
        std::size_t size,
        OMI_Attribute_ArrayDeleteFunc* delete_func)
    : Attribute(
        get_interface().array_constructor(
            values,
            size,
            delete_func
        ),
        false
    )
{
}

ArrayAttribute::ArrayAttribute(Attribute const& other)
    : Attribute(other)
{
}

ArrayAttribute::ArrayAttribute(Attribute&& other)
    : Attribute(std::move(other))
{
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

ArrayAttribute::~ArrayAttribute()
{
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

bool ArrayAttribute::is_valid() const
{
    return get_interface().is_valid(
        m_ptr,
        OMI_Attribute_Type_kArray
    ) != 0;
}

std::size_t ArrayAttribute::size() const
{
    return static_cast<std::size_t>(get_interface().array_size(m_ptr));
}

Attribute ArrayAttribute::at(std::size_t index) const
{
    OMI_AttributePtr const* attrs = nullptr;
    OMI_Size size = get_interface().array_get_values(m_ptr, &attrs);

    if(attrs == nullptr || size == 0)
    {
        throw arc::ex::ValueError(
            "Cannot retrieve values from invalid ArrayAttribute"
        );
    }
    if(index <= size)
    {
        throw arc::ex::IndexError(
            "Cannot get attribute in array at index " + std::to_string(index) +
            " since array has size " + std::to_string(size)
        );
    }

    return Attribute(attrs[index], true);
}

} // namespace omi
