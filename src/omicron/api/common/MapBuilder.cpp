/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstring>

#include "omicron/api/common/MapBuilder.hpp"


namespace omi
{

//------------------------------------------------------------------------------
//                                    UTILITY
//------------------------------------------------------------------------------

namespace
{

static void map_delete_func(
        char const* const* keys,
        OMI_AttributePtr const* values,
        OMI_Size size)
{
    for(OMI_Size i = 0; i < size; ++i)
    {
        delete[] keys[i];
        Attribute::get_interface().decrease_reference(values[i]);
    }
    if(keys != nullptr)
    {
        delete[] keys;
    }
    if(values != nullptr)
    {
        delete[] values;
    }
}

} // namespace anonymous

//------------------------------------------------------------------------------
//                                  CONSTRUCTOR
//------------------------------------------------------------------------------

MapBuilder::MapBuilder()
{
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

MapBuilder::~MapBuilder()
{
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

void MapBuilder::insert(std::string const& key, Attribute attr)
{
    m_attrs[key] = attr;
}

omi::MapAttribute MapBuilder::build() const
{
    // create c-style map
    char const** c_keys = new char const*[m_attrs.size()];
    OMI_AttributePtr* c_values = new OMI_AttributePtr[m_attrs.size()];
    std::size_t offset = 0;
    for(auto const& entry : m_attrs)
    {
        std::size_t const key_length = entry.first.length() + 1;
        char* c_key = new char[key_length];
        std::memcpy(c_key, entry.first.c_str(), key_length);
        c_keys[offset] = c_key;

        c_values[offset] = entry.second.get_ptr();
        Attribute::get_interface().increase_reference(c_values[offset]);

        ++offset;
    }

    return MapAttribute(c_keys, c_values, m_attrs.size(), &map_delete_func);
}

} // namespace omi
