/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/event/Event.hpp"


namespace omi
{
namespace event
{

//------------------------------------------------------------------------------
//                                  EVENT NAMES
//------------------------------------------------------------------------------

std::string const kMouseMove          = OMI_EVENT_kMouseMove;
std::string const kMouseButtonPress   = OMI_EVENT_kMouseButtonPress;
std::string const kMouseButtonRelease = OMI_EVENT_kMouseButtonRelease;
std::string const kMouseScroll        = OMI_EVENT_kMouseScroll;
std::string const kMouseEnter         = OMI_EVENT_kMouseEnter;
std::string const kMouseExit          = OMI_EVENT_kMouseExit;

std::string const kKeyPress      = OMI_EVENT_kKeyPress;
std::string const kKeyRelease    = OMI_EVENT_kKeyRelease;

std::string const kSurfaceResize = OMI_EVENT_kSurfaceResize;
std::string const kSurfaceMove   = OMI_EVENT_kSurfaceMove;

std::string const kEngineShutdown = OMI_EVENT_kEngineShutdown;

//------------------------------------------------------------------------------
//                                   DATA NAMES
//------------------------------------------------------------------------------

std::string const kDataModifiers = OMI_Event_kDataModifiers;

std::string const kDataMouseButton  = OMI_Event_kDataMouseButton;
std::string const kDataMouseScrollX = OMI_Event_kDataMouseScrollX;
std::string const kDataMouseScrollY = OMI_Event_kDataMouseScrollY;

std::string const kDataKeyCode = OMI_EVENT_kDataKeyCode;

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

Event::Event(std::string const& type, Attribute const& data)
    : m_ptr(get_interface().Event_constructor(type.c_str(), data.get_ptr()))
{
}

Event::Event(Event const& other)
    : m_ptr(other.m_ptr)
{
    get_interface().Event_increase_reference(m_ptr);
}

Event::Event(Event&& other)
    : m_ptr(other.m_ptr)
{
    other.m_ptr = nullptr;
}

Event::Event(OMI_EVENT_EventPtr ptr, bool increase_ref)
    : m_ptr(ptr)
{
    if(increase_ref)
    {
        get_interface().Event_increase_reference(m_ptr);
    }
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

Event::~Event()
{
    get_interface().Event_decrease_reference(m_ptr);
}

//------------------------------------------------------------------------------
//                                   OPERATORS
//------------------------------------------------------------------------------

Event& Event::operator=(const Event& other)
{
    if(m_ptr != other.m_ptr)
    {
        get_interface().Event_decrease_reference(m_ptr);
        m_ptr = other.m_ptr;
        get_interface().Event_increase_reference(m_ptr);
    }
    return *this;
}

Event& Event::operator=(Event&& other)
{
    get_interface().Event_decrease_reference(m_ptr);
    m_ptr = other.m_ptr;
    other.m_ptr = nullptr;
    return *this;
}

//------------------------------------------------------------------------------
//                            PUBLIC STATIC FUNCTIONS
//------------------------------------------------------------------------------

EventInterface& Event::get_interface()
{
    static EventInterface inst("omicron_api");
    return inst;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

OMI_EVENT_EventPtr Event::get_ptr() const
{
    return m_ptr;
}

std::string_view Event::get_type() const
{
    return std::string_view(get_interface().Event_get_type(m_ptr));
}

Attribute Event::get_data() const
{
    return Attribute(get_interface().Event_get_data(m_ptr));
}

} // namespace event
} // namespace omi
