/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstddef>
#include <cstring>
#include <string>
#include <unordered_map>
#include <vector>

#include "omicron/api/API.h"
#include "omicron/api/common/Attributes.hpp"
#include "omicron/api/event/EventCTypes.h"


//------------------------------------------------------------------------------
//                                  EVENT STRUCT
//------------------------------------------------------------------------------

struct OMI_EVENT_EventStruct
{
    //---------------------------A T T R I B U T E S----------------------------

    std::size_t m_ref_count;
    std::string m_type;
    omi::Attribute m_data;

    //--------------------------C O N S T R U C T O R---------------------------

    OMI_EVENT_EventStruct(
            char const* type,
            OMI_AttributePtr data)
        : m_ref_count(1)
        , m_type     (type)
        , m_data     (data, true)
    {
    }
};


OMI_CODEGEN_NAMESPACE namespace omi
{
OMI_CODEGEN_NAMESPACE namespace event
{
namespace
{

//------------------------------------------------------------------------------
//                                    GLOBALS
//------------------------------------------------------------------------------

static std::unordered_map<
    std::string,
    std::unordered_map<void*, OMI_EVENT_OnEventFunc*>
> g_subscriptions;

//------------------------------------------------------------------------------
//                               EVENT CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_EVENT_EventPtr Event_constructor(
        char const* type,
        OMI_AttributePtr data)
{
    return new OMI_EVENT_EventStruct(type, data);
}

//------------------------------------------------------------------------------
//                            EVENT INCREASE REFERENCE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Event_increase_reference(OMI_EVENT_EventPtr ptr)
{
    ++ptr->m_ref_count;
}

//------------------------------------------------------------------------------
//                            EVENT DECREASE REFERENCE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Event_decrease_reference(OMI_EVENT_EventPtr ptr)
{
    if(ptr == nullptr)
    {
        return;
    }

    if(ptr->m_ref_count == 1)
    {
        delete ptr;
    }
    else
    {
        --ptr->m_ref_count;
    }
}

//------------------------------------------------------------------------------
//                                 EVENT GET TYPE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static char const* Event_get_type(OMI_EVENT_EventPtrConst ptr)
{
    return ptr->m_type.c_str();
}

//------------------------------------------------------------------------------
//                                 EVENT GET DATA
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_AttributePtr Event_get_data(OMI_EVENT_EventPtrConst ptr)
{
    return ptr->m_data.get_ptr();
}

//------------------------------------------------------------------------------
//                               SERVICE BROADCAST
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Service_broadcast(OMI_EVENT_EventPtr event)
{
    auto f_listeners = g_subscriptions.find(event->m_type);
    if(f_listeners == g_subscriptions.end())
    {
        return;
    }

    for(auto& listener : f_listeners->second)
    {
        listener.second(listener.first, event);
    }
}

//------------------------------------------------------------------------------
//                               SERVICE SUBSCRIBE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Service_subscribe(
        void* self,
        char const* type,
        OMI_EVENT_OnEventFunc* on_event_func)
{
    g_subscriptions[type][self] = on_event_func;
}

//------------------------------------------------------------------------------
//                              SERVICE UNSUBSCRIBE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Service_unsubscribe(void* self, char const* type)
{
    auto f_type = g_subscriptions.find(type);
    if(f_type == g_subscriptions.end())
    {
        return;
    }

    auto f_self = f_type->second.find(self);
    if(f_self == f_type->second.end())
    {
        return;
    }

    f_type->second.erase(f_self);
}

} // namespace anonymous
} // namespace event
} // namespace omi

#include "omicron/api/event/_codegen/EventBinding.inl"
