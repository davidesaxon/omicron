/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_EVENT_EVENT_HPP_
#define OMICRON_API_EVENT_EVENT_HPP_

#include <arcanecore/base/lang/Restrictors.hpp>

#include <arcanecore/cxx17/string_view.hpp>

#include "omicron/api/API.h"
#include "omicron/api/common/Attributes.hpp"
#include "omicron/api/event/EventCTypes.h"
#include "omicron/api/event/_codegen/EventInterface.hpp"


namespace omi
{
namespace event
{

//------------------------------------------------------------------------------
//                                  EVENT NAMES
//------------------------------------------------------------------------------

extern std::string const kMouseMove;
extern std::string const kMouseButtonPress;
extern std::string const kMouseButtonRelease;
extern std::string const kMouseScroll;
extern std::string const kMouseEnter;
extern std::string const kMouseExit;

extern std::string const kKeyPress;
extern std::string const kKeyRelease;

extern std::string const kSurfaceResize;
extern std::string const kSurfaceMove;

extern std::string const kEngineShutdown;

//------------------------------------------------------------------------------
//                                   DATA NAMES
//------------------------------------------------------------------------------

extern std::string const kDataModifiers;

extern std::string const kDataMouseButton;
extern std::string const kDataMouseScrollX;
extern std::string const kDataMouseScrollY;

extern std::string const kDataKeyCode;

//------------------------------------------------------------------------------
//                                   MODIFIERS
//------------------------------------------------------------------------------

enum class Modifier : OMI_Event_Modifier
{
    kShift   = OMI_Event_Modifier_kShift,
    kControl = OMI_Event_Modifier_kControl,
    kAlt     = OMI_Event_Modifier_kAlt,
    kSuper   = OMI_Event_Modifier_kSuper
};

//------------------------------------------------------------------------------
//                                 MOUSE BUTTONS
//------------------------------------------------------------------------------

enum class MouseButton : OMI_Event_MouseButton
{
    k1      = OMI_Event_MouseButton_k1,
    k2      = OMI_Event_MouseButton_k2,
    k3      = OMI_Event_MouseButton_k3,
    k4      = OMI_Event_MouseButton_k4,
    k5      = OMI_Event_MouseButton_k5,
    k6      = OMI_Event_MouseButton_k6,
    k7      = OMI_Event_MouseButton_k7,
    k8      = OMI_Event_MouseButton_k8,
    kLeft   = OMI_Event_MouseButton_kLeft,
    kRight  = OMI_Event_MouseButton_kRight,
    kMiddle = OMI_Event_MouseButton_kMiddle
};

//------------------------------------------------------------------------------
//                                   KEY CODES
//------------------------------------------------------------------------------

enum class KeyCode : OMI_Event_KeyCode
{
    kUnkown         = OMI_Event_KeyCode_kUnkown,
    kSpace          = OMI_Event_KeyCode_kSpace,
    kApostrophe     = OMI_Event_KeyCode_kApostrophe,
    kComma          = OMI_Event_KeyCode_kComma,
    kMinus          = OMI_Event_KeyCode_kMinus,
    kPeriod         = OMI_Event_KeyCode_kPeriod,
    kSlash          = OMI_Event_KeyCode_kSlash,
    k0              = OMI_Event_KeyCode_k0,
    k1              = OMI_Event_KeyCode_k1,
    k2              = OMI_Event_KeyCode_k2,
    k3              = OMI_Event_KeyCode_k3,
    k4              = OMI_Event_KeyCode_k4,
    k5              = OMI_Event_KeyCode_k5,
    k6              = OMI_Event_KeyCode_k6,
    k7              = OMI_Event_KeyCode_k7,
    k8              = OMI_Event_KeyCode_k8,
    k9              = OMI_Event_KeyCode_k9,
    kSemicolon      = OMI_Event_KeyCode_kSemicolon,
    kEqual          = OMI_Event_KeyCode_kEqual,
    kA              = OMI_Event_KeyCode_kA,
    kB              = OMI_Event_KeyCode_kB,
    kC              = OMI_Event_KeyCode_kC,
    kD              = OMI_Event_KeyCode_kD,
    kE              = OMI_Event_KeyCode_kE,
    kF              = OMI_Event_KeyCode_kF,
    kG              = OMI_Event_KeyCode_kG,
    kH              = OMI_Event_KeyCode_kH,
    kI              = OMI_Event_KeyCode_kI,
    kJ              = OMI_Event_KeyCode_kJ,
    kK              = OMI_Event_KeyCode_kK,
    kL              = OMI_Event_KeyCode_kL,
    kM              = OMI_Event_KeyCode_kM,
    kN              = OMI_Event_KeyCode_kN,
    kO              = OMI_Event_KeyCode_kO,
    kP              = OMI_Event_KeyCode_kP,
    kQ              = OMI_Event_KeyCode_kQ,
    kR              = OMI_Event_KeyCode_kR,
    kS              = OMI_Event_KeyCode_kS,
    kT              = OMI_Event_KeyCode_kT,
    kU              = OMI_Event_KeyCode_kU,
    kV              = OMI_Event_KeyCode_kV,
    kW              = OMI_Event_KeyCode_kW,
    kX              = OMI_Event_KeyCode_kX,
    kY              = OMI_Event_KeyCode_kY,
    kZ              = OMI_Event_KeyCode_kZ,
    kLeftBracket    = OMI_Event_KeyCode_kLeftBracket,
    kBackslash      = OMI_Event_KeyCode_kBackslash,
    kRightBracket   = OMI_Event_KeyCode_kRightBracket,
    kGraveAccent    = OMI_Event_KeyCode_kGraveAccent,
    kWorld1         = OMI_Event_KeyCode_kWorld1,
    kWorld2         = OMI_Event_KeyCode_kWorld2,
    kEscape         = OMI_Event_KeyCode_kEscape,
    kEnter          = OMI_Event_KeyCode_kEnter,
    kTab            = OMI_Event_KeyCode_kTab,
    kBackspace      = OMI_Event_KeyCode_kBackspace,
    kInsert         = OMI_Event_KeyCode_kInsert,
    kDelete         = OMI_Event_KeyCode_kDelete,
    kRight          = OMI_Event_KeyCode_kRight,
    kLeft           = OMI_Event_KeyCode_kLeft,
    kDown           = OMI_Event_KeyCode_kDown,
    kUp             = OMI_Event_KeyCode_kUp,
    kPageUp         = OMI_Event_KeyCode_kPageUp,
    kPageDown       = OMI_Event_KeyCode_kPageDown,
    kHome           = OMI_Event_KeyCode_kHome,
    kEnd            = OMI_Event_KeyCode_kEnd,
    kCapsLock       = OMI_Event_KeyCode_kCapsLock,
    kScrollLock     = OMI_Event_KeyCode_kScrollLock,
    kNumLock        = OMI_Event_KeyCode_kNumLock,
    kPrintScreen    = OMI_Event_KeyCode_kPrintScreen,
    kPause          = OMI_Event_KeyCode_kPause,
    kF1             = OMI_Event_KeyCode_kF1,
    kF2             = OMI_Event_KeyCode_kF2,
    kF3             = OMI_Event_KeyCode_kF3,
    kF4             = OMI_Event_KeyCode_kF4,
    kF5             = OMI_Event_KeyCode_kF5,
    kF6             = OMI_Event_KeyCode_kF6,
    kF7             = OMI_Event_KeyCode_kF7,
    kF8             = OMI_Event_KeyCode_kF8,
    kF9             = OMI_Event_KeyCode_kF9,
    kF10            = OMI_Event_KeyCode_kF10,
    kF11            = OMI_Event_KeyCode_kF11,
    kF12            = OMI_Event_KeyCode_kF12,
    kF13            = OMI_Event_KeyCode_kF13,
    kF14            = OMI_Event_KeyCode_kF14,
    kF15            = OMI_Event_KeyCode_kF15,
    kF16            = OMI_Event_KeyCode_kF16,
    kF17            = OMI_Event_KeyCode_kF17,
    kF18            = OMI_Event_KeyCode_kF18,
    kF19            = OMI_Event_KeyCode_kF19,
    kF20            = OMI_Event_KeyCode_kF20,
    kF21            = OMI_Event_KeyCode_kF21,
    kF22            = OMI_Event_KeyCode_kF22,
    kF23            = OMI_Event_KeyCode_kF23,
    kF24            = OMI_Event_KeyCode_kF24,
    kF25            = OMI_Event_KeyCode_kF25,
    kKeyPad0        = OMI_Event_KeyCode_kKeyPad0,
    kKeyPad1        = OMI_Event_KeyCode_kKeyPad1,
    kKeyPad2        = OMI_Event_KeyCode_kKeyPad2,
    kKeyPad3        = OMI_Event_KeyCode_kKeyPad3,
    kKeyPad4        = OMI_Event_KeyCode_kKeyPad4,
    kKeyPad5        = OMI_Event_KeyCode_kKeyPad5,
    kKeyPad6        = OMI_Event_KeyCode_kKeyPad6,
    kKeyPad7        = OMI_Event_KeyCode_kKeyPad7,
    kKeyPad8        = OMI_Event_KeyCode_kKeyPad8,
    kKeyPad9        = OMI_Event_KeyCode_kKeyPad9,
    kKeyPadDecimal  = OMI_Event_KeyCode_kKeyPadDecimal,
    kKeyPadDivide   = OMI_Event_KeyCode_kKeyPadDivide,
    kKeyPadMultiply = OMI_Event_KeyCode_kKeyPadMultiply,
    kKeyPadSubtract = OMI_Event_KeyCode_kKeyPadSubtract,
    kKeyPadAdd      = OMI_Event_KeyCode_kKeyPadAdd,
    kKeyPadEnter    = OMI_Event_KeyCode_kKeyPadEnter,
    kKeyPadEqual    = OMI_Event_KeyCode_kKeyPadEqual,
    kLeftShift      = OMI_Event_KeyCode_kLeftShift,
    kLeftControl    = OMI_Event_KeyCode_kLeftControl,
    kLeftAlt        = OMI_Event_KeyCode_kLeftAlt,
    kLeftSuper      = OMI_Event_KeyCode_kLeftSuper,
    kRightShift     = OMI_Event_KeyCode_kRightShift,
    kRightControl   = OMI_Event_KeyCode_kRightControl,
    kRightAlt       = OMI_Event_KeyCode_kRightAlt,
    kRightSuper     = OMI_Event_KeyCode_kRightSuper,
    kMenu           = OMI_Event_KeyCode_kMenu
};

//------------------------------------------------------------------------------
//                                     EVENT
//------------------------------------------------------------------------------

class Event
    : private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                                CONSTRUCTORS
    //--------------------------------------------------------------------------

    Event(std::string const& type, Attribute const& data = Attribute());

    Event(Event const& other);

    Event(Event&& other);

    Event(OMI_EVENT_EventPtr ptr, bool increase_ref=true);

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    virtual ~Event();

    //--------------------------------------------------------------------------
    //                                 OPERATORS
    //--------------------------------------------------------------------------

    Event& operator=(const Event& other);

    Event& operator=(Event&& other);

    //--------------------------------------------------------------------------
    //                          PUBLIC STATIC FUNCTIONS
    //--------------------------------------------------------------------------

    static EventInterface& get_interface();

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    OMI_EVENT_EventPtr get_ptr() const;

    std::string_view get_type() const;

    Attribute get_data() const;

private:

    //--------------------------------------------------------------------------
    //                             PRIVATE ATTRIBUTES
    //--------------------------------------------------------------------------

    OMI_EVENT_EventPtr m_ptr;
};

} // namespace event
} // namespace omi

#endif
