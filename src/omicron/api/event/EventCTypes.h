/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_EVENT_EVENTCTYPES_H_
#define OMICRON_API_EVENT_EVENTCTYPES_H_

#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
//                                  EVENT STRUCT
//------------------------------------------------------------------------------

typedef struct OMI_EVENT_EventStruct* OMI_EVENT_EventPtr;
typedef struct OMI_EVENT_EventStruct const* OMI_EVENT_EventPtrConst;

//------------------------------------------------------------------------------
//                                ON EVENT FNCTION
//------------------------------------------------------------------------------

typedef void (OMI_EVENT_OnEventFunc)(void*, OMI_EVENT_EventPtr);

//------------------------------------------------------------------------------
//                                  EVENT NAMES
//------------------------------------------------------------------------------

#define OMI_EVENT_kMouseMove          ("mouse_move")
#define OMI_EVENT_kMouseButtonPress   ("mouse_button_press")
#define OMI_EVENT_kMouseButtonRelease ("mouse_button_release")
#define OMI_EVENT_kMouseScroll        ("mouse_scroll")
#define OMI_EVENT_kMouseEnter         ("mouse_enter")
#define OMI_EVENT_kMouseExit          ("mouse_exit")

#define OMI_EVENT_kKeyPress   ("key_press")
#define OMI_EVENT_kKeyRelease ("key_release")

#define OMI_EVENT_kSurfaceResize ("surface_resize")
#define OMI_EVENT_kSurfaceMove   ("surface_move")

#define OMI_EVENT_kEngineShutdown ("engine_shutdown")

//------------------------------------------------------------------------------
//                                EVENT DATA NAMES
//------------------------------------------------------------------------------

#define OMI_Event_kDataModifiers ("modifiers")

#define OMI_Event_kDataMouseButton  ("mouse_button")
#define OMI_Event_kDataMouseScrollX ("scroll_x")
#define OMI_Event_kDataMouseScrollY ("scroll_y")

#define OMI_EVENT_kDataKeyCode ("key_code")

//------------------------------------------------------------------------------
//                                 EVENT MODIFIER
//------------------------------------------------------------------------------

typedef uint32_t OMI_Event_Modifier;
#define OMI_Event_Modifier_kShift   (1)
#define OMI_Event_Modifier_kControl (2)
#define OMI_Event_Modifier_kAlt     (4)
#define OMI_Event_Modifier_kSuper   (8)

//------------------------------------------------------------------------------
//                               EVENT MOUSE BUTTON
//------------------------------------------------------------------------------

typedef uint32_t OMI_Event_MouseButton;
#define OMI_Event_MouseButton_k1      (0)
#define OMI_Event_MouseButton_k2      (1)
#define OMI_Event_MouseButton_k3      (2)
#define OMI_Event_MouseButton_k4      (3)
#define OMI_Event_MouseButton_k5      (4)
#define OMI_Event_MouseButton_k6      (5)
#define OMI_Event_MouseButton_k7      (6)
#define OMI_Event_MouseButton_k8      (7)
#define OMI_Event_MouseButton_kLeft   OMI_Event_MouseButton_k1
#define OMI_Event_MouseButton_kRight  OMI_Event_MouseButton_k2
#define OMI_Event_MouseButton_kMiddle OMI_Event_MouseButton_k3

//------------------------------------------------------------------------------
//                                 EVENT KEY CODE
//------------------------------------------------------------------------------

typedef uint32_t OMI_Event_KeyCode;
#define OMI_Event_KeyCode_kUnkown         (1)
#define OMI_Event_KeyCode_kSpace          (32)
#define OMI_Event_KeyCode_kApostrophe     (39)
#define OMI_Event_KeyCode_kComma          (44)
#define OMI_Event_KeyCode_kMinus          (45)
#define OMI_Event_KeyCode_kPeriod         (46)
#define OMI_Event_KeyCode_kSlash          (47)
#define OMI_Event_KeyCode_k0              (48)
#define OMI_Event_KeyCode_k1              (49)
#define OMI_Event_KeyCode_k2              (50)
#define OMI_Event_KeyCode_k3              (51)
#define OMI_Event_KeyCode_k4              (52)
#define OMI_Event_KeyCode_k5              (53)
#define OMI_Event_KeyCode_k6              (54)
#define OMI_Event_KeyCode_k7              (55)
#define OMI_Event_KeyCode_k8              (56)
#define OMI_Event_KeyCode_k9              (57)
#define OMI_Event_KeyCode_kSemicolon      (59)
#define OMI_Event_KeyCode_kEqual          (61)
#define OMI_Event_KeyCode_kA              (65)
#define OMI_Event_KeyCode_kB              (66)
#define OMI_Event_KeyCode_kC              (67)
#define OMI_Event_KeyCode_kD              (68)
#define OMI_Event_KeyCode_kE              (69)
#define OMI_Event_KeyCode_kF              (70)
#define OMI_Event_KeyCode_kG              (71)
#define OMI_Event_KeyCode_kH              (72)
#define OMI_Event_KeyCode_kI              (73)
#define OMI_Event_KeyCode_kJ              (74)
#define OMI_Event_KeyCode_kK              (75)
#define OMI_Event_KeyCode_kL              (76)
#define OMI_Event_KeyCode_kM              (77)
#define OMI_Event_KeyCode_kN              (78)
#define OMI_Event_KeyCode_kO              (79)
#define OMI_Event_KeyCode_kP              (80)
#define OMI_Event_KeyCode_kQ              (81)
#define OMI_Event_KeyCode_kR              (82)
#define OMI_Event_KeyCode_kS              (83)
#define OMI_Event_KeyCode_kT              (84)
#define OMI_Event_KeyCode_kU              (85)
#define OMI_Event_KeyCode_kV              (86)
#define OMI_Event_KeyCode_kW              (87)
#define OMI_Event_KeyCode_kX              (88)
#define OMI_Event_KeyCode_kY              (89)
#define OMI_Event_KeyCode_kZ              (90)
#define OMI_Event_KeyCode_kLeftBracket    (91)
#define OMI_Event_KeyCode_kBackslash      (92)
#define OMI_Event_KeyCode_kRightBracket   (93)
#define OMI_Event_KeyCode_kGraveAccent    (96)
#define OMI_Event_KeyCode_kWorld1         (161)
#define OMI_Event_KeyCode_kWorld2         (162)
#define OMI_Event_KeyCode_kEscape         (256)
#define OMI_Event_KeyCode_kEnter          (257)
#define OMI_Event_KeyCode_kTab            (258)
#define OMI_Event_KeyCode_kBackspace      (259)
#define OMI_Event_KeyCode_kInsert         (260)
#define OMI_Event_KeyCode_kDelete         (261)
#define OMI_Event_KeyCode_kRight          (262)
#define OMI_Event_KeyCode_kLeft           (263)
#define OMI_Event_KeyCode_kDown           (264)
#define OMI_Event_KeyCode_kUp             (265)
#define OMI_Event_KeyCode_kPageUp         (266)
#define OMI_Event_KeyCode_kPageDown       (267)
#define OMI_Event_KeyCode_kHome           (268)
#define OMI_Event_KeyCode_kEnd            (269)
#define OMI_Event_KeyCode_kCapsLock       (280)
#define OMI_Event_KeyCode_kScrollLock     (281)
#define OMI_Event_KeyCode_kNumLock        (282)
#define OMI_Event_KeyCode_kPrintScreen    (283)
#define OMI_Event_KeyCode_kPause          (284)
#define OMI_Event_KeyCode_kF1             (290)
#define OMI_Event_KeyCode_kF2             (291)
#define OMI_Event_KeyCode_kF3             (292)
#define OMI_Event_KeyCode_kF4             (293)
#define OMI_Event_KeyCode_kF5             (294)
#define OMI_Event_KeyCode_kF6             (295)
#define OMI_Event_KeyCode_kF7             (296)
#define OMI_Event_KeyCode_kF8             (297)
#define OMI_Event_KeyCode_kF9             (298)
#define OMI_Event_KeyCode_kF10            (299)
#define OMI_Event_KeyCode_kF11            (300)
#define OMI_Event_KeyCode_kF12            (301)
#define OMI_Event_KeyCode_kF13            (302)
#define OMI_Event_KeyCode_kF14            (303)
#define OMI_Event_KeyCode_kF15            (304)
#define OMI_Event_KeyCode_kF16            (305)
#define OMI_Event_KeyCode_kF17            (306)
#define OMI_Event_KeyCode_kF18            (307)
#define OMI_Event_KeyCode_kF19            (308)
#define OMI_Event_KeyCode_kF20            (309)
#define OMI_Event_KeyCode_kF21            (310)
#define OMI_Event_KeyCode_kF22            (311)
#define OMI_Event_KeyCode_kF23            (312)
#define OMI_Event_KeyCode_kF24            (313)
#define OMI_Event_KeyCode_kF25            (314)
#define OMI_Event_KeyCode_kKeyPad0        (320)
#define OMI_Event_KeyCode_kKeyPad1        (321)
#define OMI_Event_KeyCode_kKeyPad2        (322)
#define OMI_Event_KeyCode_kKeyPad3        (323)
#define OMI_Event_KeyCode_kKeyPad4        (324)
#define OMI_Event_KeyCode_kKeyPad5        (325)
#define OMI_Event_KeyCode_kKeyPad6        (326)
#define OMI_Event_KeyCode_kKeyPad7        (327)
#define OMI_Event_KeyCode_kKeyPad8        (328)
#define OMI_Event_KeyCode_kKeyPad9        (329)
#define OMI_Event_KeyCode_kKeyPadDecimal  (330)
#define OMI_Event_KeyCode_kKeyPadDivide   (331)
#define OMI_Event_KeyCode_kKeyPadMultiply (332)
#define OMI_Event_KeyCode_kKeyPadSubtract (333)
#define OMI_Event_KeyCode_kKeyPadAdd      (334)
#define OMI_Event_KeyCode_kKeyPadEnter    (335)
#define OMI_Event_KeyCode_kKeyPadEqual    (336)
#define OMI_Event_KeyCode_kLeftShift      (340)
#define OMI_Event_KeyCode_kLeftControl    (341)
#define OMI_Event_KeyCode_kLeftAlt        (342)
#define OMI_Event_KeyCode_kLeftSuper      (343)
#define OMI_Event_KeyCode_kRightShift     (344)
#define OMI_Event_KeyCode_kRightControl   (345)
#define OMI_Event_KeyCode_kRightAlt       (346)
#define OMI_Event_KeyCode_kRightSuper     (347)
#define OMI_Event_KeyCode_kMenu           (348)

#ifdef __cplusplus
}
#endif

#endif
