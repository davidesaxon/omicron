/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#include <cstring>

#include "omicron/api/API.h"

#include "omicron/api/event/_codegen/EventCSymbols.h"


#ifdef __cplusplus
extern "C" {
#endif

OMI_API_EXPORT int OMI_EVENT_Event_BINDING_IMPL(char const* func_def, void** func_ptr)
{
    if(strcmp(func_def, OMI_EVENT_Event_Event_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::event::Event_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_EVENT_Event_Event_increase_reference_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::event::Event_increase_reference;
        return 0;
    }
    if(strcmp(func_def, OMI_EVENT_Event_Event_decrease_reference_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::event::Event_decrease_reference;
        return 0;
    }
    if(strcmp(func_def, OMI_EVENT_Event_Event_get_type_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::event::Event_get_type;
        return 0;
    }
    if(strcmp(func_def, OMI_EVENT_Event_Event_get_data_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::event::Event_get_data;
        return 0;
    }
    if(strcmp(func_def, OMI_EVENT_Event_Service_broadcast_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::event::Service_broadcast;
        return 0;
    }
    if(strcmp(func_def, OMI_EVENT_Event_Service_subscribe_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::event::Service_subscribe;
        return 0;
    }
    if(strcmp(func_def, OMI_EVENT_Event_Service_unsubscribe_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::event::Service_unsubscribe;
        return 0;
    }

    return 1;
}

#ifdef __cplusplus
} // extern "C"
#endif

