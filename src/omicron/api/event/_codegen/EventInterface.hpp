/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_EVENT_CODEGEN_EVENTINTERFACE_HPP_
#define OMI_EVENT_CODEGEN_EVENTINTERFACE_HPP_

#include <cassert>
#include <string>

#include <arcanecore/base/dl/DLOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>
#include <arcanecore/base/lang/Restrictors.hpp>

#include "omicron/api/event/_codegen/EventCSymbols.h"


namespace omi
{
namespace event
{

class EventInterface
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                              TYPE DEFINITIONS
    //--------------------------------------------------------------------------

    typedef OMI_Bool (BindingFunc)(char const*, void**);

    //--------------------------------------------------------------------------
    //                             PUBLIC ATTRIBUTES
    //--------------------------------------------------------------------------

    // Event_constructor
    OMI_EVENT_EventPtr (*Event_constructor)(char const*, OMI_AttributePtr);
    // Event_increase_reference
    void (*Event_increase_reference)(OMI_EVENT_EventPtr);
    // Event_decrease_reference
    void (*Event_decrease_reference)(OMI_EVENT_EventPtr);
    // Event_get_type
    char const* (*Event_get_type)(OMI_EVENT_EventPtrConst);
    // Event_get_data
    OMI_AttributePtr (*Event_get_data)(OMI_EVENT_EventPtrConst);
    // Service_broadcast
    void (*Service_broadcast)(OMI_EVENT_EventPtr);
    // Service_subscribe
    void (*Service_subscribe)(void*, char const*, OMI_EVENT_OnEventFunc*);
    // Service_unsubscribe
    void (*Service_unsubscribe)(void*, char const*);

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    EventInterface(std::string const& libname)
        : Event_constructor(nullptr)
        , Event_increase_reference(nullptr)
        , Event_decrease_reference(nullptr)
        , Event_get_type(nullptr)
        , Event_get_data(nullptr)
        , Service_broadcast(nullptr)
        , Service_subscribe(nullptr)
        , Service_unsubscribe(nullptr)
    {
        #ifdef ARC_OS_WINDOWS
            arc::fsys::Path libpath({
                "build",
                "windows",
                "Release",
                libname + "_impl.dll"
            });
        #elif defined(ARC_OS_UNIX)
            arc::fsys::Path libpath({
                "build",
                "linux",
                "lib" + libname + "_impl.so"
            });
        #else
            assert(false);
        #endif

        arc::dl::Handle lib_handle = arc::dl::open_library(libpath);
        BindingFunc* binding_func = arc::dl::bind_symbol<BindingFunc>(lib_handle, "OMI_EVENT_Event_BINDING_IMPL");

        if(binding_func(OMI_EVENT_Event_Event_constructor_SYMBOL, (void**) &Event_constructor) != 0)
        {
        }
        if(binding_func(OMI_EVENT_Event_Event_increase_reference_SYMBOL, (void**) &Event_increase_reference) != 0)
        {
        }
        if(binding_func(OMI_EVENT_Event_Event_decrease_reference_SYMBOL, (void**) &Event_decrease_reference) != 0)
        {
        }
        if(binding_func(OMI_EVENT_Event_Event_get_type_SYMBOL, (void**) &Event_get_type) != 0)
        {
        }
        if(binding_func(OMI_EVENT_Event_Event_get_data_SYMBOL, (void**) &Event_get_data) != 0)
        {
        }
        if(binding_func(OMI_EVENT_Event_Service_broadcast_SYMBOL, (void**) &Service_broadcast) != 0)
        {
        }
        if(binding_func(OMI_EVENT_Event_Service_subscribe_SYMBOL, (void**) &Service_subscribe) != 0)
        {
        }
        if(binding_func(OMI_EVENT_Event_Service_unsubscribe_SYMBOL, (void**) &Service_unsubscribe) != 0)
        {
        }
    }
};

} // namespace omi
} // namespace event


#endif