/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_EVENT_CODEGEN_EVENTCSMYBOLS_H_
#define OMI_EVENT_CODEGEN_EVENTCSMYBOLS_H_


#ifdef __cplusplus
extern "C" {
#endif

// OMI_EVENT_Event_constructor
static char const* OMI_EVENT_Event_Event_constructor_SYMBOL = "OMI_EVENT_EventPtr OMI_EVENT_Event_Event_constructor(char const*, OMI_AttributePtr)";

// OMI_EVENT_Event_increase_reference
static char const* OMI_EVENT_Event_Event_increase_reference_SYMBOL = "void OMI_EVENT_Event_Event_increase_reference(OMI_EVENT_EventPtr)";

// OMI_EVENT_Event_decrease_reference
static char const* OMI_EVENT_Event_Event_decrease_reference_SYMBOL = "void OMI_EVENT_Event_Event_decrease_reference(OMI_EVENT_EventPtr)";

// OMI_EVENT_Event_get_type
static char const* OMI_EVENT_Event_Event_get_type_SYMBOL = "char const* OMI_EVENT_Event_Event_get_type(OMI_EVENT_EventPtrConst)";

// OMI_EVENT_Event_get_data
static char const* OMI_EVENT_Event_Event_get_data_SYMBOL = "OMI_AttributePtr OMI_EVENT_Event_Event_get_data(OMI_EVENT_EventPtrConst)";

// OMI_EVENT_Service_broadcast
static char const* OMI_EVENT_Event_Service_broadcast_SYMBOL = "void OMI_EVENT_Event_Service_broadcast(OMI_EVENT_EventPtr)";

// OMI_EVENT_Service_subscribe
static char const* OMI_EVENT_Event_Service_subscribe_SYMBOL = "void OMI_EVENT_Event_Service_subscribe(void*, char const*, OMI_EVENT_OnEventFunc*)";

// OMI_EVENT_Service_unsubscribe
static char const* OMI_EVENT_Event_Service_unsubscribe_SYMBOL = "void OMI_EVENT_Event_Service_unsubscribe(void*, char const*)";

#ifdef __cplusplus
} // extern "C"
#endif

#endif