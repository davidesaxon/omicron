/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#include <cstring>

#include "omicron/api/API.h"

#include "omicron/api/renderer/_codegen/RendererCSymbols.h"


#ifdef __cplusplus
extern "C" {
#endif

OMI_API_EXPORT int OMI_RENDERER_Renderer_BINDING_IMPL(char const* func_def, void** func_ptr)
{
    if(strcmp(func_def, OMI_RENDERER_Renderer_startup_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::renderer::startup;
        return 0;
    }
    if(strcmp(func_def, OMI_RENDERER_Renderer_firstframe_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::renderer::firstframe;
        return 0;
    }
    if(strcmp(func_def, OMI_RENDERER_Renderer_shutdown_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::renderer::shutdown;
        return 0;
    }
    if(strcmp(func_def, OMI_RENDERER_Renderer_add_component_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::renderer::add_component;
        return 0;
    }
    if(strcmp(func_def, OMI_RENDERER_Renderer_remove_component_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::renderer::remove_component;
        return 0;
    }
    if(strcmp(func_def, OMI_RENDERER_Renderer_load_resource_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::renderer::load_resource;
        return 0;
    }
    if(strcmp(func_def, OMI_RENDERER_Renderer_render_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::renderer::render;
        return 0;
    }

    return 1;
}

#ifdef __cplusplus
} // extern "C"
#endif

