/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_RENDERER_CODEGEN_RENDERERINTERFACE_HPP_
#define OMI_RENDERER_CODEGEN_RENDERERINTERFACE_HPP_

#include <cassert>
#include <string>

#include <arcanecore/base/dl/DLOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>
#include <arcanecore/base/lang/Restrictors.hpp>

#include "omicron/api/renderer/_codegen/RendererCSymbols.h"


namespace omi
{
namespace renderer
{

class RendererInterface
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                              TYPE DEFINITIONS
    //--------------------------------------------------------------------------

    typedef OMI_Bool (BindingFunc)(char const*, void**);

    //--------------------------------------------------------------------------
    //                             PUBLIC ATTRIBUTES
    //--------------------------------------------------------------------------

    // startup
    OMI_Bool (*startup)(OMI_REPORT_LoggerPtr);
    // firstframe
    OMI_Bool (*firstframe)(OMI_REPORT_LoggerPtr);
    // shutdown
    OMI_Bool (*shutdown)();
    // add_component
    void (*add_component)(OMI_COMP_ComponentPtr);
    // remove_component
    void (*remove_component)(OMI_COMP_ComponentPtr);
    // load_resource
    OMI_Bool (*load_resource)(OMI_RES_ResourceId, OMI_AttributePtr, OMI_AttributePtr*, OMI_RES_RELEASE_Func**);
    // render
    void (*render)();

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    RendererInterface(std::string const& libname)
        : startup(nullptr)
        , firstframe(nullptr)
        , shutdown(nullptr)
        , add_component(nullptr)
        , remove_component(nullptr)
        , load_resource(nullptr)
        , render(nullptr)
    {
        #ifdef ARC_OS_WINDOWS
            arc::fsys::Path libpath({
                "build",
                "windows",
                "Release",
                libname + "_impl.dll"
            });
        #elif defined(ARC_OS_UNIX)
            arc::fsys::Path libpath({
                "build",
                "linux",
                "lib" + libname + "_impl.so"
            });
        #else
            assert(false);
        #endif

        arc::dl::Handle lib_handle = arc::dl::open_library(libpath);
        BindingFunc* binding_func = arc::dl::bind_symbol<BindingFunc>(lib_handle, "OMI_RENDERER_Renderer_BINDING_IMPL");

        if(binding_func(OMI_RENDERER_Renderer_startup_SYMBOL, (void**) &startup) != 0)
        {
        }
        if(binding_func(OMI_RENDERER_Renderer_firstframe_SYMBOL, (void**) &firstframe) != 0)
        {
        }
        if(binding_func(OMI_RENDERER_Renderer_shutdown_SYMBOL, (void**) &shutdown) != 0)
        {
        }
        if(binding_func(OMI_RENDERER_Renderer_add_component_SYMBOL, (void**) &add_component) != 0)
        {
        }
        if(binding_func(OMI_RENDERER_Renderer_remove_component_SYMBOL, (void**) &remove_component) != 0)
        {
        }
        if(binding_func(OMI_RENDERER_Renderer_load_resource_SYMBOL, (void**) &load_resource) != 0)
        {
        }
        if(binding_func(OMI_RENDERER_Renderer_render_SYMBOL, (void**) &render) != 0)
        {
        }
    }
};

} // namespace omi
} // namespace renderer


#endif