/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_RENDERER_CODEGEN_RENDERERCSMYBOLS_H_
#define OMI_RENDERER_CODEGEN_RENDERERCSMYBOLS_H_


#ifdef __cplusplus
extern "C" {
#endif

// OMI_RENDERER_startup
static char const* OMI_RENDERER_Renderer_startup_SYMBOL = "OMI_Bool OMI_RENDERER_Renderer_startup(OMI_REPORT_LoggerPtr)";

// OMI_RENDERER_firstframe
static char const* OMI_RENDERER_Renderer_firstframe_SYMBOL = "OMI_Bool OMI_RENDERER_Renderer_firstframe(OMI_REPORT_LoggerPtr)";

// OMI_RENDERER_shutdown
static char const* OMI_RENDERER_Renderer_shutdown_SYMBOL = "OMI_Bool OMI_RENDERER_Renderer_shutdown()";

// OMI_RENDERER_add_component
static char const* OMI_RENDERER_Renderer_add_component_SYMBOL = "void OMI_RENDERER_Renderer_add_component(OMI_COMP_ComponentPtr)";

// OMI_RENDERER_remove_component
static char const* OMI_RENDERER_Renderer_remove_component_SYMBOL = "void OMI_RENDERER_Renderer_remove_component(OMI_COMP_ComponentPtr)";

// OMI_RENDERER_load_resource
static char const* OMI_RENDERER_Renderer_load_resource_SYMBOL = "OMI_Bool OMI_RENDERER_Renderer_load_resource(OMI_RES_ResourceId, OMI_AttributePtr, OMI_AttributePtr*, OMI_RES_RELEASE_Func**)";

// OMI_RENDERER_render
static char const* OMI_RENDERER_Renderer_render_SYMBOL = "void OMI_RENDERER_Renderer_render()";

#ifdef __cplusplus
} // extern "C"
#endif

#endif