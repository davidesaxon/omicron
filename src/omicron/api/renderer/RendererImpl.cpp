/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstddef>
#include <cstring>
#include <string>

#include <arcanecore/base/Preproc.hpp>
#include <arcanecore/base/dl/DLOperations.hpp>
#include <arcanecore/base/fsys/FileSystemOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>

#include "omicron/api/common/Attributes.hpp"
#include "omicron/api/comp/ComponentCTypes.h"
#include "omicron/api/config/Registry.hpp"
#include "omicron/api/report/Logger.hpp"
#include "omicron/api/res/ResourceCTypes.h"


OMI_CODEGEN_NAMESPACE namespace omi
{
OMI_CODEGEN_NAMESPACE namespace renderer
{
namespace
{

//------------------------------------------------------------------------------
//                                    TYPEDEFS
//------------------------------------------------------------------------------

typedef char const* (GetPluginNameFunc)();
typedef char const* (GetPluginVersionFunc)();
typedef OMI_Bool (StartupFunc)();
typedef OMI_Bool (FirstFrameFunc)();
typedef OMI_Bool (ShutdownFunc)();
typedef void (AddComponentFunction)(OMI_COMP_ComponentPtr);
typedef void (RemoveComponentFunction)(OMI_COMP_ComponentPtr);
typedef OMI_Bool (LoadResourceFunction)(
        OMI_RES_ResourceId,
        OMI_AttributePtr,
        OMI_AttributePtr*,
        OMI_RES_RELEASE_Func**);
typedef void (RenderFunc)();

//------------------------------------------------------------------------------
//                                    GLOBALS
//------------------------------------------------------------------------------

static std::string g_name;
static std::string g_version;

static arc::dl::Handle g_dl_handle;

static FirstFrameFunc* g_firstframe_func = nullptr;
static ShutdownFunc* g_shutdown_func = nullptr;
static AddComponentFunction* g_add_component_func = nullptr;
static RemoveComponentFunction* g_remove_component_func = nullptr;
static LoadResourceFunction* g_load_resource_func = nullptr;
static RenderFunc* g_render_func = nullptr;

//------------------------------------------------------------------------------
//                                    STARTUP
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool startup(OMI_REPORT_LoggerPtr logger_ptr)
{
    omi::report::Logger logger(logger_ptr);

    if(g_dl_handle != nullptr)
    {
        logger.error(
            "Renderer startup called after renderer subsystem already "
            "initialised"
        );
        return OMI_False;
    }

    // get the path to the renderer subsystem to use
    StringAttribute path_attr = config::Registry::instance().get_attr(
        #ifdef ARC_OS_WINDOWS
            "omicron.renderer.bind.windows_path"
        #else
            "omicron.renderer.bind.linux_path"
        #endif
    );

    arc::fsys::Path renderer_path;
    for(StringAttribute::DataType const& c : path_attr.get_values())
    {
        renderer_path << c;
    }
    logger.debug(
        "Loading renderer subsystem from: {0}",
        renderer_path.to_native()
    );

    // check that the file exists
    if(!arc::fsys::is_file(renderer_path))
    {
        logger.critical(
            "Renderer subsystem file path either does not exist or is not a "
            "file: {0}",
            renderer_path.to_native()
        );
        return OMI_False;
    }

    // open as dynamic library
    g_dl_handle = arc::dl::open_library(renderer_path);

    // bind symbols
    GetPluginNameFunc* get_name_func = nullptr;
    try
    {
        get_name_func = arc::dl::bind_symbol<GetPluginNameFunc>(
            g_dl_handle,
            "OMI_RENDERER_get_plugin_name"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_RENDERER_get_plugin_name function from "
            "renderer: {0}",
            renderer_path.to_native()
        );
        return OMI_False;
    }

    GetPluginVersionFunc* get_version_func = nullptr;
    try
    {
        get_version_func = arc::dl::bind_symbol<GetPluginVersionFunc>(
            g_dl_handle,
            "OMI_RENDERER_get_plugin_version"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_RENDERER_get_plugin_version function from "
            "renderer: {0}",
            renderer_path.to_native()
        );
        return OMI_False;
    }

    StartupFunc* startup_func = nullptr;
    try
    {
        startup_func = arc::dl::bind_symbol<StartupFunc>(
            g_dl_handle,
            "OMI_RENDERER_startup"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_RENDERER_startup function from "
            "renderer: {0}",
            renderer_path.to_native()
        );
        return OMI_False;
    }

    try
    {
        g_firstframe_func = arc::dl::bind_symbol<FirstFrameFunc>(
            g_dl_handle,
            "OMI_RENDERER_firstframe"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_RENDERER_firstframe function from "
            "renderer: {0}",
            renderer_path.to_native()
        );
        return OMI_False;
    }

    try
    {
        g_shutdown_func = arc::dl::bind_symbol<ShutdownFunc>(
            g_dl_handle,
            "OMI_RENDERER_shutdown"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_RENDERER_shutdown function from "
            "renderer: {0}",
            renderer_path.to_native()
        );
        return OMI_False;
    }

    try
    {
        g_add_component_func = arc::dl::bind_symbol<AddComponentFunction>(
            g_dl_handle,
            "OMI_RENDERER_add_component"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_RENDERER_add_component function from "
            "renderer: {0}",
            renderer_path.to_native()
        );
        return OMI_False;
    }

    try
    {
        g_remove_component_func = arc::dl::bind_symbol<RemoveComponentFunction>(
            g_dl_handle,
            "OMI_RENDERER_remove_component"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_RENDERER_remove_component function from "
            "renderer: {0}",
            renderer_path.to_native()
        );
        return OMI_False;
    }

    try
    {
        g_load_resource_func = arc::dl::bind_symbol<LoadResourceFunction>(
            g_dl_handle,
            "OMI_RENDERER_load_resource"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_RENDERER_load_resource function from "
            "renderer: {0}",
            renderer_path.to_native()
        );
        return OMI_False;
    }

    try
    {
        g_render_func = arc::dl::bind_symbol<RenderFunc>(
            g_dl_handle,
            "OMI_RENDERER_render"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_RENDERER_render function from "
            "renderer: {0}",
            renderer_path.to_native()
        );
        return OMI_False;
    }

    g_name = get_name_func();
    g_version = get_version_func();

    logger.info(
        "Bound renderer subsystem: {0}-{1}",
        g_name,
        g_version
    );

    // call startup
    return startup_func();
}

//------------------------------------------------------------------------------
//                                   FIRSTFRAME
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool firstframe(OMI_REPORT_LoggerPtr logger_ptr)
{
    // nothing to do
    if(g_dl_handle == nullptr)
    {
        return OMI_True;
    }

    return g_firstframe_func();
}

//------------------------------------------------------------------------------
//                                    SHUTDOWN
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool shutdown()
{
    // nothing to do
    if(g_dl_handle == nullptr)
    {
        return OMI_True;
    }

    OMI_Bool success = OMI_True;
    if(g_shutdown_func != nullptr)
    {
        success = g_shutdown_func();
    }

    g_render_func = nullptr;
    g_load_resource_func = nullptr;
    g_remove_component_func = nullptr;
    g_add_component_func = nullptr;
    g_firstframe_func = nullptr;
    g_shutdown_func = nullptr;
    arc::dl::close_library(g_dl_handle);
    g_dl_handle = nullptr;

    return success;
}

//------------------------------------------------------------------------------
//                                 ADD COMPONENT
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void add_component(OMI_COMP_ComponentPtr component)
{
    g_add_component_func(component);
}

//------------------------------------------------------------------------------
//                                REMOVE COMPONENT
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void remove_component(OMI_COMP_ComponentPtr component)
{
    g_remove_component_func(component);
}

//------------------------------------------------------------------------------
//                                 LOAD RESOURCE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool load_resource(
        OMI_RES_ResourceId id,
        OMI_AttributePtr data,
        OMI_AttributePtr* out_resource,
        OMI_RES_RELEASE_Func** out_release_func)
{
    return g_load_resource_func(id, data, out_resource, out_release_func);
}

//------------------------------------------------------------------------------
//                                     RENDER
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void render()
{
    g_render_func();
}

} // namespace anonymous
} // namespace renderer
} // namespace omi

#include "omicron/api/renderer/_codegen/RendererBinding.inl"
