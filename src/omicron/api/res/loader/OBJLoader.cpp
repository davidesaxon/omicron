/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <fstream>
#include <vector>

#include <arcanecore/base/str/StringOperations.hpp>

#include "omicron/api/common/Attributes.hpp"
#include "omicron/api/common/MapBuilder.hpp"
#include "omicron/api/res/Storage.hpp"
#include "omicron/api/res/loader/OBJLoader.hpp"


//------------------------------------------------------------------------------
//                                   CONSTANTS
//------------------------------------------------------------------------------

// The stride of a geometry position (in floats).
static std::size_t const POSITION_STRIDE = 3;
// The stride of uv co-ordinate (in floats)
static std::size_t const UV_STRIDE = 2;
// the stride of normal (in floats)
static std::size_t const NORMAL_STRIDE = 3;


//------------------------------------------------------------------------------
//                                   FUNCTIONS
//------------------------------------------------------------------------------

OMI_RES_Error OMI_RES_LOADER_obj(
        OMI_RES_ResourceId id,
        char const* path,
        int64_t begin,
        int64_t end,
        OMI_AttributePtr* out_data,
        OMI_RES_RELEASE_Func** out_release_func)
{
    std::ifstream input(path);
    if(!input.is_open())
    {
        omi::res::Storage::instance().append_warning(
            "OBJ Loader failed to open file \"" + std::string(path) + "\""
        );
        return OMI_RES_Error_kIO;
    }
    input.seekg(begin, std::ios::beg);

    // initial un-indexed data read from the file
    std::vector<omi::Float32Attribute::DataType> positions;
    std::vector<omi::Float32Attribute::DataType> uvs;
    std::vector<omi::Float32Attribute::DataType> normals;

    // indexed copies of the data
    std::vector<omi::Float32Attribute::DataType> index_positions;
    std::vector<omi::Float32Attribute::DataType> index_uvs;
    std::vector<omi::Float32Attribute::DataType> index_normals;

    // index data
    std::vector<omi::Int32Attribute::DataType> indices;

    // mappings from face strings to actual indices
    std::unordered_map<std::string, std::size_t> faces_to_indices;

    // TODO: does a reserve pre-pass make things slower or faster

    bool first_face = true;
    std::string line;
    while((end < 0 || input.tellg() < end) && std::getline(input, line))
    {
        // position?
        if(arc::str::starts_with(line, "v "))
        {
            // TODO: could be done faster than using split
            std::vector<std::string> values = arc::str::split(line, " ");
            // correct number of values
            if(values.size() != (POSITION_STRIDE + 1))
            {
                omi::res::Storage::instance().append_warning(
                    "OBJ Loader failed to read file \"" + std::string(path) +
                    "\" as position line contains an incorrect number of "
                    "values: \"" + line + "\""
                );
                return OMI_RES_Error_kParse;
            }

            positions.push_back(std::stof(values[1]));
            positions.push_back(std::stof(values[2]));
            positions.push_back(std::stof(values[3]));
        }
        // uv?
        else if(arc::str::starts_with(line, "vt "))
        {
            // TODO: could be done faster than using split
            std::vector<std::string> values = arc::str::split(line, " ");
            // correct number of values
            if(values.size() != (UV_STRIDE + 1))
            {
                omi::res::Storage::instance().append_warning(
                    "OBJ Loader failed to read file \"" + std::string(path) +
                    "\" as uv line contains an incorrect number of "
                    "values: \"" + line + "\""
                );
                return OMI_RES_Error_kParse;
            }

            uvs.push_back(std::stof(values[1]));
            uvs.push_back(std::stof(values[2]));
        }
        // normal?
        else if(arc::str::starts_with(line, "vn "))
        {
            // TODO: could be done faster than using split
            std::vector<std::string> values = arc::str::split(line, " ");
            // correct number of values
            if(values.size() != (NORMAL_STRIDE + 1))
            {
                omi::res::Storage::instance().append_warning(
                    "OBJ Loader failed to read file \"" + std::string(path) +
                    "\" as normal line contains an incorrect number of "
                    "values: \"" + line + "\""
                );
                return OMI_RES_Error_kParse;
            }

            normals.push_back(std::stof(values[1]));
            normals.push_back(std::stof(values[2]));
            normals.push_back(std::stof(values[3]));
        }
        // face?
        else if(arc::str::starts_with(line, "f "))
        {
            // first face? reserve indices
            if(first_face)
            {
                std::size_t const reserve_size = std::max(
                    positions.size(),
                    std::max(uvs.size(), normals.size())
                );
                // TODO: this reserve is the most conservative -
                //       could do something smarter
                index_positions.reserve(reserve_size);
                index_uvs.reserve(reserve_size);
                index_normals.reserve(reserve_size);
                indices.reserve(reserve_size);
            }

            // TODO: could be done faster than using split
            std::vector<std::string> values = arc::str::split(line, " ");
            // correct number of values?
            if(values.size() != 4)
            {
                omi::res::Storage::instance().append_warning(
                    "OBJ Loader failed to read file \"" + std::string(path) +
                    "\" as face line contains non-triangle face: \"" +
                    line + "\""
                );
                return OMI_RES_Error_kParse;
            }

            // handle each face
            for(std::size_t i = 1; i < 4; ++i)
            {
                std::string const& face = values[i];

                // face already mapped?
                auto f_index = faces_to_indices.find(face);
                if(f_index != faces_to_indices.end())
                {
                    indices.push_back(
                        static_cast<omi::Int32Attribute::DataType>(
                            f_index->second
                        )
                    );
                    continue;
                }

                // new face
                std::size_t const index = index_positions.size() / 3;
                // get the separate position, uv, and normal indices and combine
                // them
                std::vector<std::string> const sep_indices =
                    arc::str::split(face, "/");
                for(std::size_t j = 0; j < sep_indices.size() && j < 4; ++j)
                {
                    std::string const& str_index = sep_indices[j];
                    if(str_index.empty())
                    {
                        continue;
                    }
                    std::size_t sep_index = std::stoul(str_index) - 1;
                    switch(j)
                    {
                        case 0:
                        {
                            sep_index *= POSITION_STRIDE;
                            index_positions.push_back(positions[sep_index + 0]);
                            index_positions.push_back(positions[sep_index + 1]);
                            index_positions.push_back(positions[sep_index + 2]);
                            break;
                        }
                        case 1:
                        {
                            sep_index *= UV_STRIDE;
                            index_uvs.push_back(uvs[sep_index + 0]);
                            index_uvs.push_back(uvs[sep_index + 1]);
                            break;
                        }
                        case 2:
                        {
                            sep_index *= NORMAL_STRIDE;
                            index_normals.push_back(normals[sep_index + 0]);
                            index_normals.push_back(normals[sep_index + 1]);
                            index_normals.push_back(normals[sep_index + 2]);
                            break;
                        }
                    }
                }

                // write index and face mapping
                indices.push_back(
                    static_cast<omi::Int32Attribute::DataType>(index)
                );
                faces_to_indices[face] = index;
            }
        }
    }

    input.close();

    if(index_positions.empty())
    {
        omi::res::Storage::instance().append_warning(
            "OBJ Loader failed to read file \"" + std::string(path) +
            "\" mesh contains no vertex positions"
        );
        return OMI_RES_Error_kParse;
    }

    omi::MapBuilder vertex_builder;
    vertex_builder.insert(
        "p",
        omi::Float32Attribute(&index_positions[0], index_positions.size(), 3)
    );
    if(!index_uvs.empty())
    {
        vertex_builder.insert(
            "t",
            omi::Float32Attribute(&index_uvs[0], index_uvs.size(), 2)
        );
    }
    if(!index_normals.empty())
    {
        vertex_builder.insert(
            "n",
            omi::Float32Attribute(&index_normals[0], index_normals.size(), 3)
        );
    }
    vertex_builder.insert(
        "index",
        omi::Int32Attribute(&indices[0], indices.size(), 3)
    );

    omi::MapBuilder geometry_builder;
    geometry_builder.insert("vertex", vertex_builder.build());

    omi::MapBuilder builder;
    // TODO: shape names?
    builder.insert("type", omi::StringAttribute("Mesh"));
    builder.insert("geometry", geometry_builder.build());

    omi::MapAttribute output = builder.build();
    omi::Attribute::get_interface().increase_reference(output.get_ptr());
    *out_data = output.get_ptr();
    *out_release_func = nullptr;

    return OMI_RES_Error_kNone;
}
