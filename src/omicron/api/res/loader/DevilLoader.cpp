/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <fstream>

#include <IL/il.h>

#include "omicron/api/common/Attributes.hpp"
#include "omicron/api/common/MapBuilder.hpp"
#include "omicron/api/res/Storage.hpp"
#include "omicron/api/res/loader/DevilLoader.hpp"


OMI_RES_Error OMI_RES_LOADER_devil(
        OMI_RES_ResourceId id,
        char const* path,
        int64_t begin,
        int64_t end,
        OMI_AttributePtr* out_data,
        OMI_RES_RELEASE_Func** out_release_func)
{
    // initialise devIL?
    static bool initialised = false;
    if(!initialised)
    {
        ilInit();
        initialised = true;
    }

    ILuint image_id = 0;

    // generate and bind the image
    ilGenImages(1, &image_id);
    ilBindImage(image_id);

    // match the image origin to OpenGL's
    ilEnable(IL_ORIGIN_SET);
    ilOriginFunc(IL_ORIGIN_LOWER_LEFT);

    //load the image
    ILboolean success = ilLoadImage(path);

    // check that the image loading successfully
    if(!success)
    {
        ilBindImage(0);
        ilDeleteImages(1, &image_id);
        omi::res::Storage::instance().append_warning(
            "DevIL Loader failed to open image \"" + std::string(path) + "\""
        );
        return OMI_RES_Error_kIO;
    }

    //get the important parameters from the image
    int width  = ilGetInteger(IL_IMAGE_WIDTH);
    int height = ilGetInteger(IL_IMAGE_HEIGHT);
    // int type   = ilGetInteger(IL_IMAGE_TYPE);
    int format = ilGetInteger(IL_IMAGE_FORMAT);

    // build channels
    std::vector<std::string> channels;
    switch(format)
    {
        case IL_COLOUR_INDEX:
        case IL_BGR:
        case IL_RGB:
            format = IL_RGB;
            channels = {"r", "g", "b"};
            break;
        case IL_BGRA:
        case IL_RGBA:
            format = IL_RGBA;
            channels = {"r", "g", "b", "a"};
            break;
        // TODO: support IL_ALPHA, IL_LUMINANCE, and IL_LUMINANCE_ALPHA
        default:
            ilBindImage(0);
            ilDeleteImages(1, &image_id);
            omi::res::Storage::instance().append_warning(
                "DevIL Loader encountered empty image with unsupported channel "
                "format: " + std::to_string(format) + " : \"" +
                std::string(path) + "\""
            );
            return OMI_RES_Error_kParse;
    }

    // TODO: support loading floats stc
    // convert
    if(ilConvertImage(format, IL_UNSIGNED_BYTE) == IL_FALSE)
    {
        omi::res::Storage::instance().append_warning(
            "DevIL Loader failed to convert image data to expected format and "
            "type from image: \"" + std::string(path) + "\""
        );
        return OMI_RES_Error_kRuntime;
    }

    // get the data and ensure it actually contains something
    void* data = ilGetData();
    if(data == nullptr)
    {
        ilBindImage(0);
        ilDeleteImages(1, &image_id);
        omi::res::Storage::instance().append_warning(
            "DevIL Loader encountered empty image data \"" + std::string(path) +
            "\""
        );
        return OMI_RES_Error_kParse;
    }

    // build the output data
    omi::MapBuilder builder;
    // TODO: image names?
    builder.insert("type", omi::StringAttribute("Texture"));
    builder.insert("width", omi::Int32Attribute(width));
    builder.insert("height", omi::Int32Attribute(height));
    builder.insert("channels", omi::StringAttribute(channels));
    builder.insert("data_type", omi::StringAttribute("unsigned_byte"));
    builder.insert(
        "data",
        omi::ByteAttribute(
            static_cast<omi::ByteAttribute::DataType*>(data),
            static_cast<std::size_t>(width * height * channels.size())
        )
    );

    // clean up
    ilBindImage(0);
    ilDeleteImages(1, &image_id);

    omi::MapAttribute output = builder.build();
    omi::Attribute::get_interface().increase_reference(output.get_ptr());
    *out_data = output.get_ptr();
    *out_release_func = nullptr;

    return OMI_RES_Error_kNone;
}
