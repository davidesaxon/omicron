/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#include <cstring>

#include "omicron/api/API.h"

#include "omicron/api/res/_codegen/ResourceCSymbols.h"


#ifdef __cplusplus
extern "C" {
#endif

OMI_API_EXPORT int OMI_RES_Resource_BINDING_IMPL(char const* func_def, void** func_ptr)
{
    if(strcmp(func_def, OMI_RES_Resource_startup_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::res::startup;
        return 0;
    }
    if(strcmp(func_def, OMI_RES_Resource_cleanup_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::res::cleanup;
        return 0;
    }
    if(strcmp(func_def, OMI_RES_Resource_shutdown_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::res::shutdown;
        return 0;
    }
    if(strcmp(func_def, OMI_RES_Resource_ResourceId_get_id_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::res::ResourceId_get_id;
        return 0;
    }
    if(strcmp(func_def, OMI_RES_Resource_Storage_get_warning_messages_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::res::Storage_get_warning_messages;
        return 0;
    }
    if(strcmp(func_def, OMI_RES_Resource_Storage_append_warning_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::res::Storage_append_warning;
        return 0;
    }
    if(strcmp(func_def, OMI_RES_Resource_Storage_is_loaded_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::res::Storage_is_loaded;
        return 0;
    }
    if(strcmp(func_def, OMI_RES_Resource_Storage_load_blocking_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::res::Storage_load_blocking;
        return 0;
    }
    if(strcmp(func_def, OMI_RES_Resource_Storage_get_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::res::Storage_get;
        return 0;
    }
    if(strcmp(func_def, OMI_RES_Resource_Storage_set_loader_func_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::res::Storage_set_loader_func;
        return 0;
    }

    return 1;
}

#ifdef __cplusplus
} // extern "C"
#endif

