/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_RES_CODEGEN_RESOURCECSMYBOLS_H_
#define OMI_RES_CODEGEN_RESOURCECSMYBOLS_H_


#ifdef __cplusplus
extern "C" {
#endif

// OMI_RES_startup
static char const* OMI_RES_Resource_startup_SYMBOL = "OMI_Bool OMI_RES_Resource_startup(OMI_REPORT_LoggerPtr)";

// OMI_RES_cleanup
static char const* OMI_RES_Resource_cleanup_SYMBOL = "void OMI_RES_Resource_cleanup(OMI_REPORT_LoggerPtr)";

// OMI_RES_shutdown
static char const* OMI_RES_Resource_shutdown_SYMBOL = "OMI_Bool OMI_RES_Resource_shutdown()";

// OMI_RES_ResourceId_get_id
static char const* OMI_RES_Resource_ResourceId_get_id_SYMBOL = "OMI_RES_ResourceId OMI_RES_Resource_ResourceId_get_id(char const*)";

// OMI_RES_Storage_get_warning_messages
static char const* OMI_RES_Resource_Storage_get_warning_messages_SYMBOL = "char const* OMI_RES_Resource_Storage_get_warning_messages()";

// OMI_RES_Storage_append_warning
static char const* OMI_RES_Resource_Storage_append_warning_SYMBOL = "void OMI_RES_Resource_Storage_append_warning(char const*)";

// OMI_RES_Storage_is_loaded
static char const* OMI_RES_Resource_Storage_is_loaded_SYMBOL = "OMI_Bool OMI_RES_Resource_Storage_is_loaded(OMI_RES_ResourceId const*, OMI_Size)";

// OMI_RES_Storage_load_blocking
static char const* OMI_RES_Resource_Storage_load_blocking_SYMBOL = "void OMI_RES_Resource_Storage_load_blocking(OMI_RES_ResourceId const*, OMI_Size)";

// OMI_RES_Storage_get
static char const* OMI_RES_Resource_Storage_get_SYMBOL = "OMI_AttributePtr OMI_RES_Resource_Storage_get(OMI_RES_ResourceId)";

// OMI_RES_Storage_set_loader_func
static char const* OMI_RES_Resource_Storage_set_loader_func_SYMBOL = "void OMI_RES_Resource_Storage_set_loader_func(char const*, OMI_RES_LOADER_Func*)";

#ifdef __cplusplus
} // extern "C"
#endif

#endif