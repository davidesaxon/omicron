/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_RES_CODEGEN_RESOURCEINTERFACE_HPP_
#define OMI_RES_CODEGEN_RESOURCEINTERFACE_HPP_

#include <cassert>
#include <string>

#include <arcanecore/base/dl/DLOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>
#include <arcanecore/base/lang/Restrictors.hpp>

#include "omicron/api/res/_codegen/ResourceCSymbols.h"


namespace omi
{
namespace res
{

class ResourceInterface
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                              TYPE DEFINITIONS
    //--------------------------------------------------------------------------

    typedef OMI_Bool (BindingFunc)(char const*, void**);

    //--------------------------------------------------------------------------
    //                             PUBLIC ATTRIBUTES
    //--------------------------------------------------------------------------

    // startup
    OMI_Bool (*startup)(OMI_REPORT_LoggerPtr);
    // cleanup
    void (*cleanup)(OMI_REPORT_LoggerPtr);
    // shutdown
    OMI_Bool (*shutdown)();
    // ResourceId_get_id
    OMI_RES_ResourceId (*ResourceId_get_id)(char const*);
    // Storage_get_warning_messages
    char const* (*Storage_get_warning_messages)();
    // Storage_append_warning
    void (*Storage_append_warning)(char const*);
    // Storage_is_loaded
    OMI_Bool (*Storage_is_loaded)(OMI_RES_ResourceId const*, OMI_Size);
    // Storage_load_blocking
    void (*Storage_load_blocking)(OMI_RES_ResourceId const*, OMI_Size);
    // Storage_get
    OMI_AttributePtr (*Storage_get)(OMI_RES_ResourceId);
    // Storage_set_loader_func
    void (*Storage_set_loader_func)(char const*, OMI_RES_LOADER_Func*);

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    ResourceInterface(std::string const& libname)
        : startup(nullptr)
        , cleanup(nullptr)
        , shutdown(nullptr)
        , ResourceId_get_id(nullptr)
        , Storage_get_warning_messages(nullptr)
        , Storage_append_warning(nullptr)
        , Storage_is_loaded(nullptr)
        , Storage_load_blocking(nullptr)
        , Storage_get(nullptr)
        , Storage_set_loader_func(nullptr)
    {
        #ifdef ARC_OS_WINDOWS
            arc::fsys::Path libpath({
                "build",
                "windows",
                "Release",
                libname + "_impl.dll"
            });
        #elif defined(ARC_OS_UNIX)
            arc::fsys::Path libpath({
                "build",
                "linux",
                "lib" + libname + "_impl.so"
            });
        #else
            assert(false);
        #endif

        arc::dl::Handle lib_handle = arc::dl::open_library(libpath);
        BindingFunc* binding_func = arc::dl::bind_symbol<BindingFunc>(lib_handle, "OMI_RES_Resource_BINDING_IMPL");

        if(binding_func(OMI_RES_Resource_startup_SYMBOL, (void**) &startup) != 0)
        {
        }
        if(binding_func(OMI_RES_Resource_cleanup_SYMBOL, (void**) &cleanup) != 0)
        {
        }
        if(binding_func(OMI_RES_Resource_shutdown_SYMBOL, (void**) &shutdown) != 0)
        {
        }
        if(binding_func(OMI_RES_Resource_ResourceId_get_id_SYMBOL, (void**) &ResourceId_get_id) != 0)
        {
        }
        if(binding_func(OMI_RES_Resource_Storage_get_warning_messages_SYMBOL, (void**) &Storage_get_warning_messages) != 0)
        {
        }
        if(binding_func(OMI_RES_Resource_Storage_append_warning_SYMBOL, (void**) &Storage_append_warning) != 0)
        {
        }
        if(binding_func(OMI_RES_Resource_Storage_is_loaded_SYMBOL, (void**) &Storage_is_loaded) != 0)
        {
        }
        if(binding_func(OMI_RES_Resource_Storage_load_blocking_SYMBOL, (void**) &Storage_load_blocking) != 0)
        {
        }
        if(binding_func(OMI_RES_Resource_Storage_get_SYMBOL, (void**) &Storage_get) != 0)
        {
        }
        if(binding_func(OMI_RES_Resource_Storage_set_loader_func_SYMBOL, (void**) &Storage_set_loader_func) != 0)
        {
        }
    }
};

} // namespace omi
} // namespace res


#endif