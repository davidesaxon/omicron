/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/res/Storage.hpp"


namespace omi
{
namespace res
{

//------------------------------------------------------------------------------
//                            PUBLIC STATIC FUNCTIONS
//------------------------------------------------------------------------------

Storage& Storage::instance()
{
    static Storage inst;
    return inst;
}

ResourceInterface& Storage::get_interface()
{
    static ResourceInterface inst("omicron_api");
    return inst;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

std::string Storage::get_warning_messages() const
{
    return std::string(get_interface().Storage_get_warning_messages());
}

void Storage::append_warning(std::string const& message)
{
    get_interface().Storage_append_warning(message.c_str());
}

bool Storage::is_loaded(ResourceId id) const
{
    return get_interface().Storage_is_loaded(&id, 1) != OMI_False;
}

bool Storage::is_loaded(std::vector<ResourceId> const& ids)
{
    return
        get_interface().Storage_is_loaded(&ids[0], ids.size()) !=
        OMI_False;
}

void Storage::load_blocking(ResourceId id)
{
    // TODO: error handling - maybe get warnings function?
    get_interface().Storage_load_blocking(&id, 1);
}

void Storage::load_blocking(std::vector<ResourceId> const& ids)
{
    // TODO: error handling - maybe get warnings function?
    get_interface().Storage_load_blocking(&ids[0], ids.size());
}

omi::Attribute Storage::get(ResourceId id) const
{
    return omi::Attribute(get_interface().Storage_get(id), true);
}

void Storage::set_loader_func(
        std::string const& extension,
        OMI_RES_LOADER_Func* loader_func)
{
    get_interface().Storage_set_loader_func(extension.c_str(), loader_func);
}

//------------------------------------------------------------------------------
//                              PRIVATE CONSTRUCTOR
//------------------------------------------------------------------------------

Storage::Storage()
{
}

//------------------------------------------------------------------------------
//                               PRIVATE DESTRUCTOR
//------------------------------------------------------------------------------

Storage::~Storage()
{
}

} // namespace res
} // namespace omi
