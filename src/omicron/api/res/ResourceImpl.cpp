/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstddef>
#include <cstring>
#include <string>
#include <unordered_map>

#include <arcanecore/base/clock/ClockOperations.hpp>
#include <arcanecore/base/hash/Spooky.hpp>
#include <arcanecore/base/str/StringOperations.hpp>

#include "omicron/api/API.h"
#include "omicron/api/common/Attributes.hpp"
#include "omicron/api/renderer/Renderer.hpp"
#include "omicron/api/report/Logger.hpp"
#include <omicron/api/report/StatsDatabase.hpp>
#include "omicron/api/res/ResourceCTypes.h"
#include "omicron/api/res/loader/DevilLoader.hpp"
#include "omicron/api/res/loader/OBJLoader.hpp"


OMI_CODEGEN_NAMESPACE namespace omi
{
OMI_CODEGEN_NAMESPACE namespace res
{
namespace
{

//------------------------------------------------------------------------------
//                                    GLOBALS
//------------------------------------------------------------------------------

static std::string g_warning_messages;

static std::unordered_map<OMI_RES_ResourceId, std::string> g_id_to_path;
static std::unordered_map<std::string, OMI_RES_ResourceId> g_path_to_id;

static std::unordered_map<std::string, OMI_RES_LOADER_Func*> g_loader_funcs;

static std::unordered_map<OMI_RES_ResourceId, omi::Attribute> g_loaded;
static std::unordered_map<OMI_RES_ResourceId, OMI_RES_RELEASE_Func*> g_release;

//------------------------------------STATS-------------------------------------
static omi::Int32Attribute g_stat_loads          (0);
static omi::Int32Attribute g_stat_blocking_loads (0);
static omi::Int32Attribute g_stat_failed_loads   (0);
static omi::Int64Attribute g_stat_total_load_time(0);
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//                                    STARTUP
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool startup(OMI_REPORT_LoggerPtr logger_ptr)
{
    report::Logger logger(logger_ptr, true);

    // define statistics
    report::StatsDatabase::instance().define_entry(
        "Resources.Loads",
        g_stat_loads,
        "The total number of resource loads performed by Omicron during "
        "the engine's lifetime."
    );
    report::StatsDatabase::instance().define_entry(
        "Resources.Blocking Loads",
        g_stat_blocking_loads,
        "The number of resources that were loaded in a non asynchronous "
        "manner. This normal signifies required resources that were not "
        "anticipated."
    );
    report::StatsDatabase::instance().define_entry(
        "Resources.Failed Loads",
        g_stat_failed_loads,
        "The number of resources that failed to load."
    );
    report::StatsDatabase::instance().define_entry(
        "Resources.Total Load Time (ms)",
        g_stat_total_load_time,
        "The total time spent by the engine loading resources"
    );

    // define built-in loaders
    g_loader_funcs["obj"]  = &OMI_RES_LOADER_obj;
    g_loader_funcs["png"]  = &OMI_RES_LOADER_devil;
    g_loader_funcs["jpg"]  = &OMI_RES_LOADER_devil;
    g_loader_funcs["jpeg"] = &OMI_RES_LOADER_devil;
    // TODO: devil supports more formats

    return OMI_True;
}

//------------------------------------------------------------------------------
//                                    CLEANUP
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void cleanup(OMI_REPORT_LoggerPtr logger_ptr)
{
    report::Logger logger(logger_ptr, true);

    logger.debug("Resource clean up");

    // unload all resources
    for(auto& release : g_release)
    {
        // get the actual resource
        auto f_resource = g_loaded.find(release.first);
        if(f_resource == g_loaded.end())
        {
            continue;
        }

        OMI_RES_Error ec = release.second(
            release.first,
            f_resource->second.get_ptr()
        );
        if(ec != OMI_RES_Error_kNone)
        {
            logger.warning(
                "Failed to release resource: {0}",
                release.first
            );
        }
    }
    g_loaded.clear();
    g_release.clear();
}

//------------------------------------------------------------------------------
//                                    SHUTDOWN
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool shutdown()
{
    // release all loaded resources and mapping
    g_loaded.clear();
    g_path_to_id.clear();
    g_id_to_path.clear();

    return OMI_True;
}

//------------------------------------------------------------------------------
//                               RESOURCE ID GET ID
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_RES_ResourceId ResourceId_get_id(char const* resource_path)
{
    std::string const str = resource_path;
    // already computed?
    auto f_id = g_path_to_id.find(str);
    if(f_id != g_path_to_id.end())
    {
        return f_id->second;
    }

    // compute
    OMI_RES_ResourceId id = static_cast<OMI_RES_ResourceId>(
        arc::hash::spooky_64(str.c_str(), str.length())
    );

    // sanity check for hash collision
    assert(
        g_id_to_path.find(id) == g_id_to_path.end() &&
        "ResourceId hash collision detected!"
    );

    // store and return
    g_id_to_path[id] = str;
    g_path_to_id[str] = id;
    return id;
}

//------------------------------------------------------------------------------
//                     RESOURCE MANAGER GET WARNING MESSAGES
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static char const* Storage_get_warning_messages()
{
    return g_warning_messages.c_str();
}

//------------------------------------------------------------------------------
//                    RESOURCE MANAGER APPEND WARNING MESSAGE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Storage_append_warning(char const* message)
{
    if(!g_warning_messages.empty())
    {
        g_warning_messages += "\n";
    }
    g_warning_messages += message;
}

//------------------------------------------------------------------------------
//                           RESOURCE MANAGER IS LOADED
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool Storage_is_loaded(
        OMI_RES_ResourceId const* ids,
        OMI_Size size)
{
    for(OMI_Size i = 0; i < size; ++i)
    {
        if(g_loaded.find(ids[i]) == g_loaded.end())
        {
            return OMI_False;
        }
    }
    return OMI_True;
}

//------------------------------------------------------------------------------
//                             RESOURCE MANAGER LOAD
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Storage_load_blocking(
        OMI_RES_ResourceId const* ids,
        OMI_Size size)
{
    // reset warning messages first
    g_warning_messages.clear();

    for(OMI_Size i = 0; i < size; ++i)
    {
        // skip resources already loaded
        if(g_loaded.find(ids[i]) != g_loaded.end())
        {
            continue;
        }

        // check we can find the path for the id
        auto f_path = g_id_to_path.find(ids[i]);
        if(f_path == g_id_to_path.end())
        {
            std::string message =
                "Cannot load resource id: " + std::to_string(ids[i]) + " as "
                "no resource with this id exists";
            Storage_append_warning(message.c_str());
            continue;
        }

        // get the extension of the file
        std::string const extension =
            arc::str::split(f_path->second, ".").back();

        // find the loader to use
        auto f_loader = g_loader_funcs.find(extension);
        if(f_loader == g_loader_funcs.end())
        {
            std::string message =
                "Cannot load resource id: " + std::to_string(ids[i]) + " from "
                "file \"" + f_path->second + "\" as no loader function has "
                "been specified for that file type.";
            Storage_append_warning(message.c_str());
            continue;
        }

        // TODO: timing
        arc::clock::TimeInt load_start = arc::clock::get_current_time();

        OMI_AttributePtr data_raw = nullptr;
        OMI_RES_RELEASE_Func* release_func = nullptr;
        f_loader->second(
            ids[i],
            f_path->second.c_str(),
            0,
            -1,
            &data_raw,
            &release_func
        );
        // load failed?
        if(data_raw == nullptr)
        {
            std::string message =
                "Failed to read resource with id: " + std::to_string(ids[i]) +
                " from file \"" + f_path->second + "\"";
            Storage_append_warning(message.c_str());

            // update stats
            g_stat_failed_loads.set_value(
                g_stat_failed_loads.get_value() + 1
            );
        }
        else
        {
            omi::MapAttribute data(data_raw, false);
            // handle based on the type of the data
            omi::StringAttribute type_attr = data.at("type");
            std::string const type = type_attr.get_value("");

            if(type == "Mesh" || type == "Texture")
            {
                // pass off to the renderer to load
                omi::MapAttribute resource;
                OMI_RES_RELEASE_Func* temp_release_func = nullptr;
                bool success = renderer::Renderer::instance().load_resource(
                    ids[i],
                    data,
                    resource,
                    &temp_release_func
                );
                if(success)
                {
                    g_loaded[ids[i]] = resource;
                }
                else
                {
                    std::string message =
                        "Failed load \"" + type + "\" resource type with "
                        "id: " + std::to_string(ids[i]) + " from file \"" +
                        f_path->second + "\"";
                    Storage_append_warning(message.c_str());

                    // update stats
                    g_stat_failed_loads.set_value(
                        g_stat_failed_loads.get_value() + 1
                    );
                }

                // release the original data
                if(release_func != nullptr)
                {
                    release_func(ids[i], data.get_ptr());
                }
                release_func = temp_release_func;
            }
            else
            {
                // this is the loaded state of the data
                g_loaded[ids[i]] = data;
            }
        }

        // store the release function
        if(release_func != nullptr)
        {
            g_release[ids[i]] = release_func;
        }

        // update stats
        g_stat_total_load_time.set_value(
            g_stat_total_load_time.get_value() +
            (arc::clock::get_current_time() - load_start)
        );
        g_stat_loads.set_value(g_stat_loads.get_value() + 1);
        g_stat_blocking_loads.set_value(g_stat_blocking_loads.get_value() + 1);
    }
}

//------------------------------------------------------------------------------
//                                  STORAGE GET
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_AttributePtr Storage_get(OMI_RES_ResourceId id)
{
    // TODO: handle unloaded resources (place holders)
    return g_loaded[id].get_ptr();
}

//------------------------------------------------------------------------------
//                            STORAGE SET LOADER FUNC
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Storage_set_loader_func(
        char const* extension,
        OMI_RES_LOADER_Func* loader_func)
{
    g_loader_funcs[extension] = loader_func;
}

} // namespace anonymous
} // namespace config
} // namespace omi

#include "omicron/api/res/_codegen/ResourceBinding.inl"
