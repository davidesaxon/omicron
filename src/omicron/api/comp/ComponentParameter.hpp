/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_COMP_COMPONENTPARAMETER_HPP_
#define OMICRON_API_COMP_COMPONENTPARAMETER_HPP_

#include <arcanecore/base/lang/Restrictors.hpp>

#include <arcanecore/cxx17/string_view.hpp>

#include "omicron/api/API.h"
#include "omicron/api/common/attribute/Attribute.hpp"
#include "omicron/api/comp/ComponentSchemaCTypes.h"


namespace omi
{
namespace comp
{

class ComponentParameter
    : private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                                  GLOBALS
    //--------------------------------------------------------------------------

    static std::size_t const NULL_SIZE;

    //--------------------------------------------------------------------------
    //                                CONSTRUCTORS
    //--------------------------------------------------------------------------

    ComponentParameter(ComponentParameter const& other);

    ComponentParameter(ComponentParameter&& other);

    ComponentParameter(OMI_COMP_ComponentParameterPtr ptr);

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    virtual ~ComponentParameter();

    //--------------------------------------------------------------------------
    //                                 OPERATORS
    //--------------------------------------------------------------------------

    ComponentParameter& operator=(ComponentParameter const& other);

    ComponentParameter& operator=(ComponentParameter&& other);

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    OMI_COMP_ComponentParameterPtr get_ptr() const;

    std::string_view get_name() const;

    std::string_view get_type() const;

    std::size_t get_size() const;

    omi::Attribute get_default(std::string const& name) const;

private:

    //--------------------------------------------------------------------------
    //                             PRIVATE ATTRIBUTES
    //--------------------------------------------------------------------------

    OMI_COMP_ComponentParameterPtr m_ptr;
};

} // namespace comp
} // namespace omi

#endif
