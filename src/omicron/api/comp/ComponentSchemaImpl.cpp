/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstddef>
#include <cstring>
#include <fstream>
#include <string>
#include <unordered_map>

#include <json/json.h>

#include <arcanecore/base/fsys/FileSystemOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>

#include "omicron/api/API.h"
#include "omicron/api/common/Attributes.hpp"
#include "omicron/api/comp/ComponentSchemaCTypes.h"
#include "omicron/api/report/Logger.hpp"
#include "omicron/api/scene/private/AttributesFromJsonParameters.hpp"


//------------------------------------------------------------------------------
//                                    GLOBALS
//------------------------------------------------------------------------------

namespace
{

static std::unordered_map<std::string, OMI_COMP_ComponentSchemaPtr> g_schemas;

} // namespace anonymous

//------------------------------------------------------------------------------
//                           COMPONENT PARAMETER STRUCT
//------------------------------------------------------------------------------

struct OMI_COMP_ComponentParameterStruct
{
    //---------------------------A T T R I B U T E S----------------------------

    std::size_t m_ref_count;
    arc::fsys::Path m_path;
    std::string m_name;
    std::string m_type;
    std::size_t m_size;
    omi::Attribute m_default;

    //--------------------------C O N S T R U C T O R---------------------------

    OMI_COMP_ComponentParameterStruct(
            arc::fsys::Path const& path,
            Json::Value& root)
        : m_ref_count(1)
        , m_path     (path)
        , m_size     (OMI_COMP_ComponentParameterNullSize)
    {
        // get the name
        if(!root.isMember("name"))
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse component schema from file \"" +
                m_path.to_native() + "\" as parameter is missing name field: " +
                writer.write(root)
            );
        }
        Json::Value name_field = root["name"];
        if(!name_field.isString())
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse component schema from file \"" +
                m_path.to_native() + "\" as name field is not of type "
                "string: " + writer.write(root)
            );
        }
        m_name = name_field.asString();

        // get the type
        if(!root.isMember("type"))
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse component schema from file \"" +
                m_path.to_native() + "\" as parameter is missing type field: " +
                writer.write(root)
            );
        }
        Json::Value type_field = root["type"];
        if(!type_field.isString())
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse component schema from file \"" +
                m_path.to_native() + "\" as parameter type field is not of "
                "type string: " + writer.write(root)
            );
        }
        m_type = type_field.asString();
        // TODO: should really check type is valid here

        // parse size
        if(root.isMember("size"))
        {
            Json::Value size_field = root["size"];
            if(!size_field.isIntegral())
            {
                Json::FastWriter writer;
                throw arc::ex::ParseError(
                    "Failed to parse component schema from file \"" +
                    m_path.to_native() + "\" as parameter size field is not an "
                    "integral type: " + writer.write(root)
                );
            }
            m_size = static_cast<std::size_t>(size_field.asUInt64());
        }

        // get the default
        if(!root.isMember("default"))
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse component schema from file \"" +
                m_path.to_native() + "\" as parameter is missing default "
                "field: " + writer.write(root)
            );
        }
        std::vector<omi::res::ResourceId> unused;
        omi::scene_::parse_attribute_value_from_json(
            path,
            m_name,
            m_type,
            m_size,
            root["default"],
            m_default,
            unused
        );
    }
};

//------------------------------------------------------------------------------
//                            COMPONENT SCHEMA STRUCT
//------------------------------------------------------------------------------

struct OMI_COMP_ComponentSchemaStruct
{
    //---------------------------A T T R I B U T E S----------------------------

    std::size_t m_ref_count;
    arc::fsys::Path m_path;
    std::string m_name;
    std::unordered_map<
        std::string,
        OMI_COMP_ComponentParameterPtr
    > m_parameters;

    //--------------------------C O N S T R U C T O R---------------------------

    OMI_COMP_ComponentSchemaStruct(arc::fsys::Path const& path)
        : m_ref_count(1)
        , m_path     (path)
    {
        // read the file
        std::ifstream input(m_path.to_native());
        std::string contents;
        input.seekg(0, std::ios::end);
        contents.reserve(input.tellg());
        input.seekg(0, std::ios::beg);
        contents.assign(
            (std::istreambuf_iterator<char>(input)),
            std::istreambuf_iterator<char>()
        );
        input.close();

        Json::Value root;
        Json::Reader reader;
        bool parse_sucess = reader.parse(contents, root);
        if(!parse_sucess)
        {
            throw arc::ex::ParseError(
                "Failed to parse component schema from file \"" +
                m_path.to_native() + "\" with error: " +
                reader.getFormattedErrorMessages()
            );
        }

        // ensure root is an object
        if(!root.isObject())
        {
            throw arc::ex::ParseError(
                "Failed to parse component schema file \"" +
                m_path.to_native() + "\" because the root value is not a map"
            );
        }

        // get the name
        if(!root.isMember("name"))
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse component schema from file \"" +
                m_path.to_native() + "\" root value is missing name field"
            );
        }
        Json::Value name_field = root["name"];
        if(!name_field.isString())
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse component schema from file \"" +
                m_path.to_native() + "\" as name field is not of type string: " +
                writer.write(name_field)
            );
        }
        m_name = name_field.asString();

        // make sure there isn't already a schema with this name
        auto f_schema = g_schemas.find(m_name);
        if(f_schema != g_schemas.end())
        {
            throw arc::ex::KeyError(
                "Failed to create component schema from file \"" +
                m_path.to_native() + "\" as the name \"" + m_name + "\" has "
                "already been used by another schema"
            );
        }

        // parse the parameters
        if(!root.isMember("parameters"))
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse component schema from file \"" +
                m_path.to_native() + "\" root value is missing parameters field"
            );
        }
        Json::Value parameters_field = root["parameters"];
        if(!parameters_field.isArray())
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse component schema from file \"" +
                m_path.to_native() + "\" as parameters field is not of type "
                "array: " + writer.write(parameters_field)
            );
        }

        for(std::size_t i = 0; i < parameters_field.size(); ++i)
        {
            Json::Value parameter_value =
                parameters_field[static_cast<Json::ArrayIndex>(i)];
            if(!parameter_value.isObject())
            {
                Json::FastWriter writer;
                throw arc::ex::ParseError(
                    "Failed to parse component schema from file \"" +
                    m_path.to_native() + "\" as parameter entry is not of "
                    "type object: " + writer.write(parameter_value)
                );
            }

            // parse the parameter
            OMI_COMP_ComponentParameterPtr parameter =
                new OMI_COMP_ComponentParameterStruct(
                    m_path,
                    parameter_value
                );
            m_parameters[parameter->m_name] = parameter;
        }
    }

    //---------------------------D E S T R U C T O R----------------------------

    virtual ~OMI_COMP_ComponentSchemaStruct()
    {
        for(auto& parameter : m_parameters)
        {
            delete parameter.second;
        }
        m_parameters.clear();
    }
};


OMI_CODEGEN_NAMESPACE namespace omi
{
OMI_CODEGEN_NAMESPACE namespace comp
{
namespace
{

//------------------------------------------------------------------------------
//                               INCREASE REFERENCE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void ComponentSchema_increase_reference(OMI_COMP_ComponentSchemaPtr ptr)
{
    ++ptr->m_ref_count;
}

//------------------------------------------------------------------------------
//                               DECREASE REFERENCE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void ComponentSchema_decrease_reference(OMI_COMP_ComponentSchemaPtr ptr)
{
    if(ptr == nullptr)
    {
        return;
    }

    if(ptr->m_ref_count == 1)
    {
        delete ptr;
    }
    else
    {
        --ptr->m_ref_count;
    }
}

//------------------------------------------------------------------------------
//                                      LOAD
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool load(OMI_REPORT_LoggerPtr logger_ptr)
{
    report::Logger logger(logger_ptr, true);

    // build the path to component schemas directory
    arc::fsys::Path const schemas_dir({"schemas", "components"});
    if(!arc::fsys::is_directory(schemas_dir))
    {
        logger.critical(
            "Component schemas path either does not exist or is not a "
            "directory: {0}",
            schemas_dir.to_native()
        );
        return OMI_False;
    };

    for(arc::fsys::Path const& subpath : arc::fsys::list_rec(schemas_dir))
    {
        if(subpath.get_extension() != "json")
        {
            continue;
        }

        // attempt to create a new component schema
        OMI_COMP_ComponentSchemaPtr schema = nullptr;
        try
        {
            schema = new OMI_COMP_ComponentSchemaStruct(subpath);
            g_schemas[schema->m_name] = schema;
        }
        catch(std::exception const& exc)
        {
            logger.error(
                "Failed to parse component schema file \"{0}\" with error: {1}",
                subpath.to_native(),
                exc.what()
            );
        }
    }

    return OMI_True;
}

//------------------------------------------------------------------------------
//                                     UNLOAD
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool unload()
{
    // clean up globally loaded schemas
    for(auto& schema : g_schemas)
    {
        ComponentSchema_decrease_reference(schema.second);
    }
    g_schemas.clear();

    return OMI_True;
}

//------------------------------------------------------------------------------
//                                   GET SCHEMA
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_COMP_Component_Error get_schema(
        char const* name,
        OMI_COMP_ComponentSchemaPtr* out_schema)
{
    auto f_schema = g_schemas.find(name);
    if(f_schema == g_schemas.end())
    {
        return OMI_COMP_Component_Error_kKey;
    }

    *out_schema = f_schema->second;
    return OMI_COMP_Component_Error_kNone;
}

//------------------------------------------------------------------------------
//                           COMPONENT SCHEMA GET NAME
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static char const* ComponentSchema_get_name(
        OMI_COMP_ComponentSchemaPtrConst ptr)
{
    return ptr->m_name.c_str();
}

//------------------------------------------------------------------------------
//                      COMPONENT SCHEMA GET PARAMETER NAMES
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size ComponentSchema_get_parameter_names(
        OMI_COMP_ComponentSchemaPtrConst ptr,
        char const* const** out_parameter_names)
{
    char const** parameter_names = new char const*[ptr->m_parameters.size()];
    std::size_t i = 0;
    for(auto& parameter : ptr->m_parameters)
    {
        parameter_names[i] = parameter.first.c_str();
        ++i;
    }
    *out_parameter_names = parameter_names;
    return static_cast<OMI_Size>(ptr->m_parameters.size());
}

//------------------------------------------------------------------------------
//                    COMPONENT SCHEMA DELETE PARAMETER NAMES
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void ComponentSchema_delete_parameter_names(
        char const* const* parameter_names)
{
    if(parameter_names != nullptr)
    {
        delete[] parameter_names;
    }
}

//------------------------------------------------------------------------------
//                         COMPONENT SCHEMA HAS PARAMETER
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool ComponentSchema_has_parameter(
        OMI_COMP_ComponentSchemaPtrConst ptr,
        char const* name)
{
    auto f_parameter = ptr->m_parameters.find(name);
    return f_parameter != ptr->m_parameters.end();
}

//------------------------------------------------------------------------------
//                         COMPONENT SCHEMA GET PARAMETER
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_COMP_Component_Error ComponentSchema_get_parameter(
        OMI_COMP_ComponentSchemaPtrConst ptr,
        char const* name,
        OMI_COMP_ComponentParameterPtr* out_parameter)
{
    auto f_parameter = ptr->m_parameters.find(name);
    if(f_parameter == ptr->m_parameters.end())
    {
        return OMI_COMP_Component_Error_kKey;
    }

    *out_parameter = f_parameter->second;
    return OMI_COMP_Component_Error_kNone;
}

//------------------------------------------------------------------------------
//                          COMPONENT PARAMETER GET NAME
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static char const* ComponentParameter_get_name(
        OMI_COMP_ComponentParameterPtrConst ptr)
{
    return ptr->m_name.c_str();
}

//------------------------------------------------------------------------------
//                          COMPONENT PARAMETER GET TYPE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static char const* ComponentParameter_get_type(
        OMI_COMP_ComponentParameterPtrConst ptr)
{
    return ptr->m_type.c_str();
}

//------------------------------------------------------------------------------
//                          COMPONENT PARAMETER GET SIZE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size ComponentParameter_get_size(
        OMI_COMP_ComponentParameterPtrConst ptr)
{
    return ptr->m_size;
}

//------------------------------------------------------------------------------
//                        COMPONENT PARAMETER GET DEFAULT
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_AttributePtr ComponentParameter_get_default(
        OMI_COMP_ComponentParameterPtrConst ptr)
{
    return ptr->m_default.get_ptr();
}

} // namespace anonymous
} // namespace comp
} // namespace omi

#include "omicron/api/comp/_codegen/ComponentSchemaBinding.inl"
