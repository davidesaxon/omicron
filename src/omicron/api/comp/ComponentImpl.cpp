/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstddef>
#include <cstring>
#include <string>

#include "omicron/api/comp/ComponentImpl.hpp"


//------------------------------------------------------------------------------
//                                    GLOBALS
//------------------------------------------------------------------------------

static std::size_t g_component_id = 0;

//------------------------------------------------------------------------------
//                          COMPONENT STRUCT CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_COMP_ComponentStruct::OMI_COMP_ComponentStruct(
        OMI_COMP_ComponentType type,
        void* internals,
        OMI_COMP_Component_DeleteFunc* delete_func)
    : m_ref_count  (1)
    , m_id         (++g_component_id)
    , m_type       (type)
    , m_internals  (internals)
    , m_delete_func(delete_func)
{
}


OMI_CODEGEN_NAMESPACE namespace omi
{
OMI_CODEGEN_NAMESPACE namespace comp
{

//------------------------------------------------------------------------------
//                               INCREASE REFERENCE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
void Component_increase_reference(OMI_COMP_ComponentPtr ptr)
{
    ++ptr->m_ref_count;
}

//------------------------------------------------------------------------------
//                               DECREASE REFERENCE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
void Component_decrease_reference(OMI_COMP_ComponentPtr ptr)
{
    if(ptr == nullptr)
    {
        return;
    }

    if(ptr->m_ref_count == 1)
    {
        if(ptr->m_internals != nullptr && ptr->m_delete_func != nullptr)
        {
            ptr->m_delete_func(ptr->m_internals);
        }
        delete ptr;
    }
    else
    {
        --ptr->m_ref_count;
    }
}

//------------------------------------------------------------------------------
//                                     GET ID
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_COMP_ComponentId get_id(OMI_COMP_ComponentPtrConst ptr)
{
    return ptr->m_id;
}

//------------------------------------------------------------------------------
//                                    GET TYPE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_COMP_ComponentType get_component_type(
        OMI_COMP_ComponentPtrConst ptr)
{
    return ptr->m_type;
}

//------------------------------------------------------------------------------
//                                    IS VALID
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool is_valid(
        OMI_COMP_ComponentPtrConst ptr,
        OMI_COMP_ComponentType expected_type)
{
    if(expected_type == OMI_COMP_ComponentType_kTrivial)
    {
        return OMI_True;
    }
    if(ptr->m_type != expected_type)
    {
        return OMI_False;
    }
    return OMI_True;
}

} // namespace comp
} // namespace omi

#include "omicron/api/comp/_codegen/ComponentBinding.inl"
