/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_COMP_COMPONENTSCHEMA_HPP_
#define OMICRON_API_COMP_COMPONENTSCHEMA_HPP_

#include <arcanecore/base/lang/Restrictors.hpp>

#include <arcanecore/cxx17/string_view.hpp>

#include "omicron/api/API.h"
#include "omicron/api/comp/ComponentParameter.hpp"
#include "omicron/api/comp/ComponentSchemaCTypes.h"
#include "omicron/api/report/LoggerCTypes.h"
#include "omicron/api/comp/_codegen/ComponentSchemaInterface.hpp"


namespace omi
{
namespace comp
{

class ComponentSchema
    : private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                                CONSTRUCTORS
    //--------------------------------------------------------------------------

    ComponentSchema(ComponentSchema const& other);

    ComponentSchema(ComponentSchema&& other);

    ComponentSchema(OMI_COMP_ComponentSchemaPtr ptr, bool increase_ref=true);

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    virtual ~ComponentSchema();

    //--------------------------------------------------------------------------
    //                                 OPERATORS
    //--------------------------------------------------------------------------

    ComponentSchema& operator=(ComponentSchema const& other);

    ComponentSchema& operator=(ComponentSchema&& other);

    //--------------------------------------------------------------------------
    //                          PUBLIC STATIC FUNCTIONS
    //--------------------------------------------------------------------------

    static ComponentSchemaInterface& get_interface();

    static ComponentSchema get_schema(std::string const& name);

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    OMI_COMP_ComponentSchemaPtr get_ptr() const;

    std::string_view get_name() const;

    std::vector<char const*> get_parameter_names() const;

    bool has_parameter(std::string const& name) const;

    ComponentParameter get_parameter(std::string const& name) const;

private:

    //--------------------------------------------------------------------------
    //                             PRIVATE ATTRIBUTES
    //--------------------------------------------------------------------------

    OMI_COMP_ComponentSchemaPtr m_ptr;
};

} // namespace comp
} // namespace omi

#endif
