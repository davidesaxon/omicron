/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_COMP_COMPONENTIMPL_HPP_
#define OMICRON_API_COMP_COMPONENTIMPL_HPP_

#include "omicron/api/API.h"
#include "omicron/api/comp/ComponentCTypes.h"


//------------------------------------------------------------------------------
//                                    TYPEDEFS
//------------------------------------------------------------------------------

typedef void (OMI_COMP_Component_DeleteFunc)(void*);

//------------------------------------------------------------------------------
//                                COMPONENT STRUCT
//------------------------------------------------------------------------------

struct OMI_COMP_ComponentStruct
{
    //---------------------------A T T R I B U T E S----------------------------

    std::size_t m_ref_count;
    OMI_COMP_ComponentId m_id;
    OMI_COMP_ComponentType m_type;
    void* m_internals;
    OMI_COMP_Component_DeleteFunc* m_delete_func;

    //--------------------------C O N S T R U C T O R---------------------------

    OMI_COMP_ComponentStruct(
            OMI_COMP_ComponentType type,
            void* internals,
            OMI_COMP_Component_DeleteFunc* delete_func);
};


namespace omi
{
namespace comp
{

//------------------------------------------------------------------------------
//                                   FUNCTIONS
//------------------------------------------------------------------------------

void Component_increase_reference(OMI_COMP_ComponentPtr ptr);

//------------------------------------------------------------------------------
//                               DECREASE REFERENCE
//------------------------------------------------------------------------------

void Component_decrease_reference(OMI_COMP_ComponentPtr ptr);

} // namespace comp
} // namespace omi

#endif
