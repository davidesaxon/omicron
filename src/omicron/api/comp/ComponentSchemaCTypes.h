/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_COMP_COMPONENTSCHEMACTYPES_H_
#define OMICRON_API_COMP_COMPONENTSCHEMACTYPES_H_

#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
//                           COMPONENT PARAMETER STRUCT
//------------------------------------------------------------------------------

typedef struct OMI_COMP_ComponentParameterStruct*
    OMI_COMP_ComponentParameterPtr;
typedef struct OMI_COMP_ComponentParameterStruct const*
    OMI_COMP_ComponentParameterPtrConst;

//------------------------------------------------------------------------------
//                         COMPONENT PARAMETER NULL SIZE
//------------------------------------------------------------------------------

#define OMI_COMP_ComponentParameterNullSize 0xFFFFFFFFFFFFFFFFU

//------------------------------------------------------------------------------
//                            COMPONENT SCHEMA STRUCT
//------------------------------------------------------------------------------

typedef struct OMI_COMP_ComponentSchemaStruct* OMI_COMP_ComponentSchemaPtr;
typedef struct OMI_COMP_ComponentSchemaStruct const*
    OMI_COMP_ComponentSchemaPtrConst;

//------------------------------------------------------------------------------
//                             COMPONENT ERROR CODES
//------------------------------------------------------------------------------

typedef uint32_t OMI_COMP_Component_Error;
#define OMI_COMP_Component_Error_kNone 0
#define OMI_COMP_Component_Error_kKey  1

#ifdef __cplusplus
}
#endif

#endif
