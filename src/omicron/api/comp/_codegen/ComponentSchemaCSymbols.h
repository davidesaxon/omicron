/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_COMP_CODEGEN_COMPONENTSCHEMACSMYBOLS_H_
#define OMI_COMP_CODEGEN_COMPONENTSCHEMACSMYBOLS_H_


#ifdef __cplusplus
extern "C" {
#endif

// OMI_COMP_ComponentSchema_increase_reference
static char const* OMI_COMP_ComponentSchema_ComponentSchema_increase_reference_SYMBOL = "void OMI_COMP_ComponentSchema_ComponentSchema_increase_reference(OMI_COMP_ComponentSchemaPtr)";

// OMI_COMP_ComponentSchema_decrease_reference
static char const* OMI_COMP_ComponentSchema_ComponentSchema_decrease_reference_SYMBOL = "void OMI_COMP_ComponentSchema_ComponentSchema_decrease_reference(OMI_COMP_ComponentSchemaPtr)";

// OMI_COMP_load
static char const* OMI_COMP_ComponentSchema_load_SYMBOL = "OMI_Bool OMI_COMP_ComponentSchema_load(OMI_REPORT_LoggerPtr)";

// OMI_COMP_unload
static char const* OMI_COMP_ComponentSchema_unload_SYMBOL = "OMI_Bool OMI_COMP_ComponentSchema_unload()";

// OMI_COMP_get_schema
static char const* OMI_COMP_ComponentSchema_get_schema_SYMBOL = "OMI_COMP_Component_Error OMI_COMP_ComponentSchema_get_schema(char const*, OMI_COMP_ComponentSchemaPtr*)";

// OMI_COMP_ComponentSchema_get_name
static char const* OMI_COMP_ComponentSchema_ComponentSchema_get_name_SYMBOL = "char const* OMI_COMP_ComponentSchema_ComponentSchema_get_name(OMI_COMP_ComponentSchemaPtrConst)";

// OMI_COMP_ComponentSchema_get_parameter_names
static char const* OMI_COMP_ComponentSchema_ComponentSchema_get_parameter_names_SYMBOL = "OMI_Size OMI_COMP_ComponentSchema_ComponentSchema_get_parameter_names(OMI_COMP_ComponentSchemaPtrConst, char const* const**)";

// OMI_COMP_ComponentSchema_delete_parameter_names
static char const* OMI_COMP_ComponentSchema_ComponentSchema_delete_parameter_names_SYMBOL = "void OMI_COMP_ComponentSchema_ComponentSchema_delete_parameter_names(char const* const*)";

// OMI_COMP_ComponentSchema_has_parameter
static char const* OMI_COMP_ComponentSchema_ComponentSchema_has_parameter_SYMBOL = "OMI_Bool OMI_COMP_ComponentSchema_ComponentSchema_has_parameter(OMI_COMP_ComponentSchemaPtrConst, char const*)";

// OMI_COMP_ComponentSchema_get_parameter
static char const* OMI_COMP_ComponentSchema_ComponentSchema_get_parameter_SYMBOL = "OMI_COMP_Component_Error OMI_COMP_ComponentSchema_ComponentSchema_get_parameter(OMI_COMP_ComponentSchemaPtrConst, char const*, OMI_COMP_ComponentParameterPtr*)";

// OMI_COMP_ComponentParameter_get_name
static char const* OMI_COMP_ComponentSchema_ComponentParameter_get_name_SYMBOL = "char const* OMI_COMP_ComponentSchema_ComponentParameter_get_name(OMI_COMP_ComponentParameterPtrConst)";

// OMI_COMP_ComponentParameter_get_type
static char const* OMI_COMP_ComponentSchema_ComponentParameter_get_type_SYMBOL = "char const* OMI_COMP_ComponentSchema_ComponentParameter_get_type(OMI_COMP_ComponentParameterPtrConst)";

// OMI_COMP_ComponentParameter_get_size
static char const* OMI_COMP_ComponentSchema_ComponentParameter_get_size_SYMBOL = "OMI_Size OMI_COMP_ComponentSchema_ComponentParameter_get_size(OMI_COMP_ComponentParameterPtrConst)";

// OMI_COMP_ComponentParameter_get_default
static char const* OMI_COMP_ComponentSchema_ComponentParameter_get_default_SYMBOL = "OMI_AttributePtr OMI_COMP_ComponentSchema_ComponentParameter_get_default(OMI_COMP_ComponentParameterPtrConst)";

#ifdef __cplusplus
} // extern "C"
#endif

#endif