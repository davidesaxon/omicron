/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#include <cstring>

#include "omicron/api/API.h"

#include "omicron/api/comp/_codegen/ComponentCSymbols.h"


#ifdef __cplusplus
extern "C" {
#endif

OMI_API_EXPORT int OMI_COMP_Component_BINDING_IMPL(char const* func_def, void** func_ptr)
{
    if(strcmp(func_def, OMI_COMP_Component_Component_increase_reference_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Component_increase_reference;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Component_Component_decrease_reference_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Component_decrease_reference;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Component_get_id_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::get_id;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Component_get_component_type_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::get_component_type;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Component_is_valid_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::is_valid;
        return 0;
    }

    return 1;
}

#ifdef __cplusplus
} // extern "C"
#endif

