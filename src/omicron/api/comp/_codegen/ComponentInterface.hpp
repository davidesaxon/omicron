/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_COMP_CODEGEN_COMPONENTINTERFACE_HPP_
#define OMI_COMP_CODEGEN_COMPONENTINTERFACE_HPP_

#include <cassert>
#include <string>

#include <arcanecore/base/dl/DLOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>
#include <arcanecore/base/lang/Restrictors.hpp>

#include "omicron/api/comp/_codegen/ComponentCSymbols.h"


namespace omi
{
namespace comp
{

class ComponentInterface
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                              TYPE DEFINITIONS
    //--------------------------------------------------------------------------

    typedef OMI_Bool (BindingFunc)(char const*, void**);

    //--------------------------------------------------------------------------
    //                             PUBLIC ATTRIBUTES
    //--------------------------------------------------------------------------

    // Component_increase_reference
    void (*Component_increase_reference)(OMI_COMP_ComponentPtr);
    // Component_decrease_reference
    void (*Component_decrease_reference)(OMI_COMP_ComponentPtr);
    // get_id
    OMI_COMP_ComponentId (*get_id)(OMI_COMP_ComponentPtrConst);
    // get_component_type
    OMI_COMP_ComponentType (*get_component_type)(OMI_COMP_ComponentPtrConst);
    // is_valid
    OMI_Bool (*is_valid)(OMI_COMP_ComponentPtrConst, OMI_COMP_ComponentType);

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    ComponentInterface(std::string const& libname)
        : Component_increase_reference(nullptr)
        , Component_decrease_reference(nullptr)
        , get_id(nullptr)
        , get_component_type(nullptr)
        , is_valid(nullptr)
    {
        #ifdef ARC_OS_WINDOWS
            arc::fsys::Path libpath({
                "build",
                "windows",
                "Release",
                libname + "_impl.dll"
            });
        #elif defined(ARC_OS_UNIX)
            arc::fsys::Path libpath({
                "build",
                "linux",
                "lib" + libname + "_impl.so"
            });
        #else
            assert(false);
        #endif

        arc::dl::Handle lib_handle = arc::dl::open_library(libpath);
        BindingFunc* binding_func = arc::dl::bind_symbol<BindingFunc>(lib_handle, "OMI_COMP_Component_BINDING_IMPL");

        if(binding_func(OMI_COMP_Component_Component_increase_reference_SYMBOL, (void**) &Component_increase_reference) != 0)
        {
        }
        if(binding_func(OMI_COMP_Component_Component_decrease_reference_SYMBOL, (void**) &Component_decrease_reference) != 0)
        {
        }
        if(binding_func(OMI_COMP_Component_get_id_SYMBOL, (void**) &get_id) != 0)
        {
        }
        if(binding_func(OMI_COMP_Component_get_component_type_SYMBOL, (void**) &get_component_type) != 0)
        {
        }
        if(binding_func(OMI_COMP_Component_is_valid_SYMBOL, (void**) &is_valid) != 0)
        {
        }
    }
};

} // namespace omi
} // namespace comp


#endif