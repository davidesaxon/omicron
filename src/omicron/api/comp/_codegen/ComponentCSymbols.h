/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_COMP_CODEGEN_COMPONENTCSMYBOLS_H_
#define OMI_COMP_CODEGEN_COMPONENTCSMYBOLS_H_


#ifdef __cplusplus
extern "C" {
#endif

// OMI_COMP_Component_increase_reference
static char const* OMI_COMP_Component_Component_increase_reference_SYMBOL = "void OMI_COMP_Component_Component_increase_reference(OMI_COMP_ComponentPtr)";

// OMI_COMP_Component_decrease_reference
static char const* OMI_COMP_Component_Component_decrease_reference_SYMBOL = "void OMI_COMP_Component_Component_decrease_reference(OMI_COMP_ComponentPtr)";

// OMI_COMP_get_id
static char const* OMI_COMP_Component_get_id_SYMBOL = "OMI_COMP_ComponentId OMI_COMP_Component_get_id(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_get_component_type
static char const* OMI_COMP_Component_get_component_type_SYMBOL = "OMI_COMP_ComponentType OMI_COMP_Component_get_component_type(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_is_valid
static char const* OMI_COMP_Component_is_valid_SYMBOL = "OMI_Bool OMI_COMP_Component_is_valid(OMI_COMP_ComponentPtrConst, OMI_COMP_ComponentType)";

#ifdef __cplusplus
} // extern "C"
#endif

#endif