/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#include <cstring>

#include "omicron/api/API.h"

#include "omicron/api/comp/_codegen/ComponentSchemaCSymbols.h"


#ifdef __cplusplus
extern "C" {
#endif

OMI_API_EXPORT int OMI_COMP_ComponentSchema_BINDING_IMPL(char const* func_def, void** func_ptr)
{
    if(strcmp(func_def, OMI_COMP_ComponentSchema_ComponentSchema_increase_reference_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::ComponentSchema_increase_reference;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_ComponentSchema_ComponentSchema_decrease_reference_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::ComponentSchema_decrease_reference;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_ComponentSchema_load_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::load;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_ComponentSchema_unload_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::unload;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_ComponentSchema_get_schema_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::get_schema;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_ComponentSchema_ComponentSchema_get_name_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::ComponentSchema_get_name;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_ComponentSchema_ComponentSchema_get_parameter_names_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::ComponentSchema_get_parameter_names;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_ComponentSchema_ComponentSchema_delete_parameter_names_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::ComponentSchema_delete_parameter_names;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_ComponentSchema_ComponentSchema_has_parameter_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::ComponentSchema_has_parameter;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_ComponentSchema_ComponentSchema_get_parameter_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::ComponentSchema_get_parameter;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_ComponentSchema_ComponentParameter_get_name_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::ComponentParameter_get_name;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_ComponentSchema_ComponentParameter_get_type_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::ComponentParameter_get_type;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_ComponentSchema_ComponentParameter_get_size_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::ComponentParameter_get_size;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_ComponentSchema_ComponentParameter_get_default_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::ComponentParameter_get_default;
        return 0;
    }

    return 1;
}

#ifdef __cplusplus
} // extern "C"
#endif

