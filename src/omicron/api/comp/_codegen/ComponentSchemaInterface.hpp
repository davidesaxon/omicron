/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_COMP_CODEGEN_COMPONENTSCHEMAINTERFACE_HPP_
#define OMI_COMP_CODEGEN_COMPONENTSCHEMAINTERFACE_HPP_

#include <cassert>
#include <string>

#include <arcanecore/base/dl/DLOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>
#include <arcanecore/base/lang/Restrictors.hpp>

#include "omicron/api/comp/_codegen/ComponentSchemaCSymbols.h"


namespace omi
{
namespace comp
{

class ComponentSchemaInterface
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                              TYPE DEFINITIONS
    //--------------------------------------------------------------------------

    typedef OMI_Bool (BindingFunc)(char const*, void**);

    //--------------------------------------------------------------------------
    //                             PUBLIC ATTRIBUTES
    //--------------------------------------------------------------------------

    // ComponentSchema_increase_reference
    void (*ComponentSchema_increase_reference)(OMI_COMP_ComponentSchemaPtr);
    // ComponentSchema_decrease_reference
    void (*ComponentSchema_decrease_reference)(OMI_COMP_ComponentSchemaPtr);
    // load
    OMI_Bool (*load)(OMI_REPORT_LoggerPtr);
    // unload
    OMI_Bool (*unload)();
    // get_schema
    OMI_COMP_Component_Error (*get_schema)(char const*, OMI_COMP_ComponentSchemaPtr*);
    // ComponentSchema_get_name
    char const* (*ComponentSchema_get_name)(OMI_COMP_ComponentSchemaPtrConst);
    // ComponentSchema_get_parameter_names
    OMI_Size (*ComponentSchema_get_parameter_names)(OMI_COMP_ComponentSchemaPtrConst, char const* const**);
    // ComponentSchema_delete_parameter_names
    void (*ComponentSchema_delete_parameter_names)(char const* const*);
    // ComponentSchema_has_parameter
    OMI_Bool (*ComponentSchema_has_parameter)(OMI_COMP_ComponentSchemaPtrConst, char const*);
    // ComponentSchema_get_parameter
    OMI_COMP_Component_Error (*ComponentSchema_get_parameter)(OMI_COMP_ComponentSchemaPtrConst, char const*, OMI_COMP_ComponentParameterPtr*);
    // ComponentParameter_get_name
    char const* (*ComponentParameter_get_name)(OMI_COMP_ComponentParameterPtrConst);
    // ComponentParameter_get_type
    char const* (*ComponentParameter_get_type)(OMI_COMP_ComponentParameterPtrConst);
    // ComponentParameter_get_size
    OMI_Size (*ComponentParameter_get_size)(OMI_COMP_ComponentParameterPtrConst);
    // ComponentParameter_get_default
    OMI_AttributePtr (*ComponentParameter_get_default)(OMI_COMP_ComponentParameterPtrConst);

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    ComponentSchemaInterface(std::string const& libname)
        : ComponentSchema_increase_reference(nullptr)
        , ComponentSchema_decrease_reference(nullptr)
        , load(nullptr)
        , unload(nullptr)
        , get_schema(nullptr)
        , ComponentSchema_get_name(nullptr)
        , ComponentSchema_get_parameter_names(nullptr)
        , ComponentSchema_delete_parameter_names(nullptr)
        , ComponentSchema_has_parameter(nullptr)
        , ComponentSchema_get_parameter(nullptr)
        , ComponentParameter_get_name(nullptr)
        , ComponentParameter_get_type(nullptr)
        , ComponentParameter_get_size(nullptr)
        , ComponentParameter_get_default(nullptr)
    {
        #ifdef ARC_OS_WINDOWS
            arc::fsys::Path libpath({
                "build",
                "windows",
                "Release",
                libname + "_impl.dll"
            });
        #elif defined(ARC_OS_UNIX)
            arc::fsys::Path libpath({
                "build",
                "linux",
                "lib" + libname + "_impl.so"
            });
        #else
            assert(false);
        #endif

        arc::dl::Handle lib_handle = arc::dl::open_library(libpath);
        BindingFunc* binding_func = arc::dl::bind_symbol<BindingFunc>(lib_handle, "OMI_COMP_ComponentSchema_BINDING_IMPL");

        if(binding_func(OMI_COMP_ComponentSchema_ComponentSchema_increase_reference_SYMBOL, (void**) &ComponentSchema_increase_reference) != 0)
        {
        }
        if(binding_func(OMI_COMP_ComponentSchema_ComponentSchema_decrease_reference_SYMBOL, (void**) &ComponentSchema_decrease_reference) != 0)
        {
        }
        if(binding_func(OMI_COMP_ComponentSchema_load_SYMBOL, (void**) &load) != 0)
        {
        }
        if(binding_func(OMI_COMP_ComponentSchema_unload_SYMBOL, (void**) &unload) != 0)
        {
        }
        if(binding_func(OMI_COMP_ComponentSchema_get_schema_SYMBOL, (void**) &get_schema) != 0)
        {
        }
        if(binding_func(OMI_COMP_ComponentSchema_ComponentSchema_get_name_SYMBOL, (void**) &ComponentSchema_get_name) != 0)
        {
        }
        if(binding_func(OMI_COMP_ComponentSchema_ComponentSchema_get_parameter_names_SYMBOL, (void**) &ComponentSchema_get_parameter_names) != 0)
        {
        }
        if(binding_func(OMI_COMP_ComponentSchema_ComponentSchema_delete_parameter_names_SYMBOL, (void**) &ComponentSchema_delete_parameter_names) != 0)
        {
        }
        if(binding_func(OMI_COMP_ComponentSchema_ComponentSchema_has_parameter_SYMBOL, (void**) &ComponentSchema_has_parameter) != 0)
        {
        }
        if(binding_func(OMI_COMP_ComponentSchema_ComponentSchema_get_parameter_SYMBOL, (void**) &ComponentSchema_get_parameter) != 0)
        {
        }
        if(binding_func(OMI_COMP_ComponentSchema_ComponentParameter_get_name_SYMBOL, (void**) &ComponentParameter_get_name) != 0)
        {
        }
        if(binding_func(OMI_COMP_ComponentSchema_ComponentParameter_get_type_SYMBOL, (void**) &ComponentParameter_get_type) != 0)
        {
        }
        if(binding_func(OMI_COMP_ComponentSchema_ComponentParameter_get_size_SYMBOL, (void**) &ComponentParameter_get_size) != 0)
        {
        }
        if(binding_func(OMI_COMP_ComponentSchema_ComponentParameter_get_default_SYMBOL, (void**) &ComponentParameter_get_default) != 0)
        {
        }
    }
};

} // namespace omi
} // namespace comp


#endif