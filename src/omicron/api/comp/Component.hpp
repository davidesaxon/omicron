/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_COMP_COMPONENT_HPP_
#define OMICRON_API_COMP_COMPONENT_HPP_

#include "omicron/api/API.h"
#include "omicron/api/comp/ComponentCTypes.h"
#include "omicron/api/comp/_codegen/ComponentInterface.hpp"


namespace omi
{
namespace comp
{

enum class ComponentType
{
    kTrivial    = OMI_COMP_ComponentType_kTrivial,
    kScript     = OMI_COMP_ComponentType_kScript,
    kTransform  = OMI_COMP_ComponentType_kTransform,
    kRenderable = OMI_COMP_ComponentType_kRenderable
};

class Component
{
public:

    //--------------------------------------------------------------------------
    //                                  TYPEDEFS
    //--------------------------------------------------------------------------

    typedef OMI_COMP_ComponentId Id;

    //--------------------------------------------------------------------------
    //                                CONSTRUCTORS
    //--------------------------------------------------------------------------

    Component(Component const& other);

    Component(Component&& other);

    Component(OMI_COMP_ComponentPtr ptr, bool increase_ref=true);

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    virtual ~Component();

    //--------------------------------------------------------------------------
    //                                 OPERATORS
    //--------------------------------------------------------------------------

    Component& operator=(Component const& other);

    Component& operator=(Component&& other);

    bool operator==(Component const& other) const;

    bool operator!=(Component const& other) const;

    //--------------------------------------------------------------------------
    //                          PUBLIC STATIC FUNCTIONS
    //--------------------------------------------------------------------------

    static ComponentInterface& get_interface();

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    OMI_COMP_ComponentPtr get_ptr() const;

    virtual bool is_valid() const;

    Id get_id() const;

    ComponentType get_component_type() const;

protected:

    //--------------------------------------------------------------------------
    //                            PROTECTED ATTRIBUTES
    //--------------------------------------------------------------------------

    OMI_COMP_ComponentPtr m_ptr;
};

} // namespace comp
} // namespace omi

//------------------------------------------------------------------------------
//                                      HASH
//------------------------------------------------------------------------------

namespace std
{

template<>
struct hash<omi::comp::Component> :
    public unary_function<omi::comp::Component, size_t>
{
    std::size_t operator()(const omi::comp::Component& component) const
    {
        return static_cast<std::size_t>(component.get_id());
    }
};

} // namespace std

#endif
