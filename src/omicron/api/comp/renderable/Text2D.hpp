/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_COMP_RENERABLE_TEXT2D_HPP_
#define OMICRON_API_COMP_RENERABLE_TEXT2D_HPP_

#include <arcanecore/cxx17/string_view.hpp>

#include <arcanecore/lx/Vector.hpp>

#include "omicron/api/common/Attributes.hpp"
#include "omicron/api/comp/renderable/Renderable.hpp"
#include "omicron/api/res/ResourceId.hpp"


namespace omi
{
namespace comp
{

class Text2D
    : public Renderable
{
public:

    //--------------------------------------------------------------------------
    //                                ENUMERATORS
    //--------------------------------------------------------------------------

    enum class HorizontalOrigin : OMI_COMP_Text2D_HorizontalOrigin
    {
        kLeft   = OMI_COMP_Text2D_HorizontalOrigin_kLeft,
        kCentre = OMI_COMP_Text2D_HorizontalOrigin_kCentre,
        kRight  = OMI_COMP_Text2D_HorizontalOrigin_kRight
    };

    enum class VerticalOrigin : OMI_COMP_Text2D_VerticalOrigin
    {
        kTop    = OMI_COMP_Text2D_VerticalOrigin_kTop,
        kCentre = OMI_COMP_Text2D_VerticalOrigin_kCentre,
        kBottom = OMI_COMP_Text2D_VerticalOrigin_kBottom
    };

    //--------------------------------------------------------------------------
    //                                CONSTRUCTORS
    //--------------------------------------------------------------------------

    // TODO: other parameters
    Text2D(
            std::string const& text,
            omi::res::ResourceId font_resource,
            arclx::Vector4f const& colour,
            float size,
            arclx::Vector2f const& position,
            HorizontalOrigin horizontal_origin,
            VerticalOrigin vertical_origin);

    Text2D(Component const& other);

    Text2D(Component&& other);

    Text2D(OMI_COMP_ComponentPtr ptr, bool increase_ref=true);

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    virtual ~Text2D();

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    virtual bool is_valid() const override;

    std::string_view get_text() const;

    void set_text(std::string const& text);

    omi::Attribute get_font_resource() const;

    arclx::Vector4f const& get_colour() const;

    void set_colour(arclx::Vector4f const& colour);

    float get_size() const;

    void set_size(float size);

    arclx::Vector2f const& get_position() const;

    void set_position(arclx::Vector2f const& position);

    HorizontalOrigin get_horizontal_origin() const;

    void set_horizontal_origin(HorizontalOrigin horizontal_origin);

    VerticalOrigin get_vertical_origin() const;

    void set_vertical_origin(VerticalOrigin vertical_origin);
};

} // namespace comp
} // namespace omi

//------------------------------------------------------------------------------
//                                      HASH
//------------------------------------------------------------------------------

namespace std
{

template<>
struct hash<omi::comp::Text2D> :
    public unary_function<omi::comp::Text2D, size_t>
{
    std::size_t operator()(const omi::comp::Text2D& component) const
    {
        return static_cast<std::size_t>(component.get_id());
    }
};

} // namespace std

#endif
