/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/comp/renderable/Camera.hpp"


namespace omi
{
namespace comp
{

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

Camera::Camera(
        float focal_length,
        arclx::Vector2f const& sensor_size,
        arclx::Vector2f const& sensor_offset,
        float near_clip,
        float far_clip,
        OMI_COMP_ComponentPtr transform)
    : Renderable(get_renderable_interface().Camera_constructor(
        focal_length,
        sensor_size(0),
        sensor_size(1),
        sensor_offset(0),
        sensor_offset(1),
        near_clip,
        far_clip,
        transform
    ), false)
{
}

Camera::Camera(Component const& other)
    : Renderable(other)
{
}

Camera::Camera(Component&& other)
    : Renderable(std::move(other))
{
}

Camera::Camera(OMI_COMP_ComponentPtr ptr, bool increase_ref)
    : Renderable(ptr, increase_ref)
{
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

Camera::~Camera()
{
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

bool Camera::is_valid() const
{
    return get_renderable_interface().Renderable_is_valid(
        m_ptr,
        OMI_COMP_RenderableType_kCamera
    ) != OMI_False;
}

float Camera::get_focal_length() const
{
    return get_renderable_interface().Camera_get_focal_length(m_ptr);
}

void Camera::set_focal_length(float focal_length)
{
    get_renderable_interface().Camera_set_focal_length(m_ptr, focal_length);
}

arclx::Vector2f const& Camera::get_sensor_size() const
{
    return *reinterpret_cast<arclx::Vector2f const*>(
        get_renderable_interface().Camera_get_sensor_size(m_ptr)
    );
}

void Camera::set_sensor_size(arclx::Vector2f const& sensor_size)
{
    get_renderable_interface().Camera_set_sensor_size(
        m_ptr,
        sensor_size(0),
        sensor_size(1)
    );
}

arclx::Vector2f const& Camera::get_sensor_offset() const
{
    return *reinterpret_cast<arclx::Vector2f const*>(
        get_renderable_interface().Camera_get_sensor_offset(m_ptr)
    );
}

void Camera::set_sensor_offset(arclx::Vector2f const& sensor_offset)
{
    get_renderable_interface().Camera_set_sensor_offset(
        m_ptr,
        sensor_offset(0),
        sensor_offset(1)
    );
}

float Camera::get_near_clip() const
{
    return get_renderable_interface().Camera_get_near_clip(m_ptr);
}

void Camera::set_near_clip(float near_clip)
{
    get_renderable_interface().Camera_set_near_clip(m_ptr, near_clip);
}

float Camera::get_far_clip() const
{
    return get_renderable_interface().Camera_get_far_clip(m_ptr);
}

void Camera::set_far_clip(float far_clip)
{
    get_renderable_interface().Camera_set_far_clip(m_ptr, far_clip);
}

Transform Camera::get_transform() const
{
    return Transform(get_renderable_interface().Camera_get_transform(m_ptr));
}

void Camera::set_transform(Transform const& transform)
{
    get_renderable_interface().Camera_set_transform(m_ptr, transform.get_ptr());
}

float Camera::get_diagonal_sensor_size() const
{
    return get_renderable_interface().Camera_get_diagonal_sensor_size(m_ptr);
}

float Camera::get_horizontal_fov() const
{
    return get_renderable_interface().Camera_get_horizontal_fov(m_ptr);
}

float Camera::get_vertical_fov() const
{
    return get_renderable_interface().Camera_get_vertical_fov(m_ptr);
}

float Camera::get_diagonal_fov() const
{
    return get_renderable_interface().Camera_get_diagonal_fov(m_ptr);
}

float Camera::get_aspect_ratio() const
{
    return get_renderable_interface().Camera_get_aspect_ratio(m_ptr);
}

} // namespace comp
} // namespace omi
