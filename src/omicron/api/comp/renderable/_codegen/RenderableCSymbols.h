/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_COMP_CODEGEN_RENDERABLECSMYBOLS_H_
#define OMI_COMP_CODEGEN_RENDERABLECSMYBOLS_H_


#ifdef __cplusplus
extern "C" {
#endif

// OMI_COMP_Renderable_is_valid
static char const* OMI_COMP_Renderable_Renderable_is_valid_SYMBOL = "OMI_Bool OMI_COMP_Renderable_Renderable_is_valid(OMI_COMP_ComponentPtrConst, OMI_COMP_RenderableType)";

// OMI_COMP_get_renderable_type
static char const* OMI_COMP_Renderable_get_renderable_type_SYMBOL = "OMI_COMP_Renderable_Error OMI_COMP_Renderable_get_renderable_type(OMI_COMP_ComponentPtrConst, OMI_COMP_RenderableType*)";

// OMI_COMP_Camera_constructor
static char const* OMI_COMP_Renderable_Camera_constructor_SYMBOL = "OMI_COMP_ComponentPtr OMI_COMP_Renderable_Camera_constructor(float, float, float, float, float, float, float, OMI_COMP_ComponentPtr)";

// OMI_COMP_Camera_get_focal_length
static char const* OMI_COMP_Renderable_Camera_get_focal_length_SYMBOL = "float OMI_COMP_Renderable_Camera_get_focal_length(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Camera_set_focal_length
static char const* OMI_COMP_Renderable_Camera_set_focal_length_SYMBOL = "void OMI_COMP_Renderable_Camera_set_focal_length(OMI_COMP_ComponentPtr, float)";

// OMI_COMP_Camera_get_sensor_size
static char const* OMI_COMP_Renderable_Camera_get_sensor_size_SYMBOL = "float const* OMI_COMP_Renderable_Camera_get_sensor_size(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Camera_set_sensor_size
static char const* OMI_COMP_Renderable_Camera_set_sensor_size_SYMBOL = "void OMI_COMP_Renderable_Camera_set_sensor_size(OMI_COMP_ComponentPtr, float, float)";

// OMI_COMP_Camera_get_sensor_offset
static char const* OMI_COMP_Renderable_Camera_get_sensor_offset_SYMBOL = "float const* OMI_COMP_Renderable_Camera_get_sensor_offset(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Camera_set_sensor_offset
static char const* OMI_COMP_Renderable_Camera_set_sensor_offset_SYMBOL = "void OMI_COMP_Renderable_Camera_set_sensor_offset(OMI_COMP_ComponentPtr, float, float)";

// OMI_COMP_Camera_get_near_clip
static char const* OMI_COMP_Renderable_Camera_get_near_clip_SYMBOL = "float OMI_COMP_Renderable_Camera_get_near_clip(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Camera_set_near_clip
static char const* OMI_COMP_Renderable_Camera_set_near_clip_SYMBOL = "void OMI_COMP_Renderable_Camera_set_near_clip(OMI_COMP_ComponentPtr, float)";

// OMI_COMP_Camera_get_far_clip
static char const* OMI_COMP_Renderable_Camera_get_far_clip_SYMBOL = "float OMI_COMP_Renderable_Camera_get_far_clip(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Camera_set_far_clip
static char const* OMI_COMP_Renderable_Camera_set_far_clip_SYMBOL = "void OMI_COMP_Renderable_Camera_set_far_clip(OMI_COMP_ComponentPtr, float)";

// OMI_COMP_Camera_get_transform
static char const* OMI_COMP_Renderable_Camera_get_transform_SYMBOL = "OMI_COMP_ComponentPtr OMI_COMP_Renderable_Camera_get_transform(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Camera_set_transform
static char const* OMI_COMP_Renderable_Camera_set_transform_SYMBOL = "void OMI_COMP_Renderable_Camera_set_transform(OMI_COMP_ComponentPtr, OMI_COMP_ComponentPtr)";

// OMI_COMP_Camera_get_diagonal_sensor_size
static char const* OMI_COMP_Renderable_Camera_get_diagonal_sensor_size_SYMBOL = "float OMI_COMP_Renderable_Camera_get_diagonal_sensor_size(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Camera_get_horizontal_fov
static char const* OMI_COMP_Renderable_Camera_get_horizontal_fov_SYMBOL = "float OMI_COMP_Renderable_Camera_get_horizontal_fov(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Camera_get_vertical_fov
static char const* OMI_COMP_Renderable_Camera_get_vertical_fov_SYMBOL = "float OMI_COMP_Renderable_Camera_get_vertical_fov(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Camera_get_diagonal_fov
static char const* OMI_COMP_Renderable_Camera_get_diagonal_fov_SYMBOL = "float OMI_COMP_Renderable_Camera_get_diagonal_fov(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Camera_get_aspect_ratio
static char const* OMI_COMP_Renderable_Camera_get_aspect_ratio_SYMBOL = "float OMI_COMP_Renderable_Camera_get_aspect_ratio(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Texture_constructor
static char const* OMI_COMP_Renderable_Texture_constructor_SYMBOL = "OMI_COMP_ComponentPtr OMI_COMP_Renderable_Texture_constructor(OMI_RES_ResourceId, char const*)";

// OMI_COMP_Texture_get_data
static char const* OMI_COMP_Renderable_Texture_get_data_SYMBOL = "OMI_AttributePtr OMI_COMP_Renderable_Texture_get_data(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Material_constructor
static char const* OMI_COMP_Renderable_Material_constructor_SYMBOL = "OMI_COMP_ComponentPtr OMI_COMP_Renderable_Material_constructor(float const*, float, OMI_COMP_ComponentPtr)";

// OMI_COMP_Material_get_albedo
static char const* OMI_COMP_Renderable_Material_get_albedo_SYMBOL = "float const* OMI_COMP_Renderable_Material_get_albedo(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Material_get_opacity
static char const* OMI_COMP_Renderable_Material_get_opacity_SYMBOL = "float OMI_COMP_Renderable_Material_get_opacity(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Material_has_texture
static char const* OMI_COMP_Renderable_Material_has_texture_SYMBOL = "OMI_Bool OMI_COMP_Renderable_Material_has_texture(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Material_get_texture
static char const* OMI_COMP_Renderable_Material_get_texture_SYMBOL = "OMI_COMP_ComponentPtr OMI_COMP_Renderable_Material_get_texture(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Mesh_constructor
static char const* OMI_COMP_Renderable_Mesh_constructor_SYMBOL = "OMI_COMP_ComponentPtr OMI_COMP_Renderable_Mesh_constructor(OMI_RES_ResourceId, char const*, OMI_COMP_ComponentPtr, OMI_COMP_ComponentPtr, OMI_Bool)";

// OMI_COMP_Mesh_get_data
static char const* OMI_COMP_Renderable_Mesh_get_data_SYMBOL = "OMI_AttributePtr OMI_COMP_Renderable_Mesh_get_data(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Mesh_has_transform
static char const* OMI_COMP_Renderable_Mesh_has_transform_SYMBOL = "OMI_Bool OMI_COMP_Renderable_Mesh_has_transform(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Mesh_get_transform
static char const* OMI_COMP_Renderable_Mesh_get_transform_SYMBOL = "OMI_COMP_ComponentPtr OMI_COMP_Renderable_Mesh_get_transform(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Mesh_has_material
static char const* OMI_COMP_Renderable_Mesh_has_material_SYMBOL = "OMI_Bool OMI_COMP_Renderable_Mesh_has_material(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Mesh_get_material
static char const* OMI_COMP_Renderable_Mesh_get_material_SYMBOL = "OMI_COMP_ComponentPtr OMI_COMP_Renderable_Mesh_get_material(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Mesh_get_cull_backfaces
static char const* OMI_COMP_Renderable_Mesh_get_cull_backfaces_SYMBOL = "OMI_Bool OMI_COMP_Renderable_Mesh_get_cull_backfaces(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Text2D_constructor
static char const* OMI_COMP_Renderable_Text2D_constructor_SYMBOL = "OMI_COMP_ComponentPtr OMI_COMP_Renderable_Text2D_constructor(char const*, OMI_RES_ResourceId, float const*, float, float const*, OMI_COMP_Text2D_HorizontalOrigin, OMI_COMP_Text2D_VerticalOrigin)";

// OMI_COMP_Text2D_get_text
static char const* OMI_COMP_Renderable_Text2D_get_text_SYMBOL = "char const* OMI_COMP_Renderable_Text2D_get_text(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Text2D_set_text
static char const* OMI_COMP_Renderable_Text2D_set_text_SYMBOL = "void OMI_COMP_Renderable_Text2D_set_text(OMI_COMP_ComponentPtrConst, char const*)";

// OMI_COMP_Text2D_get_font_resource
static char const* OMI_COMP_Renderable_Text2D_get_font_resource_SYMBOL = "OMI_AttributePtr OMI_COMP_Renderable_Text2D_get_font_resource(OMI_COMP_ComponentPtrConst)";

// OMI_COMP_Text2D_get_colour
static char const* OMI_COMP_Renderable_Text2D_get_colour_SYMBOL = "float const* OMI_COMP_Renderable_Text2D_get_colour(OMI_COMP_ComponentPtr)";

// OMI_COMP_Text2D_set_colour
static char const* OMI_COMP_Renderable_Text2D_set_colour_SYMBOL = "void OMI_COMP_Renderable_Text2D_set_colour(OMI_COMP_ComponentPtr, float const*)";

// OMI_COMP_Text2D_get_size
static char const* OMI_COMP_Renderable_Text2D_get_size_SYMBOL = "float OMI_COMP_Renderable_Text2D_get_size(OMI_COMP_ComponentPtr)";

// OMI_COMP_Text2D_set_size
static char const* OMI_COMP_Renderable_Text2D_set_size_SYMBOL = "void OMI_COMP_Renderable_Text2D_set_size(OMI_COMP_ComponentPtr, float)";

// OMI_COMP_Text2D_get_position
static char const* OMI_COMP_Renderable_Text2D_get_position_SYMBOL = "float const* OMI_COMP_Renderable_Text2D_get_position(OMI_COMP_ComponentPtr)";

// OMI_COMP_Text2D_set_position
static char const* OMI_COMP_Renderable_Text2D_set_position_SYMBOL = "void OMI_COMP_Renderable_Text2D_set_position(OMI_COMP_ComponentPtr, float const*)";

// OMI_COMP_Text2D_get_horizontal_origin
static char const* OMI_COMP_Renderable_Text2D_get_horizontal_origin_SYMBOL = "OMI_COMP_Text2D_HorizontalOrigin OMI_COMP_Renderable_Text2D_get_horizontal_origin(OMI_COMP_ComponentPtr)";

// OMI_COMP_Text2D_set_horizontal_origin
static char const* OMI_COMP_Renderable_Text2D_set_horizontal_origin_SYMBOL = "void OMI_COMP_Renderable_Text2D_set_horizontal_origin(OMI_COMP_ComponentPtr, OMI_COMP_Text2D_HorizontalOrigin)";

// OMI_COMP_Text2D_get_vertical_origin
static char const* OMI_COMP_Renderable_Text2D_get_vertical_origin_SYMBOL = "OMI_COMP_Text2D_VerticalOrigin OMI_COMP_Renderable_Text2D_get_vertical_origin(OMI_COMP_ComponentPtr)";

// OMI_COMP_Text2D_set_vertical_origin
static char const* OMI_COMP_Renderable_Text2D_set_vertical_origin_SYMBOL = "void OMI_COMP_Renderable_Text2D_set_vertical_origin(OMI_COMP_ComponentPtr, OMI_COMP_Text2D_VerticalOrigin)";

#ifdef __cplusplus
} // extern "C"
#endif

#endif