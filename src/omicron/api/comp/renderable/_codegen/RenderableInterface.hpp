/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_COMP_CODEGEN_RENDERABLEINTERFACE_HPP_
#define OMI_COMP_CODEGEN_RENDERABLEINTERFACE_HPP_

#include <cassert>
#include <string>

#include <arcanecore/base/dl/DLOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>
#include <arcanecore/base/lang/Restrictors.hpp>

#include "omicron/api/comp/renderable/_codegen/RenderableCSymbols.h"


namespace omi
{
namespace comp
{

class RenderableInterface
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                              TYPE DEFINITIONS
    //--------------------------------------------------------------------------

    typedef OMI_Bool (BindingFunc)(char const*, void**);

    //--------------------------------------------------------------------------
    //                             PUBLIC ATTRIBUTES
    //--------------------------------------------------------------------------

    // Renderable_is_valid
    OMI_Bool (*Renderable_is_valid)(OMI_COMP_ComponentPtrConst, OMI_COMP_RenderableType);
    // get_renderable_type
    OMI_COMP_Renderable_Error (*get_renderable_type)(OMI_COMP_ComponentPtrConst, OMI_COMP_RenderableType*);
    // Camera_constructor
    OMI_COMP_ComponentPtr (*Camera_constructor)(float, float, float, float, float, float, float, OMI_COMP_ComponentPtr);
    // Camera_get_focal_length
    float (*Camera_get_focal_length)(OMI_COMP_ComponentPtrConst);
    // Camera_set_focal_length
    void (*Camera_set_focal_length)(OMI_COMP_ComponentPtr, float);
    // Camera_get_sensor_size
    float const* (*Camera_get_sensor_size)(OMI_COMP_ComponentPtrConst);
    // Camera_set_sensor_size
    void (*Camera_set_sensor_size)(OMI_COMP_ComponentPtr, float, float);
    // Camera_get_sensor_offset
    float const* (*Camera_get_sensor_offset)(OMI_COMP_ComponentPtrConst);
    // Camera_set_sensor_offset
    void (*Camera_set_sensor_offset)(OMI_COMP_ComponentPtr, float, float);
    // Camera_get_near_clip
    float (*Camera_get_near_clip)(OMI_COMP_ComponentPtrConst);
    // Camera_set_near_clip
    void (*Camera_set_near_clip)(OMI_COMP_ComponentPtr, float);
    // Camera_get_far_clip
    float (*Camera_get_far_clip)(OMI_COMP_ComponentPtrConst);
    // Camera_set_far_clip
    void (*Camera_set_far_clip)(OMI_COMP_ComponentPtr, float);
    // Camera_get_transform
    OMI_COMP_ComponentPtr (*Camera_get_transform)(OMI_COMP_ComponentPtrConst);
    // Camera_set_transform
    void (*Camera_set_transform)(OMI_COMP_ComponentPtr, OMI_COMP_ComponentPtr);
    // Camera_get_diagonal_sensor_size
    float (*Camera_get_diagonal_sensor_size)(OMI_COMP_ComponentPtrConst);
    // Camera_get_horizontal_fov
    float (*Camera_get_horizontal_fov)(OMI_COMP_ComponentPtrConst);
    // Camera_get_vertical_fov
    float (*Camera_get_vertical_fov)(OMI_COMP_ComponentPtrConst);
    // Camera_get_diagonal_fov
    float (*Camera_get_diagonal_fov)(OMI_COMP_ComponentPtrConst);
    // Camera_get_aspect_ratio
    float (*Camera_get_aspect_ratio)(OMI_COMP_ComponentPtrConst);
    // Texture_constructor
    OMI_COMP_ComponentPtr (*Texture_constructor)(OMI_RES_ResourceId, char const*);
    // Texture_get_data
    OMI_AttributePtr (*Texture_get_data)(OMI_COMP_ComponentPtrConst);
    // Material_constructor
    OMI_COMP_ComponentPtr (*Material_constructor)(float const*, float, OMI_COMP_ComponentPtr);
    // Material_get_albedo
    float const* (*Material_get_albedo)(OMI_COMP_ComponentPtrConst);
    // Material_get_opacity
    float (*Material_get_opacity)(OMI_COMP_ComponentPtrConst);
    // Material_has_texture
    OMI_Bool (*Material_has_texture)(OMI_COMP_ComponentPtrConst);
    // Material_get_texture
    OMI_COMP_ComponentPtr (*Material_get_texture)(OMI_COMP_ComponentPtrConst);
    // Mesh_constructor
    OMI_COMP_ComponentPtr (*Mesh_constructor)(OMI_RES_ResourceId, char const*, OMI_COMP_ComponentPtr, OMI_COMP_ComponentPtr, OMI_Bool);
    // Mesh_get_data
    OMI_AttributePtr (*Mesh_get_data)(OMI_COMP_ComponentPtrConst);
    // Mesh_has_transform
    OMI_Bool (*Mesh_has_transform)(OMI_COMP_ComponentPtrConst);
    // Mesh_get_transform
    OMI_COMP_ComponentPtr (*Mesh_get_transform)(OMI_COMP_ComponentPtrConst);
    // Mesh_has_material
    OMI_Bool (*Mesh_has_material)(OMI_COMP_ComponentPtrConst);
    // Mesh_get_material
    OMI_COMP_ComponentPtr (*Mesh_get_material)(OMI_COMP_ComponentPtrConst);
    // Mesh_get_cull_backfaces
    OMI_Bool (*Mesh_get_cull_backfaces)(OMI_COMP_ComponentPtrConst);
    // Text2D_constructor
    OMI_COMP_ComponentPtr (*Text2D_constructor)(char const*, OMI_RES_ResourceId, float const*, float, float const*, OMI_COMP_Text2D_HorizontalOrigin, OMI_COMP_Text2D_VerticalOrigin);
    // Text2D_get_text
    char const* (*Text2D_get_text)(OMI_COMP_ComponentPtrConst);
    // Text2D_set_text
    void (*Text2D_set_text)(OMI_COMP_ComponentPtrConst, char const*);
    // Text2D_get_font_resource
    OMI_AttributePtr (*Text2D_get_font_resource)(OMI_COMP_ComponentPtrConst);
    // Text2D_get_colour
    float const* (*Text2D_get_colour)(OMI_COMP_ComponentPtr);
    // Text2D_set_colour
    void (*Text2D_set_colour)(OMI_COMP_ComponentPtr, float const*);
    // Text2D_get_size
    float (*Text2D_get_size)(OMI_COMP_ComponentPtr);
    // Text2D_set_size
    void (*Text2D_set_size)(OMI_COMP_ComponentPtr, float);
    // Text2D_get_position
    float const* (*Text2D_get_position)(OMI_COMP_ComponentPtr);
    // Text2D_set_position
    void (*Text2D_set_position)(OMI_COMP_ComponentPtr, float const*);
    // Text2D_get_horizontal_origin
    OMI_COMP_Text2D_HorizontalOrigin (*Text2D_get_horizontal_origin)(OMI_COMP_ComponentPtr);
    // Text2D_set_horizontal_origin
    void (*Text2D_set_horizontal_origin)(OMI_COMP_ComponentPtr, OMI_COMP_Text2D_HorizontalOrigin);
    // Text2D_get_vertical_origin
    OMI_COMP_Text2D_VerticalOrigin (*Text2D_get_vertical_origin)(OMI_COMP_ComponentPtr);
    // Text2D_set_vertical_origin
    void (*Text2D_set_vertical_origin)(OMI_COMP_ComponentPtr, OMI_COMP_Text2D_VerticalOrigin);

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    RenderableInterface(std::string const& libname)
        : Renderable_is_valid(nullptr)
        , get_renderable_type(nullptr)
        , Camera_constructor(nullptr)
        , Camera_get_focal_length(nullptr)
        , Camera_set_focal_length(nullptr)
        , Camera_get_sensor_size(nullptr)
        , Camera_set_sensor_size(nullptr)
        , Camera_get_sensor_offset(nullptr)
        , Camera_set_sensor_offset(nullptr)
        , Camera_get_near_clip(nullptr)
        , Camera_set_near_clip(nullptr)
        , Camera_get_far_clip(nullptr)
        , Camera_set_far_clip(nullptr)
        , Camera_get_transform(nullptr)
        , Camera_set_transform(nullptr)
        , Camera_get_diagonal_sensor_size(nullptr)
        , Camera_get_horizontal_fov(nullptr)
        , Camera_get_vertical_fov(nullptr)
        , Camera_get_diagonal_fov(nullptr)
        , Camera_get_aspect_ratio(nullptr)
        , Texture_constructor(nullptr)
        , Texture_get_data(nullptr)
        , Material_constructor(nullptr)
        , Material_get_albedo(nullptr)
        , Material_get_opacity(nullptr)
        , Material_has_texture(nullptr)
        , Material_get_texture(nullptr)
        , Mesh_constructor(nullptr)
        , Mesh_get_data(nullptr)
        , Mesh_has_transform(nullptr)
        , Mesh_get_transform(nullptr)
        , Mesh_has_material(nullptr)
        , Mesh_get_material(nullptr)
        , Mesh_get_cull_backfaces(nullptr)
        , Text2D_constructor(nullptr)
        , Text2D_get_text(nullptr)
        , Text2D_set_text(nullptr)
        , Text2D_get_font_resource(nullptr)
        , Text2D_get_colour(nullptr)
        , Text2D_set_colour(nullptr)
        , Text2D_get_size(nullptr)
        , Text2D_set_size(nullptr)
        , Text2D_get_position(nullptr)
        , Text2D_set_position(nullptr)
        , Text2D_get_horizontal_origin(nullptr)
        , Text2D_set_horizontal_origin(nullptr)
        , Text2D_get_vertical_origin(nullptr)
        , Text2D_set_vertical_origin(nullptr)
    {
        #ifdef ARC_OS_WINDOWS
            arc::fsys::Path libpath({
                "build",
                "windows",
                "Release",
                libname + "_impl.dll"
            });
        #elif defined(ARC_OS_UNIX)
            arc::fsys::Path libpath({
                "build",
                "linux",
                "lib" + libname + "_impl.so"
            });
        #else
            assert(false);
        #endif

        arc::dl::Handle lib_handle = arc::dl::open_library(libpath);
        BindingFunc* binding_func = arc::dl::bind_symbol<BindingFunc>(lib_handle, "OMI_COMP_Renderable_BINDING_IMPL");

        if(binding_func(OMI_COMP_Renderable_Renderable_is_valid_SYMBOL, (void**) &Renderable_is_valid) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_get_renderable_type_SYMBOL, (void**) &get_renderable_type) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Camera_constructor_SYMBOL, (void**) &Camera_constructor) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Camera_get_focal_length_SYMBOL, (void**) &Camera_get_focal_length) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Camera_set_focal_length_SYMBOL, (void**) &Camera_set_focal_length) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Camera_get_sensor_size_SYMBOL, (void**) &Camera_get_sensor_size) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Camera_set_sensor_size_SYMBOL, (void**) &Camera_set_sensor_size) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Camera_get_sensor_offset_SYMBOL, (void**) &Camera_get_sensor_offset) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Camera_set_sensor_offset_SYMBOL, (void**) &Camera_set_sensor_offset) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Camera_get_near_clip_SYMBOL, (void**) &Camera_get_near_clip) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Camera_set_near_clip_SYMBOL, (void**) &Camera_set_near_clip) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Camera_get_far_clip_SYMBOL, (void**) &Camera_get_far_clip) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Camera_set_far_clip_SYMBOL, (void**) &Camera_set_far_clip) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Camera_get_transform_SYMBOL, (void**) &Camera_get_transform) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Camera_set_transform_SYMBOL, (void**) &Camera_set_transform) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Camera_get_diagonal_sensor_size_SYMBOL, (void**) &Camera_get_diagonal_sensor_size) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Camera_get_horizontal_fov_SYMBOL, (void**) &Camera_get_horizontal_fov) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Camera_get_vertical_fov_SYMBOL, (void**) &Camera_get_vertical_fov) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Camera_get_diagonal_fov_SYMBOL, (void**) &Camera_get_diagonal_fov) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Camera_get_aspect_ratio_SYMBOL, (void**) &Camera_get_aspect_ratio) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Texture_constructor_SYMBOL, (void**) &Texture_constructor) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Texture_get_data_SYMBOL, (void**) &Texture_get_data) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Material_constructor_SYMBOL, (void**) &Material_constructor) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Material_get_albedo_SYMBOL, (void**) &Material_get_albedo) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Material_get_opacity_SYMBOL, (void**) &Material_get_opacity) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Material_has_texture_SYMBOL, (void**) &Material_has_texture) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Material_get_texture_SYMBOL, (void**) &Material_get_texture) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Mesh_constructor_SYMBOL, (void**) &Mesh_constructor) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Mesh_get_data_SYMBOL, (void**) &Mesh_get_data) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Mesh_has_transform_SYMBOL, (void**) &Mesh_has_transform) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Mesh_get_transform_SYMBOL, (void**) &Mesh_get_transform) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Mesh_has_material_SYMBOL, (void**) &Mesh_has_material) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Mesh_get_material_SYMBOL, (void**) &Mesh_get_material) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Mesh_get_cull_backfaces_SYMBOL, (void**) &Mesh_get_cull_backfaces) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Text2D_constructor_SYMBOL, (void**) &Text2D_constructor) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Text2D_get_text_SYMBOL, (void**) &Text2D_get_text) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Text2D_set_text_SYMBOL, (void**) &Text2D_set_text) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Text2D_get_font_resource_SYMBOL, (void**) &Text2D_get_font_resource) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Text2D_get_colour_SYMBOL, (void**) &Text2D_get_colour) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Text2D_set_colour_SYMBOL, (void**) &Text2D_set_colour) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Text2D_get_size_SYMBOL, (void**) &Text2D_get_size) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Text2D_set_size_SYMBOL, (void**) &Text2D_set_size) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Text2D_get_position_SYMBOL, (void**) &Text2D_get_position) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Text2D_set_position_SYMBOL, (void**) &Text2D_set_position) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Text2D_get_horizontal_origin_SYMBOL, (void**) &Text2D_get_horizontal_origin) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Text2D_set_horizontal_origin_SYMBOL, (void**) &Text2D_set_horizontal_origin) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Text2D_get_vertical_origin_SYMBOL, (void**) &Text2D_get_vertical_origin) != 0)
        {
        }
        if(binding_func(OMI_COMP_Renderable_Text2D_set_vertical_origin_SYMBOL, (void**) &Text2D_set_vertical_origin) != 0)
        {
        }
    }
};

} // namespace omi
} // namespace comp


#endif