/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#include <cstring>

#include "omicron/api/API.h"

#include "omicron/api/comp/renderable/_codegen/RenderableCSymbols.h"


#ifdef __cplusplus
extern "C" {
#endif

OMI_API_EXPORT int OMI_COMP_Renderable_BINDING_IMPL(char const* func_def, void** func_ptr)
{
    if(strcmp(func_def, OMI_COMP_Renderable_Renderable_is_valid_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Renderable_is_valid;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_get_renderable_type_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::get_renderable_type;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Camera_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Camera_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Camera_get_focal_length_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Camera_get_focal_length;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Camera_set_focal_length_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Camera_set_focal_length;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Camera_get_sensor_size_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Camera_get_sensor_size;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Camera_set_sensor_size_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Camera_set_sensor_size;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Camera_get_sensor_offset_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Camera_get_sensor_offset;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Camera_set_sensor_offset_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Camera_set_sensor_offset;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Camera_get_near_clip_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Camera_get_near_clip;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Camera_set_near_clip_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Camera_set_near_clip;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Camera_get_far_clip_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Camera_get_far_clip;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Camera_set_far_clip_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Camera_set_far_clip;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Camera_get_transform_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Camera_get_transform;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Camera_set_transform_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Camera_set_transform;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Camera_get_diagonal_sensor_size_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Camera_get_diagonal_sensor_size;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Camera_get_horizontal_fov_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Camera_get_horizontal_fov;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Camera_get_vertical_fov_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Camera_get_vertical_fov;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Camera_get_diagonal_fov_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Camera_get_diagonal_fov;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Camera_get_aspect_ratio_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Camera_get_aspect_ratio;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Texture_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Texture_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Texture_get_data_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Texture_get_data;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Material_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Material_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Material_get_albedo_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Material_get_albedo;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Material_get_opacity_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Material_get_opacity;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Material_has_texture_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Material_has_texture;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Material_get_texture_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Material_get_texture;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Mesh_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Mesh_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Mesh_get_data_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Mesh_get_data;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Mesh_has_transform_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Mesh_has_transform;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Mesh_get_transform_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Mesh_get_transform;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Mesh_has_material_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Mesh_has_material;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Mesh_get_material_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Mesh_get_material;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Mesh_get_cull_backfaces_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Mesh_get_cull_backfaces;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Text2D_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Text2D_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Text2D_get_text_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Text2D_get_text;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Text2D_set_text_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Text2D_set_text;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Text2D_get_font_resource_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Text2D_get_font_resource;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Text2D_get_colour_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Text2D_get_colour;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Text2D_set_colour_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Text2D_set_colour;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Text2D_get_size_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Text2D_get_size;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Text2D_set_size_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Text2D_set_size;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Text2D_get_position_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Text2D_get_position;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Text2D_set_position_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Text2D_set_position;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Text2D_get_horizontal_origin_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Text2D_get_horizontal_origin;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Text2D_set_horizontal_origin_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Text2D_set_horizontal_origin;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Text2D_get_vertical_origin_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Text2D_get_vertical_origin;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Renderable_Text2D_set_vertical_origin_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Text2D_set_vertical_origin;
        return 0;
    }

    return 1;
}

#ifdef __cplusplus
} // extern "C"
#endif

