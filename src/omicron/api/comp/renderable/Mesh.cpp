/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/comp/renderable/Mesh.hpp"


namespace omi
{
namespace comp
{

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

Mesh::Mesh(
        omi::res::ResourceId resource_id,
        std::string const& resource_name,
        OMI_COMP_ComponentPtr transform,
        OMI_COMP_ComponentPtr material,
        bool cull_backfaces)
    : Renderable(get_renderable_interface().Mesh_constructor(
        resource_id,
        resource_name.c_str(),
        transform,
        material,
        cull_backfaces
    ), false)
{
}

Mesh::Mesh(Component const& other)
    : Renderable(other)
{
}

Mesh::Mesh(Component&& other)
    : Renderable(std::move(other))
{
}

Mesh::Mesh(OMI_COMP_ComponentPtr ptr, bool increase_ref)
    : Renderable(ptr, increase_ref)
{
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

Mesh::~Mesh()
{
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

bool Mesh::is_valid() const
{
    return get_renderable_interface().Renderable_is_valid(
        m_ptr,
        OMI_COMP_RenderableType_kMesh
    ) != OMI_False;
}

omi::Attribute Mesh::get_data() const
{
    return omi::Attribute(get_renderable_interface().Mesh_get_data(m_ptr));
}

bool Mesh::has_transform() const
{
    return get_renderable_interface().Mesh_has_transform(m_ptr) != OMI_False;
}

Transform Mesh::get_transform() const
{
    return Transform(get_renderable_interface().Mesh_get_transform(m_ptr));
}

bool Mesh::has_material() const
{
    return get_renderable_interface().Mesh_has_material(m_ptr) != OMI_False;
}

Material Mesh::get_material() const
{
    return Material(get_renderable_interface().Mesh_get_material(m_ptr));
}

bool Mesh::get_cull_backfaces() const
{
    return
        get_renderable_interface().Mesh_get_cull_backfaces(m_ptr) != OMI_False;
}

} // namespace comp
} // namespace omi
