/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_COMP_RENDERABLE_RENDERABLECTYPES_H_
#define OMICRON_API_COMP_RENDERABLE_RENDERABLECTYPES_H_

#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
//                                  ERROR CODES
//------------------------------------------------------------------------------

typedef uint32_t OMI_COMP_Renderable_Error;
#define OMI_COMP_Renderable_Error_kNone    0
#define OMI_COMP_Renderable_Error_kInvalid 1

//------------------------------------------------------------------------------
//                                RENDERABLE TYPE
//------------------------------------------------------------------------------

typedef uint32_t OMI_COMP_RenderableType;
#define OMI_COMP_RenderableType_kCamera   (0)
#define OMI_COMP_RenderableType_kTexture  (1)
#define OMI_COMP_RenderableType_kMaterial (2)
#define OMI_COMP_RenderableType_kMesh     (3)
#define OMI_COMP_RenderableType_kText2D   (4)

//------------------------------------------------------------------------------
//                            TEXT2D HORIZONTAL ORIGIN
//------------------------------------------------------------------------------

typedef uint32_t OMI_COMP_Text2D_HorizontalOrigin;
#define OMI_COMP_Text2D_HorizontalOrigin_kLeft   (0)
#define OMI_COMP_Text2D_HorizontalOrigin_kCentre (1)
#define OMI_COMP_Text2D_HorizontalOrigin_kRight  (2)

//------------------------------------------------------------------------------
//                             TEXT2D VERTICAL ORIGIN
//------------------------------------------------------------------------------

typedef uint32_t OMI_COMP_Text2D_VerticalOrigin;
#define OMI_COMP_Text2D_VerticalOrigin_kTop    (0)
#define OMI_COMP_Text2D_VerticalOrigin_kCentre (1)
#define OMI_COMP_Text2D_VerticalOrigin_kBottom (2)


#ifdef __cplusplus
}
#endif

#endif
