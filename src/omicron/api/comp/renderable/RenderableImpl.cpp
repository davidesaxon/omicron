/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstddef>
#include <cstring>
#include <string>

#include <arcanecore/base/math/MathOperations.hpp>

#include <arcanecore/lx/Alignment.hpp>
#include <arcanecore/lx/Vector.hpp>

#include "omicron/api/API.h"
#include "omicron/api/common/Attributes.hpp"
#include "omicron/api/comp/ComponentImpl.hpp"
#include "omicron/api/comp/renderable/RenderableCTypes.h"
#include "omicron/api/comp/transform/MatrixTransform.hpp"
#include "omicron/api/comp/transform/Transform.hpp"
#include "omicron/api/res/Storage.hpp"
#include "omicron/api/res/ResourceId.hpp"


OMI_CODEGEN_NAMESPACE namespace omi
{
OMI_CODEGEN_NAMESPACE namespace comp
{
namespace
{

//------------------------------------------------------------------------------
//                                    STRUCTS
//------------------------------------------------------------------------------

struct AbstractRenderable
{
    //---------------------------A T T R I B U T E S----------------------------

    OMI_COMP_RenderableType m_type;

    //--------------------------C O N S T R U C T O R---------------------------

    AbstractRenderable(OMI_COMP_RenderableType type)
        : m_type(type)
    {
    }

    //---------------------------D E S T R U C T O R----------------------------

    virtual ~AbstractRenderable()
    {
    }
};

//------------------------------------------------------------------------------
//                               TRANSFORM DEFAULT
//------------------------------------------------------------------------------

static omi::comp::MatrixTransform TRANSFORM_DEFAULT(
    arclx::Matrix44f::Identity(),
    omi::comp::TransformConstraint::kSRT,
    nullptr
);

//------------------------------------------------------------------------------
//                               CAMERA RENDERABLE
//------------------------------------------------------------------------------

struct CameraRenderable
    : public AbstractRenderable
{
    ARCLX_ALIGNED_NEW;

    //---------------------------A T T R I B U T E S----------------------------

    float m_focal_length;
    arclx::Vector2f m_sensor_size;
    arclx::Vector2f m_sensor_offset;
    float m_near_clip;
    float m_far_clip;
    OMI_COMP_ComponentPtr m_transform;

    //--------------------------C O N S T R U C T O R---------------------------

    CameraRenderable(
            float focal_length,
            float sensor_size_x,
            float sensor_size_y,
            float sensor_offset_x,
            float sensor_offset_y,
            float near_clip,
            float far_clip,
            OMI_COMP_ComponentPtr transform)
        : AbstractRenderable(OMI_COMP_RenderableType_kCamera)
        , m_focal_length    (focal_length)
        , m_sensor_size     (sensor_size_x, sensor_size_y)
        , m_sensor_offset   (sensor_offset_x, sensor_offset_y)
        , m_near_clip       (near_clip)
        , m_far_clip        (far_clip)
        , m_transform       (transform)
    {
        if(m_transform != nullptr)
        {
            omi::comp::Component_increase_reference(m_transform);
        }
    }

    //---------------------------D E S T R U C T O R----------------------------

    virtual ~CameraRenderable()
    {
        if(m_transform != nullptr)
        {
            omi::comp::Component_decrease_reference(m_transform);
        }
    }
};

//------------------------------------------------------------------------------
//                               TEXTURE RENDERABLE
//------------------------------------------------------------------------------

struct TextureRenderable
    : public AbstractRenderable
{
    //---------------------------A T T R I B U T E S----------------------------

    omi::Attribute m_data;

    //--------------------------C O N S T R U C T O R---------------------------

    TextureRenderable(omi::Attribute const& data)
        : AbstractRenderable(OMI_COMP_RenderableType_kTexture)
        , m_data            (data)
    {
    }

    //---------------------------D E S T R U C T O R----------------------------

    virtual ~TextureRenderable()
    {
    }
};

//------------------------------------------------------------------------------
//                              MATERIAL RENDERABLE
//------------------------------------------------------------------------------

struct MaterialRenderable
    : public AbstractRenderable
{
    ARCLX_ALIGNED_NEW

    //---------------------------A T T R I B U T E S----------------------------

    arclx::Vector3f m_albedo;
    float m_opacity;
    OMI_COMP_ComponentPtr m_texture;

    //--------------------------C O N S T R U C T O R---------------------------

    MaterialRenderable(
            float const* albedo,
            float opacity,
            OMI_COMP_ComponentPtr texture)
        : AbstractRenderable(OMI_COMP_RenderableType_kMaterial)
        , m_albedo          (*reinterpret_cast<arclx::Vector3f const*>(albedo))
        , m_opacity         (opacity)
        , m_texture         (texture)
    {
        if(m_texture != nullptr)
        {
            omi::comp::Component_increase_reference(m_texture);
        }
    }

    //---------------------------D E S T R U C T O R----------------------------

    virtual ~MaterialRenderable()
    {
        if(m_texture != nullptr)
        {
            omi::comp::Component_decrease_reference(m_texture);
        }
    }
};

//------------------------------------------------------------------------------
//                                MESH RENDERABLE
//------------------------------------------------------------------------------

struct MeshRenderable
    : public AbstractRenderable
{
    //---------------------------A T T R I B U T E S----------------------------

    omi::Attribute m_data;
    OMI_COMP_ComponentPtr m_transform;
    OMI_COMP_ComponentPtr m_material;
    bool m_cull_backfaces;

    //--------------------------C O N S T R U C T O R---------------------------

    MeshRenderable(
            omi::Attribute const& data,
            OMI_COMP_ComponentPtr transform,
            OMI_COMP_ComponentPtr material,
            bool cull_backfaces)
        : AbstractRenderable(OMI_COMP_RenderableType_kMesh)
        , m_data            (data)
        , m_transform       (transform)
        , m_material        (material)
        , m_cull_backfaces  (cull_backfaces)
    {
        if(m_transform != nullptr)
        {
            omi::comp::Component_increase_reference(m_transform);
        }
        if(m_material != nullptr)
        {
            omi::comp::Component_increase_reference(m_material);
        }
    }

    //---------------------------D E S T R U C T O R----------------------------

    virtual ~MeshRenderable()
    {
        if(m_material != nullptr)
        {
            omi::comp::Component_decrease_reference(m_material);
        }
        if(m_transform != nullptr)
        {
            omi::comp::Component_decrease_reference(m_transform);
        }
    }
};

//------------------------------------------------------------------------------
//                               TEXT2D RENDERABLE
//------------------------------------------------------------------------------

struct Text2DRenderable
    : public AbstractRenderable
{
    ARCLX_ALIGNED_NEW

    //---------------------------A T T R I B U T E S----------------------------

    std::string m_text;
    omi::Attribute m_font_resource;
    arclx::Vector4f m_colour;
    float m_size;
    arclx::Vector2f m_position;
    OMI_COMP_Text2D_HorizontalOrigin m_horizontal_origin;
    OMI_COMP_Text2D_VerticalOrigin m_vertical_origin;

    //--------------------------C O N S T R U C T O R---------------------------

    Text2DRenderable(
            std::string const& text,
            omi::Attribute const& font_resource,
            float const* colour,
            float size,
            float const* position,
            OMI_COMP_Text2D_HorizontalOrigin horizontal_origin,
            OMI_COMP_Text2D_VerticalOrigin vertical_origin)
        : AbstractRenderable (OMI_COMP_RenderableType_kText2D)
        , m_text             (text)
        , m_font_resource    (font_resource)
        , m_colour           (*reinterpret_cast<arclx::Vector4f const*>(colour))
        , m_size             (size)
        , m_position         (
            *reinterpret_cast<arclx::Vector2f const*>(position)
        )
        , m_horizontal_origin(horizontal_origin)
        , m_vertical_origin  (vertical_origin)
    {
    }

    //---------------------------D E S T R U C T O R----------------------------

    virtual ~Text2DRenderable()
    {
    }
};

//------------------------------------------------------------------------------
//                              RENDERABLE IS VALID
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool Renderable_is_valid(
        OMI_COMP_ComponentPtrConst ptr,
        OMI_COMP_RenderableType expected_type)
{
    if(ptr->m_type != OMI_COMP_ComponentType_kRenderable ||
       ptr->m_internals == nullptr)
    {
        return OMI_False;
    }

    AbstractRenderable* internals = (AbstractRenderable*) ptr->m_internals;
    if(internals->m_type != expected_type)
    {
        return OMI_False;
    }
    return OMI_True;
}

//------------------------------------------------------------------------------
//                              GET RENDERABLE TYPE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_COMP_Renderable_Error get_renderable_type(
        OMI_COMP_ComponentPtrConst ptr,
        OMI_COMP_RenderableType* out_type)
{
    if(ptr->m_type != OMI_COMP_ComponentType_kRenderable ||
       ptr->m_internals == nullptr)
    {
        return OMI_COMP_Renderable_Error_kInvalid;
    }

    AbstractRenderable* internals = (AbstractRenderable*) ptr->m_internals;
    *out_type = internals->m_type;

    return OMI_COMP_Renderable_Error_kNone;
}

//------------------------------------------------------------------------------
//                               CAMERA CONSTRUCTOR
//------------------------------------------------------------------------------

static void Camera_delete(void* internals)
{
    CameraRenderable* camera = (CameraRenderable*) internals;
    delete camera;
}

OMI_CODEGEN_FUNCTION
static OMI_COMP_ComponentPtr Camera_constructor(
        float focal_length,
        float sensor_size_x,
        float sensor_size_y,
        float sensor_offset_x,
        float sensor_offset_y,
        float near_clip,
        float far_clip,
        OMI_COMP_ComponentPtr transform)
{
    CameraRenderable* camera = new CameraRenderable(
        focal_length,
        sensor_size_x,
        sensor_size_y,
        sensor_offset_x,
        sensor_offset_y,
        near_clip,
        far_clip,
        transform
    );
    return new OMI_COMP_ComponentStruct(
        OMI_COMP_ComponentType_kRenderable,
        camera,
        &Camera_delete
    );
}

//------------------------------------------------------------------------------
//                            CAMERA GET FOCAL LENGTH
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float Camera_get_focal_length(OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    CameraRenderable* camera = (CameraRenderable*) ptr->m_internals;
    return camera->m_focal_length;
}

//------------------------------------------------------------------------------
//                            CAMERA SET FOCAL LENGTH
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Camera_set_focal_length(
        OMI_COMP_ComponentPtr ptr,
        float focal_length)
{
    // no error checking for performance

    CameraRenderable* camera = (CameraRenderable*) ptr->m_internals;
    camera->m_focal_length = focal_length;
}

//------------------------------------------------------------------------------
//                             CAMERA GET SENSOR SIZE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float const* Camera_get_sensor_size(OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    CameraRenderable* camera = (CameraRenderable*) ptr->m_internals;
    return &camera->m_sensor_size(0);
}

//------------------------------------------------------------------------------
//                             CAMERA SET SENSOR SIZE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Camera_set_sensor_size(
        OMI_COMP_ComponentPtr ptr,
        float sensor_size_x,
        float sensor_size_y)
{
    // no error checking for performance

    CameraRenderable* camera = (CameraRenderable*) ptr->m_internals;
    camera->m_sensor_size = arclx::Vector2f(sensor_size_x, sensor_size_y);
}

//------------------------------------------------------------------------------
//                            CAMERA GET SENSOR OFFSET
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float const* Camera_get_sensor_offset(OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    CameraRenderable* camera = (CameraRenderable*) ptr->m_internals;
    return &camera->m_sensor_offset(0);
}

//------------------------------------------------------------------------------
//                            CAMERA SET SENSOR OFFSET
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Camera_set_sensor_offset(
        OMI_COMP_ComponentPtr ptr,
        float sensor_offset_x,
        float sensor_offset_y)
{
    // no error checking for performance

    CameraRenderable* camera = (CameraRenderable*) ptr->m_internals;
    camera->m_sensor_offset = arclx::Vector2f(sensor_offset_x, sensor_offset_y);
}

//------------------------------------------------------------------------------
//                                 GET NEAR CLIP
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float Camera_get_near_clip(OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    CameraRenderable* camera = (CameraRenderable*) ptr->m_internals;
    return camera->m_near_clip;
}

//------------------------------------------------------------------------------
//                                 SET NEAR CLIP
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Camera_set_near_clip(
        OMI_COMP_ComponentPtr ptr,
        float near_clip)
{
    // no error checking for performance

    CameraRenderable* camera = (CameraRenderable*) ptr->m_internals;
    camera->m_near_clip = near_clip;
}

//------------------------------------------------------------------------------
//                                  GET FAR CLIP
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float Camera_get_far_clip(OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    CameraRenderable* camera = (CameraRenderable*) ptr->m_internals;
    return camera->m_far_clip;
}

//------------------------------------------------------------------------------
//                                  SET FAR CLIP
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Camera_set_far_clip(
        OMI_COMP_ComponentPtr ptr,
        float far_clip)
{
    // no error checking for performance

    CameraRenderable* camera = (CameraRenderable*) ptr->m_internals;
    camera->m_far_clip = far_clip;
}

//------------------------------------------------------------------------------
//                              CAMERA GET TRANSFORM
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_COMP_ComponentPtr Camera_get_transform(
        OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    CameraRenderable* camera = (CameraRenderable*) ptr->m_internals;
    if(camera->m_transform != nullptr)
    {
        return camera->m_transform;
    }
    return TRANSFORM_DEFAULT.get_ptr();
}

//------------------------------------------------------------------------------
//                              CAMERA SET TRANSFORM
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Camera_set_transform(
        OMI_COMP_ComponentPtr ptr,
        OMI_COMP_ComponentPtr transform)
{
    // no error checking for performance

    CameraRenderable* camera = (CameraRenderable*) ptr->m_internals;

    if(camera->m_transform != nullptr)
    {
        omi::comp::Component_decrease_reference(camera->m_transform);
    }
    camera->m_transform = transform;
    if(camera->m_transform != nullptr)
    {
        omi::comp::Component_increase_reference(camera->m_transform);
    }
}

//------------------------------------------------------------------------------
//                        CAMERA GET DIAGONAL SENSOR SIZE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float Camera_get_diagonal_sensor_size(OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    CameraRenderable* camera = (CameraRenderable*) ptr->m_internals;
    return std::sqrt(
        (camera->m_sensor_size(0) * camera->m_sensor_size(0)) +
        (camera->m_sensor_size(1) * camera->m_sensor_size(1))
    );

}

//------------------------------------------------------------------------------
//                           CAMERA GET HORIZONTAL FOV
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float Camera_get_horizontal_fov(OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    CameraRenderable* camera = (CameraRenderable*) ptr->m_internals;
    return 2.0F * std::atan2(
        camera->m_sensor_size(0),
        (2.0F * camera->m_focal_length)
    );
}

//------------------------------------------------------------------------------
//                            CAMERA GET VERTICAL FOV
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float Camera_get_vertical_fov(OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    CameraRenderable* camera = (CameraRenderable*) ptr->m_internals;
    return 2.0F * std::atan2(
        camera->m_sensor_size(1),
        (2.0F * camera->m_focal_length)
    );
}

//------------------------------------------------------------------------------
//                            CAMERA GET DIAGONAL FOV
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float Camera_get_diagonal_fov(OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    CameraRenderable* camera = (CameraRenderable*) ptr->m_internals;
    return 2.0F * std::atan2(
        Camera_get_diagonal_sensor_size(ptr),
        (2.0F * camera->m_focal_length)
    );
}

//------------------------------------------------------------------------------
//                            CAMERA GET ASPECT RATIO
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float Camera_get_aspect_ratio(OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    CameraRenderable* camera = (CameraRenderable*) ptr->m_internals;
    return camera->m_sensor_size(0) / camera->m_sensor_size(1);
}

//------------------------------------------------------------------------------
//                              TEXTURE CONSTRUCTOR
//------------------------------------------------------------------------------

static void Texture_delete(void* internals)
{
    TextureRenderable* texture = (TextureRenderable*) internals;
    delete texture;
}

OMI_CODEGEN_FUNCTION
static OMI_COMP_ComponentPtr Texture_constructor(
        OMI_RES_ResourceId resource_id,
        char const* resource_name)
{
    // get the resource
    omi::Attribute resource = omi::res::Storage::instance().get(resource_id);
    // TODO: handle resource name

    TextureRenderable* texture = new TextureRenderable(resource);
    return new OMI_COMP_ComponentStruct(
        OMI_COMP_ComponentType_kRenderable,
        texture,
        &Texture_delete
    );
}

//------------------------------------------------------------------------------
//                                TEXTURE GET DATA
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_AttributePtr Texture_get_data(OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    TextureRenderable* texture = (TextureRenderable*) ptr->m_internals;
    return texture->m_data.get_ptr();
}

//------------------------------------------------------------------------------
//                              MATERIAL CONSTRUCTOR
//------------------------------------------------------------------------------

static void Material_delete(void* internals)
{
    MaterialRenderable* material = (MaterialRenderable*) internals;
    delete material;
}

OMI_CODEGEN_FUNCTION
static OMI_COMP_ComponentPtr Material_constructor(
        float const* albedo,
        float opacity,
        OMI_COMP_ComponentPtr texture)
{
    MaterialRenderable* material = new MaterialRenderable(
        albedo,
        opacity,
        texture
    );
    return new OMI_COMP_ComponentStruct(
        OMI_COMP_ComponentType_kRenderable,
        material,
        &Material_delete
    );
}

//------------------------------------------------------------------------------
//                                DEFAULT MATERIAL
//------------------------------------------------------------------------------

class DefaultMaterial
{
public:

    OMI_COMP_ComponentPtr ptr;

    DefaultMaterial()
        : ptr(nullptr)
    {
        arclx::Vector3f const albedo(0.85F, 0.85F, 0.85F);
        ptr = Material_constructor(
            &albedo(0),
            1.0F,
            nullptr
        );
    }

    ~DefaultMaterial()
    {
        if(ptr != nullptr)
        {
            omi::comp::Component_decrease_reference(ptr);
        }
    }
};
static DefaultMaterial MATERIAL_DEFAULT;

//------------------------------------------------------------------------------
//                              MATERIAL GET ALBEDO
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float const* Material_get_albedo(OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    MaterialRenderable* material = (MaterialRenderable*) ptr->m_internals;
    return &material->m_albedo(0);
}

//------------------------------------------------------------------------------
//                              MATERIAL GET OPACITY
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float Material_get_opacity(OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    MaterialRenderable* material = (MaterialRenderable*) ptr->m_internals;
    return material->m_opacity;
}

//------------------------------------------------------------------------------
//                              MATERIAL HAS TEXTURE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool Material_has_texture(OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    MaterialRenderable* material = (MaterialRenderable*) ptr->m_internals;
    return material->m_texture != nullptr;
}

//------------------------------------------------------------------------------
//                              MATERIAL GET TEXTURE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_COMP_ComponentPtr Material_get_texture(
        OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    MaterialRenderable* material = (MaterialRenderable*) ptr->m_internals;
    return material->m_texture;
}

//------------------------------------------------------------------------------
//                                MESH CONSTRUCTOR
//------------------------------------------------------------------------------

static void Mesh_delete(void* internals)
{
    MeshRenderable* mesh = (MeshRenderable*) internals;
    delete mesh;
}

OMI_CODEGEN_FUNCTION
static OMI_COMP_ComponentPtr Mesh_constructor(
        OMI_RES_ResourceId resource_id,
        char const* resource_name,
        OMI_COMP_ComponentPtr transform,
        OMI_COMP_ComponentPtr material,
        OMI_Bool cull_backfaces)
{
    // get the resource
    omi::Attribute resource = omi::res::Storage::instance().get(resource_id);
    // TODO: handle resource name

    MeshRenderable* mesh = new MeshRenderable(
        resource,
        transform,
        material,
        cull_backfaces
    );
    return new OMI_COMP_ComponentStruct(
        OMI_COMP_ComponentType_kRenderable,
        mesh,
        &Mesh_delete
    );
}

//------------------------------------------------------------------------------
//                                 MESH GET DATA
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_AttributePtr Mesh_get_data(OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    MeshRenderable* mesh = (MeshRenderable*) ptr->m_internals;
    return mesh->m_data.get_ptr();
}

//------------------------------------------------------------------------------
//                               MESH HAS TRANSFORM
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool Mesh_has_transform(OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    MeshRenderable* mesh = (MeshRenderable*) ptr->m_internals;
    return mesh->m_transform != nullptr;
}

//------------------------------------------------------------------------------
//                               MESH GET TRANSFORM
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_COMP_ComponentPtr Mesh_get_transform(OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    MeshRenderable* mesh = (MeshRenderable*) ptr->m_internals;
    if(mesh->m_transform != nullptr)
    {
        return mesh->m_transform;
    }
    return TRANSFORM_DEFAULT.get_ptr();
}

//------------------------------------------------------------------------------
//                               MESH HAS MATERIAL
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool Mesh_has_material(OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    MeshRenderable* mesh = (MeshRenderable*) ptr->m_internals;
    return mesh->m_material != nullptr;
}

//------------------------------------------------------------------------------
//                               MESH GET MATERIAL
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_COMP_ComponentPtr Mesh_get_material(
        OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    MeshRenderable* mesh = (MeshRenderable*) ptr->m_internals;
    if(mesh->m_material != nullptr)
    {
        return mesh->m_material;
    }
    return MATERIAL_DEFAULT.ptr;
}

//------------------------------------------------------------------------------
//                            MESH GET CULL BACKFACES
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool Mesh_get_cull_backfaces(OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    MeshRenderable* mesh = (MeshRenderable*) ptr->m_internals;
    return mesh->m_cull_backfaces;
}

//------------------------------------------------------------------------------
//                               TEXT2D CONSTRUCTOR
//------------------------------------------------------------------------------

static void Text2D_delete(void* internals)
{
    Text2DRenderable* text2d = (Text2DRenderable*) internals;
    delete text2d;
}

OMI_CODEGEN_FUNCTION
static OMI_COMP_ComponentPtr Text2D_constructor(
        char const* text,
        OMI_RES_ResourceId font_resource,
        float const* colour,
        float size,
        float const* position,
        OMI_COMP_Text2D_HorizontalOrigin horizontal_origin,
        OMI_COMP_Text2D_VerticalOrigin vertical_origin)
{
    // get the resource
    omi::Attribute resource = omi::res::Storage::instance().get(font_resource);

    Text2DRenderable* text2d = new Text2DRenderable(
        text,
        resource,
        colour,
        size,
        position,
        horizontal_origin,
        vertical_origin
    );
    return new OMI_COMP_ComponentStruct(
        OMI_COMP_ComponentType_kRenderable,
        text2d,
        &Text2D_delete
    );
}

//------------------------------------------------------------------------------
//                                TEXT2D GET TEXT
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static char const* Text2D_get_text(OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    Text2DRenderable* text2d = (Text2DRenderable*) ptr->m_internals;
    return text2d->m_text.c_str();
}

//------------------------------------------------------------------------------
//                                TEXT2D SET TEXT
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Text2D_set_text(
        OMI_COMP_ComponentPtrConst ptr,
        char const* text)
{
    // no error checking for performance

    Text2DRenderable* text2d = (Text2DRenderable*) ptr->m_internals;
    text2d->m_text = text;
}

//------------------------------------------------------------------------------
//                            TEXT2D GET FONT RESOURCE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_AttributePtr Text2D_get_font_resource(OMI_COMP_ComponentPtrConst ptr)
{
    // no error checking for performance

    Text2DRenderable* text2d = (Text2DRenderable*) ptr->m_internals;
    return text2d->m_font_resource.get_ptr();
}

//------------------------------------------------------------------------------
//                               TEXT2D GET COLOUR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float const* Text2D_get_colour(OMI_COMP_ComponentPtr ptr)
{
    // no error checking for performance

    Text2DRenderable* text2d = (Text2DRenderable*) ptr->m_internals;
    return &text2d->m_colour(0);
}

//------------------------------------------------------------------------------
//                               TEXT2D SET COLOUR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Text2D_set_colour(OMI_COMP_ComponentPtr ptr, float const* colour)
{
    // no error checking for performance

    Text2DRenderable* text2d = (Text2DRenderable*) ptr->m_internals;
    text2d->m_colour = *reinterpret_cast<arclx::Vector4f const*>(colour);
}

//------------------------------------------------------------------------------
//                                TEXT2D GET SIZE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float Text2D_get_size(OMI_COMP_ComponentPtr ptr)
{
    // no error checking for performance

    Text2DRenderable* text2d = (Text2DRenderable*) ptr->m_internals;
    return text2d->m_size;
}

//------------------------------------------------------------------------------
//                                TEXT2D SET SIZE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Text2D_set_size(OMI_COMP_ComponentPtr ptr, float size)
{
    // no error checking for performance

    Text2DRenderable* text2d = (Text2DRenderable*) ptr->m_internals;
    text2d->m_size = size;
}

//------------------------------------------------------------------------------
//                              TEXT2D GET POSITION
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float const* Text2D_get_position(OMI_COMP_ComponentPtr ptr)
{
    // no error checking for performance

    Text2DRenderable* text2d = (Text2DRenderable*) ptr->m_internals;
    return &text2d->m_position(0);
}

//------------------------------------------------------------------------------
//                              TEXT2D SET POSITION
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Text2D_set_position(
        OMI_COMP_ComponentPtr ptr,
        float const* position)
{
    // no error checking for performance

    Text2DRenderable* text2d = (Text2DRenderable*) ptr->m_internals;
    text2d->m_position = *reinterpret_cast<arclx::Vector2f const*>(position);
}

//------------------------------------------------------------------------------
//                          TEXT2D GET HORIZONTAL ORIGIN
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_COMP_Text2D_HorizontalOrigin Text2D_get_horizontal_origin(
        OMI_COMP_ComponentPtr ptr)
{
    // no error checking for performance

    Text2DRenderable* text2d = (Text2DRenderable*) ptr->m_internals;
    return text2d->m_horizontal_origin;
}

//------------------------------------------------------------------------------
//                          TEXT2D SET HORIZONTAL ORIGIN
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Text2D_set_horizontal_origin(
        OMI_COMP_ComponentPtr ptr,
        OMI_COMP_Text2D_HorizontalOrigin horizontal_origin)
{
    // no error checking for performance

    Text2DRenderable* text2d = (Text2DRenderable*) ptr->m_internals;
    text2d->m_horizontal_origin = horizontal_origin;
}

//------------------------------------------------------------------------------
//                           TEXT2D GET VERTICAL ORIGIN
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_COMP_Text2D_VerticalOrigin Text2D_get_vertical_origin(
        OMI_COMP_ComponentPtr ptr)
{
    // no error checking for performance

    Text2DRenderable* text2d = (Text2DRenderable*) ptr->m_internals;
    return text2d->m_vertical_origin;
}

//------------------------------------------------------------------------------
//                           TEXT2D SET VERTICAL ORIGIN
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Text2D_set_vertical_origin(
        OMI_COMP_ComponentPtr ptr,
        OMI_COMP_Text2D_VerticalOrigin vertical_origin)
{
    // no error checking for performance

    Text2DRenderable* text2d = (Text2DRenderable*) ptr->m_internals;
    text2d->m_vertical_origin = vertical_origin;
}

} // namespace anonymous
} // namespace comp
} // namespace omi

#include "omicron/api/comp/renderable/_codegen/RenderableBinding.inl"
