/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/comp/renderable/Text2D.hpp"


namespace omi
{
namespace comp
{

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

Text2D::Text2D(
        std::string const& text,
        omi::res::ResourceId font_resource,
        arclx::Vector4f const& colour,
        float size,
        arclx::Vector2f const& position,
        HorizontalOrigin horizontal_origin,
        VerticalOrigin vertical_origin)
    : Renderable(get_renderable_interface().Text2D_constructor(
        text.c_str(),
        font_resource,
        &colour(0),
        size,
        &position(0),
        static_cast<OMI_COMP_Text2D_HorizontalOrigin>(horizontal_origin),
        static_cast<OMI_COMP_Text2D_VerticalOrigin>(vertical_origin)
    ), false)
{
}

Text2D::Text2D(Component const& other)
    : Renderable(other)
{
}

Text2D::Text2D(Component&& other)
    : Renderable(std::move(other))
{
}

Text2D::Text2D(OMI_COMP_ComponentPtr ptr, bool increase_ref)
    : Renderable(ptr, increase_ref)
{
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

Text2D::~Text2D()
{
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

bool Text2D::is_valid() const
{
    return get_renderable_interface().Renderable_is_valid(
        m_ptr,
        OMI_COMP_RenderableType_kText2D
    ) != OMI_False;
}

std::string_view Text2D::get_text() const
{
    return std::string_view(get_renderable_interface().Text2D_get_text(m_ptr));
}

void Text2D::set_text(std::string const& text)
{
    get_renderable_interface().Text2D_set_text(m_ptr, text.c_str());
}

omi::Attribute Text2D::get_font_resource() const
{
    return omi::Attribute(
        get_renderable_interface().Text2D_get_font_resource(m_ptr),
        true
    );
}

arclx::Vector4f const& Text2D::get_colour() const
{
    return *reinterpret_cast<arclx::Vector4f const*>(
        get_renderable_interface().Text2D_get_colour(m_ptr)
    );
}

void Text2D::set_colour(arclx::Vector4f const& colour)
{
    get_renderable_interface().Text2D_set_colour(m_ptr, &colour(0));
}

float Text2D::get_size() const
{
    return get_renderable_interface().Text2D_get_size(m_ptr);
}

void Text2D::set_size(float size)
{
    get_renderable_interface().Text2D_set_size(m_ptr, size);
}

arclx::Vector2f const& Text2D::get_position() const
{
    return *reinterpret_cast<arclx::Vector2f const*>(
        get_renderable_interface().Text2D_get_position(m_ptr)
    );
}

void Text2D::set_position(arclx::Vector2f const& position)
{
    get_renderable_interface().Text2D_set_position(m_ptr, &position(0));
}

Text2D::HorizontalOrigin Text2D::get_horizontal_origin() const
{
    return static_cast<HorizontalOrigin>(
        get_renderable_interface().Text2D_get_horizontal_origin(m_ptr)
    );
}

void Text2D::set_horizontal_origin(HorizontalOrigin horizontal_origin)
{
    get_renderable_interface().Text2D_set_horizontal_origin(
        m_ptr,
        static_cast<OMI_COMP_Text2D_HorizontalOrigin>(horizontal_origin)
    );
}

Text2D::VerticalOrigin Text2D::get_vertical_origin() const
{
    return static_cast<VerticalOrigin>(
        get_renderable_interface().Text2D_get_vertical_origin(m_ptr)
    );
}

void Text2D::set_vertical_origin(VerticalOrigin vertical_origin)
{
    get_renderable_interface().Text2D_set_vertical_origin(
        m_ptr,
        static_cast<OMI_COMP_Text2D_VerticalOrigin>(vertical_origin)
    );
}

} // namespace comp
} // namespace omi
