/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_COMP_RENDERABLE_CAMERA_HPP_
#define OMICRON_API_COMP_RENDERABLE_CAMERA_HPP_

#include <arcanecore/lx/Vector.hpp>

#include "omicron/api/comp/renderable/Material.hpp"
#include "omicron/api/comp/renderable/Renderable.hpp"
#include "omicron/api/comp/transform/Transform.hpp"


namespace omi
{
namespace comp
{

class Camera
    : public Renderable
{
public:

    //--------------------------------------------------------------------------
    //                                CONSTRUCTORS
    //--------------------------------------------------------------------------

    /*!
     * \brief Constructs a camera from the given physical parameters.
     *
     * \param focal_length The optical distance (in mm) from the point where the
     *                     light converges to the camera's sensor.
     * \param sensor_size The width and height (in mm) of the camera's sensor.
     * \param sensor_offset The offset (in mm) on the camera's imaging plane of
     *                      the sensor.
     * \param near_clip The nearest distance from the camera that objects will
     *                  be visible.
     * \param far_clip The furtherest distance from the camera that objects will
     *                  be visible.
     * \param transform The transform controlling the position of this camera.
     */
    Camera(
            float focal_length,
            arclx::Vector2f const& sensor_size,
            arclx::Vector2f const& sensor_offset,
            float near_clip,
            float far_clip,
            OMI_COMP_ComponentPtr transform);

    Camera(Component const& other);

    Camera(Component&& other);

    Camera(OMI_COMP_ComponentPtr ptr, bool increase_ref=true);

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    virtual ~Camera();

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    virtual bool is_valid() const override;

    float get_focal_length() const;

    void set_focal_length(float focal_length);

    arclx::Vector2f const& get_sensor_size() const;

    void set_sensor_size(arclx::Vector2f const& sensor_size);

    arclx::Vector2f const& get_sensor_offset() const;

    void set_sensor_offset(arclx::Vector2f const& sensor_offset);

    float get_near_clip() const;

    void set_near_clip(float near_clip);

    float get_far_clip() const;

    void set_far_clip(float far_clip);

    Transform get_transform() const;

    void set_transform(Transform const& transform);

    float get_diagonal_sensor_size() const;

    float get_horizontal_fov() const;

    float get_vertical_fov() const;

    float get_diagonal_fov() const;

    float get_aspect_ratio() const;
};

} // namespace comp
} // namespace omi

//------------------------------------------------------------------------------
//                                      HASH
//------------------------------------------------------------------------------

namespace std
{

template<>
struct hash<omi::comp::Camera> :
    public unary_function<omi::comp::Camera, size_t>
{
    std::size_t operator()(const omi::comp::Camera& component) const
    {
        return static_cast<std::size_t>(component.get_id());
    }
};

} // namespace std

#endif
