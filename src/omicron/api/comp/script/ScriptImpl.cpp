/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstddef>
#include <cstring>
#include <string>

#include "omicron/api/API.h"
#include "omicron/api/comp/ComponentImpl.hpp"
#include "omicron/api/comp/script/ScriptCTypes.h"


OMI_CODEGEN_NAMESPACE namespace omi
{
OMI_CODEGEN_NAMESPACE namespace comp
{
namespace
{

//------------------------------------------------------------------------------
//                                    STRUCTS
//------------------------------------------------------------------------------

struct ScriptData
{
    //---------------------------A T T R I B U T E S----------------------------

    std::string m_script_name;
    OMI_SCENE_EntityScriptPtr m_entity_script;
    DestroyEntityScriptFunc* m_destory_func;

    //--------------------------C O N S T R U C T O R---------------------------

    ScriptData(char const* script_name)
        : m_script_name  (script_name)
        , m_entity_script(nullptr)
        , m_destory_func (nullptr)
    {
    }

    //---------------------------D E S T R U C T O R----------------------------

    virtual ~ScriptData()
    {
    }
};

//------------------------------------------------------------------------------
//                                    GLOBALS
//------------------------------------------------------------------------------

static std::string g_error_message;

static CreateEntityScriptFunc* g_create_entity_script_func = nullptr;

//------------------------------------------------------------------------------
//                               GET ERROR MESSAGE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static char const* get_error_message()
{
    return g_error_message.c_str();
}

//------------------------------------------------------------------------------
//                       SET CREATE ENTITY SCRIPT FUNCTION
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void set_create_entity_script_func(CreateEntityScriptFunc* func)
{
    g_create_entity_script_func = func;
}

//------------------------------------------------------------------------------
//                               SCRIPT CONSTRUCTOR
//------------------------------------------------------------------------------

static void Script_delete(void* internals)
{
    ScriptData* script = (ScriptData*) internals;
    script->m_destory_func(script->m_entity_script);
    delete script;
}

OMI_CODEGEN_FUNCTION
static OMI_COMP_ComponentPtr Script_constructor(char const* script_name)
{
    // TODO: actually make the script object?????

    ScriptData* script = new ScriptData(script_name);
    return new OMI_COMP_ComponentStruct(
        OMI_COMP_ComponentType_kScript,
        script,
        &Script_delete
    );
}

//------------------------------------------------------------------------------
//                            SCRIPT CALL CONSTRUCTOR
//------------------------------------------------------------------------------

// TODO: could be moved to SceneImpl?
OMI_CODEGEN_FUNCTION
static OMI_COMP_Script_Error Script_call_constructor(
        OMI_COMP_ComponentPtr ptr,
        OMI_SCENE_EntityId entity_id,
        char const* entitiy_name,
        OMI_AttributePtr data)
{
    ScriptData* script = (ScriptData*) ptr->m_internals;
    try
    {
        script->m_entity_script = g_create_entity_script_func(
            script->m_script_name.c_str(),
            entity_id,
            entitiy_name,
            data,
            &script->m_destory_func
        );
    }
    catch(std::exception const& exc)
    {
        g_error_message = exc.what();
        return OMI_COMP_Script_Error_kRuntime;
    }

    if(script->m_entity_script == nullptr)
    {
        g_error_message =
            "Cannot create entity script with name \"" + script->m_script_name +
            "\" as no entity script with this name is registered";
        return OMI_COMP_Script_Error_kKey;
    }

    return OMI_COMP_Script_Error_kNone;
}

//------------------------------------------------------------------------------
//                             GET ENTITY SCRIPT PTR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_SCENE_EntityScriptPtr Script_get_entity_script_ptr(
        OMI_COMP_ComponentPtr ptr)
{
    ScriptData* script = (ScriptData*) ptr->m_internals;
    return script->m_entity_script;
}

} // namespace anonymous
} // namespace comp
} // namespace omi

#include "omicron/api/comp/script/_codegen/ScriptBinding.inl"
