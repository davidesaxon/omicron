/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#include <cstring>

#include "omicron/api/API.h"

#include "omicron/api/comp/script/_codegen/ScriptCSymbols.h"


#ifdef __cplusplus
extern "C" {
#endif

OMI_API_EXPORT int OMI_COMP_Script_BINDING_IMPL(char const* func_def, void** func_ptr)
{
    if(strcmp(func_def, OMI_COMP_Script_get_error_message_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::get_error_message;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Script_set_create_entity_script_func_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::set_create_entity_script_func;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Script_Script_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Script_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Script_Script_call_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Script_call_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Script_Script_get_entity_script_ptr_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Script_get_entity_script_ptr;
        return 0;
    }

    return 1;
}

#ifdef __cplusplus
} // extern "C"
#endif

