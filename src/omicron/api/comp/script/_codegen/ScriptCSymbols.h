/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_COMP_CODEGEN_SCRIPTCSMYBOLS_H_
#define OMI_COMP_CODEGEN_SCRIPTCSMYBOLS_H_


#ifdef __cplusplus
extern "C" {
#endif

// OMI_COMP_get_error_message
static char const* OMI_COMP_Script_get_error_message_SYMBOL = "char const* OMI_COMP_Script_get_error_message()";

// OMI_COMP_set_create_entity_script_func
static char const* OMI_COMP_Script_set_create_entity_script_func_SYMBOL = "void OMI_COMP_Script_set_create_entity_script_func(CreateEntityScriptFunc*)";

// OMI_COMP_Script_constructor
static char const* OMI_COMP_Script_Script_constructor_SYMBOL = "OMI_COMP_ComponentPtr OMI_COMP_Script_Script_constructor(char const*)";

// OMI_COMP_Script_call_constructor
static char const* OMI_COMP_Script_Script_call_constructor_SYMBOL = "OMI_COMP_Script_Error OMI_COMP_Script_Script_call_constructor(OMI_COMP_ComponentPtr, OMI_SCENE_EntityId, char const*, OMI_AttributePtr)";

// OMI_COMP_Script_get_entity_script_ptr
static char const* OMI_COMP_Script_Script_get_entity_script_ptr_SYMBOL = "OMI_SCENE_EntityScriptPtr OMI_COMP_Script_Script_get_entity_script_ptr(OMI_COMP_ComponentPtr)";

#ifdef __cplusplus
} // extern "C"
#endif

#endif