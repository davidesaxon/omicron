/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_COMP_CODEGEN_SCRIPTINTERFACE_HPP_
#define OMI_COMP_CODEGEN_SCRIPTINTERFACE_HPP_

#include <cassert>
#include <string>

#include <arcanecore/base/dl/DLOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>
#include <arcanecore/base/lang/Restrictors.hpp>

#include "omicron/api/comp/script/_codegen/ScriptCSymbols.h"


namespace omi
{
namespace comp
{

class ScriptInterface
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                              TYPE DEFINITIONS
    //--------------------------------------------------------------------------

    typedef OMI_Bool (BindingFunc)(char const*, void**);

    //--------------------------------------------------------------------------
    //                             PUBLIC ATTRIBUTES
    //--------------------------------------------------------------------------

    // get_error_message
    char const* (*get_error_message)();
    // set_create_entity_script_func
    void (*set_create_entity_script_func)(CreateEntityScriptFunc*);
    // Script_constructor
    OMI_COMP_ComponentPtr (*Script_constructor)(char const*);
    // Script_call_constructor
    OMI_COMP_Script_Error (*Script_call_constructor)(OMI_COMP_ComponentPtr, OMI_SCENE_EntityId, char const*, OMI_AttributePtr);
    // Script_get_entity_script_ptr
    OMI_SCENE_EntityScriptPtr (*Script_get_entity_script_ptr)(OMI_COMP_ComponentPtr);

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    ScriptInterface(std::string const& libname)
        : get_error_message(nullptr)
        , set_create_entity_script_func(nullptr)
        , Script_constructor(nullptr)
        , Script_call_constructor(nullptr)
        , Script_get_entity_script_ptr(nullptr)
    {
        #ifdef ARC_OS_WINDOWS
            arc::fsys::Path libpath({
                "build",
                "windows",
                "Release",
                libname + "_impl.dll"
            });
        #elif defined(ARC_OS_UNIX)
            arc::fsys::Path libpath({
                "build",
                "linux",
                "lib" + libname + "_impl.so"
            });
        #else
            assert(false);
        #endif

        arc::dl::Handle lib_handle = arc::dl::open_library(libpath);
        BindingFunc* binding_func = arc::dl::bind_symbol<BindingFunc>(lib_handle, "OMI_COMP_Script_BINDING_IMPL");

        if(binding_func(OMI_COMP_Script_get_error_message_SYMBOL, (void**) &get_error_message) != 0)
        {
        }
        if(binding_func(OMI_COMP_Script_set_create_entity_script_func_SYMBOL, (void**) &set_create_entity_script_func) != 0)
        {
        }
        if(binding_func(OMI_COMP_Script_Script_constructor_SYMBOL, (void**) &Script_constructor) != 0)
        {
        }
        if(binding_func(OMI_COMP_Script_Script_call_constructor_SYMBOL, (void**) &Script_call_constructor) != 0)
        {
        }
        if(binding_func(OMI_COMP_Script_Script_get_entity_script_ptr_SYMBOL, (void**) &Script_get_entity_script_ptr) != 0)
        {
        }
    }
};

} // namespace omi
} // namespace comp


#endif