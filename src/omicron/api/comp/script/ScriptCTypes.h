/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_COMP_SCRIPT_SCRIPTCTYPES_H_
#define OMICRON_API_COMP_SCRIPT_SCRIPTCTYPES_H_

#include <stdint.h>

#include "omicron/api/common/attribute/AttributeCTypes.h"
#include "omicron/api/scene/SceneCTypes.h"


#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
//                             CREATE SCRIPT TYPEDEFS
//------------------------------------------------------------------------------

typedef void (DestroyEntityScriptFunc)(OMI_SCENE_EntityScriptPtr);
typedef OMI_SCENE_EntityScriptPtr (CreateEntityScriptFunc)(
        char const*,
        OMI_SCENE_EntityId,
        char const*,
        OMI_AttributePtr,
        DestroyEntityScriptFunc**);

//------------------------------------------------------------------------------
//                                  ERROR CODES
//------------------------------------------------------------------------------

typedef uint32_t OMI_COMP_Script_Error;
#define OMI_COMP_Script_Error_kNone    0
#define OMI_COMP_Script_Error_kRuntime 1
#define OMI_COMP_Script_Error_kInvalid 2
#define OMI_COMP_Script_Error_kKey 3

#ifdef __cplusplus
}
#endif

#endif
