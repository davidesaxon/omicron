/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/comp/Component.hpp"


namespace omi
{
namespace comp
{

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

Component::Component(Component const& other)
    : m_ptr(other.m_ptr)
{
    get_interface().Component_increase_reference(m_ptr);
}

Component::Component(Component&& other)
    : m_ptr(other.m_ptr)
{
    other.m_ptr = nullptr;
}

Component::Component(OMI_COMP_ComponentPtr ptr, bool increase_ref)
    : m_ptr(ptr)
{
    if(increase_ref)
    {
        get_interface().Component_increase_reference(m_ptr);
    }
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

Component::~Component()
{
    get_interface().Component_decrease_reference(m_ptr);
}

//------------------------------------------------------------------------------
//                                   OPERATORS
//------------------------------------------------------------------------------

Component& Component::operator=(Component const& other)
{
    if(m_ptr != other.m_ptr)
    {
        get_interface().Component_decrease_reference(m_ptr);
        m_ptr = other.m_ptr;
        get_interface().Component_increase_reference(m_ptr);
    }
    return *this;
}

Component& Component::operator=(Component&& other)
{
    get_interface().Component_decrease_reference(m_ptr);
    m_ptr = other.m_ptr;
    other.m_ptr = nullptr;
    return *this;
}

bool Component::operator==(Component const& other) const
{
    return get_id() == other.get_id();
}

bool Component::operator!=(Component const& other) const
{
    return get_id() != other.get_id();
}

//------------------------------------------------------------------------------
//                            PUBLIC STATIC FUNCTIONS
//------------------------------------------------------------------------------

ComponentInterface& Component::get_interface()
{
    static ComponentInterface inst("omicron_api");
    return inst;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

OMI_COMP_ComponentPtr Component::get_ptr() const
{
    return m_ptr;
}

bool Component::is_valid() const
{
    return get_interface().is_valid(
        m_ptr,
        OMI_COMP_ComponentType_kTrivial
    ) != OMI_False;
}

Component::Id Component::get_id() const
{
    return static_cast<Component::Id>(get_interface().get_id(m_ptr));
}

ComponentType Component::get_component_type() const
{
    return static_cast<ComponentType>(
        get_interface().get_component_type(m_ptr)
    );
}

} // namespace comp
} // namespace omi
