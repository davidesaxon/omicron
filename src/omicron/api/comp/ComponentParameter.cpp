/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/comp/ComponentParameter.hpp"
#include "omicron/api/comp/ComponentSchema.hpp"


namespace omi
{
namespace comp
{

//------------------------------------------------------------------------------
//                                    GLOBALS
//------------------------------------------------------------------------------

std::size_t const ComponentParameter::NULL_SIZE =
    OMI_COMP_ComponentParameterNullSize;

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

ComponentParameter::ComponentParameter(ComponentParameter const& other)
    : m_ptr(other.m_ptr)
{
}

ComponentParameter::ComponentParameter(ComponentParameter&& other)
    : m_ptr(other.m_ptr)
{
    other.m_ptr = nullptr;
}

ComponentParameter::ComponentParameter(OMI_COMP_ComponentParameterPtr ptr)
    : m_ptr(ptr)
{
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

ComponentParameter::~ComponentParameter()
{
    // component parameters are deleted internally
}

//------------------------------------------------------------------------------
//                                   OPERATORS
//------------------------------------------------------------------------------

ComponentParameter& ComponentParameter::operator=(
        ComponentParameter const& other)
{
    m_ptr = other.m_ptr;
    return *this;
}

ComponentParameter& ComponentParameter::operator=(ComponentParameter&& other)
{
    m_ptr = other.m_ptr;
    other.m_ptr = nullptr;
    return *this;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

OMI_COMP_ComponentParameterPtr ComponentParameter::get_ptr() const
{
    return m_ptr;
}

std::string_view ComponentParameter::get_name() const
{
    return std::string_view(
        ComponentSchema::get_interface().ComponentParameter_get_name(m_ptr)
    );
}

std::string_view ComponentParameter::get_type() const
{
    return std::string_view(
        ComponentSchema::get_interface().ComponentParameter_get_type(m_ptr)
    );
}

std::size_t ComponentParameter::get_size() const
{
    return static_cast<std::size_t>(
        ComponentSchema::get_interface().ComponentParameter_get_size(m_ptr)
    );
}

omi::Attribute ComponentParameter::get_default(std::string const& name) const
{
    return omi::Attribute(
        ComponentSchema::get_interface().ComponentParameter_get_default(m_ptr)
    );
}

} // namespace comp
} // namespace omi
