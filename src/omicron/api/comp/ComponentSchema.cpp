/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/comp/ComponentSchema.hpp"


namespace omi
{
namespace comp
{

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

ComponentSchema::ComponentSchema(ComponentSchema const& other)
    : m_ptr(other.m_ptr)
{
    get_interface().ComponentSchema_increase_reference(m_ptr);
}

ComponentSchema::ComponentSchema(ComponentSchema&& other)
    : m_ptr(other.m_ptr)
{
    other.m_ptr = nullptr;
}

ComponentSchema::ComponentSchema(
        OMI_COMP_ComponentSchemaPtr ptr,
        bool increase_ref)
    : m_ptr(ptr)
{
    if(increase_ref)
    {
        get_interface().ComponentSchema_increase_reference(m_ptr);
    }
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

ComponentSchema::~ComponentSchema()
{
    get_interface().ComponentSchema_decrease_reference(m_ptr);
}

//------------------------------------------------------------------------------
//                                   OPERATORS
//------------------------------------------------------------------------------

ComponentSchema& ComponentSchema::operator=(ComponentSchema const& other)
{
    if(m_ptr != other.m_ptr)
    {
        get_interface().ComponentSchema_decrease_reference(m_ptr);
        m_ptr = other.m_ptr;
        get_interface().ComponentSchema_increase_reference(m_ptr);
    }
    return *this;
}

ComponentSchema& ComponentSchema::operator=(ComponentSchema&& other)
{
    get_interface().ComponentSchema_decrease_reference(m_ptr);
    m_ptr = other.m_ptr;
    other.m_ptr = nullptr;
    return *this;
}

//------------------------------------------------------------------------------
//                            PUBLIC STATIC FUNCTIONS
//------------------------------------------------------------------------------

ComponentSchemaInterface& ComponentSchema::get_interface()
{
    static ComponentSchemaInterface inst("omicron_api");
    return inst;
}

ComponentSchema ComponentSchema::get_schema(std::string const& name)
{
    OMI_COMP_ComponentSchemaPtr schema = nullptr;
    OMI_COMP_Component_Error ec = get_interface().get_schema(
        name.c_str(),
        &schema
    );

    if(ec == OMI_COMP_Component_Error_kKey)
    {
        throw arc::ex::KeyError(
            "No component schema with name \"" + name + "\""
        );
    }

    return ComponentSchema(schema, true);
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

OMI_COMP_ComponentSchemaPtr ComponentSchema::get_ptr() const
{
    return m_ptr;
}

std::string_view ComponentSchema::get_name() const
{
    return std::string_view(get_interface().ComponentSchema_get_name(m_ptr));
}

std::vector<char const*> ComponentSchema::get_parameter_names() const
{
    char const* const* parameter_names = nullptr;
    OMI_Size size = get_interface().ComponentSchema_get_parameter_names(
        m_ptr,
        &parameter_names
    );

    std::vector<char const*> ret;
    ret.reserve(size);
    for(OMI_Size i = 0; i < size; ++i)
    {
        ret.push_back(parameter_names[i]);
    }

    get_interface().ComponentSchema_delete_parameter_names(parameter_names);

    return ret;
}

bool ComponentSchema::has_parameter(std::string const& name) const
{
    return get_interface().ComponentSchema_has_parameter(
        m_ptr,
        name.c_str()
    ) != OMI_False;
}

ComponentParameter ComponentSchema::get_parameter(std::string const& name) const
{
    OMI_COMP_ComponentParameterPtr parameter = nullptr;
    OMI_COMP_Component_Error ec =
        get_interface().ComponentSchema_get_parameter(
            m_ptr,
            name.c_str(),
            &parameter
        );

    if(ec == OMI_COMP_Component_Error_kKey)
    {
        throw arc::ex::KeyError(
            "No parameter in schema \"" + std::to_string(get_name()) +
            "\"with name \"" + name + "\""
        );
    }

    return ComponentParameter(parameter);
}

} // namespace comp
} // namespace omi
