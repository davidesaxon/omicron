/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/comp/transform/Transform.hpp"


namespace omi
{
namespace comp
{

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

Transform::Transform(Component const& other)
    : Component(other)
{
}

Transform::Transform(Component&& other)
    : Component(std::move(other))
{
}

Transform::Transform(OMI_COMP_ComponentPtr ptr, bool increase_ref)
    : Component(ptr, increase_ref)
{
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

Transform::~Transform()
{
}

//------------------------------------------------------------------------------
//                            PUBLIC STATIC FUNCTIONS
//------------------------------------------------------------------------------

TransformInterface& Transform::get_transform_interface()
{
    static TransformInterface inst("omicron_api");
    return inst;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

bool Transform::is_valid() const
{
    return get_interface().is_valid(
        m_ptr,
        OMI_COMP_ComponentType_kTransform
    ) != OMI_False;
}

TransformType Transform::get_transform_type() const
{
    OMI_COMP_TransformType c_type;
    OMI_COMP_Transform_Error ec =
        get_transform_interface().get_transform_type(m_ptr, &c_type);
    if(ec == OMI_COMP_Transform_Error_kInvalid)
    {
        throw arc::ex::ValueError(
            "Cannot retrieve transform type from invalid Transform component"
        );
    }

    return static_cast<TransformType>(c_type);
}

void Transform::set_constraint(
            TransformConstraint constraint,
            Transform const& constrained_to)
{
    OMI_COMP_Transform_Error ec = get_transform_interface().set_constraint(
        m_ptr,
        static_cast<OMI_COMP_TransformConstraint>(constraint),
        constrained_to.get_ptr()
    );
    if(ec == OMI_COMP_Transform_Error_kInvalid)
    {
        throw arc::ex::ValueError(
            "Cannot set constraint using an invalid transform"
        );
    }

}

arclx::Matrix44f Transform::eval() const
{
    arclx::Matrix44f ret;
    get_transform_interface().eval(m_ptr, &ret(0, 0));
    return ret;
}

} // namespace comp
} // namespace omi
