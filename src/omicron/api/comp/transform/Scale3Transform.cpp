/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/comp/transform/Scale3Transform.hpp"


namespace omi
{
namespace comp
{

//------------------------------------------------------------------------------
//                                    GLOBALS
//------------------------------------------------------------------------------

namespace
{

static arclx::Vector3f const g_one_scale(1.0F, 1.0F, 1.0F);

} // namespace anonymous

//------------------------------------------------------------------------------
//                                  CONSTRUCTOR
//------------------------------------------------------------------------------

Scale3Transform::Scale3Transform(
        arclx::Vector3f const& scale,
        TransformConstraint constraint,
        OMI_COMP_ComponentPtr constrained_to)
    : Transform(get_transform_interface().Scale3Transform_constructor(
        &scale(0),
        static_cast<OMI_COMP_TransformConstraint>(constraint),
        constrained_to
    ), false)
{
}

Scale3Transform::Scale3Transform(Component const& other)
    : Transform(other)
{
}

Scale3Transform::Scale3Transform(Component&& other)
    : Transform(std::move(other))
{
}

Scale3Transform::Scale3Transform(OMI_COMP_ComponentPtr ptr, bool increase_ref)
    : Transform(ptr, increase_ref)
{
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

Scale3Transform::~Scale3Transform()
{
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

bool Scale3Transform::is_valid() const
{
    return get_transform_interface().Transform_is_valid(
        m_ptr,
        OMI_COMP_TransformType_kScale3
    ) != OMI_False;
}

arclx::Vector3f const& Scale3Transform::scale() const
{
    return *reinterpret_cast<arclx::Vector3f*>(
        get_transform_interface().Scale3Transform_scale(m_ptr)
    );
}

arclx::Vector3f& Scale3Transform::scale()
{
    return *reinterpret_cast<arclx::Vector3f*>(
        get_transform_interface().Scale3Transform_scale(m_ptr)
    );
}

} // namespace comp
} // namespace omi
