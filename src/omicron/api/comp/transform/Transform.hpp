/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_COMP_TRANSFORM_TRANSFORM_HPP_
#define OMICRON_API_COMP_TRANSFORM_TRANSFORM_HPP_

#include <arcanecore/lx/Matrix.hpp>

#include "omicron/api/comp/Component.hpp"
#include "omicron/api/comp/transform/TransformCTypes.h"
#include "omicron/api/comp/transform/_codegen/TransformInterface.hpp"


namespace omi
{
namespace comp
{

enum class TransformType
{
    kMatrix     = OMI_COMP_TransformType_kMatrix,
    kTranslate  = OMI_COMP_TransformType_kTranslate,
    kAxisAngle  = OMI_COMP_TransformType_kAxisAngle,
    kQuaternion = OMI_COMP_TransformType_kQuaternion,
    kScale      = OMI_COMP_TransformType_kScale,
    kScale3     = OMI_COMP_TransformType_kScale3
};

enum class TransformConstraint
{
    kNone       = OMI_COMP_TransformConstraint_kNone,
    kTranslateX = OMI_COMP_TransformConstraint_kTranslateX,
    kTranslateY = OMI_COMP_TransformConstraint_kTranslateY,
    kTranslateZ = OMI_COMP_TransformConstraint_kTranslateZ,
    kRotateX    = OMI_COMP_TransformConstraint_kRotateX,
    kRotateY    = OMI_COMP_TransformConstraint_kRotateY,
    kRotateZ    = OMI_COMP_TransformConstraint_kRotateZ,
    kScaleX     = OMI_COMP_TransformConstraint_kScaleX,
    kScaleY     = OMI_COMP_TransformConstraint_kScaleY,
    kScaleZ     = OMI_COMP_TransformConstraint_kScaleZ,
    kTranslate  = OMI_COMP_TransformConstraint_kTranslate,
    kRotate     = OMI_COMP_TransformConstraint_kRotate,
    kScale      = OMI_COMP_TransformConstraint_kScale,
    kSRT        = OMI_COMP_TransformConstraint_kSRT
};

class Transform
    : public Component
{
public:

    //--------------------------------------------------------------------------
    //                                CONSTRUCTORS
    //--------------------------------------------------------------------------

    Transform(Component const& other);

    Transform(Component&& other);

    Transform(OMI_COMP_ComponentPtr ptr, bool increase_ref=true);

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    virtual ~Transform();

    //--------------------------------------------------------------------------
    //                          PUBLIC STATIC FUNCTIONS
    //--------------------------------------------------------------------------

    static TransformInterface& get_transform_interface();

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    virtual bool is_valid() const override;

    TransformType get_transform_type() const;

    void set_constraint(
            TransformConstraint constraint,
            Transform const& constrained_to);

    arclx::Matrix44f eval() const;
};

} // namespace comp
} // namespace omi

//------------------------------------------------------------------------------
//                                      HASH
//------------------------------------------------------------------------------

namespace std
{

template<>
struct hash<omi::comp::Transform> :
    public unary_function<omi::comp::Transform, size_t>
{
    std::size_t operator()(const omi::comp::Transform& component) const
    {
        return static_cast<std::size_t>(component.get_id());
    }
};

} // namespace std

#endif
