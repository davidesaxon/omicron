/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_COMP_TRANSFORM_MATRIXTRANSFORM_HPP_
#define OMICRON_API_COMP_TRANSFORM_MATRIXTRANSFORM_HPP_

#include <arcanecore/lx/Matrix.hpp>

#include "omicron/api/comp/transform/Transform.hpp"


namespace omi
{
namespace comp
{

class MatrixTransform
    : public Transform
{
public:

    //--------------------------------------------------------------------------
    //                                CONSTRUCTORS
    //--------------------------------------------------------------------------

    MatrixTransform(
            arclx::Matrix44f const& matrix,
            TransformConstraint constraint,
            OMI_COMP_ComponentPtr constrained_to);

    MatrixTransform(Component const& other);

    MatrixTransform(Component&& other);

    MatrixTransform(OMI_COMP_ComponentPtr ptr, bool increase_ref=true);

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    virtual ~MatrixTransform();

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    virtual bool is_valid() const override;

    arclx::Matrix44f const& matrix() const;

    arclx::Matrix44f& matrix();
};

} // namespace comp
} // namespace omi

//------------------------------------------------------------------------------
//                                      HASH
//------------------------------------------------------------------------------

namespace std
{

template<>
struct hash<omi::comp::MatrixTransform> :
    public unary_function<omi::comp::MatrixTransform, size_t>
{
    std::size_t operator()(const omi::comp::MatrixTransform& component) const
    {
        return static_cast<std::size_t>(component.get_id());
    }
};

} // namespace std

#endif
