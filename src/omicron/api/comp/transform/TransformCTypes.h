/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_COMP_TRANSFORM_TRANSFORMCTYPES_H_
#define OMICRON_API_COMP_TRANSFORM_TRANSFORMCTYPES_H_

#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
//                                  ERROR CODES
//------------------------------------------------------------------------------

typedef uint32_t OMI_COMP_Transform_Error;
#define OMI_COMP_Transform_Error_kNone    0
#define OMI_COMP_Transform_Error_kInvalid 1

//------------------------------------------------------------------------------
//                                 TRANSFORM TYPE
//------------------------------------------------------------------------------

typedef uint32_t OMI_COMP_TransformType;
#define OMI_COMP_TransformType_kMatrix     0
#define OMI_COMP_TransformType_kTranslate  1
#define OMI_COMP_TransformType_kAxisAngle  2
#define OMI_COMP_TransformType_kQuaternion 3
#define OMI_COMP_TransformType_kScale      4
#define OMI_COMP_TransformType_kScale3     5

//------------------------------------------------------------------------------
//                                   CONSTRAINT
//------------------------------------------------------------------------------

typedef uint32_t OMI_COMP_TransformConstraint;
#define OMI_COMP_TransformConstraint_kNone       0
#define OMI_COMP_TransformConstraint_kTranslateX 1
#define OMI_COMP_TransformConstraint_kTranslateY 2
#define OMI_COMP_TransformConstraint_kTranslateZ 4
#define OMI_COMP_TransformConstraint_kRotateX    8
#define OMI_COMP_TransformConstraint_kRotateY    16
#define OMI_COMP_TransformConstraint_kRotateZ    32
#define OMI_COMP_TransformConstraint_kScaleX     64
#define OMI_COMP_TransformConstraint_kScaleY     128
#define OMI_COMP_TransformConstraint_kScaleZ     256
#define OMI_COMP_TransformConstraint_kTranslate \
    OMI_COMP_TransformConstraint_kTranslateX |  \
    OMI_COMP_TransformConstraint_kTranslateY |  \
    OMI_COMP_TransformConstraint_kTranslateZ
#define OMI_COMP_TransformConstraint_kRotate \
    OMI_COMP_TransformConstraint_kRotateX |  \
    OMI_COMP_TransformConstraint_kRotateY |  \
    OMI_COMP_TransformConstraint_kRotateZ
#define OMI_COMP_TransformConstraint_kScale \
    OMI_COMP_TransformConstraint_kScaleX |  \
    OMI_COMP_TransformConstraint_kScaleY |  \
    OMI_COMP_TransformConstraint_kScaleZ
#define OMI_COMP_TransformConstraint_kSRT      \
    OMI_COMP_TransformConstraint_kTranslateX | \
    OMI_COMP_TransformConstraint_kTranslateY | \
    OMI_COMP_TransformConstraint_kTranslateZ | \
    OMI_COMP_TransformConstraint_kRotateX    | \
    OMI_COMP_TransformConstraint_kRotateY    | \
    OMI_COMP_TransformConstraint_kRotateZ    | \
    OMI_COMP_TransformConstraint_kScaleX     | \
    OMI_COMP_TransformConstraint_kScaleY     | \
    OMI_COMP_TransformConstraint_kScaleZ

#ifdef __cplusplus
}
#endif

#endif
