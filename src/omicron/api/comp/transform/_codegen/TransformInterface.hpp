/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_COMP_CODEGEN_TRANSFORMINTERFACE_HPP_
#define OMI_COMP_CODEGEN_TRANSFORMINTERFACE_HPP_

#include <cassert>
#include <string>

#include <arcanecore/base/dl/DLOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>
#include <arcanecore/base/lang/Restrictors.hpp>

#include "omicron/api/comp/transform/_codegen/TransformCSymbols.h"


namespace omi
{
namespace comp
{

class TransformInterface
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                              TYPE DEFINITIONS
    //--------------------------------------------------------------------------

    typedef OMI_Bool (BindingFunc)(char const*, void**);

    //--------------------------------------------------------------------------
    //                             PUBLIC ATTRIBUTES
    //--------------------------------------------------------------------------

    // Transform_is_valid
    OMI_Bool (*Transform_is_valid)(OMI_COMP_ComponentPtrConst, OMI_COMP_TransformType);
    // get_transform_type
    OMI_COMP_Transform_Error (*get_transform_type)(OMI_COMP_ComponentPtrConst, OMI_COMP_TransformType*);
    // set_constraint
    OMI_COMP_Transform_Error (*set_constraint)(OMI_COMP_ComponentPtr, OMI_COMP_TransformConstraint, OMI_COMP_ComponentPtr);
    // eval
    void (*eval)(OMI_COMP_ComponentPtrConst, float*);
    // MatrixTransform_constructor
    OMI_COMP_ComponentPtr (*MatrixTransform_constructor)(float const*, OMI_COMP_TransformConstraint, OMI_COMP_ComponentPtr);
    // MatrixTransform_matrix
    float* (*MatrixTransform_matrix)(OMI_COMP_ComponentPtr);
    // TranslateTransform_constructor
    OMI_COMP_ComponentPtr (*TranslateTransform_constructor)(float const*, OMI_COMP_TransformConstraint, OMI_COMP_ComponentPtr);
    // TranslateTransform_translation
    float* (*TranslateTransform_translation)(OMI_COMP_ComponentPtr);
    // AxisAngleTransform_constructor
    OMI_COMP_ComponentPtr (*AxisAngleTransform_constructor)(float, float const*, OMI_COMP_TransformConstraint, OMI_COMP_ComponentPtr);
    // AxisAngleTransform_angle
    float* (*AxisAngleTransform_angle)(OMI_COMP_ComponentPtr);
    // AxisAngleTransform_axis
    float* (*AxisAngleTransform_axis)(OMI_COMP_ComponentPtr);
    // QuaternionTransform_constructor
    OMI_COMP_ComponentPtr (*QuaternionTransform_constructor)(float const*, OMI_COMP_TransformConstraint, OMI_COMP_ComponentPtr);
    // QuaternionTransform_quaternion
    float* (*QuaternionTransform_quaternion)(OMI_COMP_ComponentPtr);
    // ScaleTransform_constructor
    OMI_COMP_ComponentPtr (*ScaleTransform_constructor)(float, OMI_COMP_TransformConstraint, OMI_COMP_ComponentPtr);
    // ScaleTransform_scale
    float* (*ScaleTransform_scale)(OMI_COMP_ComponentPtr);
    // Scale3Transform_constructor
    OMI_COMP_ComponentPtr (*Scale3Transform_constructor)(float const*, OMI_COMP_TransformConstraint, OMI_COMP_ComponentPtr);
    // Scale3Transform_scale
    float* (*Scale3Transform_scale)(OMI_COMP_ComponentPtr);

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    TransformInterface(std::string const& libname)
        : Transform_is_valid(nullptr)
        , get_transform_type(nullptr)
        , set_constraint(nullptr)
        , eval(nullptr)
        , MatrixTransform_constructor(nullptr)
        , MatrixTransform_matrix(nullptr)
        , TranslateTransform_constructor(nullptr)
        , TranslateTransform_translation(nullptr)
        , AxisAngleTransform_constructor(nullptr)
        , AxisAngleTransform_angle(nullptr)
        , AxisAngleTransform_axis(nullptr)
        , QuaternionTransform_constructor(nullptr)
        , QuaternionTransform_quaternion(nullptr)
        , ScaleTransform_constructor(nullptr)
        , ScaleTransform_scale(nullptr)
        , Scale3Transform_constructor(nullptr)
        , Scale3Transform_scale(nullptr)
    {
        #ifdef ARC_OS_WINDOWS
            arc::fsys::Path libpath({
                "build",
                "windows",
                "Release",
                libname + "_impl.dll"
            });
        #elif defined(ARC_OS_UNIX)
            arc::fsys::Path libpath({
                "build",
                "linux",
                "lib" + libname + "_impl.so"
            });
        #else
            assert(false);
        #endif

        arc::dl::Handle lib_handle = arc::dl::open_library(libpath);
        BindingFunc* binding_func = arc::dl::bind_symbol<BindingFunc>(lib_handle, "OMI_COMP_Transform_BINDING_IMPL");

        if(binding_func(OMI_COMP_Transform_Transform_is_valid_SYMBOL, (void**) &Transform_is_valid) != 0)
        {
        }
        if(binding_func(OMI_COMP_Transform_get_transform_type_SYMBOL, (void**) &get_transform_type) != 0)
        {
        }
        if(binding_func(OMI_COMP_Transform_set_constraint_SYMBOL, (void**) &set_constraint) != 0)
        {
        }
        if(binding_func(OMI_COMP_Transform_eval_SYMBOL, (void**) &eval) != 0)
        {
        }
        if(binding_func(OMI_COMP_Transform_MatrixTransform_constructor_SYMBOL, (void**) &MatrixTransform_constructor) != 0)
        {
        }
        if(binding_func(OMI_COMP_Transform_MatrixTransform_matrix_SYMBOL, (void**) &MatrixTransform_matrix) != 0)
        {
        }
        if(binding_func(OMI_COMP_Transform_TranslateTransform_constructor_SYMBOL, (void**) &TranslateTransform_constructor) != 0)
        {
        }
        if(binding_func(OMI_COMP_Transform_TranslateTransform_translation_SYMBOL, (void**) &TranslateTransform_translation) != 0)
        {
        }
        if(binding_func(OMI_COMP_Transform_AxisAngleTransform_constructor_SYMBOL, (void**) &AxisAngleTransform_constructor) != 0)
        {
        }
        if(binding_func(OMI_COMP_Transform_AxisAngleTransform_angle_SYMBOL, (void**) &AxisAngleTransform_angle) != 0)
        {
        }
        if(binding_func(OMI_COMP_Transform_AxisAngleTransform_axis_SYMBOL, (void**) &AxisAngleTransform_axis) != 0)
        {
        }
        if(binding_func(OMI_COMP_Transform_QuaternionTransform_constructor_SYMBOL, (void**) &QuaternionTransform_constructor) != 0)
        {
        }
        if(binding_func(OMI_COMP_Transform_QuaternionTransform_quaternion_SYMBOL, (void**) &QuaternionTransform_quaternion) != 0)
        {
        }
        if(binding_func(OMI_COMP_Transform_ScaleTransform_constructor_SYMBOL, (void**) &ScaleTransform_constructor) != 0)
        {
        }
        if(binding_func(OMI_COMP_Transform_ScaleTransform_scale_SYMBOL, (void**) &ScaleTransform_scale) != 0)
        {
        }
        if(binding_func(OMI_COMP_Transform_Scale3Transform_constructor_SYMBOL, (void**) &Scale3Transform_constructor) != 0)
        {
        }
        if(binding_func(OMI_COMP_Transform_Scale3Transform_scale_SYMBOL, (void**) &Scale3Transform_scale) != 0)
        {
        }
    }
};

} // namespace omi
} // namespace comp


#endif