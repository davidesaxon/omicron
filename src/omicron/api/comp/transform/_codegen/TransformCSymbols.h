/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_COMP_CODEGEN_TRANSFORMCSMYBOLS_H_
#define OMI_COMP_CODEGEN_TRANSFORMCSMYBOLS_H_


#ifdef __cplusplus
extern "C" {
#endif

// OMI_COMP_Transform_is_valid
static char const* OMI_COMP_Transform_Transform_is_valid_SYMBOL = "OMI_Bool OMI_COMP_Transform_Transform_is_valid(OMI_COMP_ComponentPtrConst, OMI_COMP_TransformType)";

// OMI_COMP_get_transform_type
static char const* OMI_COMP_Transform_get_transform_type_SYMBOL = "OMI_COMP_Transform_Error OMI_COMP_Transform_get_transform_type(OMI_COMP_ComponentPtrConst, OMI_COMP_TransformType*)";

// OMI_COMP_set_constraint
static char const* OMI_COMP_Transform_set_constraint_SYMBOL = "OMI_COMP_Transform_Error OMI_COMP_Transform_set_constraint(OMI_COMP_ComponentPtr, OMI_COMP_TransformConstraint, OMI_COMP_ComponentPtr)";

// OMI_COMP_eval
static char const* OMI_COMP_Transform_eval_SYMBOL = "void OMI_COMP_Transform_eval(OMI_COMP_ComponentPtrConst, float*)";

// OMI_COMP_MatrixTransform_constructor
static char const* OMI_COMP_Transform_MatrixTransform_constructor_SYMBOL = "OMI_COMP_ComponentPtr OMI_COMP_Transform_MatrixTransform_constructor(float const*, OMI_COMP_TransformConstraint, OMI_COMP_ComponentPtr)";

// OMI_COMP_MatrixTransform_matrix
static char const* OMI_COMP_Transform_MatrixTransform_matrix_SYMBOL = "float* OMI_COMP_Transform_MatrixTransform_matrix(OMI_COMP_ComponentPtr)";

// OMI_COMP_TranslateTransform_constructor
static char const* OMI_COMP_Transform_TranslateTransform_constructor_SYMBOL = "OMI_COMP_ComponentPtr OMI_COMP_Transform_TranslateTransform_constructor(float const*, OMI_COMP_TransformConstraint, OMI_COMP_ComponentPtr)";

// OMI_COMP_TranslateTransform_translation
static char const* OMI_COMP_Transform_TranslateTransform_translation_SYMBOL = "float* OMI_COMP_Transform_TranslateTransform_translation(OMI_COMP_ComponentPtr)";

// OMI_COMP_AxisAngleTransform_constructor
static char const* OMI_COMP_Transform_AxisAngleTransform_constructor_SYMBOL = "OMI_COMP_ComponentPtr OMI_COMP_Transform_AxisAngleTransform_constructor(float, float const*, OMI_COMP_TransformConstraint, OMI_COMP_ComponentPtr)";

// OMI_COMP_AxisAngleTransform_angle
static char const* OMI_COMP_Transform_AxisAngleTransform_angle_SYMBOL = "float* OMI_COMP_Transform_AxisAngleTransform_angle(OMI_COMP_ComponentPtr)";

// OMI_COMP_AxisAngleTransform_axis
static char const* OMI_COMP_Transform_AxisAngleTransform_axis_SYMBOL = "float* OMI_COMP_Transform_AxisAngleTransform_axis(OMI_COMP_ComponentPtr)";

// OMI_COMP_QuaternionTransform_constructor
static char const* OMI_COMP_Transform_QuaternionTransform_constructor_SYMBOL = "OMI_COMP_ComponentPtr OMI_COMP_Transform_QuaternionTransform_constructor(float const*, OMI_COMP_TransformConstraint, OMI_COMP_ComponentPtr)";

// OMI_COMP_QuaternionTransform_quaternion
static char const* OMI_COMP_Transform_QuaternionTransform_quaternion_SYMBOL = "float* OMI_COMP_Transform_QuaternionTransform_quaternion(OMI_COMP_ComponentPtr)";

// OMI_COMP_ScaleTransform_constructor
static char const* OMI_COMP_Transform_ScaleTransform_constructor_SYMBOL = "OMI_COMP_ComponentPtr OMI_COMP_Transform_ScaleTransform_constructor(float, OMI_COMP_TransformConstraint, OMI_COMP_ComponentPtr)";

// OMI_COMP_ScaleTransform_scale
static char const* OMI_COMP_Transform_ScaleTransform_scale_SYMBOL = "float* OMI_COMP_Transform_ScaleTransform_scale(OMI_COMP_ComponentPtr)";

// OMI_COMP_Scale3Transform_constructor
static char const* OMI_COMP_Transform_Scale3Transform_constructor_SYMBOL = "OMI_COMP_ComponentPtr OMI_COMP_Transform_Scale3Transform_constructor(float const*, OMI_COMP_TransformConstraint, OMI_COMP_ComponentPtr)";

// OMI_COMP_Scale3Transform_scale
static char const* OMI_COMP_Transform_Scale3Transform_scale_SYMBOL = "float* OMI_COMP_Transform_Scale3Transform_scale(OMI_COMP_ComponentPtr)";

#ifdef __cplusplus
} // extern "C"
#endif

#endif