/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#include <cstring>

#include "omicron/api/API.h"

#include "omicron/api/comp/transform/_codegen/TransformCSymbols.h"


#ifdef __cplusplus
extern "C" {
#endif

OMI_API_EXPORT int OMI_COMP_Transform_BINDING_IMPL(char const* func_def, void** func_ptr)
{
    if(strcmp(func_def, OMI_COMP_Transform_Transform_is_valid_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Transform_is_valid;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Transform_get_transform_type_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::get_transform_type;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Transform_set_constraint_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::set_constraint;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Transform_eval_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::eval;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Transform_MatrixTransform_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::MatrixTransform_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Transform_MatrixTransform_matrix_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::MatrixTransform_matrix;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Transform_TranslateTransform_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::TranslateTransform_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Transform_TranslateTransform_translation_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::TranslateTransform_translation;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Transform_AxisAngleTransform_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::AxisAngleTransform_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Transform_AxisAngleTransform_angle_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::AxisAngleTransform_angle;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Transform_AxisAngleTransform_axis_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::AxisAngleTransform_axis;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Transform_QuaternionTransform_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::QuaternionTransform_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Transform_QuaternionTransform_quaternion_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::QuaternionTransform_quaternion;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Transform_ScaleTransform_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::ScaleTransform_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Transform_ScaleTransform_scale_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::ScaleTransform_scale;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Transform_Scale3Transform_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Scale3Transform_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_COMP_Transform_Scale3Transform_scale_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::comp::Scale3Transform_scale;
        return 0;
    }

    return 1;
}

#ifdef __cplusplus
} // extern "C"
#endif

