/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/comp/transform/ScaleTransform.hpp"


namespace omi
{
namespace comp
{

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

ScaleTransform::ScaleTransform(
        float scale,
        TransformConstraint constraint,
        OMI_COMP_ComponentPtr constrained_to)
    : Transform(get_transform_interface().ScaleTransform_constructor(
        scale,
        static_cast<OMI_COMP_TransformConstraint>(constraint),
        constrained_to
    ), false)
{
}

ScaleTransform::ScaleTransform(Component const& other)
    : Transform(other)
{
}

ScaleTransform::ScaleTransform(Component&& other)
    : Transform(std::move(other))
{
}

ScaleTransform::ScaleTransform(OMI_COMP_ComponentPtr ptr, bool increase_ref)
    : Transform(ptr, increase_ref)
{
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

ScaleTransform::~ScaleTransform()
{
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

bool ScaleTransform::is_valid() const
{
    return get_transform_interface().Transform_is_valid(
        m_ptr,
        OMI_COMP_TransformType_kScale
    ) != OMI_False;
}

float ScaleTransform::scale() const
{
    return *get_transform_interface().ScaleTransform_scale(m_ptr);
}

float& ScaleTransform::scale()
{
    return *get_transform_interface().ScaleTransform_scale(m_ptr);
}

} // namespace comp
} // namespace omi
