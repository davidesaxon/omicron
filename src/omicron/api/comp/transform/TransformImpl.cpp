/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstddef>
#include <cstring>
#include <string>

#include <arcanecore/lx/Alignment.hpp>
#include <arcanecore/lx/Matrix.hpp>
#include <arcanecore/lx/MatrixMath44f.hpp>
#include <arcanecore/lx/Vector.hpp>

#include "omicron/api/API.h"
#include "omicron/api/comp/ComponentImpl.hpp"
#include "omicron/api/comp/transform/TransformCTypes.h"


OMI_CODEGEN_NAMESPACE namespace omi
{
OMI_CODEGEN_NAMESPACE namespace comp
{
namespace
{

//------------------------------------------------------------------------------
//                                    STRUCTS
//------------------------------------------------------------------------------

struct AbstractTransform
{
    //---------------------------A T T R I B U T E S----------------------------

    OMI_COMP_TransformType m_type;
    OMI_COMP_TransformConstraint m_constraint;
    OMI_COMP_ComponentPtr m_constrained_to;

    //--------------------------C O N S T R U C T O R---------------------------

    AbstractTransform(
            OMI_COMP_TransformType type,
            OMI_COMP_TransformConstraint constraint,
            OMI_COMP_ComponentPtr constrained_to)
        : m_type          (type)
        , m_constraint    (constraint)
        , m_constrained_to(constrained_to)
    {
        // increase reference?
        if(m_constrained_to != nullptr)
        {
            Component_increase_reference(m_constrained_to);
        }
    }

    //---------------------------D E S T R U C T O R----------------------------

    virtual ~AbstractTransform()
    {
        // decrease reference?
        if(m_constrained_to != nullptr)
        {
            Component_decrease_reference(m_constrained_to);
        }
    }
};

struct MatrixTransform
    : public AbstractTransform
{
    ARCLX_ALIGNED_NEW;

    //---------------------------A T T R I B U T E S----------------------------

    arclx::Matrix44f m_matrix;

    //--------------------------C O N S T R U C T O R---------------------------

    MatrixTransform(
            float const* matrix,
            OMI_COMP_TransformConstraint constraint,
            OMI_COMP_ComponentPtr constrained_to)
        : AbstractTransform(
            OMI_COMP_TransformType_kMatrix,
            constraint,
            constrained_to
        )
        , m_matrix(*reinterpret_cast<arclx::Matrix44f const*>(matrix))
    {
    }

    //---------------------------D E S T R U C T O R----------------------------

    virtual ~MatrixTransform()
    {
    }
};

struct TranslateTransform
    : public AbstractTransform
{
    ARCLX_ALIGNED_NEW;

    //---------------------------A T T R I B U T E S----------------------------

    arclx::Vector3f m_translation;

    //--------------------------C O N S T R U C T O R---------------------------

    TranslateTransform(
            float const* translation,
            OMI_COMP_TransformConstraint constraint,
            OMI_COMP_ComponentPtr constrained_to)
        : AbstractTransform(
            OMI_COMP_TransformType_kTranslate,
            constraint,
            constrained_to
        )
        , m_translation(*reinterpret_cast<arclx::Vector3f const*>(translation))
    {
    }

    //---------------------------D E S T R U C T O R----------------------------

    virtual ~TranslateTransform()
    {
    }
};

struct AxisAngleTransform
    : public AbstractTransform
{
    ARCLX_ALIGNED_NEW;

    //---------------------------A T T R I B U T E S----------------------------

    float m_angle;
    arclx::Vector3f m_axis;

    //--------------------------C O N S T R U C T O R---------------------------

    AxisAngleTransform(
            float angle,
            float const* axis,
            OMI_COMP_TransformConstraint constraint,
            OMI_COMP_ComponentPtr constrained_to)
        : AbstractTransform(
            OMI_COMP_TransformType_kAxisAngle,
            constraint,
            constrained_to
        )
        , m_angle(angle)
        , m_axis (*reinterpret_cast<arclx::Vector3f const*>(axis))
    {
    }

    //---------------------------D E S T R U C T O R----------------------------

    virtual ~AxisAngleTransform()
    {
    }
};

struct QuaternionTransform
    : public AbstractTransform
{
    ARCLX_ALIGNED_NEW;

    //---------------------------A T T R I B U T E S----------------------------

    arclx::Quaternionf m_quaternion;

    //--------------------------C O N S T R U C T O R---------------------------

    QuaternionTransform(
            float const* quaternion,
            OMI_COMP_TransformConstraint constraint,
            OMI_COMP_ComponentPtr constrained_to)
        : AbstractTransform(
            OMI_COMP_TransformType_kQuaternion,
            constraint,
            constrained_to
        )
        , m_quaternion(
            *reinterpret_cast<arclx::Quaternionf const*>(quaternion)
        )
    {
    }

    //---------------------------D E S T R U C T O R----------------------------

    virtual ~QuaternionTransform()
    {
    }
};

struct ScaleTransform
    : public AbstractTransform
{
    //---------------------------A T T R I B U T E S----------------------------

    float m_scale;

    //--------------------------C O N S T R U C T O R---------------------------

    ScaleTransform(
            float scale,
            OMI_COMP_TransformConstraint constraint,
            OMI_COMP_ComponentPtr constrained_to)
        : AbstractTransform(
            OMI_COMP_TransformType_kScale,
            constraint,
            constrained_to
        )
        , m_scale(scale)
    {
    }

    //---------------------------D E S T R U C T O R----------------------------

    virtual ~ScaleTransform()
    {
    }
};

struct Scale3Transform
    : public AbstractTransform
{
    ARCLX_ALIGNED_NEW;

    //---------------------------A T T R I B U T E S----------------------------

    arclx::Vector3f m_scale;

    //--------------------------C O N S T R U C T O R---------------------------

    Scale3Transform(
            float const* scale,
            OMI_COMP_TransformConstraint constraint,
            OMI_COMP_ComponentPtr constrained_to)
        : AbstractTransform(
            OMI_COMP_TransformType_kScale3,
            constraint,
            constrained_to
        )
        , m_scale(*reinterpret_cast<arclx::Vector3f const*>(scale))
    {
    }

    //---------------------------D E S T R U C T O R----------------------------

    virtual ~Scale3Transform()
    {
    }
};

//------------------------------------------------------------------------------
//                               TRANSFORM IS VALID
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool Transform_is_valid(
        OMI_COMP_ComponentPtrConst ptr,
        OMI_COMP_TransformType expected_type)
{
    if(ptr->m_type != OMI_COMP_ComponentType_kTransform ||
       ptr->m_internals == nullptr)
    {
        return OMI_False;
    }

    AbstractTransform* internals = (AbstractTransform*) ptr->m_internals;
    if(internals->m_type != expected_type)
    {
        return OMI_False;
    }
    return OMI_True;
}

//------------------------------------------------------------------------------
//                               GET TRANSFORM TYPE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_COMP_Transform_Error get_transform_type(
        OMI_COMP_ComponentPtrConst ptr,
        OMI_COMP_TransformType* out_type)
{
    if(ptr->m_type != OMI_COMP_ComponentType_kTransform ||
       ptr->m_internals == nullptr)
    {
        return OMI_COMP_Transform_Error_kInvalid;
    }

    AbstractTransform* internals = (AbstractTransform*) ptr->m_internals;
    *out_type = internals->m_type;

    return OMI_COMP_Transform_Error_kNone;
}

//------------------------------------------------------------------------------
//                                 SET CONSTRAINT
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_COMP_Transform_Error set_constraint(
        OMI_COMP_ComponentPtr ptr,
        OMI_COMP_TransformConstraint constraint,
        OMI_COMP_ComponentPtr constrained_to)
{
    if(ptr->m_type != OMI_COMP_ComponentType_kTransform ||
       ptr->m_internals == nullptr ||
       constrained_to->m_type != OMI_COMP_ComponentType_kTransform ||
       constrained_to->m_internals == nullptr)
    {
        return OMI_COMP_Transform_Error_kInvalid;
    }

    AbstractTransform* internals = (AbstractTransform*) ptr->m_internals;
    // clean up?
    if(internals->m_constrained_to != nullptr)
    {
        Component_decrease_reference(internals->m_constrained_to);
    }
    // set
    internals->m_constraint = constraint;
    internals->m_constrained_to = constrained_to;
    Component_increase_reference(internals->m_constrained_to);

    return OMI_COMP_Transform_Error_kNone;
}

//------------------------------------------------------------------------------
//                                      EVAL
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void eval(
        OMI_COMP_ComponentPtrConst ptr,
        float* out_matrix)
{
    // TODO: some caching could be used here - upstream constraints have to
    //       eval'd each time even if multiple things are constrained to them

    // no error checking for performance
    arclx::Matrix44f& ret = *reinterpret_cast<arclx::Matrix44f*>(out_matrix);
    AbstractTransform* transform = (AbstractTransform*) ptr->m_internals;

    // build the matrix based on type
    switch(transform->m_type)
    {
        case OMI_COMP_TransformType_kMatrix:
        {
            MatrixTransform* t = (MatrixTransform*) transform;
            ret = t->m_matrix;
            break;
        }
        case OMI_COMP_TransformType_kTranslate:
        {
            TranslateTransform* t = (TranslateTransform*) transform;
            ret = arclx::translate_44f(t->m_translation);
            break;
        }
        case OMI_COMP_TransformType_kAxisAngle:
        {
            AxisAngleTransform* t = (AxisAngleTransform*) transform;
            ret = arclx::rotate_axis_44f(t->m_angle, t->m_axis);
            break;
        }
        case OMI_COMP_TransformType_kQuaternion:
        {
            QuaternionTransform* t = (QuaternionTransform*) transform;
            ret = arclx::Matrix44f::Identity();
            ret.block<3, 3>(0, 0) = t->m_quaternion.toRotationMatrix();
            break;
        }
        case OMI_COMP_TransformType_kScale:
        {
            ScaleTransform* t = (ScaleTransform*) transform;
            ret = arclx::scale_44f(t->m_scale);
            break;
        }
        case OMI_COMP_TransformType_kScale3:
        {
            Scale3Transform* t = (Scale3Transform*) transform;
            ret = arclx::scale_44f(t->m_scale);
            break;
        }
    }

    // apply constraints?
    if(transform->m_constraint == OMI_COMP_TransformConstraint_kNone ||
       transform->m_constrained_to == nullptr)
    {
        return;
    }

    if(transform->m_constraint == (OMI_COMP_TransformConstraint_kSRT))
    {
        arclx::Matrix44f upstream;
        eval(transform->m_constrained_to, &upstream(0, 0));
        ret = upstream * ret;
    }
    // TODO: support other constraint types
}

//------------------------------------------------------------------------------
//                          MATRIXTRANSFORM CONSTRUCTOR
//------------------------------------------------------------------------------

static void MatrixTransform_delete(void* internals)
{
    MatrixTransform* transform = (MatrixTransform*) internals;
    delete transform;
}

OMI_CODEGEN_FUNCTION
static OMI_COMP_ComponentPtr MatrixTransform_constructor(
        float const* matrix,
        OMI_COMP_TransformConstraint constraint,
        OMI_COMP_ComponentPtr constrained_to)
{
    MatrixTransform* transform = new MatrixTransform(
        matrix,
        constraint,
        constrained_to
    );
    return new OMI_COMP_ComponentStruct(
        OMI_COMP_ComponentType_kTransform,
        transform,
        &MatrixTransform_delete
    );
}

//------------------------------------------------------------------------------
//                             MATRIXTRANSFORM MATRIX
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float* MatrixTransform_matrix(OMI_COMP_ComponentPtr ptr)
{
    // no error checking for performance

    MatrixTransform* transform = (MatrixTransform*) ptr->m_internals;
    return &transform->m_matrix(0);
}

//------------------------------------------------------------------------------
//                         TRANSLATETRANSFORM CONSTRUCTOR
//------------------------------------------------------------------------------

static void TranslateTransform_delete(void* internals)
{
    TranslateTransform* transform = (TranslateTransform*) internals;
    delete transform;
}

OMI_CODEGEN_FUNCTION
static OMI_COMP_ComponentPtr TranslateTransform_constructor(
        float const* translation,
        OMI_COMP_TransformConstraint constraint,
        OMI_COMP_ComponentPtr constrained_to)
{
    TranslateTransform* transform = new TranslateTransform(
        translation,
        constraint,
        constrained_to
    );
    return new OMI_COMP_ComponentStruct(
        OMI_COMP_ComponentType_kTransform,
        transform,
        &TranslateTransform_delete
    );
}

//------------------------------------------------------------------------------
//                        TRANSLATETRANSFORM TRANSLATION
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float* TranslateTransform_translation(OMI_COMP_ComponentPtr ptr)
{
    // no error checking for performance

    TranslateTransform* transform = (TranslateTransform*) ptr->m_internals;
    return &transform->m_translation(0);
}

//------------------------------------------------------------------------------
//                         AXISANGLETRANSFORM CONSTRUCTOR
//------------------------------------------------------------------------------

static void AxisAngleTransform_delete(void* internals)
{
    AxisAngleTransform* transform = (AxisAngleTransform*) internals;
    delete transform;
}

OMI_CODEGEN_FUNCTION
static OMI_COMP_ComponentPtr AxisAngleTransform_constructor(
        float angle,
        float const* axis,
        OMI_COMP_TransformConstraint constraint,
        OMI_COMP_ComponentPtr constrained_to)
{
    AxisAngleTransform* transform = new AxisAngleTransform(
        angle,
        axis,
        constraint,
        constrained_to
    );
    return new OMI_COMP_ComponentStruct(
        OMI_COMP_ComponentType_kTransform,
        transform,
        &AxisAngleTransform_delete
    );
}

//------------------------------------------------------------------------------
//                            AXISANGLETRANSFORM ANGLE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float* AxisAngleTransform_angle(OMI_COMP_ComponentPtr ptr)
{
    // no error checking for performance

    AxisAngleTransform* transform = (AxisAngleTransform*) ptr->m_internals;
    return &transform->m_angle;
}

//------------------------------------------------------------------------------
//                            AXISANGLETRANSFORM AXIS
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float* AxisAngleTransform_axis(OMI_COMP_ComponentPtr ptr)
{
    // no error checking for performance

    AxisAngleTransform* transform = (AxisAngleTransform*) ptr->m_internals;
    return &transform->m_axis(0);
}

//------------------------------------------------------------------------------
//                        QUATERNIONTRANSFORM CONSTRUCTOR
//------------------------------------------------------------------------------

static void QuaternionTransform_delete(void* internals)
{
    QuaternionTransform* transform = (QuaternionTransform*) internals;
    delete transform;
}

OMI_CODEGEN_FUNCTION
static OMI_COMP_ComponentPtr QuaternionTransform_constructor(
        float const* quaternion,
        OMI_COMP_TransformConstraint constraint,
        OMI_COMP_ComponentPtr constrained_to)
{
    QuaternionTransform* transform = new QuaternionTransform(
        quaternion,
        constraint,
        constrained_to
    );
    return new OMI_COMP_ComponentStruct(
        OMI_COMP_ComponentType_kTransform,
        transform,
        &QuaternionTransform_delete
    );
}

//------------------------------------------------------------------------------
//                         QUATERNIONTRANSFORM QUATERNION
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float* QuaternionTransform_quaternion(OMI_COMP_ComponentPtr ptr)
{
    // no error checking for performance

    QuaternionTransform* transform = (QuaternionTransform*) ptr->m_internals;
    return &transform->m_quaternion.coeffs()(0);
}

//------------------------------------------------------------------------------
//                           SCALETRANSFORM CONSTRUCTOR
//------------------------------------------------------------------------------

static void ScaleTransform_delete(void* internals)
{
    ScaleTransform* transform = (ScaleTransform*) internals;
    delete transform;
}

OMI_CODEGEN_FUNCTION
static OMI_COMP_ComponentPtr ScaleTransform_constructor(
        float scale,
        OMI_COMP_TransformConstraint constraint,
        OMI_COMP_ComponentPtr constrained_to)
{
    ScaleTransform* transform = new ScaleTransform(
        scale,
        constraint,
        constrained_to
    );
    return new OMI_COMP_ComponentStruct(
        OMI_COMP_ComponentType_kTransform,
        transform,
        &ScaleTransform_delete
    );
}

//------------------------------------------------------------------------------
//                              SCALETRANSFORM SCALE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float* ScaleTransform_scale(OMI_COMP_ComponentPtr ptr)
{
    // no error checking for performance

    ScaleTransform* transform = (ScaleTransform*) ptr->m_internals;
    return &transform->m_scale;
}

//------------------------------------------------------------------------------
//                          SCALE3TRANSFORM CONSTRUCTOR
//------------------------------------------------------------------------------

static void Scale3Transform_delete(void* internals)
{
    Scale3Transform* transform = (Scale3Transform*) internals;
    delete transform;
}

OMI_CODEGEN_FUNCTION
static OMI_COMP_ComponentPtr Scale3Transform_constructor(
        float const* scale,
        OMI_COMP_TransformConstraint constraint,
        OMI_COMP_ComponentPtr constrained_to)
{
    Scale3Transform* transform = new Scale3Transform(
        scale,
        constraint,
        constrained_to
    );
    return new OMI_COMP_ComponentStruct(
        OMI_COMP_ComponentType_kTransform,
        transform,
        &Scale3Transform_delete
    );
}

//------------------------------------------------------------------------------
//                          SCALE3TRANSFORM TRANSLATION
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float* Scale3Transform_scale(OMI_COMP_ComponentPtr ptr)
{
    // no error checking for performance

    Scale3Transform* transform = (Scale3Transform*) ptr->m_internals;
    return &transform->m_scale(0);
}

} // namespace anonymous
} // namespace comp
} // namespace omi

#include "omicron/api/comp/transform/_codegen/TransformBinding.inl"
