/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/host/Host.hpp"
#include "omicron/api/host/Surface.hpp"


namespace omi
{
namespace host
{

//------------------------------------------------------------------------------
//                            PUBLIC STATIC FUNCTIONS
//------------------------------------------------------------------------------

Surface& Surface::instance()
{
    static Surface inst;
    return inst;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

std::string_view Surface::get_title() const
{
    return std::string_view(Host::get_interface().get_surface_title());
}

void Surface::set_title(std::string const& title)
{
    Host::get_interface().set_surface_title(title.c_str());
}

arclx::Vector2u Surface::get_size() const
{
    arclx::Vector2u ret;
    Host::get_interface().get_surface_size(&ret(0), &ret(1));
    return ret;
}

void Surface::set_size(arclx::Vector2u const& size)
{
    Host::get_interface().set_surface_size(size(0), size(1));
}

arclx::Vector2i Surface::get_position() const
{
    arclx::Vector2i ret;
    Host::get_interface().get_surface_position(&ret(0), &ret(1));
    return ret;
}

void Surface::set_position(arclx::Vector2i const& position)
{
    Host::get_interface().set_surface_position(position(0), position(1));
}

bool Surface::is_fullscreen() const
{
    return Host::get_interface().is_fullscreen() != OMI_False;
}

void Surface::set_fullscreen(bool state)
{
    Host::get_interface().set_fullscreen(state);
}

bool Surface::is_cursor_hidden() const
{
    return Host::get_interface().is_cursor_hidden() != OMI_False;
}

void Surface::set_cursor_hidden(bool state)
{
    Host::get_interface().set_cursor_hidden(state);
}

//------------------------------------------------------------------------------
//                              PRIVATE CONSTRUCTOR
//------------------------------------------------------------------------------

Surface::Surface()
{
}

//------------------------------------------------------------------------------
//                               PRIVATE DESTRUCTOR
//------------------------------------------------------------------------------

Surface::~Surface()
{
}

} // namespace host
} // namespace omi
