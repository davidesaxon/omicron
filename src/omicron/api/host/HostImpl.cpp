/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstddef>
#include <cstring>
#include <string>

#include <arcanecore/base/Preproc.hpp>
#include <arcanecore/base/dl/DLOperations.hpp>
#include <arcanecore/base/fsys/FileSystemOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>

#include "omicron/api/API.h"
#include "omicron/api/common/Attributes.hpp"
#include "omicron/api/config/Registry.hpp"
#include "omicron/api/host/HostCTypes.h"
#include "omicron/api/report/Logger.hpp"


OMI_CODEGEN_NAMESPACE namespace omi
{
OMI_CODEGEN_NAMESPACE namespace host
{
namespace
{

//------------------------------------------------------------------------------
//                                    TYPEDEFS
//------------------------------------------------------------------------------

typedef char const* (GetPluginNameFunc)();
typedef char const* (GetPluginVersionFunc)();
typedef OMI_Bool (StartupFunc)();
typedef OMI_Bool (ShutdownFunc)();
typedef char const* (GetSurfaceTitleFunc)();
typedef void (SetSurfaceTitleFunc)(char const*);
typedef void (GetSurfaceSizeFunc)(uint32_t*, uint32_t*);
typedef void (SetSurfaceSizeFunc)(uint32_t, uint32_t);
typedef void (GetSurfacePositionFunc)(int32_t*, int32_t*);
typedef void (SetSurfacePositionFunc)(int32_t, int32_t);
typedef OMI_Bool (IsFullscreen)();
typedef void (SetFullscreen)(OMI_Bool);
typedef OMI_Bool (IsCursorHidden)();
typedef void (SetCursorHidden)(OMI_Bool);
typedef OMI_Bool (OpenSurfaceFunc)();
typedef void (MainLoopFunc)(OMI_HOST_EngineCycleFunc*);

//------------------------------------------------------------------------------
//                                    GLOBALS
//------------------------------------------------------------------------------

static std::string g_name;
static std::string g_version;

static arc::dl::Handle g_dl_handle = nullptr;

static ShutdownFunc* g_shutdown_func                       = nullptr;
static GetSurfaceTitleFunc* g_get_surface_title_func       = nullptr;
static SetSurfaceTitleFunc* g_set_surface_title_func       = nullptr;
static GetSurfaceSizeFunc* g_get_surface_size_func         = nullptr;
static SetSurfaceSizeFunc* g_set_surface_size_func         = nullptr;
static GetSurfacePositionFunc* g_get_surface_position_func = nullptr;
static SetSurfacePositionFunc* g_set_surface_position_func = nullptr;
static IsFullscreen* g_is_fullscreen_func                  = nullptr;
static SetFullscreen* g_set_fullscreen_func                = nullptr;
static IsCursorHidden* g_is_cursor_hidden_func             = nullptr;
static SetCursorHidden* g_set_cursor_hidden_func           = nullptr;
static OpenSurfaceFunc* g_open_surface_func                = nullptr;
static MainLoopFunc* g_main_loop_func                      = nullptr;

//------------------------------------------------------------------------------
//                                    STARTUP
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool startup(OMI_REPORT_LoggerPtr logger_ptr)
{
    omi::report::Logger logger(logger_ptr);

    if(g_dl_handle != nullptr)
    {
        logger.error(
            "Host startup called after host subsystem already initialised"
        );
        return OMI_False;
    }

    // get the path to the host subsystem to use
    StringAttribute path_attr = config::Registry::instance().get_attr(
        #ifdef ARC_OS_WINDOWS
            "omicron.host.bind.windows_path"
        #else
            "omicron.host.bind.linux_path"
        #endif
    );

    arc::fsys::Path host_path;
    for(StringAttribute::DataType const& c : path_attr.get_values())
    {
        host_path << c;
    }
    logger.debug(
        "Loading host subsystem from: {0}",
        host_path.to_native()
    );

    // check that the file exists
    if(!arc::fsys::is_file(host_path))
    {
        logger.critical(
            "Host subsystem file path either does not exist or is not a file: "
            "{0}",
            host_path.to_native()
        );
        return OMI_False;
    }

    // open as dynamic library
    g_dl_handle = arc::dl::open_library(host_path);

    // bind symbols
    GetPluginNameFunc* get_name_func = nullptr;
    try
    {
        get_name_func = arc::dl::bind_symbol<GetPluginNameFunc>(
            g_dl_handle,
            "OMI_HOST_get_plugin_name"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_HOST_get_plugin_name function from host: {0}",
            host_path.to_native()
        );
        return OMI_False;
    }

    GetPluginVersionFunc* get_version_func = nullptr;
    try
    {
        get_version_func = arc::dl::bind_symbol<GetPluginVersionFunc>(
            g_dl_handle,
            "OMI_HOST_get_plugin_version"
        );
        }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_HOST_get_plugin_version function from host: {0}",
            host_path.to_native()
        );
        return OMI_False;
    }

    StartupFunc* startup_func = nullptr;
    try
    {
        startup_func = arc::dl::bind_symbol<StartupFunc>(
            g_dl_handle,
            "OMI_HOST_startup"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_HOST_startup function from host: {0}",
            host_path.to_native()
        );
        return OMI_False;
    }

    try
    {
        g_shutdown_func = arc::dl::bind_symbol<ShutdownFunc>(
            g_dl_handle,
            "OMI_HOST_shutdown"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_HOST_shutdown function from host: {0}",
            host_path.to_native()
        );
        return OMI_False;
    }

    try
    {
        g_get_surface_title_func = arc::dl::bind_symbol<GetSurfaceTitleFunc>(
            g_dl_handle,
            "OMI_HOST_get_surface_title"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_HOST_get_surface_title function from host: {0}",
            host_path.to_native()
        );
        return OMI_False;
    }

    try
    {
        g_set_surface_title_func = arc::dl::bind_symbol<SetSurfaceTitleFunc>(
            g_dl_handle,
            "OMI_HOST_set_surface_size"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_HOST_set_surface_size function from host: {0}",
            host_path.to_native()
        );
        return OMI_False;
    }

    try
    {
        g_get_surface_size_func = arc::dl::bind_symbol<GetSurfaceSizeFunc>(
            g_dl_handle,
            "OMI_HOST_get_surface_size"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_HOST_get_surface_size function from host: {0}",
            host_path.to_native()
        );
        return OMI_False;
    }

    try
    {
        g_set_surface_size_func = arc::dl::bind_symbol<SetSurfaceSizeFunc>(
            g_dl_handle,
            "OMI_HOST_set_surface_size"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_HOST_set_surface_size function from host: {0}",
            host_path.to_native()
        );
        return OMI_False;
    }

    try
    {
        g_get_surface_position_func =
            arc::dl::bind_symbol<GetSurfacePositionFunc>(
                g_dl_handle,
                "OMI_HOST_get_surface_position"
            );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_HOST_get_surface_position function from host: "
            "{0}",
            host_path.to_native()
        );
        return OMI_False;
    }

    try
    {
        g_set_surface_position_func =
            arc::dl::bind_symbol<SetSurfacePositionFunc>(
                g_dl_handle,
                "OMI_HOST_set_surface_position"
            );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_HOST_set_surface_position function from host: "
            "{0}",
            host_path.to_native()
        );
        return OMI_False;
    }

    try
    {
        g_is_fullscreen_func = arc::dl::bind_symbol<IsFullscreen>(
            g_dl_handle,
            "OMI_HOST_is_fullscreen"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_HOST_is_fullscreen function from host: {0}",
            host_path.to_native()
        );
        return OMI_False;
    }

    try
    {
        g_set_fullscreen_func = arc::dl::bind_symbol<SetFullscreen>(
            g_dl_handle,
            "OMI_HOST_set_fullscreen"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_HOST_set_fullscreen function from host: {0}",
            host_path.to_native()
        );
        return OMI_False;
    }

    try
    {
        g_is_cursor_hidden_func = arc::dl::bind_symbol<IsCursorHidden>(
            g_dl_handle,
            "OMI_HOST_is_cursor_hidden"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_HOST_is_cursor_hidden function from host: {0}",
            host_path.to_native()
        );
        return OMI_False;
    }

    try
    {
        g_set_cursor_hidden_func = arc::dl::bind_symbol<SetCursorHidden>(
            g_dl_handle,
            "OMI_HOST_set_cursor_hidden"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_HOST_set_cursor_hidden function from host: {0}",
            host_path.to_native()
        );
        return OMI_False;
    }

    try
    {
        g_open_surface_func = arc::dl::bind_symbol<OpenSurfaceFunc>(
            g_dl_handle,
            "OMI_HOST_open_surface"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_HOST_open_surface function from host: {0}",
            host_path.to_native()
        );
        return OMI_False;
    }

    try
    {
        g_main_loop_func = arc::dl::bind_symbol<MainLoopFunc>(
            g_dl_handle,
            "OMI_HOST_main_loop"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_HOST_main_loop function from host: {0}",
            host_path.to_native()
        );
        return OMI_False;
    }

    g_name = get_name_func();
    g_version = get_version_func();

    logger.info(
        "Bound host subsystem: {0}-{1}",
        g_name,
        g_version
    );

    // call startup
    return startup_func();
}

//------------------------------------------------------------------------------
//                                    SHUTDOWN
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool shutdown()
{
    // nothing to do
    if(g_dl_handle == nullptr)
    {
        return OMI_True;
    }

    OMI_Bool success = OMI_True;
    if(g_shutdown_func != nullptr)
    {
        success = g_shutdown_func();
    }

    g_main_loop_func = nullptr;
    g_open_surface_func = nullptr;
    g_set_cursor_hidden_func = nullptr;
    g_is_cursor_hidden_func = nullptr;
    g_set_fullscreen_func = nullptr;
    g_is_fullscreen_func = nullptr;
    g_set_surface_position_func = nullptr;
    g_get_surface_position_func = nullptr;
    g_set_surface_size_func = nullptr;
    g_get_surface_size_func = nullptr;
    g_set_surface_title_func = nullptr;
    g_get_surface_title_func = nullptr;
    g_shutdown_func = nullptr;
    arc::dl::close_library(g_dl_handle);
    g_dl_handle = nullptr;

    return success;
}

//------------------------------------------------------------------------------
//                               GET SURFACE TITLE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static char const* get_surface_title()
{
    return g_get_surface_title_func();
}

//------------------------------------------------------------------------------
//                               SET SURFACE TITLE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void set_surface_title(char const* title)
{
    g_set_surface_title_func(title);
}

//------------------------------------------------------------------------------
//                                GET SURFACE SIZE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void get_surface_size(uint32_t* width, uint32_t* height)
{
    g_get_surface_size_func(width, height);
}

//------------------------------------------------------------------------------
//                                SET SURFACE SIZE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void set_surface_size(uint32_t width, uint32_t height)
{
    g_set_surface_size_func(width, height);
}

//------------------------------------------------------------------------------
//                              GET SURFACE POSITION
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void get_surface_position(int32_t* x, int32_t* y)
{
    g_get_surface_position_func(x, y);
}

//------------------------------------------------------------------------------
//                              SET SURFACE POSITION
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void set_surface_position(int32_t x, int32_t y)
{
    g_set_surface_position_func(x, y);
}

//------------------------------------------------------------------------------
//                                 IS FULLSCREEN
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool is_fullscreen()
{
    return g_is_fullscreen_func();
}

//------------------------------------------------------------------------------
//                                 SET FULLSCREEN
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void set_fullscreen(OMI_Bool state)
{
    return g_set_fullscreen_func(state);
}

//------------------------------------------------------------------------------
//                                IS CURSOR HIDDEN
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool is_cursor_hidden()
{
    return g_is_cursor_hidden_func();
}

//------------------------------------------------------------------------------
//                               SET CURSOR HIDDEN
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void set_cursor_hidden(OMI_Bool state)
{
    return g_set_cursor_hidden_func(state);
}

//------------------------------------------------------------------------------
//                                  OPEN SURFACE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool open_surface(OMI_REPORT_LoggerPtr logger_ptr)
{
    omi::report::Logger logger(logger_ptr);

    if(g_dl_handle == nullptr || g_open_surface_func == nullptr)
    {
        logger.critical(
            "Failed to open surface as host subsystem has not been correctly "
            "bound"
        );
        return OMI_False;
    }

    // get the initial setttings
    StringAttribute title_attr = config::Registry::instance().get_attr(
        "omicron.host.surface.title"
    );
    Int32Attribute size_attr = config::Registry::instance().get_attr(
        "omicron.host.surface.size"
    );
    Int32Attribute position_attr = config::Registry::instance().get_attr(
        "omicron.host.surface.position"
    );
    ByteAttribute fullscreen_attr = config::Registry::instance().get_attr(
        "omicron.host.surface.fullscreen"
    );
    bool fullscreen = fullscreen_attr.get_value() != 0;
    ByteAttribute hide_cursor_attr = config::Registry::instance().get_attr(
        "omicron.host.surface.hide_cursor"
    );

    // apply the initial settings
    g_set_surface_title_func(title_attr.get_value());
    Int32Attribute::ArrayType size = size_attr.get_values();
    g_set_surface_size_func(
        static_cast<uint32_t>(size[0]),
        static_cast<uint32_t>(size[1])
    );
    Int32Attribute::ArrayType position = position_attr.get_values();
    g_set_surface_position_func(position[0], position[1]);
    if(fullscreen)
    {
        g_set_fullscreen_func(fullscreen);
    }
    g_set_cursor_hidden_func(
        static_cast<OMI_Bool>(hide_cursor_attr.get_value())
    );

    // now open the surface
    return g_open_surface_func();
}

//------------------------------------------------------------------------------
//                                   MAIN LOOP
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool main_loop(
        OMI_HOST_EngineCycleFunc* engine_cycle_func,
        OMI_REPORT_LoggerPtr logger_ptr)
{
    omi::report::Logger logger(logger_ptr);

    if(g_dl_handle == nullptr || g_main_loop_func == nullptr)
    {
        logger.critical(
            "Failed to enter main loop as host subsystem has not been "
            "correctly bound"
        );
        return OMI_False;
    }

    g_main_loop_func(engine_cycle_func);

    return OMI_True;
}

} // namespace anonymous
} // namespace host
} // namespace omi

#include "omicron/api/host/_codegen/HostBinding.inl"
