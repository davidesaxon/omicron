/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_HOST_CODEGEN_HOSTINTERFACE_HPP_
#define OMI_HOST_CODEGEN_HOSTINTERFACE_HPP_

#include <cassert>
#include <string>

#include <arcanecore/base/dl/DLOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>
#include <arcanecore/base/lang/Restrictors.hpp>

#include "omicron/api/host/_codegen/HostCSymbols.h"


namespace omi
{
namespace host
{

class HostInterface
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                              TYPE DEFINITIONS
    //--------------------------------------------------------------------------

    typedef OMI_Bool (BindingFunc)(char const*, void**);

    //--------------------------------------------------------------------------
    //                             PUBLIC ATTRIBUTES
    //--------------------------------------------------------------------------

    // startup
    OMI_Bool (*startup)(OMI_REPORT_LoggerPtr);
    // shutdown
    OMI_Bool (*shutdown)();
    // get_surface_title
    char const* (*get_surface_title)();
    // set_surface_title
    void (*set_surface_title)(char const*);
    // get_surface_size
    void (*get_surface_size)(uint32_t*, uint32_t*);
    // set_surface_size
    void (*set_surface_size)(uint32_t, uint32_t);
    // get_surface_position
    void (*get_surface_position)(int32_t*, int32_t*);
    // set_surface_position
    void (*set_surface_position)(int32_t, int32_t);
    // is_fullscreen
    OMI_Bool (*is_fullscreen)();
    // set_fullscreen
    void (*set_fullscreen)(OMI_Bool);
    // is_cursor_hidden
    OMI_Bool (*is_cursor_hidden)();
    // set_cursor_hidden
    void (*set_cursor_hidden)(OMI_Bool);
    // open_surface
    OMI_Bool (*open_surface)(OMI_REPORT_LoggerPtr);
    // main_loop
    OMI_Bool (*main_loop)(OMI_HOST_EngineCycleFunc*, OMI_REPORT_LoggerPtr);

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    HostInterface(std::string const& libname)
        : startup(nullptr)
        , shutdown(nullptr)
        , get_surface_title(nullptr)
        , set_surface_title(nullptr)
        , get_surface_size(nullptr)
        , set_surface_size(nullptr)
        , get_surface_position(nullptr)
        , set_surface_position(nullptr)
        , is_fullscreen(nullptr)
        , set_fullscreen(nullptr)
        , is_cursor_hidden(nullptr)
        , set_cursor_hidden(nullptr)
        , open_surface(nullptr)
        , main_loop(nullptr)
    {
        #ifdef ARC_OS_WINDOWS
            arc::fsys::Path libpath({
                "build",
                "windows",
                "Release",
                libname + "_impl.dll"
            });
        #elif defined(ARC_OS_UNIX)
            arc::fsys::Path libpath({
                "build",
                "linux",
                "lib" + libname + "_impl.so"
            });
        #else
            assert(false);
        #endif

        arc::dl::Handle lib_handle = arc::dl::open_library(libpath);
        BindingFunc* binding_func = arc::dl::bind_symbol<BindingFunc>(lib_handle, "OMI_HOST_Host_BINDING_IMPL");

        if(binding_func(OMI_HOST_Host_startup_SYMBOL, (void**) &startup) != 0)
        {
        }
        if(binding_func(OMI_HOST_Host_shutdown_SYMBOL, (void**) &shutdown) != 0)
        {
        }
        if(binding_func(OMI_HOST_Host_get_surface_title_SYMBOL, (void**) &get_surface_title) != 0)
        {
        }
        if(binding_func(OMI_HOST_Host_set_surface_title_SYMBOL, (void**) &set_surface_title) != 0)
        {
        }
        if(binding_func(OMI_HOST_Host_get_surface_size_SYMBOL, (void**) &get_surface_size) != 0)
        {
        }
        if(binding_func(OMI_HOST_Host_set_surface_size_SYMBOL, (void**) &set_surface_size) != 0)
        {
        }
        if(binding_func(OMI_HOST_Host_get_surface_position_SYMBOL, (void**) &get_surface_position) != 0)
        {
        }
        if(binding_func(OMI_HOST_Host_set_surface_position_SYMBOL, (void**) &set_surface_position) != 0)
        {
        }
        if(binding_func(OMI_HOST_Host_is_fullscreen_SYMBOL, (void**) &is_fullscreen) != 0)
        {
        }
        if(binding_func(OMI_HOST_Host_set_fullscreen_SYMBOL, (void**) &set_fullscreen) != 0)
        {
        }
        if(binding_func(OMI_HOST_Host_is_cursor_hidden_SYMBOL, (void**) &is_cursor_hidden) != 0)
        {
        }
        if(binding_func(OMI_HOST_Host_set_cursor_hidden_SYMBOL, (void**) &set_cursor_hidden) != 0)
        {
        }
        if(binding_func(OMI_HOST_Host_open_surface_SYMBOL, (void**) &open_surface) != 0)
        {
        }
        if(binding_func(OMI_HOST_Host_main_loop_SYMBOL, (void**) &main_loop) != 0)
        {
        }
    }
};

} // namespace omi
} // namespace host


#endif