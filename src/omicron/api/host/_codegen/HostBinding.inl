/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#include <cstring>

#include "omicron/api/API.h"

#include "omicron/api/host/_codegen/HostCSymbols.h"


#ifdef __cplusplus
extern "C" {
#endif

OMI_API_EXPORT int OMI_HOST_Host_BINDING_IMPL(char const* func_def, void** func_ptr)
{
    if(strcmp(func_def, OMI_HOST_Host_startup_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::host::startup;
        return 0;
    }
    if(strcmp(func_def, OMI_HOST_Host_shutdown_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::host::shutdown;
        return 0;
    }
    if(strcmp(func_def, OMI_HOST_Host_get_surface_title_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::host::get_surface_title;
        return 0;
    }
    if(strcmp(func_def, OMI_HOST_Host_set_surface_title_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::host::set_surface_title;
        return 0;
    }
    if(strcmp(func_def, OMI_HOST_Host_get_surface_size_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::host::get_surface_size;
        return 0;
    }
    if(strcmp(func_def, OMI_HOST_Host_set_surface_size_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::host::set_surface_size;
        return 0;
    }
    if(strcmp(func_def, OMI_HOST_Host_get_surface_position_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::host::get_surface_position;
        return 0;
    }
    if(strcmp(func_def, OMI_HOST_Host_set_surface_position_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::host::set_surface_position;
        return 0;
    }
    if(strcmp(func_def, OMI_HOST_Host_is_fullscreen_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::host::is_fullscreen;
        return 0;
    }
    if(strcmp(func_def, OMI_HOST_Host_set_fullscreen_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::host::set_fullscreen;
        return 0;
    }
    if(strcmp(func_def, OMI_HOST_Host_is_cursor_hidden_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::host::is_cursor_hidden;
        return 0;
    }
    if(strcmp(func_def, OMI_HOST_Host_set_cursor_hidden_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::host::set_cursor_hidden;
        return 0;
    }
    if(strcmp(func_def, OMI_HOST_Host_open_surface_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::host::open_surface;
        return 0;
    }
    if(strcmp(func_def, OMI_HOST_Host_main_loop_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::host::main_loop;
        return 0;
    }

    return 1;
}

#ifdef __cplusplus
} // extern "C"
#endif

