/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_HOST_CODEGEN_HOSTCSMYBOLS_H_
#define OMI_HOST_CODEGEN_HOSTCSMYBOLS_H_


#ifdef __cplusplus
extern "C" {
#endif

// OMI_HOST_startup
static char const* OMI_HOST_Host_startup_SYMBOL = "OMI_Bool OMI_HOST_Host_startup(OMI_REPORT_LoggerPtr)";

// OMI_HOST_shutdown
static char const* OMI_HOST_Host_shutdown_SYMBOL = "OMI_Bool OMI_HOST_Host_shutdown()";

// OMI_HOST_get_surface_title
static char const* OMI_HOST_Host_get_surface_title_SYMBOL = "char const* OMI_HOST_Host_get_surface_title()";

// OMI_HOST_set_surface_title
static char const* OMI_HOST_Host_set_surface_title_SYMBOL = "void OMI_HOST_Host_set_surface_title(char const*)";

// OMI_HOST_get_surface_size
static char const* OMI_HOST_Host_get_surface_size_SYMBOL = "void OMI_HOST_Host_get_surface_size(uint32_t*, uint32_t*)";

// OMI_HOST_set_surface_size
static char const* OMI_HOST_Host_set_surface_size_SYMBOL = "void OMI_HOST_Host_set_surface_size(uint32_t, uint32_t)";

// OMI_HOST_get_surface_position
static char const* OMI_HOST_Host_get_surface_position_SYMBOL = "void OMI_HOST_Host_get_surface_position(int32_t*, int32_t*)";

// OMI_HOST_set_surface_position
static char const* OMI_HOST_Host_set_surface_position_SYMBOL = "void OMI_HOST_Host_set_surface_position(int32_t, int32_t)";

// OMI_HOST_is_fullscreen
static char const* OMI_HOST_Host_is_fullscreen_SYMBOL = "OMI_Bool OMI_HOST_Host_is_fullscreen()";

// OMI_HOST_set_fullscreen
static char const* OMI_HOST_Host_set_fullscreen_SYMBOL = "void OMI_HOST_Host_set_fullscreen(OMI_Bool)";

// OMI_HOST_is_cursor_hidden
static char const* OMI_HOST_Host_is_cursor_hidden_SYMBOL = "OMI_Bool OMI_HOST_Host_is_cursor_hidden()";

// OMI_HOST_set_cursor_hidden
static char const* OMI_HOST_Host_set_cursor_hidden_SYMBOL = "void OMI_HOST_Host_set_cursor_hidden(OMI_Bool)";

// OMI_HOST_open_surface
static char const* OMI_HOST_Host_open_surface_SYMBOL = "OMI_Bool OMI_HOST_Host_open_surface(OMI_REPORT_LoggerPtr)";

// OMI_HOST_main_loop
static char const* OMI_HOST_Host_main_loop_SYMBOL = "OMI_Bool OMI_HOST_Host_main_loop(OMI_HOST_EngineCycleFunc*, OMI_REPORT_LoggerPtr)";

#ifdef __cplusplus
} // extern "C"
#endif

#endif