/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/config/Registry.hpp"

namespace omi
{
namespace config
{

//------------------------------------------------------------------------------
//                            PUBLIC STATIC FUNCTIONS
//------------------------------------------------------------------------------

Registry& Registry::instance()
{
    static Registry inst;
    return inst;
}

ConfigInterface& Registry::get_interface()
{
    static ConfigInterface inst("omicron_api");
    return inst;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

bool Registry::has(std::string const& name) const
{
    OMI_CONFIG_SettingPtr setting = nullptr;
    return
        get_interface().Registry_get(name.c_str(), &setting) !=
        OMI_CONFIG_Registry_Error_kKey;
}

Setting Registry::get(std::string const& name) const
{
    OMI_CONFIG_SettingPtr setting = nullptr;
    OMI_CONFIG_Registry_Error ec =
        get_interface().Registry_get(name.c_str(), &setting);

    if(ec == OMI_CONFIG_Registry_Error_kKey)
    {
        throw arc::ex::KeyError(
            "No setting existing with name \"" + name + "\""
        );
    }

    return Setting(setting, true);
}

Attribute Registry::get_attr(std::string const& name) const
{
    OMI_CONFIG_SettingPtr c_setting = nullptr;
    OMI_CONFIG_Registry_Error ec =
        get_interface().Registry_get(name.c_str(), &c_setting);

    if(ec == OMI_CONFIG_Registry_Error_kKey)
    {
        return Attribute();
    }

    Setting setting(c_setting, true);
    return setting.get_attr();
}

//------------------------------------------------------------------------------
//                              PRIVATE CONSTRUCTOR
//------------------------------------------------------------------------------

Registry::Registry()
{
}

//------------------------------------------------------------------------------
//                               PRIVATE DESTRUCTOR
//------------------------------------------------------------------------------

Registry::~Registry()
{
}

} // namespace config
} // namespace omi
