/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstddef>
#include <cstring>
#include <fstream>
#include <string>
#include <unordered_map>

#include <json/json.h>

#include <arcanecore/base/fsys/FileSystemOperations.hpp>

#include "omicron/api/API.h"
#include "omicron/api/common/Attributes.hpp"
#include "omicron/api/common/MapBuilder.hpp"
#include "omicron/api/config/ConfigCTypes.h"


//------------------------------------------------------------------------------
//                                 SETTING STRUCT
//------------------------------------------------------------------------------

struct OMI_CONFIG_SettingStruct
{
    //---------------------------A T T R I B U T E S----------------------------

    std::size_t m_ref_count;
    omi::Attribute m_attr;
    std::size_t m_size;
    std::string m_description;

    //--------------------------C O N S T R U C T O R---------------------------

    OMI_CONFIG_SettingStruct(
            omi::Attribute attr,
            OMI_Size size,
            std::string const& description)
        : m_ref_count  (1)
        , m_attr       (attr)
        , m_size       (size)
        , m_description(description)
    {
    }
};


OMI_CODEGEN_NAMESPACE namespace omi
{
OMI_CODEGEN_NAMESPACE namespace config
{
namespace
{

//------------------------------------------------------------------------------
//                                    GLOBALS
//------------------------------------------------------------------------------

static std::string g_error_message;

static std::string g_warnings;

static std::unordered_map<std::string, OMI_CONFIG_SettingStruct*> g_settings;

//------------------------------------------------------------------------------
//                                   FUNCTIONS
//------------------------------------------------------------------------------

static Attribute parse_default_setting(
        Json::Value& root,
        arc::fsys::Path const& path,
        std::string& out_name);

static void append_warning(std::string const& warning)
{
    if(!g_warnings.empty())
    {
        g_warnings += "\n";
    }
    g_warnings += "\t> " + warning;
}

static MapAttribute parse_map(
        Json::Value& root,
        Json::Value& value_field,
        arc::fsys::Path const& path,
        std::string const& name)
{
    MapBuilder builder;
    // iterate over the array
    for(std::size_t i = 0; i < value_field.size(); ++i)
    {
        Json::Value sub_value = value_field[static_cast<Json::ArrayIndex>(i)];
        if(!sub_value.isObject())
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse config from file \"" + path.to_native() +
                "\" as map value field contains non-object entry: " +
                writer.write(root)
            );
        }

        std::string child_name = name;
        Attribute attr = parse_default_setting(sub_value, path, child_name);
        builder.insert(child_name, attr);
    }

    return builder.build();
}

static void byte_data_delete(void const* data, std::size_t)
{
    if(data != nullptr)
    {
        delete[] (ByteAttribute::DataType const*) data;
    }
}

static ByteAttribute parse_byte(
        Json::Value& root,
        Json::Value& value_field,
        arc::fsys::Path const& path)
{
    std::size_t const size = value_field.size();
    ByteAttribute::DataType* data = new ByteAttribute::DataType[size];
    for(std::size_t i = 0; i < value_field.size(); ++i)
    {
        Json::Value sub_value = value_field[static_cast<Json::ArrayIndex>(i)];
        if(!sub_value.isIntegral())
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse config from file \"" + path.to_native() +
                "\" as byte value field contains non-integral entry: " +
                writer.write(root)
            );
            delete[] data;
        }

        data[i] = static_cast<ByteAttribute::DataType>(sub_value.asInt());
    }

    return ByteAttribute(
        data,
        size,
        1,
        &byte_data_delete
    );
}

static void int16_data_delete(void const* data, std::size_t)
{
    if(data != nullptr)
    {
        delete[] (Int16Attribute::DataType const*) data;
    }
}

static Int16Attribute parse_int16(
        Json::Value& root,
        Json::Value& value_field,
        arc::fsys::Path const& path)
{
    std::size_t const size = value_field.size();
    Int16Attribute::DataType* data = new Int16Attribute::DataType[size];
    for(std::size_t i = 0; i < value_field.size(); ++i)
    {
        Json::Value sub_value = value_field[static_cast<Json::ArrayIndex>(i)];
        if(!sub_value.isIntegral())
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse config from file \"" + path.to_native() +
                "\" as int16 value field contains non-integral entry: " +
                writer.write(root)
            );
            delete[] data;
        }

        data[i] = static_cast<Int16Attribute::DataType>(sub_value.asInt());
    }

    return Int16Attribute(
        data,
        size,
        1,
        &int16_data_delete
    );
}

static void int32_data_delete(void const* data, std::size_t)
{
    if(data != nullptr)
    {
        delete[] (Int32Attribute::DataType const*) data;
    }
}

static Int32Attribute parse_int32(
        Json::Value& root,
        Json::Value& value_field,
        arc::fsys::Path const& path)
{
    std::size_t const size = value_field.size();
    Int32Attribute::DataType* data = new Int32Attribute::DataType[size];
    for(std::size_t i = 0; i < value_field.size(); ++i)
    {
        Json::Value sub_value = value_field[static_cast<Json::ArrayIndex>(i)];
        if(!sub_value.isIntegral())
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse config from file \"" + path.to_native() +
                "\" as int32 value field contains non-integral entry: " +
                writer.write(root)
            );
            delete[] data;
        }

        data[i] = static_cast<Int32Attribute::DataType>(sub_value.asInt());
    }

    return Int32Attribute(
        data,
        size,
        1,
        &int32_data_delete
    );
}

static void int64_data_delete(void const* data, std::size_t)
{
    if(data != nullptr)
    {
        delete[] (Int64Attribute::DataType const*) data;
    }
}

static Int64Attribute parse_int64(
        Json::Value& root,
        Json::Value& value_field,
        arc::fsys::Path const& path)
{
    std::size_t const size = value_field.size();
    Int64Attribute::DataType* data = new Int64Attribute::DataType[size];
    for(std::size_t i = 0; i < value_field.size(); ++i)
    {
        Json::Value sub_value = value_field[static_cast<Json::ArrayIndex>(i)];
        if(!sub_value.isIntegral())
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse config from file \"" + path.to_native() +
                "\" as int64 value field contains non-integral entry: " +
                writer.write(root)
            );
            delete[] data;
        }

        data[i] = static_cast<Int64Attribute::DataType>(sub_value.asInt64());
    }

    return Int64Attribute(
        data,
        size,
        1,
        &int64_data_delete
    );
}

static void float32_data_delete(void const* data, std::size_t)
{
    if(data != nullptr)
    {
        delete[] (Float32Attribute::DataType const*) data;
    }
}

static Float32Attribute parse_float32(
        Json::Value& root,
        Json::Value& value_field,
        arc::fsys::Path const& path)
{
    std::size_t const size = value_field.size();
    Float32Attribute::DataType* data = new Float32Attribute::DataType[size];
    for(std::size_t i = 0; i < value_field.size(); ++i)
    {
        Json::Value sub_value = value_field[static_cast<Json::ArrayIndex>(i)];
        if(!sub_value.isDouble())
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse config from file \"" + path.to_native() +
                "\" as float32 value field contains non floating point "
                "entry: " + writer.write(root)
            );
            delete[] data;
        }

        data[i] = static_cast<Float32Attribute::DataType>(sub_value.asFloat());
    }

    return Float32Attribute(
        data,
        size,
        1,
        &float32_data_delete
    );
}

static void float64_data_delete(void const* data, std::size_t)
{
    if(data != nullptr)
    {
        delete[] (Float64Attribute::DataType const*) data;
    }
}

static Float64Attribute parse_float64(
        Json::Value& root,
        Json::Value& value_field,
        arc::fsys::Path const& path)
{
    std::size_t const size = value_field.size();
    Float64Attribute::DataType* data = new Float64Attribute::DataType[size];
    for(std::size_t i = 0; i < value_field.size(); ++i)
    {
        Json::Value sub_value = value_field[static_cast<Json::ArrayIndex>(i)];
        if(!sub_value.isDouble())
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse config from file \"" + path.to_native() +
                "\" as float64 value field contains non floating point "
                "entry: " + writer.write(root)
            );
            delete[] data;
        }

        data[i] = static_cast<Float64Attribute::DataType>(sub_value.asDouble());
    }

    return Float64Attribute(
        data,
        size,
        1,
        &float64_data_delete
    );
}

static Float64Attribute parse_string(
        Json::Value& root,
        Json::Value& value_field,
        arc::fsys::Path const& path)
{
    std::size_t const size = value_field.size();
    std::vector<std::string> data;
    data.reserve(size);

    for(std::size_t i = 0; i < value_field.size(); ++i)
    {
        Json::Value sub_value = value_field[static_cast<Json::ArrayIndex>(i)];
        if(!sub_value.isString())
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse config from file \"" + path.to_native() +
                "\" as float64 value field contains non string entry: " +
                writer.write(root)
            );
        }

        data.push_back(sub_value.asString());
    }

    return StringAttribute(data);
}

static Attribute parse_default_setting(
        Json::Value& root,
        arc::fsys::Path const& path,
        std::string& out_name)
{
    // ensure the value is an object
    if(!root.isObject())
    {
        Json::FastWriter writer;
        throw arc::ex::ParseError(
            "Failed to parse default config from file \"" + path.to_native() +
            "\" encountered non object map entry: " + writer.write(root)
        );
    }

    // get the name
    if(!root.isMember("name"))
    {
        Json::FastWriter writer;
        throw arc::ex::ParseError(
            "Failed to parse default config from file \"" + path.to_native() +
            "\" value object is missing name field: " + writer.write(root)
        );
    }
    Json::Value name_field = root["name"];
    if(!name_field.isString())
    {
        Json::FastWriter writer;
        throw arc::ex::ParseError(
            "Failed to parse default config from file \"" + path.to_native() +
            "\" as name field is not of type string: " + writer.write(root)
        );
    }
    out_name = out_name + "." + name_field.asString();

    // get size?
    std::size_t size = OMI_CONFIG_Setting_NullSize;
    if(root.isMember("size"))
    {
        Json::Value size_field = root["size"];
        if(!size_field.isIntegral())
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse default config from file \"" +
                path.to_native() + "\" as size field is not an integral "
                "type: " + writer.write(root)
            );
        }
        size = static_cast<std::size_t>(size_field.asUInt64());
    }

    // get the value field
    if(!root.isMember("value"))
    {
        Json::FastWriter writer;
        throw arc::ex::ParseError(
            "Failed to parse default config from file \"" + path.to_native() +
            "\" as object is missing value field: " + writer.write(root)
        );
    }
    Json::Value value_field = root["value"];
    if(!value_field.isArray())
    {
        Json::FastWriter writer;
        throw arc::ex::ParseError(
            "Failed to parse default config from file \"" + path.to_native() +
            "\" as value field is not of type array: " + writer.write(root)
        );
    }

    // get description
    std::string description;
    if(root.isMember("description"))
    {
        Json::Value description_field = root["description"];
        if(!description_field.isString())
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse default config from file \"" +
                path.to_native() + "\" as description field is not of type "
                "string: " + writer.write(root)
            );
        }
        description = description_field.asString();
    }

    // get the type
    if(!root.isMember("type"))
    {
        Json::FastWriter writer;
        throw arc::ex::ParseError(
            "Failed to parse default config from file \"" + path.to_native() +
            "\" value object is missing type field: " + writer.write(root)
        );
    }
    Json::Value type_field = root["type"];
    if(!type_field.isString())
    {
        Json::FastWriter writer;
        throw arc::ex::ParseError(
            "Failed to parse default config from file \"" + path.to_native() +
            "\" as type field is not of type string: " + writer.write(root)
        );
    }
    std::string const type = type_field.asString();
    Attribute attr;
    if(type == "map")
    {
        attr = parse_map(root, value_field, path, out_name);
    }
    else if(type == "byte")
    {
        attr = parse_byte(root, value_field, path);
    }
    else if(type == "int16")
    {
        attr = parse_int16(root, value_field, path);
    }
    else if(type == "int32")
    {
        attr = parse_int32(root, value_field, path);
    }
    else if(type == "int64")
    {
        attr = parse_int64(root, value_field, path);
    }
    else if(type == "float32")
    {
        attr = parse_float32(root, value_field, path);
    }
    else if(type == "float64")
    {
        attr = parse_float64(root, value_field, path);
    }
    else if(type == "string")
    {
        attr = parse_string(root, value_field, path);
    }
    else
    {
        Json::FastWriter writer;
        throw arc::ex::ParseError(
            "Failed to parse default config from file \"" + path.to_native() +
            "\" as type field contains an unrecognised value: " +
            writer.write(root)
        );
    }

    // construct and store the setting
    g_settings[out_name] =
        new OMI_CONFIG_SettingStruct(attr, size, description);

    return attr;
}

static Attribute load_default_file(
        arc::fsys::Path const& path,
        std::string& out_name)
{
    // read the file
    std::ifstream input(path.to_native());
    std::string contents;
    input.seekg(0, std::ios::end);
    contents.reserve(input.tellg());
    input.seekg(0, std::ios::beg);
    contents.assign(
        (std::istreambuf_iterator<char>(input)),
        std::istreambuf_iterator<char>()
    );
    input.close();

    // read the root JSON value
    Json::Value root;
    Json::Reader reader;
    bool parse_sucess = reader.parse(contents, root);
    if(!parse_sucess)
    {
        throw arc::ex::ParseError(
            "Failed to parse default config from file \"" + path.to_native() +
            "\" with error: " + reader.getFormattedErrorMessages()
        );
    }

    // ensure root is an object
    if(!root.isObject())
    {
        throw arc::ex::ParseError(
            "Failed to parse default config from file \"" + path.to_native() +
            "\" because the root value is not a map "
        );
    }

    return parse_default_setting(root, path, out_name);
}

static MapAttribute load_default_directory(
        arc::fsys::Path const& path,
        std::string const& name)
{
    MapBuilder builder;

    // iterate over the files in this directory
    for(arc::fsys::Path const& subpath : arc::fsys::list(path))
    {
        // recurse if this is a directory
        if(arc::fsys::is_directory(subpath))
        {
            std::string child_name = subpath.back();
            if(!name.empty())
            {
                child_name = name + "." + child_name;
            }
            builder.insert(
                child_name,
                load_default_directory(subpath, child_name)
            );
        }
        // read if this is a JSON file
        else if(subpath.get_extension() == "json")
        {
            std::string child_name = name;
            Attribute attr = load_default_file(subpath, child_name);
            builder.insert(child_name, attr);
        }
        else
        {
            append_warning(
                "Ignoring unrecognised file format in config default "
                "directory: " + subpath.to_native()
            );
        }
    }

    MapAttribute attr = builder.build();

    g_settings[name] = new OMI_CONFIG_SettingStruct(
        attr,
        OMI_CONFIG_Setting_NullSize,
        ""
    );

    return attr;
}

static void load_override_file(
        arc::fsys::Path const& path,
        std::string const& name)
{
    // read the file
    std::ifstream input(path.to_native());
    std::string contents;
    input.seekg(0, std::ios::end);
    contents.reserve(input.tellg());
    input.seekg(0, std::ios::beg);
    contents.assign(
        (std::istreambuf_iterator<char>(input)),
        std::istreambuf_iterator<char>()
    );
    input.close();

    // read the root JSON value
    Json::Value root;
    Json::Reader reader;
    bool parse_sucess = reader.parse(contents, root);
    if(!parse_sucess)
    {
        throw arc::ex::ParseError(
            "Failed to parse override config from file \"" + path.to_native() +
            "\" with error: " + reader.getFormattedErrorMessages()
        );
    }

    // ensure root is an object
    if(!root.isObject())
    {
        throw arc::ex::ParseError(
            "Failed to parse override config from file \"" + path.to_native() +
            "\" because the root value is not a map "
        );
    }

    // iterate through the key-value pairs
    for(std::string const& key : root.getMemberNames())
    {
        // build the full name
        std::string setting_name = key;
        if(!name.empty())
        {
            setting_name = name + "." + setting_name;
        }

        // ensure the setting exists
        auto f_setting = g_settings.find(setting_name);
        if(f_setting == g_settings.end())
        {
            append_warning(
                "Override specify for setting \"" + setting_name +
                "\" which does not exist. From file: " + path.to_native()
            );
        }

        // get the value
        Json::Value value = root[key];

        // get the type of the setting
        Attribute attr;
        Attribute::Type setting_type = f_setting->second->m_attr.get_type();
        switch(setting_type)
        {
            case Attribute::Type::kByte:
            {
                attr = parse_byte(value, value, path);
                break;
            }

            case Attribute::Type::kInt16:
            {
                attr = parse_int16(value, value, path);
                break;
            }
            case Attribute::Type::kInt32:
            {
                attr = parse_int32(value, value, path);
                break;
            }
            case Attribute::Type::kInt64:
            {
                attr = parse_int64(value, value, path);
                break;
            }
            case Attribute::Type::kFloat32:
            {
                attr = parse_float32(value, value, path);
                break;
            }
            case Attribute::Type::kFloat64:
            {
                attr = parse_float64(value, value, path);
                break;
            }
            case Attribute::Type::kString:
            {
                attr = parse_string(value, value, path);
                break;
            }
            default:
            {
                append_warning(
                    "Attempted to set override for setting which has a "
                    "non-overrideable type: " + setting_name
                );
                break;
            }
        }
        DataAttribute data_attr = attr;

        if(f_setting->second->m_size != OMI_CONFIG_Setting_NullSize &&
           f_setting->second->m_size != data_attr.size())
        {
            append_warning(
                "Attempted to set override with an invalid size for a setting "
                " which has an explicit size: " + setting_name
            );
        }
        else
        {
            // set the override
            f_setting->second->m_attr = data_attr;
        }
    }
}

static void load_override_directory(
        arc::fsys::Path const& path,
        std::string const& name)
{
    // iterate over the files in this directory
    for(arc::fsys::Path const& subpath : arc::fsys::list(path))
    {
        // recurse if this is a directory
        if(arc::fsys::is_directory(subpath))
        {
            std::string child_name = subpath.back();
            if(!name.empty())
            {
                child_name = name + "." + child_name;
            }
            load_override_directory(subpath, child_name);
        }
        // read if this is a JSON file
        else if(subpath.get_extension() == "json")
        {
            load_override_file(subpath, name);
        }
        else
        {
            append_warning(
                "Ignoring unrecognised file format in config override "
                "directory: " + subpath.to_native()
            );
        }
    }
}

//------------------------------------------------------------------------------
//                               GET ERROR MESSAGE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static char const* get_error_message()
{
    return g_error_message.c_str();
}

//------------------------------------------------------------------------------
//                                  GET WARNINGS
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static char const* get_warnings()
{
    return g_warnings.c_str();
}

//------------------------------------------------------------------------------
//                           SETTING INCREASE REFERENCE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Setting_increase_reference(OMI_CONFIG_SettingPtr ptr)
{
    ++ptr->m_ref_count;
}

//------------------------------------------------------------------------------
//                           SETTING DECREASE REFERENCE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Setting_decrease_reference(OMI_CONFIG_SettingPtr ptr)
{
    if(ptr == nullptr)
    {
        return;
    }

    if(ptr->m_ref_count == 1)
    {
        delete ptr;
    }
    else
    {
        --ptr->m_ref_count;
    }
}

//------------------------------------------------------------------------------
//                                SETTING GET ATTR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_AttributePtr Setting_get_attr(OMI_CONFIG_SettingPtrConst ptr)
{
    return ptr->m_attr.get_ptr();
}

//------------------------------------------------------------------------------
//                                    GET SIZE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size Setting_get_size(OMI_CONFIG_SettingPtrConst ptr)
{
    return ptr->m_size;
}

//------------------------------------------------------------------------------
//                                GET DESCRIPTION
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static char const* Setting_get_description(OMI_CONFIG_SettingPtrConst ptr)
{
    return ptr->m_description.c_str();
}

//------------------------------------------------------------------------------
//                              CONFIG MANAGER LOAD
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_CONFIG_Registry_Error Registry_load()
{
    g_warnings = "";

    arc::fsys::Path const default_root_path({"config", "default"});
    // ensure the directory exists
    if(!arc::fsys::exists(default_root_path))
    {
        g_error_message =
            "Default config root directory does not exist: " +
            default_root_path.to_native();
        return OMI_CONFIG_Registry_Error_kIO;
    }
    if(!arc::fsys::is_directory(default_root_path))
    {
        g_error_message =
            "Default config root directory is not a directory: " +
            default_root_path.to_native();
        return OMI_CONFIG_Registry_Error_kIO;
    }

    try
    {
        load_default_directory(default_root_path, "");
    }
    catch(std::exception const& exc)
    {
        g_error_message = exc.what();
        return OMI_CONFIG_Registry_Error_kParse;
    }

    // TODO: read multiple from environment variable?
    arc::fsys::Path const override_root_path({"config", "dev"});
    // ensure the directory exists
    if(!arc::fsys::exists(override_root_path))
    {
        g_error_message =
            "Override config root directory does not exist: " +
            override_root_path.to_native();
        return OMI_CONFIG_Registry_Error_kIO;
    }
    if(!arc::fsys::is_directory(override_root_path))
    {
        g_error_message =
            "Override config root directory is not a directory: " +
            override_root_path.to_native();
        return OMI_CONFIG_Registry_Error_kIO;
    }

    try
    {
        load_override_directory(override_root_path, "");
    }
    catch(std::exception const& exc)
    {
        g_error_message = exc.what();
        return OMI_CONFIG_Registry_Error_kParse;
    }

    return OMI_CONFIG_Registry_Error_kNone;
}

OMI_CODEGEN_FUNCTION
static void Registry_destroy()
{
    // decrease the references of settings
    for(auto& setting : g_settings)
    {
        Setting_decrease_reference(setting.second);
    }
    g_settings.clear();
}

OMI_CODEGEN_FUNCTION
static OMI_CONFIG_Registry_Error Registry_get(
        char const* name,
        OMI_CONFIG_SettingPtr* out_setting)
{
    auto f_setting = g_settings.find(name);
    if(f_setting == g_settings.end())
    {
        return OMI_CONFIG_Registry_Error_kKey;
    }

    *out_setting = f_setting->second;
    return OMI_CONFIG_Registry_Error_kNone;
}

} // namespace anonymous
} // namespace config
} // namespace omi

#include "omicron/api/config/_codegen/ConfigBinding.inl"
