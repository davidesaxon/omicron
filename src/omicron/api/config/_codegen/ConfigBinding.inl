/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#include <cstring>

#include "omicron/api/API.h"

#include "omicron/api/config/_codegen/ConfigCSymbols.h"


#ifdef __cplusplus
extern "C" {
#endif

OMI_API_EXPORT int OMI_CONFIG_Config_BINDING_IMPL(char const* func_def, void** func_ptr)
{
    if(strcmp(func_def, OMI_CONFIG_Config_get_error_message_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::config::get_error_message;
        return 0;
    }
    if(strcmp(func_def, OMI_CONFIG_Config_get_warnings_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::config::get_warnings;
        return 0;
    }
    if(strcmp(func_def, OMI_CONFIG_Config_Setting_increase_reference_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::config::Setting_increase_reference;
        return 0;
    }
    if(strcmp(func_def, OMI_CONFIG_Config_Setting_decrease_reference_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::config::Setting_decrease_reference;
        return 0;
    }
    if(strcmp(func_def, OMI_CONFIG_Config_Setting_get_attr_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::config::Setting_get_attr;
        return 0;
    }
    if(strcmp(func_def, OMI_CONFIG_Config_Setting_get_size_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::config::Setting_get_size;
        return 0;
    }
    if(strcmp(func_def, OMI_CONFIG_Config_Setting_get_description_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::config::Setting_get_description;
        return 0;
    }
    if(strcmp(func_def, OMI_CONFIG_Config_Registry_load_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::config::Registry_load;
        return 0;
    }
    if(strcmp(func_def, OMI_CONFIG_Config_Registry_destroy_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::config::Registry_destroy;
        return 0;
    }
    if(strcmp(func_def, OMI_CONFIG_Config_Registry_get_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::config::Registry_get;
        return 0;
    }

    return 1;
}

#ifdef __cplusplus
} // extern "C"
#endif

