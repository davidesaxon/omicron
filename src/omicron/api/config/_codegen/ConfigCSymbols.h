/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_CONFIG_CODEGEN_CONFIGCSMYBOLS_H_
#define OMI_CONFIG_CODEGEN_CONFIGCSMYBOLS_H_


#ifdef __cplusplus
extern "C" {
#endif

// OMI_CONFIG_get_error_message
static char const* OMI_CONFIG_Config_get_error_message_SYMBOL = "char const* OMI_CONFIG_Config_get_error_message()";

// OMI_CONFIG_get_warnings
static char const* OMI_CONFIG_Config_get_warnings_SYMBOL = "char const* OMI_CONFIG_Config_get_warnings()";

// OMI_CONFIG_Setting_increase_reference
static char const* OMI_CONFIG_Config_Setting_increase_reference_SYMBOL = "void OMI_CONFIG_Config_Setting_increase_reference(OMI_CONFIG_SettingPtr)";

// OMI_CONFIG_Setting_decrease_reference
static char const* OMI_CONFIG_Config_Setting_decrease_reference_SYMBOL = "void OMI_CONFIG_Config_Setting_decrease_reference(OMI_CONFIG_SettingPtr)";

// OMI_CONFIG_Setting_get_attr
static char const* OMI_CONFIG_Config_Setting_get_attr_SYMBOL = "OMI_AttributePtr OMI_CONFIG_Config_Setting_get_attr(OMI_CONFIG_SettingPtrConst)";

// OMI_CONFIG_Setting_get_size
static char const* OMI_CONFIG_Config_Setting_get_size_SYMBOL = "OMI_Size OMI_CONFIG_Config_Setting_get_size(OMI_CONFIG_SettingPtrConst)";

// OMI_CONFIG_Setting_get_description
static char const* OMI_CONFIG_Config_Setting_get_description_SYMBOL = "char const* OMI_CONFIG_Config_Setting_get_description(OMI_CONFIG_SettingPtrConst)";

// OMI_CONFIG_Registry_load
static char const* OMI_CONFIG_Config_Registry_load_SYMBOL = "OMI_CONFIG_Registry_Error OMI_CONFIG_Config_Registry_load()";

// OMI_CONFIG_Registry_destroy
static char const* OMI_CONFIG_Config_Registry_destroy_SYMBOL = "void OMI_CONFIG_Config_Registry_destroy()";

// OMI_CONFIG_Registry_get
static char const* OMI_CONFIG_Config_Registry_get_SYMBOL = "OMI_CONFIG_Registry_Error OMI_CONFIG_Config_Registry_get(char const*, OMI_CONFIG_SettingPtr*)";

#ifdef __cplusplus
} // extern "C"
#endif

#endif