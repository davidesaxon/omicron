/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_CONFIG_CODEGEN_CONFIGINTERFACE_HPP_
#define OMI_CONFIG_CODEGEN_CONFIGINTERFACE_HPP_

#include <cassert>
#include <string>

#include <arcanecore/base/dl/DLOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>
#include <arcanecore/base/lang/Restrictors.hpp>

#include "omicron/api/config/_codegen/ConfigCSymbols.h"


namespace omi
{
namespace config
{

class ConfigInterface
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                              TYPE DEFINITIONS
    //--------------------------------------------------------------------------

    typedef OMI_Bool (BindingFunc)(char const*, void**);

    //--------------------------------------------------------------------------
    //                             PUBLIC ATTRIBUTES
    //--------------------------------------------------------------------------

    // get_error_message
    char const* (*get_error_message)();
    // get_warnings
    char const* (*get_warnings)();
    // Setting_increase_reference
    void (*Setting_increase_reference)(OMI_CONFIG_SettingPtr);
    // Setting_decrease_reference
    void (*Setting_decrease_reference)(OMI_CONFIG_SettingPtr);
    // Setting_get_attr
    OMI_AttributePtr (*Setting_get_attr)(OMI_CONFIG_SettingPtrConst);
    // Setting_get_size
    OMI_Size (*Setting_get_size)(OMI_CONFIG_SettingPtrConst);
    // Setting_get_description
    char const* (*Setting_get_description)(OMI_CONFIG_SettingPtrConst);
    // Registry_load
    OMI_CONFIG_Registry_Error (*Registry_load)();
    // Registry_destroy
    void (*Registry_destroy)();
    // Registry_get
    OMI_CONFIG_Registry_Error (*Registry_get)(char const*, OMI_CONFIG_SettingPtr*);

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    ConfigInterface(std::string const& libname)
        : get_error_message(nullptr)
        , get_warnings(nullptr)
        , Setting_increase_reference(nullptr)
        , Setting_decrease_reference(nullptr)
        , Setting_get_attr(nullptr)
        , Setting_get_size(nullptr)
        , Setting_get_description(nullptr)
        , Registry_load(nullptr)
        , Registry_destroy(nullptr)
        , Registry_get(nullptr)
    {
        #ifdef ARC_OS_WINDOWS
            arc::fsys::Path libpath({
                "build",
                "windows",
                "Release",
                libname + "_impl.dll"
            });
        #elif defined(ARC_OS_UNIX)
            arc::fsys::Path libpath({
                "build",
                "linux",
                "lib" + libname + "_impl.so"
            });
        #else
            assert(false);
        #endif

        arc::dl::Handle lib_handle = arc::dl::open_library(libpath);
        BindingFunc* binding_func = arc::dl::bind_symbol<BindingFunc>(lib_handle, "OMI_CONFIG_Config_BINDING_IMPL");

        if(binding_func(OMI_CONFIG_Config_get_error_message_SYMBOL, (void**) &get_error_message) != 0)
        {
        }
        if(binding_func(OMI_CONFIG_Config_get_warnings_SYMBOL, (void**) &get_warnings) != 0)
        {
        }
        if(binding_func(OMI_CONFIG_Config_Setting_increase_reference_SYMBOL, (void**) &Setting_increase_reference) != 0)
        {
        }
        if(binding_func(OMI_CONFIG_Config_Setting_decrease_reference_SYMBOL, (void**) &Setting_decrease_reference) != 0)
        {
        }
        if(binding_func(OMI_CONFIG_Config_Setting_get_attr_SYMBOL, (void**) &Setting_get_attr) != 0)
        {
        }
        if(binding_func(OMI_CONFIG_Config_Setting_get_size_SYMBOL, (void**) &Setting_get_size) != 0)
        {
        }
        if(binding_func(OMI_CONFIG_Config_Setting_get_description_SYMBOL, (void**) &Setting_get_description) != 0)
        {
        }
        if(binding_func(OMI_CONFIG_Config_Registry_load_SYMBOL, (void**) &Registry_load) != 0)
        {
        }
        if(binding_func(OMI_CONFIG_Config_Registry_destroy_SYMBOL, (void**) &Registry_destroy) != 0)
        {
        }
        if(binding_func(OMI_CONFIG_Config_Registry_get_SYMBOL, (void**) &Registry_get) != 0)
        {
        }
    }
};

} // namespace omi
} // namespace config


#endif