/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/config/Registry.hpp"
#include "omicron/api/config/Setting.hpp"


namespace omi
{
namespace config
{

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

Setting::Setting(Setting const& other)
    : m_ptr(other.m_ptr)
{
    Registry::get_interface().Setting_increase_reference(m_ptr);
}

Setting::Setting(Setting&& other)
    : m_ptr(other.m_ptr)
{
    other.m_ptr = nullptr;
}

Setting::Setting(OMI_CONFIG_SettingPtr ptr, bool increase_ref)
    : m_ptr(ptr)
{
    if(increase_ref)
    {
        Registry::get_interface().Setting_increase_reference(m_ptr);
    }
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

Setting::~Setting()
{
    Registry::get_interface().Setting_decrease_reference(m_ptr);
}

//------------------------------------------------------------------------------
//                                   OPERATORS
//------------------------------------------------------------------------------

Setting& Setting::operator=(const Setting& other)
{
    if(m_ptr != other.m_ptr)
    {
        Registry::get_interface().Setting_decrease_reference(m_ptr);
        m_ptr = other.m_ptr;
        Registry::get_interface().Setting_increase_reference(m_ptr);
    }
    return *this;
}

Setting& Setting::operator=(Setting&& other)
{
    Registry::get_interface().Setting_decrease_reference(m_ptr);
    m_ptr = other.m_ptr;
    other.m_ptr = nullptr;
    return *this;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

OMI_CONFIG_SettingPtr Setting::get_ptr() const
{
    return m_ptr;
}

Attribute Setting::get_attr() const
{
    return Attribute(
        Registry::get_interface().Setting_get_attr(m_ptr),
        true
    );
}

std::size_t Setting::get_size() const
{
    return static_cast<std::size_t>(
        Registry::get_interface().Setting_get_size(m_ptr)
    );
}

std::string_view Setting::get_description() const
{
    return std::string_view(
        Registry::get_interface().Setting_get_description(m_ptr)
    );
}

} // namespace config
} // namespace omi
