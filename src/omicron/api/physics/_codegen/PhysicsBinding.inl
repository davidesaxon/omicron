/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#include <cstring>

#include "omicron/api/API.h"

#include "omicron/api/physics/_codegen/PhysicsCSymbols.h"


#ifdef __cplusplus
extern "C" {
#endif

OMI_API_EXPORT int OMI_PHYSICS_Physics_BINDING_IMPL(char const* func_def, void** func_ptr)
{
    if(strcmp(func_def, OMI_PHYSICS_Physics_startup_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::physics::startup;
        return 0;
    }
    if(strcmp(func_def, OMI_PHYSICS_Physics_shutdown_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::physics::shutdown;
        return 0;
    }

    return 1;
}

#ifdef __cplusplus
} // extern "C"
#endif

