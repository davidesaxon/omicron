/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_PHYSICS_CODEGEN_PHYSICSCSMYBOLS_H_
#define OMI_PHYSICS_CODEGEN_PHYSICSCSMYBOLS_H_


#ifdef __cplusplus
extern "C" {
#endif

// OMI_PHYSICS_startup
static char const* OMI_PHYSICS_Physics_startup_SYMBOL = "OMI_Bool OMI_PHYSICS_Physics_startup(OMI_REPORT_LoggerPtr)";

// OMI_PHYSICS_shutdown
static char const* OMI_PHYSICS_Physics_shutdown_SYMBOL = "OMI_Bool OMI_PHYSICS_Physics_shutdown()";

#ifdef __cplusplus
} // extern "C"
#endif

#endif