/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_PHYSICS_CODEGEN_PHYSICSINTERFACE_HPP_
#define OMI_PHYSICS_CODEGEN_PHYSICSINTERFACE_HPP_

#include <cassert>
#include <string>

#include <arcanecore/base/dl/DLOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>
#include <arcanecore/base/lang/Restrictors.hpp>

#include "omicron/api/physics/_codegen/PhysicsCSymbols.h"


namespace omi
{
namespace physics
{

class PhysicsInterface
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                              TYPE DEFINITIONS
    //--------------------------------------------------------------------------

    typedef OMI_Bool (BindingFunc)(char const*, void**);

    //--------------------------------------------------------------------------
    //                             PUBLIC ATTRIBUTES
    //--------------------------------------------------------------------------

    // startup
    OMI_Bool (*startup)(OMI_REPORT_LoggerPtr);
    // shutdown
    OMI_Bool (*shutdown)();

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    PhysicsInterface(std::string const& libname)
        : startup(nullptr)
        , shutdown(nullptr)
    {
        #ifdef ARC_OS_WINDOWS
            arc::fsys::Path libpath({
                "build",
                "windows",
                "Release",
                libname + "_impl.dll"
            });
        #elif defined(ARC_OS_UNIX)
            arc::fsys::Path libpath({
                "build",
                "linux",
                "lib" + libname + "_impl.so"
            });
        #else
            assert(false);
        #endif

        arc::dl::Handle lib_handle = arc::dl::open_library(libpath);
        BindingFunc* binding_func = arc::dl::bind_symbol<BindingFunc>(lib_handle, "OMI_PHYSICS_Physics_BINDING_IMPL");

        if(binding_func(OMI_PHYSICS_Physics_startup_SYMBOL, (void**) &startup) != 0)
        {
        }
        if(binding_func(OMI_PHYSICS_Physics_shutdown_SYMBOL, (void**) &shutdown) != 0)
        {
        }
    }
};

} // namespace omi
} // namespace physics


#endif