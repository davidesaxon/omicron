/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstddef>
#include <cstring>
#include <string>

#include <arcanecore/base/Preproc.hpp>
#include <arcanecore/base/dl/DLOperations.hpp>
#include <arcanecore/base/fsys/FileSystemOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>

#include "omicron/api/common/Attributes.hpp"
#include "omicron/api/comp/ComponentCTypes.h"
#include "omicron/api/config/Registry.hpp"
#include "omicron/api/report/Logger.hpp"


OMI_CODEGEN_NAMESPACE namespace omi
{
OMI_CODEGEN_NAMESPACE namespace physics
{
namespace
{

//------------------------------------------------------------------------------
//                                    TYPEDEFS
//------------------------------------------------------------------------------

typedef char const* (GetPluginNameFunc)();
typedef char const* (GetPluginVersionFunc)();
typedef OMI_Bool (StartupFunc)();
typedef OMI_Bool (ShutdownFunc)();

//------------------------------------------------------------------------------
//                                    GLOBALS
//------------------------------------------------------------------------------

static std::string g_name;
static std::string g_version;

static arc::dl::Handle g_dl_handle;

static ShutdownFunc* g_shutdown_func = nullptr;

//------------------------------------------------------------------------------
//                                    STARTUP
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool startup(OMI_REPORT_LoggerPtr logger_ptr)
{
    omi::report::Logger logger(logger_ptr);

    if(g_dl_handle != nullptr)
    {
        logger.error(
            "Physics startup called after physics subsystem already initialised"
        );
        return OMI_False;
    }

    // get the path to the physics subsystem to use
    StringAttribute path_attr = config::Registry::instance().get_attr(
        #ifdef ARC_OS_WINDOWS
            "omicron.physics.bind.windows_path"
        #else
            "omicron.physics.bind.linux_path"
        #endif
    );

    arc::fsys::Path physics_path;
    for(StringAttribute::DataType const& c : path_attr.get_values())
    {
        physics_path << c;
    }
    logger.debug(
        "Loading physics subsystem from: {0}",
        physics_path.to_native()
    );

    // check that the file exists
    if(!arc::fsys::is_file(physics_path))
    {
        logger.critical(
            "Physics subsystem file path either does not exist or is not a "
            "file: {0}",
            physics_path.to_native()
        );
        return OMI_False;
    }

    // open as dynamic library
    g_dl_handle = arc::dl::open_library(physics_path);

    // bind symbols
    GetPluginNameFunc* get_name_func = nullptr;
    try
    {
        get_name_func = arc::dl::bind_symbol<GetPluginNameFunc>(
            g_dl_handle,
            "OMI_PHYSICS_get_plugin_name"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_PHYSICS_get_plugin_name function from "
            "physics: {0}",
            physics_path.to_native()
        );
        return OMI_False;
    }

    GetPluginVersionFunc* get_version_func = nullptr;
    try
    {
        get_version_func = arc::dl::bind_symbol<GetPluginVersionFunc>(
            g_dl_handle,
            "OMI_PHYSICS_get_plugin_version"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_PHYSICS_get_plugin_version function from "
            "physics: {0}",
            physics_path.to_native()
        );
        return OMI_False;
    }

    StartupFunc* startup_func = nullptr;
    try
    {
        startup_func = arc::dl::bind_symbol<StartupFunc>(
            g_dl_handle,
            "OMI_PHYSICS_startup"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_PHYSICS_startup function from physics: {0}",
            physics_path.to_native()
        );
        return OMI_False;
    }

    try
    {
        g_shutdown_func = arc::dl::bind_symbol<ShutdownFunc>(
            g_dl_handle,
            "OMI_PHYSICS_shutdown"
        );
    }
    catch(...)
    {
        logger.critical(
            "Failed to bind OMI_PHYSICS_shutdown function from physics: {0}",
            physics_path.to_native()
        );
        return OMI_False;
    }

    g_name = get_name_func();
    g_version = get_version_func();

    logger.info(
        "Bound physics subsystem: {0}-{1}",
        g_name,
        g_version
    );

    // call startup
    return startup_func();
}

//------------------------------------------------------------------------------
//                                    SHUTDOWN
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool shutdown()
{
    // nothing to do
    if(g_dl_handle == nullptr)
    {
        return OMI_True;
    }

    OMI_Bool success = OMI_True;
    if(g_shutdown_func != nullptr)
    {
        success = g_shutdown_func();
    }

    g_shutdown_func = nullptr;
    arc::dl::close_library(g_dl_handle);
    g_dl_handle = nullptr;

    return success;
}

} // namespace anonymous
} // namespace physics
} // namespace omi

#include "omicron/api/physics/_codegen/PhysicsBinding.inl"
