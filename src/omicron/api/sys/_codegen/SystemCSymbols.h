/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_SYS_CODEGEN_SYSTEMCSMYBOLS_H_
#define OMI_SYS_CODEGEN_SYSTEMCSMYBOLS_H_


#ifdef __cplusplus
extern "C" {
#endif

// OMI_SYS_startup
static char const* OMI_SYS_System_startup_SYMBOL = "OMI_Bool OMI_SYS_System_startup(OMI_REPORT_LoggerPtr)";

// OMI_SYS_cleanup
static char const* OMI_SYS_System_cleanup_SYMBOL = "void OMI_SYS_System_cleanup(OMI_REPORT_LoggerPtr)";

// OMI_SYS_shutdown
static char const* OMI_SYS_System_shutdown_SYMBOL = "OMI_Bool OMI_SYS_System_shutdown()";

// OMI_SYS_update
static char const* OMI_SYS_System_update_SYMBOL = "void OMI_SYS_System_update()";

// OMI_SYS_Timing_get_fps
static char const* OMI_SYS_System_Timing_get_fps_SYMBOL = "float OMI_SYS_System_Timing_get_fps()";

// OMI_SYS_Timing_get_fps_scale
static char const* OMI_SYS_System_Timing_get_fps_scale_SYMBOL = "float OMI_SYS_System_Timing_get_fps_scale()";

// OMI_SYS_Timing_get_time_scale
static char const* OMI_SYS_System_Timing_get_time_scale_SYMBOL = "float OMI_SYS_System_Timing_get_time_scale()";

// OMI_SYS_Timing_set_time_scale
static char const* OMI_SYS_System_Timing_set_time_scale_SYMBOL = "void OMI_SYS_System_Timing_set_time_scale(float)";

// OMI_SYS_Timing_get_scale
static char const* OMI_SYS_System_Timing_get_scale_SYMBOL = "float OMI_SYS_System_Timing_get_scale()";

// OMI_SYS_Profiler_display_fps
static char const* OMI_SYS_System_Profiler_display_fps_SYMBOL = "void OMI_SYS_System_Profiler_display_fps(OMI_Bool)";

#ifdef __cplusplus
} // extern "C"
#endif

#endif