/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#include <cstring>

#include "omicron/api/API.h"

#include "omicron/api/sys/_codegen/SystemCSymbols.h"


#ifdef __cplusplus
extern "C" {
#endif

OMI_API_EXPORT int OMI_SYS_System_BINDING_IMPL(char const* func_def, void** func_ptr)
{
    if(strcmp(func_def, OMI_SYS_System_startup_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::sys::startup;
        return 0;
    }
    if(strcmp(func_def, OMI_SYS_System_cleanup_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::sys::cleanup;
        return 0;
    }
    if(strcmp(func_def, OMI_SYS_System_shutdown_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::sys::shutdown;
        return 0;
    }
    if(strcmp(func_def, OMI_SYS_System_update_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::sys::update;
        return 0;
    }
    if(strcmp(func_def, OMI_SYS_System_Timing_get_fps_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::sys::Timing_get_fps;
        return 0;
    }
    if(strcmp(func_def, OMI_SYS_System_Timing_get_fps_scale_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::sys::Timing_get_fps_scale;
        return 0;
    }
    if(strcmp(func_def, OMI_SYS_System_Timing_get_time_scale_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::sys::Timing_get_time_scale;
        return 0;
    }
    if(strcmp(func_def, OMI_SYS_System_Timing_set_time_scale_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::sys::Timing_set_time_scale;
        return 0;
    }
    if(strcmp(func_def, OMI_SYS_System_Timing_get_scale_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::sys::Timing_get_scale;
        return 0;
    }
    if(strcmp(func_def, OMI_SYS_System_Profiler_display_fps_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::sys::Profiler_display_fps;
        return 0;
    }

    return 1;
}

#ifdef __cplusplus
} // extern "C"
#endif

