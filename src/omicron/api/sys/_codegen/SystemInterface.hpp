/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_SYS_CODEGEN_SYSTEMINTERFACE_HPP_
#define OMI_SYS_CODEGEN_SYSTEMINTERFACE_HPP_

#include <cassert>
#include <string>

#include <arcanecore/base/dl/DLOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>
#include <arcanecore/base/lang/Restrictors.hpp>

#include "omicron/api/sys/_codegen/SystemCSymbols.h"


namespace omi
{
namespace sys
{

class SystemInterface
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                              TYPE DEFINITIONS
    //--------------------------------------------------------------------------

    typedef OMI_Bool (BindingFunc)(char const*, void**);

    //--------------------------------------------------------------------------
    //                             PUBLIC ATTRIBUTES
    //--------------------------------------------------------------------------

    // startup
    OMI_Bool (*startup)(OMI_REPORT_LoggerPtr);
    // cleanup
    void (*cleanup)(OMI_REPORT_LoggerPtr);
    // shutdown
    OMI_Bool (*shutdown)();
    // update
    void (*update)();
    // Timing_get_fps
    float (*Timing_get_fps)();
    // Timing_get_fps_scale
    float (*Timing_get_fps_scale)();
    // Timing_get_time_scale
    float (*Timing_get_time_scale)();
    // Timing_set_time_scale
    void (*Timing_set_time_scale)(float);
    // Timing_get_scale
    float (*Timing_get_scale)();
    // Profiler_display_fps
    void (*Profiler_display_fps)(OMI_Bool);

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    SystemInterface(std::string const& libname)
        : startup(nullptr)
        , cleanup(nullptr)
        , shutdown(nullptr)
        , update(nullptr)
        , Timing_get_fps(nullptr)
        , Timing_get_fps_scale(nullptr)
        , Timing_get_time_scale(nullptr)
        , Timing_set_time_scale(nullptr)
        , Timing_get_scale(nullptr)
        , Profiler_display_fps(nullptr)
    {
        #ifdef ARC_OS_WINDOWS
            arc::fsys::Path libpath({
                "build",
                "windows",
                "Release",
                libname + "_impl.dll"
            });
        #elif defined(ARC_OS_UNIX)
            arc::fsys::Path libpath({
                "build",
                "linux",
                "lib" + libname + "_impl.so"
            });
        #else
            assert(false);
        #endif

        arc::dl::Handle lib_handle = arc::dl::open_library(libpath);
        BindingFunc* binding_func = arc::dl::bind_symbol<BindingFunc>(lib_handle, "OMI_SYS_System_BINDING_IMPL");

        if(binding_func(OMI_SYS_System_startup_SYMBOL, (void**) &startup) != 0)
        {
        }
        if(binding_func(OMI_SYS_System_cleanup_SYMBOL, (void**) &cleanup) != 0)
        {
        }
        if(binding_func(OMI_SYS_System_shutdown_SYMBOL, (void**) &shutdown) != 0)
        {
        }
        if(binding_func(OMI_SYS_System_update_SYMBOL, (void**) &update) != 0)
        {
        }
        if(binding_func(OMI_SYS_System_Timing_get_fps_SYMBOL, (void**) &Timing_get_fps) != 0)
        {
        }
        if(binding_func(OMI_SYS_System_Timing_get_fps_scale_SYMBOL, (void**) &Timing_get_fps_scale) != 0)
        {
        }
        if(binding_func(OMI_SYS_System_Timing_get_time_scale_SYMBOL, (void**) &Timing_get_time_scale) != 0)
        {
        }
        if(binding_func(OMI_SYS_System_Timing_set_time_scale_SYMBOL, (void**) &Timing_set_time_scale) != 0)
        {
        }
        if(binding_func(OMI_SYS_System_Timing_get_scale_SYMBOL, (void**) &Timing_get_scale) != 0)
        {
        }
        if(binding_func(OMI_SYS_System_Profiler_display_fps_SYMBOL, (void**) &Profiler_display_fps) != 0)
        {
        }
    }
};

} // namespace omi
} // namespace sys


#endif