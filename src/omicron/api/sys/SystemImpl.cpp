/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstddef>
#include <cstring>
#include <iomanip>
#include <memory>
#include <sstream>
#include <string>

#include <arcanecore/base/clock/ClockOperations.hpp>

#include "omicron/api/API.h"
#include "omicron/api/common/Attributes.hpp"
#include "omicron/api/comp/renderable/Text2D.hpp"
#include "omicron/api/config/Registry.hpp"
#include "omicron/api/renderer/Renderer.hpp"
#include "omicron/api/report/Logger.hpp"
#include <omicron/api/report/StatsDatabase.hpp>
#include <omicron/api/res/ResourceId.hpp>
#include <omicron/api/res/Storage.hpp>
#include "omicron/api/sys/SystemCTypes.h"


OMI_CODEGEN_NAMESPACE namespace omi
{
OMI_CODEGEN_NAMESPACE namespace sys
{
namespace
{

//------------------------------------------------------------------------------
//                                    GLOBALS
//------------------------------------------------------------------------------

static bool g_first_frame = true;
static arc::clock::TimeInt g_last_frame_time;

static float g_fps        = 60.0F;
static float g_fps_scale  = 1.0F;
static float g_time_scale = 1.0F;

// system resources
static omi::res::ResourceId const g_system_font =
    omi::res::get_id("res/builtin/fonts/whitrabt.ttf");

//-----------------------------------PROFILER-----------------------------------
static bool g_display_fps = false;
static bool g_display_memory = false;
static bool g_display_init = false;
static arc::clock::TimeInt g_sample_time = 500;
static arc::clock::TimeInt g_current_sample_time = 0xFFFFFFFF;
static std::unique_ptr<omi::comp::Text2D> g_profiler_text2d(nullptr);

//------------------------------------STATS-------------------------------------
static omi::Float32Attribute g_stat_fps(60.0F);
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//                                    STARTUP
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool startup(OMI_REPORT_LoggerPtr logger_ptr)
{
    report::Logger logger(logger_ptr, true);

    // define statistics
    report::StatsDatabase::instance().define_entry(
        "Timing.FPS",
        g_stat_fps,
        "The current frames per second the engine is running at."
    );

    // determine initial profiler state
    ByteAttribute display_fps_attr = config::Registry::instance().get_attr(
        "omicron.system.profiler.display_fps"
    );
    if(display_fps_attr.get_value() != 0)
    {
        g_display_fps = true;
    }
    ByteAttribute display_memory_attr = config::Registry::instance().get_attr(
        "omicron.system.profiler.display_memory"
    );
    if(display_memory_attr.get_value() != 0)
    {
        g_display_memory = true;
    }

    Int64Attribute sample_time_attr = config::Registry::instance().get_attr(
        "omicron.system.profiler.sample_time"
    );
    g_sample_time =
        static_cast<arc::clock::TimeInt>(sample_time_attr.get_value());

    return OMI_True;
}

//------------------------------------------------------------------------------
//                                    CLEANUP
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void cleanup(OMI_REPORT_LoggerPtr logger_ptr)
{
    report::Logger logger(logger_ptr, true);

    if(g_display_init)
    {
        omi::renderer::Renderer::get_interface().remove_component(
            g_profiler_text2d->get_ptr()
        );
    }
}

//------------------------------------------------------------------------------
//                                    SHUTDOWN
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool shutdown()
{
    return OMI_True;
}

//------------------------------------------------------------------------------
//                                 SYSTEM UPDATE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void update()
{
    arc::clock::TimeInt current_time = arc::clock::get_current_time();

    // first frame - don't try compute fps
    if(g_first_frame)
    {
        g_last_frame_time = current_time;
        g_first_frame = false;

        // load system resources
        omi::res::Storage::instance().load_blocking(g_system_font);

        // get profiler config
        Float32Attribute colour_attr = config::Registry::instance().get_attr(
            "omicron.system.profiler.text_colour"
        );
        Float32Attribute size_attr = config::Registry::instance().get_attr(
            "omicron.system.profiler.text_size"
        );

        // build the profiler text
        g_profiler_text2d.reset(
            new omi::comp::Text2D(
                "",
                g_system_font,
                *reinterpret_cast<arclx::Vector4f const*>(
                    &colour_attr.get_values()[0]
                ),
                size_attr.get_value(),
                arclx::Vector2f(0.99F, 0.99F),
                omi::comp::Text2D::HorizontalOrigin::kRight,
                omi::comp::Text2D::VerticalOrigin::kTop
            )
        );

        return;
    }
    else
    {
        // compute how long this frame took
        float const delta  = static_cast<float>(current_time - g_last_frame_time);
        g_last_frame_time = current_time;

        // set globals
        g_fps = 1000.0F / delta;
        // TODO: this should be clamped above 0.1F?
        g_fps_scale = 60.0F / g_fps;

        // update stats
        g_stat_fps.set_value(g_fps);
    }

    // display fps?
    if(g_display_fps || g_display_memory)
    {
        // need to add components?
        if(!g_display_init)
        {
            g_current_sample_time = 0xFFFFFFFF;
            omi::renderer::Renderer::get_interface().add_component(
                g_profiler_text2d->get_ptr()
            );
            g_display_init = true;
        }

        // only update fps every second
        if((current_time - g_current_sample_time) >= g_sample_time)
        {
            g_current_sample_time = current_time;
            std::stringstream ss;
            if(g_display_fps)
            {
                ss << std::fixed << std::setprecision(2) << g_fps << " FPS";
                if(g_display_memory)
                {
                    ss << "\n";
                }
            }
            if(g_display_memory)
            {

                omi::Float64Attribute rss_stat =
                    omi::report::StatsDatabase::instance().get_entry(
                        "System.Memory.RSS (mb)"
                    );
                omi::Float64Attribute free_stat =
                    omi::report::StatsDatabase::instance().get_entry(
                        "System.Memory.Free RAM (mb)"
                    );
                ss
                    << std::fixed << std::setprecision(2)
                    << rss_stat.get_value() << "mb RAM used\n"
                    << free_stat.get_value() << "mb RAM free";
            }
            g_profiler_text2d->set_text(ss.str());
        }
    }
    else if(g_display_init)
    {
        omi::renderer::Renderer::get_interface().remove_component(
            g_profiler_text2d->get_ptr()
        );
        g_display_init = false;
    }
}

//------------------------------------------------------------------------------
//                                 TIMING GET FPS
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float Timing_get_fps()
{
    return g_fps;
}

//------------------------------------------------------------------------------
//                              TIMING GET FPS SCALE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float Timing_get_fps_scale()
{
    return g_fps_scale;
}

//------------------------------------------------------------------------------
//                             TIMING GET TIME SCALE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float Timing_get_time_scale()
{
    return g_time_scale;
}

//------------------------------------------------------------------------------
//                             TIMING SET TIME SCALE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Timing_set_time_scale(float scale)
{
    g_time_scale = scale;
}

//------------------------------------------------------------------------------
//                                TIMING GET SCALE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static float Timing_get_scale()
{
    return g_fps_scale * g_time_scale;
}

//------------------------------------------------------------------------------
//                              PROFILER DISPLAY FPS
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Profiler_display_fps(OMI_Bool state)
{
    g_display_fps = state;
}

} // namespace anonymous
} // namespace sys
} // namespace omi

#include "omicron/api/sys/_codegen/SystemBinding.inl"
