/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_REPORT_STATSQUERY_HPP_
#define OMICRON_API_REPORT_STATSQUERY_HPP_

#include <arcanecore/base/collection/ConstWeakArray.hpp>
#include <arcanecore/base/fsys/Path.hpp>
#include <arcanecore/base/lang/Restrictors.hpp>

#include "omicron/api/API.h"
#include "omicron/api/common/attribute/DataAttribute.hpp"
#include "omicron/api/report/StatsCTypes.h"
#include "omicron/api/report/StatsDatabase.hpp"
#include "omicron/api/report/_codegen/StatsInterface.hpp"


namespace omi
{
namespace report
{

class StatsQuery
    : private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                              TYPE DEFINITION
    //--------------------------------------------------------------------------

    typedef arc::collection::ConstWeakArray<char const*> PatternArray;

    //--------------------------------------------------------------------------
    //                                CONSTRUCTORS
    //--------------------------------------------------------------------------

    StatsQuery();

    template<typename T_InputIterator>
    StatsQuery(const T_InputIterator& first, const T_InputIterator& last)
        : m_ptr(nullptr)
    {
        std::size_t const size = std::distance(first, last);
        char const** c_array = new char*[size];
        for(std::size_t i = 0; i < size; ++i)
        {
            c_array[i] = (first + i)->c_str();
        }

        m_ptr = StatsDatabase::get_interface().StatsQuery_constructor(
            c_array,
            size
        );

        delete[] c_array;
    }

    StatsQuery(arc::fsys::Path const& file_path);

    StatsQuery(StatsQuery const& other);

    StatsQuery(StatsQuery&& other);

    StatsQuery(OMI_REPORT_StatsQueryPtr ptr, bool increase_ref=true);

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    virtual ~StatsQuery();

    //--------------------------------------------------------------------------
    //                                 OPERATORS
    //--------------------------------------------------------------------------

    StatsQuery& operator=(const StatsQuery& other);

    StatsQuery& operator=(StatsQuery&& other);

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    OMI_REPORT_StatsQueryPtr get_ptr() const;

    PatternArray get_patterns() const;

    void add_pattern(std::string const& pattern);

    void clear();

private:

    //--------------------------------------------------------------------------
    //                             PRIVATE ATTRIBUTES
    //--------------------------------------------------------------------------

    OMI_REPORT_StatsQueryPtr m_ptr;
};

} // namespace report
} // namespace omi

#endif
