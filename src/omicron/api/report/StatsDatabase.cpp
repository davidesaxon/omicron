/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/report/StatsDatabase.hpp"
#include "omicron/api/report/StatsQuery.hpp"


namespace omi
{
namespace report
{

//------------------------------------------------------------------------------
//                            PUBLIC STATIC FUNCTIONS
//------------------------------------------------------------------------------

StatsDatabase& StatsDatabase::instance()
{
    static StatsDatabase inst;
    return inst;
}

StatsInterface& StatsDatabase::get_interface()
{
    static StatsInterface inst("omicron_api");
    return inst;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

std::size_t StatsDatabase::size() const
{
    return static_cast<std::size_t>(get_interface().StatsDatabase_size());
}

void StatsDatabase::define_entry(
        std::string const& name,
        omi::DataAttribute attr,
        std::string const& description)
{
    get_interface().StatsDatabase_define_entry(
        name.c_str(),
        attr.get_ptr(),
        description.c_str()
    );
}

omi::DataAttribute StatsDatabase::get_entry(std::string const& name) const
{
    OMI_AttributePtr attr = nullptr;
    OMI_REPORT_StatsDatabase_Error ec = get_interface().StatsDatabase_get_entry(
        name.c_str(),
        &attr
    );

    if(ec == OMI_REPORT_StatsDatabase_Error_kKeyError)
    {
        throw arc::ex::KeyError(
            "No StatsDatabase entry with name \"" + name + "\""
        );
    }

    return omi::DataAttribute(attr, true);
}

std::string_view StatsDatabase::get_description(std::string const& name) const
{
    char const* description = nullptr;
    OMI_REPORT_StatsDatabase_Error ec =
        get_interface().StatsDatabase_get_description(
            name.c_str(),
            &description
        );

    if(ec == OMI_REPORT_StatsDatabase_Error_kKeyError)
    {
        throw arc::ex::KeyError(
            "No StatsDatabase entry with name \"" + name + "\""
        );
    }

    return std::string_view(description);
}

std::vector<char const*> StatsDatabase::get_names() const
{
    std::size_t const names_size = size();
    std::vector<char const*> ret(names_size, nullptr);

    get_interface().StatsDatabase_get_names(&ret[0]);

    return ret;
}

StatsDatabase::QueryResult StatsDatabase::execute_query(StatsQuery const& query)
{
    char const** result_names = nullptr;
    OMI_AttributePtr* result_attrs = nullptr;

    OMI_Size size = get_interface().StatsDatabase_execute_query(
        query.get_ptr(),
        &result_names,
        &result_attrs
    );

    QueryResult result;
    for(OMI_Size i = 0; i < size; ++i)
    {
        result[result_names[i]] = omi::Attribute(result_attrs[i], true);
    }

    get_interface().StatsDatabase_delete_results(result_names, result_attrs);

    return result;
}

//------------------------------------------------------------------------------
//                              PRIVATE CONSTRUCTOR
//------------------------------------------------------------------------------

StatsDatabase::StatsDatabase()
{
}

//------------------------------------------------------------------------------
//                               PRIVATE DESTRUCTOR
//------------------------------------------------------------------------------

StatsDatabase::~StatsDatabase()
{
}

} // namespace report
} // namespace omi
