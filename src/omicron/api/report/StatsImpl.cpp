/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstddef>
#include <cstring>
#include <fstream>
#include <list>
#include <string>
#include <unordered_map>
#include <vector>

#include <arcanecore/base/str/FnMatch.hpp>
#include <arcanecore/base/str/StringOperations.hpp>

#include "omicron/api/API.h"
#include "omicron/api/common/attribute/DataAttribute.hpp"
#include "omicron/api/report/StatsCTypes.h"


//------------------------------------------------------------------------------
//                              STATS QUERY C STRUCT
//------------------------------------------------------------------------------

struct OMI_REPORT_StatsQueryStruct
{
    //---------------------------A T T R I B U T E S----------------------------

    std::size_t m_ref_count;
    std::vector<char const*> m_patterns;

    //--------------------------C O N S T R U C T O R---------------------------

    OMI_REPORT_StatsQueryStruct(
            char const* const* patterns,
            OMI_Size size)
        : m_ref_count(1)
    {
        m_patterns.reserve(size);
        for(OMI_Size i = 0; i < size; ++i)
        {
            m_patterns.push_back(patterns[i]);
        }
    }
};


OMI_CODEGEN_NAMESPACE namespace omi
{
OMI_CODEGEN_NAMESPACE namespace report
{
namespace
{

//------------------------------------------------------------------------------
//                                    GLOBALS
//------------------------------------------------------------------------------

static std::unordered_map<std::string, omi::Attribute> g_entries;
static std::unordered_map<std::string, std::string> g_descriptions;

//------------------------------------------------------------------------------
//                            STATS QUERY CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_REPORT_StatsQueryPtr StatsQuery_constructor(
        char const* const* patterns,
        OMI_Size size)
{
    OMI_REPORT_StatsQueryStruct* ret = new OMI_REPORT_StatsQueryStruct(
        patterns,
        size
    );
    return ret;
}

//------------------------------------------------------------------------------
//                          STATS QUERY FILE CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_REPORT_StatsQueryPtr StatsQuery_file_constructor(
        char const* file_path)
{
    // read lines from the file
    std::ifstream input(file_path);
    std::list<std::string> lines;
    std::string line;
    while(std::getline(input, line))
    {
        if(!line.empty() && !arc::str::starts_with(line, "//"))
        {
            lines.push_back(line);
        }
    }

    // create empty struct
    OMI_REPORT_StatsQueryStruct* ret = new OMI_REPORT_StatsQueryStruct(
        nullptr,
        0
    );

    // allocate pattern memory
    ret->m_patterns.reserve(lines.size());
    for(std::string const& p : lines)
    {
        std::size_t const length = p.length() + 1;
        char* copy = new char[length];
        std::memcpy(copy, p.c_str(), length);
        ret->m_patterns.push_back(copy);
    }

    return ret;
}

//------------------------------------------------------------------------------
//                         STATS QUERY INCREASE REFERENCE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void StatsQuery_increase_reference(OMI_REPORT_StatsQueryPtr ptr)
{
    ++ptr->m_ref_count;
}

//------------------------------------------------------------------------------
//                         STATS QUERY DECREASE REFERENCE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void StatsQuery_decrease_reference(OMI_REPORT_StatsQueryPtr ptr)
{
    if(ptr == nullptr)
    {
        return;
    }

    if(ptr->m_ref_count == 1)
    {
        for(char const* pattern : ptr->m_patterns)
        {
            delete[] pattern;
        }
        delete ptr;
    }
    else
    {
        --ptr->m_ref_count;
    }
}

//------------------------------------------------------------------------------
//                            STATS QUERY GET PATTERNS
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size StatsQuery_get_patterns(
        OMI_REPORT_StatsQueryPtrConst ptr,
        char const* const** out_patterns)
{
    *out_patterns = &ptr->m_patterns[0];
    return ptr->m_patterns.size();
}

//------------------------------------------------------------------------------
//                            STATS QUERY ADD PATTERN
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void StatsQuery_add_pattern(
        OMI_REPORT_StatsQueryPtr ptr,
        char const* pattern)
{
    std::size_t const size = std::strlen(pattern) + 1;
    char* copy = new char[size];
    std::memcpy(copy, pattern, size);
    ptr->m_patterns.push_back(copy);
}

//------------------------------------------------------------------------------
//                               STATS QUERY CLEAR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void StatsQuery_clear(OMI_REPORT_StatsQueryPtr ptr)
{
    for(char const* pattern : ptr->m_patterns)
    {
        delete[] pattern;
    }
    ptr->m_patterns.clear();
}

//------------------------------------------------------------------------------
//                              STATS DATABASE SIZE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size StatsDatabase_size()
{
    return g_entries.size();
}

//------------------------------------------------------------------------------
//                          STATS DATABASE DEFINE ENTRY
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void StatsDatabase_define_entry(
        char const* name,
        OMI_AttributePtr attr,
        char const* description)
{
    g_entries[name] = attr;
    g_descriptions[name ] = description;
}

//------------------------------------------------------------------------------
//                            STATS DATABASE GET ENTRY
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_REPORT_StatsDatabase_Error StatsDatabase_get_entry(
        char const* name,
        OMI_AttributePtr* out_attr)
{
    // find the entry
    auto f_entry = g_entries.find(name);
    if(f_entry == g_entries.end())
    {
        return OMI_REPORT_StatsDatabase_Error_kKeyError;
    }

    *out_attr = f_entry->second.get_ptr();
    return OMI_REPORT_StatsDatabase_Error_kNone;
}

//------------------------------------------------------------------------------
//                         STATS DATABASE GET DESCRIPTION
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_REPORT_StatsDatabase_Error StatsDatabase_get_description(
        char const* name,
        char const** out_description)
{
    // find the entry
    auto f_entry = g_descriptions.find(name);
    if(f_entry == g_descriptions.end())
    {
        return OMI_REPORT_StatsDatabase_Error_kKeyError;
    }

    *out_description = f_entry->second.c_str();
    return OMI_REPORT_StatsDatabase_Error_kNone;
}

//------------------------------------------------------------------------------
//                            STATS DATABASE GET NAMES
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void StatsDatabase_get_names(char const** out_names)
{
    std::size_t offset = 0;
    for(auto const& entry : g_entries)
    {
        out_names[offset] = entry.first.c_str();
        ++offset;
    }
}

//------------------------------------------------------------------------------
//                          STATS DATABASE EXECUTE QUERY
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size StatsDatabase_execute_query(
        OMI_REPORT_StatsQueryPtrConst query,
        char const*** out_names,
        OMI_AttributePtr** out_attrs)
{
    std::unordered_map<std::string, omi::Attribute> result;
    std::list<char const*> names;
    std::list<OMI_AttributePtr> attrs;
    for(auto const& entry : g_entries)
    {
        for(char const* pattern : query->m_patterns)
        {
            if(arc::str::fnmatch(pattern, entry.first))
            {
                names.push_back(entry.first.c_str());
                attrs.push_back(entry.second.get_ptr());
                break;
            }
        }
    }

    std::size_t const size = names.size();
    *out_names = new char const*[size];
    *out_attrs = new OMI_AttributePtr[size];
    auto names_it = names.begin();
    auto attrs_it = attrs.begin();
    for(std::size_t i = 0; i < size; ++i)
    {
        (*out_names)[i] = *names_it;
        (*out_attrs)[i] = *attrs_it;

        ++names_it;
        ++attrs_it;
    }
    return size;
}

//------------------------------------------------------------------------------
//                         STATS DATABASE DELETE RESULTS
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void StatsDatabase_delete_results(
        char const* const* names,
        OMI_AttributePtr const* attrs)
{
    delete[] names;
    delete[] attrs;
}

} // namespace anonymous
} // namespace report
} // namespace omi

#include "omicron/api/report/_codegen/StatsBinding.inl"
