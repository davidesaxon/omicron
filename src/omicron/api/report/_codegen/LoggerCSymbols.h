/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_REPORT_CODEGEN_LOGGERCSMYBOLS_H_
#define OMI_REPORT_CODEGEN_LOGGERCSMYBOLS_H_


#ifdef __cplusplus
extern "C" {
#endif

// OMI_REPORT_startup
static char const* OMI_REPORT_Logger_startup_SYMBOL = "OMI_REPORT_Logger_Error OMI_REPORT_Logger_startup()";

// OMI_REPORT_constructor
static char const* OMI_REPORT_Logger_constructor_SYMBOL = "OMI_REPORT_LoggerPtr OMI_REPORT_Logger_constructor(char const*)";

// OMI_REPORT_increase_reference
static char const* OMI_REPORT_Logger_increase_reference_SYMBOL = "void OMI_REPORT_Logger_increase_reference(OMI_REPORT_LoggerPtr)";

// OMI_REPORT_decrease_reference
static char const* OMI_REPORT_Logger_decrease_reference_SYMBOL = "void OMI_REPORT_Logger_decrease_reference(OMI_REPORT_LoggerPtr)";

// OMI_REPORT_log
static char const* OMI_REPORT_Logger_log_SYMBOL = "void OMI_REPORT_Logger_log(OMI_REPORT_LoggerPtrConst, OMI_REPORT_LoggerVerbosity, char const*)";

#ifdef __cplusplus
} // extern "C"
#endif

#endif