/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_REPORT_CODEGEN_LOGGERINTERFACE_HPP_
#define OMI_REPORT_CODEGEN_LOGGERINTERFACE_HPP_

#include <cassert>
#include <string>

#include <arcanecore/base/dl/DLOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>
#include <arcanecore/base/lang/Restrictors.hpp>

#include "omicron/api/report/_codegen/LoggerCSymbols.h"


namespace omi
{
namespace report
{

class LoggerInterface
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                              TYPE DEFINITIONS
    //--------------------------------------------------------------------------

    typedef OMI_Bool (BindingFunc)(char const*, void**);

    //--------------------------------------------------------------------------
    //                             PUBLIC ATTRIBUTES
    //--------------------------------------------------------------------------

    // startup
    OMI_REPORT_Logger_Error (*startup)();
    // constructor
    OMI_REPORT_LoggerPtr (*constructor)(char const*);
    // increase_reference
    void (*increase_reference)(OMI_REPORT_LoggerPtr);
    // decrease_reference
    void (*decrease_reference)(OMI_REPORT_LoggerPtr);
    // log
    void (*log)(OMI_REPORT_LoggerPtrConst, OMI_REPORT_LoggerVerbosity, char const*);

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    LoggerInterface(std::string const& libname)
        : startup(nullptr)
        , constructor(nullptr)
        , increase_reference(nullptr)
        , decrease_reference(nullptr)
        , log(nullptr)
    {
        #ifdef ARC_OS_WINDOWS
            arc::fsys::Path libpath({
                "build",
                "windows",
                "Release",
                libname + "_impl.dll"
            });
        #elif defined(ARC_OS_UNIX)
            arc::fsys::Path libpath({
                "build",
                "linux",
                "lib" + libname + "_impl.so"
            });
        #else
            assert(false);
        #endif

        arc::dl::Handle lib_handle = arc::dl::open_library(libpath);
        BindingFunc* binding_func = arc::dl::bind_symbol<BindingFunc>(lib_handle, "OMI_REPORT_Logger_BINDING_IMPL");

        if(binding_func(OMI_REPORT_Logger_startup_SYMBOL, (void**) &startup) != 0)
        {
        }
        if(binding_func(OMI_REPORT_Logger_constructor_SYMBOL, (void**) &constructor) != 0)
        {
        }
        if(binding_func(OMI_REPORT_Logger_increase_reference_SYMBOL, (void**) &increase_reference) != 0)
        {
        }
        if(binding_func(OMI_REPORT_Logger_decrease_reference_SYMBOL, (void**) &decrease_reference) != 0)
        {
        }
        if(binding_func(OMI_REPORT_Logger_log_SYMBOL, (void**) &log) != 0)
        {
        }
    }
};

} // namespace omi
} // namespace report


#endif