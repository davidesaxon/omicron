/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_REPORT_CODEGEN_STATSCSMYBOLS_H_
#define OMI_REPORT_CODEGEN_STATSCSMYBOLS_H_


#ifdef __cplusplus
extern "C" {
#endif

// OMI_REPORT_StatsQuery_constructor
static char const* OMI_REPORT_Stats_StatsQuery_constructor_SYMBOL = "OMI_REPORT_StatsQueryPtr OMI_REPORT_Stats_StatsQuery_constructor(char const* const*, OMI_Size)";

// OMI_REPORT_StatsQuery_file_constructor
static char const* OMI_REPORT_Stats_StatsQuery_file_constructor_SYMBOL = "OMI_REPORT_StatsQueryPtr OMI_REPORT_Stats_StatsQuery_file_constructor(char const*)";

// OMI_REPORT_StatsQuery_increase_reference
static char const* OMI_REPORT_Stats_StatsQuery_increase_reference_SYMBOL = "void OMI_REPORT_Stats_StatsQuery_increase_reference(OMI_REPORT_StatsQueryPtr)";

// OMI_REPORT_StatsQuery_decrease_reference
static char const* OMI_REPORT_Stats_StatsQuery_decrease_reference_SYMBOL = "void OMI_REPORT_Stats_StatsQuery_decrease_reference(OMI_REPORT_StatsQueryPtr)";

// OMI_REPORT_StatsQuery_get_patterns
static char const* OMI_REPORT_Stats_StatsQuery_get_patterns_SYMBOL = "OMI_Size OMI_REPORT_Stats_StatsQuery_get_patterns(OMI_REPORT_StatsQueryPtrConst, char const* const**)";

// OMI_REPORT_StatsQuery_add_pattern
static char const* OMI_REPORT_Stats_StatsQuery_add_pattern_SYMBOL = "void OMI_REPORT_Stats_StatsQuery_add_pattern(OMI_REPORT_StatsQueryPtr, char const*)";

// OMI_REPORT_StatsQuery_clear
static char const* OMI_REPORT_Stats_StatsQuery_clear_SYMBOL = "void OMI_REPORT_Stats_StatsQuery_clear(OMI_REPORT_StatsQueryPtr)";

// OMI_REPORT_StatsDatabase_size
static char const* OMI_REPORT_Stats_StatsDatabase_size_SYMBOL = "OMI_Size OMI_REPORT_Stats_StatsDatabase_size()";

// OMI_REPORT_StatsDatabase_define_entry
static char const* OMI_REPORT_Stats_StatsDatabase_define_entry_SYMBOL = "void OMI_REPORT_Stats_StatsDatabase_define_entry(char const*, OMI_AttributePtr, char const*)";

// OMI_REPORT_StatsDatabase_get_entry
static char const* OMI_REPORT_Stats_StatsDatabase_get_entry_SYMBOL = "OMI_REPORT_StatsDatabase_Error OMI_REPORT_Stats_StatsDatabase_get_entry(char const*, OMI_AttributePtr*)";

// OMI_REPORT_StatsDatabase_get_description
static char const* OMI_REPORT_Stats_StatsDatabase_get_description_SYMBOL = "OMI_REPORT_StatsDatabase_Error OMI_REPORT_Stats_StatsDatabase_get_description(char const*, char const**)";

// OMI_REPORT_StatsDatabase_get_names
static char const* OMI_REPORT_Stats_StatsDatabase_get_names_SYMBOL = "void OMI_REPORT_Stats_StatsDatabase_get_names(char const**)";

// OMI_REPORT_StatsDatabase_execute_query
static char const* OMI_REPORT_Stats_StatsDatabase_execute_query_SYMBOL = "OMI_Size OMI_REPORT_Stats_StatsDatabase_execute_query(OMI_REPORT_StatsQueryPtrConst, char const***, OMI_AttributePtr**)";

// OMI_REPORT_StatsDatabase_delete_results
static char const* OMI_REPORT_Stats_StatsDatabase_delete_results_SYMBOL = "void OMI_REPORT_Stats_StatsDatabase_delete_results(char const* const*, OMI_AttributePtr const*)";

#ifdef __cplusplus
} // extern "C"
#endif

#endif