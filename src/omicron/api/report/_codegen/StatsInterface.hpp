/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_REPORT_CODEGEN_STATSINTERFACE_HPP_
#define OMI_REPORT_CODEGEN_STATSINTERFACE_HPP_

#include <cassert>
#include <string>

#include <arcanecore/base/dl/DLOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>
#include <arcanecore/base/lang/Restrictors.hpp>

#include "omicron/api/report/_codegen/StatsCSymbols.h"


namespace omi
{
namespace report
{

class StatsInterface
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                              TYPE DEFINITIONS
    //--------------------------------------------------------------------------

    typedef OMI_Bool (BindingFunc)(char const*, void**);

    //--------------------------------------------------------------------------
    //                             PUBLIC ATTRIBUTES
    //--------------------------------------------------------------------------

    // StatsQuery_constructor
    OMI_REPORT_StatsQueryPtr (*StatsQuery_constructor)(char const* const*, OMI_Size);
    // StatsQuery_file_constructor
    OMI_REPORT_StatsQueryPtr (*StatsQuery_file_constructor)(char const*);
    // StatsQuery_increase_reference
    void (*StatsQuery_increase_reference)(OMI_REPORT_StatsQueryPtr);
    // StatsQuery_decrease_reference
    void (*StatsQuery_decrease_reference)(OMI_REPORT_StatsQueryPtr);
    // StatsQuery_get_patterns
    OMI_Size (*StatsQuery_get_patterns)(OMI_REPORT_StatsQueryPtrConst, char const* const**);
    // StatsQuery_add_pattern
    void (*StatsQuery_add_pattern)(OMI_REPORT_StatsQueryPtr, char const*);
    // StatsQuery_clear
    void (*StatsQuery_clear)(OMI_REPORT_StatsQueryPtr);
    // StatsDatabase_size
    OMI_Size (*StatsDatabase_size)();
    // StatsDatabase_define_entry
    void (*StatsDatabase_define_entry)(char const*, OMI_AttributePtr, char const*);
    // StatsDatabase_get_entry
    OMI_REPORT_StatsDatabase_Error (*StatsDatabase_get_entry)(char const*, OMI_AttributePtr*);
    // StatsDatabase_get_description
    OMI_REPORT_StatsDatabase_Error (*StatsDatabase_get_description)(char const*, char const**);
    // StatsDatabase_get_names
    void (*StatsDatabase_get_names)(char const**);
    // StatsDatabase_execute_query
    OMI_Size (*StatsDatabase_execute_query)(OMI_REPORT_StatsQueryPtrConst, char const***, OMI_AttributePtr**);
    // StatsDatabase_delete_results
    void (*StatsDatabase_delete_results)(char const* const*, OMI_AttributePtr const*);

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    StatsInterface(std::string const& libname)
        : StatsQuery_constructor(nullptr)
        , StatsQuery_file_constructor(nullptr)
        , StatsQuery_increase_reference(nullptr)
        , StatsQuery_decrease_reference(nullptr)
        , StatsQuery_get_patterns(nullptr)
        , StatsQuery_add_pattern(nullptr)
        , StatsQuery_clear(nullptr)
        , StatsDatabase_size(nullptr)
        , StatsDatabase_define_entry(nullptr)
        , StatsDatabase_get_entry(nullptr)
        , StatsDatabase_get_description(nullptr)
        , StatsDatabase_get_names(nullptr)
        , StatsDatabase_execute_query(nullptr)
        , StatsDatabase_delete_results(nullptr)
    {
        #ifdef ARC_OS_WINDOWS
            arc::fsys::Path libpath({
                "build",
                "windows",
                "Release",
                libname + "_impl.dll"
            });
        #elif defined(ARC_OS_UNIX)
            arc::fsys::Path libpath({
                "build",
                "linux",
                "lib" + libname + "_impl.so"
            });
        #else
            assert(false);
        #endif

        arc::dl::Handle lib_handle = arc::dl::open_library(libpath);
        BindingFunc* binding_func = arc::dl::bind_symbol<BindingFunc>(lib_handle, "OMI_REPORT_Stats_BINDING_IMPL");

        if(binding_func(OMI_REPORT_Stats_StatsQuery_constructor_SYMBOL, (void**) &StatsQuery_constructor) != 0)
        {
        }
        if(binding_func(OMI_REPORT_Stats_StatsQuery_file_constructor_SYMBOL, (void**) &StatsQuery_file_constructor) != 0)
        {
        }
        if(binding_func(OMI_REPORT_Stats_StatsQuery_increase_reference_SYMBOL, (void**) &StatsQuery_increase_reference) != 0)
        {
        }
        if(binding_func(OMI_REPORT_Stats_StatsQuery_decrease_reference_SYMBOL, (void**) &StatsQuery_decrease_reference) != 0)
        {
        }
        if(binding_func(OMI_REPORT_Stats_StatsQuery_get_patterns_SYMBOL, (void**) &StatsQuery_get_patterns) != 0)
        {
        }
        if(binding_func(OMI_REPORT_Stats_StatsQuery_add_pattern_SYMBOL, (void**) &StatsQuery_add_pattern) != 0)
        {
        }
        if(binding_func(OMI_REPORT_Stats_StatsQuery_clear_SYMBOL, (void**) &StatsQuery_clear) != 0)
        {
        }
        if(binding_func(OMI_REPORT_Stats_StatsDatabase_size_SYMBOL, (void**) &StatsDatabase_size) != 0)
        {
        }
        if(binding_func(OMI_REPORT_Stats_StatsDatabase_define_entry_SYMBOL, (void**) &StatsDatabase_define_entry) != 0)
        {
        }
        if(binding_func(OMI_REPORT_Stats_StatsDatabase_get_entry_SYMBOL, (void**) &StatsDatabase_get_entry) != 0)
        {
        }
        if(binding_func(OMI_REPORT_Stats_StatsDatabase_get_description_SYMBOL, (void**) &StatsDatabase_get_description) != 0)
        {
        }
        if(binding_func(OMI_REPORT_Stats_StatsDatabase_get_names_SYMBOL, (void**) &StatsDatabase_get_names) != 0)
        {
        }
        if(binding_func(OMI_REPORT_Stats_StatsDatabase_execute_query_SYMBOL, (void**) &StatsDatabase_execute_query) != 0)
        {
        }
        if(binding_func(OMI_REPORT_Stats_StatsDatabase_delete_results_SYMBOL, (void**) &StatsDatabase_delete_results) != 0)
        {
        }
    }
};

} // namespace omi
} // namespace report


#endif