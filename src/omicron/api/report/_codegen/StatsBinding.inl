/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#include <cstring>

#include "omicron/api/API.h"

#include "omicron/api/report/_codegen/StatsCSymbols.h"


#ifdef __cplusplus
extern "C" {
#endif

OMI_API_EXPORT int OMI_REPORT_Stats_BINDING_IMPL(char const* func_def, void** func_ptr)
{
    if(strcmp(func_def, OMI_REPORT_Stats_StatsQuery_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::report::StatsQuery_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_REPORT_Stats_StatsQuery_file_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::report::StatsQuery_file_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_REPORT_Stats_StatsQuery_increase_reference_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::report::StatsQuery_increase_reference;
        return 0;
    }
    if(strcmp(func_def, OMI_REPORT_Stats_StatsQuery_decrease_reference_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::report::StatsQuery_decrease_reference;
        return 0;
    }
    if(strcmp(func_def, OMI_REPORT_Stats_StatsQuery_get_patterns_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::report::StatsQuery_get_patterns;
        return 0;
    }
    if(strcmp(func_def, OMI_REPORT_Stats_StatsQuery_add_pattern_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::report::StatsQuery_add_pattern;
        return 0;
    }
    if(strcmp(func_def, OMI_REPORT_Stats_StatsQuery_clear_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::report::StatsQuery_clear;
        return 0;
    }
    if(strcmp(func_def, OMI_REPORT_Stats_StatsDatabase_size_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::report::StatsDatabase_size;
        return 0;
    }
    if(strcmp(func_def, OMI_REPORT_Stats_StatsDatabase_define_entry_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::report::StatsDatabase_define_entry;
        return 0;
    }
    if(strcmp(func_def, OMI_REPORT_Stats_StatsDatabase_get_entry_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::report::StatsDatabase_get_entry;
        return 0;
    }
    if(strcmp(func_def, OMI_REPORT_Stats_StatsDatabase_get_description_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::report::StatsDatabase_get_description;
        return 0;
    }
    if(strcmp(func_def, OMI_REPORT_Stats_StatsDatabase_get_names_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::report::StatsDatabase_get_names;
        return 0;
    }
    if(strcmp(func_def, OMI_REPORT_Stats_StatsDatabase_execute_query_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::report::StatsDatabase_execute_query;
        return 0;
    }
    if(strcmp(func_def, OMI_REPORT_Stats_StatsDatabase_delete_results_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::report::StatsDatabase_delete_results;
        return 0;
    }

    return 1;
}

#ifdef __cplusplus
} // extern "C"
#endif

