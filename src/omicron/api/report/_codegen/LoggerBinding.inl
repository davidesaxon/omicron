/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#include <cstring>

#include "omicron/api/API.h"

#include "omicron/api/report/_codegen/LoggerCSymbols.h"


#ifdef __cplusplus
extern "C" {
#endif

OMI_API_EXPORT int OMI_REPORT_Logger_BINDING_IMPL(char const* func_def, void** func_ptr)
{
    if(strcmp(func_def, OMI_REPORT_Logger_startup_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::report::startup;
        return 0;
    }
    if(strcmp(func_def, OMI_REPORT_Logger_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::report::constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_REPORT_Logger_increase_reference_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::report::increase_reference;
        return 0;
    }
    if(strcmp(func_def, OMI_REPORT_Logger_decrease_reference_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::report::decrease_reference;
        return 0;
    }
    if(strcmp(func_def, OMI_REPORT_Logger_log_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::report::log;
        return 0;
    }

    return 1;
}

#ifdef __cplusplus
} // extern "C"
#endif

