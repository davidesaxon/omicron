/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/report/StatsQuery.hpp"


namespace omi
{
namespace report
{

//------------------------------------------------------------------------------
//                                  CONSTRUCTOR
//------------------------------------------------------------------------------

StatsQuery::StatsQuery()
    : m_ptr(StatsDatabase::get_interface().StatsQuery_constructor(nullptr, 0))
{
}

StatsQuery::StatsQuery(arc::fsys::Path const& file_path)
    : m_ptr(StatsDatabase::get_interface().StatsQuery_file_constructor(
        file_path.to_native().c_str()
    ))
{
}

StatsQuery::StatsQuery(StatsQuery const& other)
    : m_ptr(other.m_ptr)
{
    StatsDatabase::get_interface().StatsQuery_increase_reference(m_ptr);
}

StatsQuery::StatsQuery(StatsQuery&& other)
    : m_ptr(other.m_ptr)
{
    other.m_ptr = nullptr;
}

StatsQuery::StatsQuery(OMI_REPORT_StatsQueryPtr ptr, bool increase_ref)
    : m_ptr(ptr)
{
    if(increase_ref)
    {
        StatsDatabase::get_interface().StatsQuery_increase_reference(m_ptr);
    }
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

StatsQuery::~StatsQuery()
{
    StatsDatabase::get_interface().StatsQuery_decrease_reference(m_ptr);
}

//------------------------------------------------------------------------------
//                                   OPERATORS
//------------------------------------------------------------------------------

StatsQuery& StatsQuery::operator=(const StatsQuery& other)
{
    if(m_ptr != other.m_ptr)
    {
        StatsDatabase::get_interface().StatsQuery_decrease_reference(m_ptr);
        m_ptr = other.m_ptr;
        StatsDatabase::get_interface().StatsQuery_increase_reference(m_ptr);
    }
    return *this;
}

StatsQuery& StatsQuery::operator=(StatsQuery&& other)
{
    StatsDatabase::get_interface().StatsQuery_decrease_reference(m_ptr);
    m_ptr = other.m_ptr;
    other.m_ptr = nullptr;
    return *this;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

OMI_REPORT_StatsQueryPtr StatsQuery::get_ptr() const
{
    return m_ptr;
}

StatsQuery::PatternArray StatsQuery::get_patterns() const
{
    char const* const* patterns = nullptr;
    OMI_Size size = StatsDatabase::get_interface().StatsQuery_get_patterns(
        m_ptr,
        &patterns
    );

    return PatternArray(patterns, size);
}

void StatsQuery::add_pattern(std::string const& pattern)
{
    StatsDatabase::get_interface().StatsQuery_add_pattern(
        m_ptr,
        pattern.c_str()
    );
}

void StatsQuery::clear()
{
    StatsDatabase::get_interface().StatsQuery_clear(m_ptr);
}

} // namespace report
} // namespace omi
