/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstddef>
#include <cstring>
#include <iostream>
#include <string>

#include <arcanecore/base/Preproc.hpp>

#ifdef ARC_OS_WINDOWS
    #include <windows.h>
#endif

#include "omicron/api/API.h"
#include "omicron/api/common/Attributes.hpp"
#include "omicron/api/config/Registry.hpp"
#include "omicron/api/report/LoggerCTypes.h"




//------------------------------------------------------------------------------
//                                    C STRUCT
//------------------------------------------------------------------------------

struct OMI_REPORT_LoggerStruct
{
    //---------------------------A T T R I B U T E S----------------------------

    std::size_t m_ref_count;
    std::string const m_name;

    //--------------------------C O N S T R U C T O R---------------------------

    OMI_REPORT_LoggerStruct(char const* name)
        : m_ref_count(1)
        , m_name     (name)
    {
    }
};


OMI_CODEGEN_NAMESPACE namespace omi
{
OMI_CODEGEN_NAMESPACE namespace report
{
namespace
{

//------------------------------------------------------------------------------
//                                    GLOBALS
//------------------------------------------------------------------------------

// std output globals
static OMI_REPORT_LoggerVerbosity g_std_output_verbosity =
    OMI_REPORT_LoggerVerbosity_kWarning;
static bool g_std_output_use_colour = true;
// file output globals
// TODO:

//------------------------------------------------------------------------------
//                                   FUNCTIONS
//------------------------------------------------------------------------------

static void enable_std_output()
{
    // get verbosity
    StringAttribute verbosity_attr = config::Registry::instance().get_attr(
        "omicron.logging.std_output.verbosity"
    );
    std::string const verbosity = verbosity_attr.get_value("info");
    if(verbosity == "debug")
    {
        g_std_output_verbosity = OMI_REPORT_LoggerVerbosity_kDebug;
    }
    else if(verbosity == "warning")
    {
        g_std_output_verbosity = OMI_REPORT_LoggerVerbosity_kWarning;
    }
    else if(verbosity == "error")
    {
        g_std_output_verbosity = OMI_REPORT_LoggerVerbosity_kError;
    }
    else if(verbosity == "critical")
    {
        g_std_output_verbosity = OMI_REPORT_LoggerVerbosity_kCritical;
    }
    else
    {
        g_std_output_verbosity = OMI_REPORT_LoggerVerbosity_kInfo;
    }

    // get use colour
    ByteAttribute use_colour_attr = config::Registry::instance().get_attr(
        "omicron.logging.std_output.use_colour"
    );

    g_std_output_use_colour = use_colour_attr.get_value() != 0;
}

//------------------------------------------------------------------------------
//                                    STARTUP
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_REPORT_Logger_Error startup()
{
    // enable std output?
    ByteAttribute enable_std_out = config::Registry::instance().get_attr(
        "omicron.logging.std_output.enable"
    );
    if(enable_std_out.get_value() != 0)
    {
        enable_std_output();
    }

    // TODO: enable file output

    return OMI_REPORT_Logger_Error_kNone;
}

//------------------------------------------------------------------------------
//                                  CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_REPORT_LoggerPtr constructor(char const* name)
{
    return new OMI_REPORT_LoggerStruct(name);
}

//------------------------------------------------------------------------------
//                               INCREASE REFERENCE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void increase_reference(OMI_REPORT_LoggerPtr ptr)
{
    ++ptr->m_ref_count;
}

//------------------------------------------------------------------------------
//                               DECREASE REFERENCE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void decrease_reference(OMI_REPORT_LoggerPtr ptr)
{
    if(ptr == nullptr)
    {
        return;
    }

    if(ptr->m_ref_count == 1)
    {
        delete ptr;
    }
    else
    {
        --ptr->m_ref_count;
    }
}

//------------------------------------------------------------------------------
//                                      LOG
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void log(
        OMI_REPORT_LoggerPtrConst ptr,
        OMI_REPORT_LoggerVerbosity verbosity,
        char const* message)
{
    // std output?
    if(verbosity <= g_std_output_verbosity)
    {
        #ifdef ARC_OS_WINDOWS
            HANDLE console_handle = GetStdHandle(STD_OUTPUT_HANDLE);
            int original_colour = 0;
            if(g_std_output_use_colour)
            {
                CONSOLE_SCREEN_BUFFER_INFO console_info;
                GetConsoleScreenBufferInfo(console_handle, &console_info);
                original_colour = console_info.wAttributes;
            }
        #endif

        switch(verbosity)
        {
            case OMI_REPORT_LoggerVerbosity_kCritical:
            {
                if(g_std_output_use_colour)
                {
                    #ifdef ARC_OS_WINDOWS
                        SetConsoleTextAttribute(
                            console_handle,
                            FOREGROUND_RED       |
                            FOREGROUND_INTENSITY |
                            COMMON_LVB_UNDERSCORE
                        );
                    #elif defined(ARC_OS_UNIX)
                        std::cerr << "\033[04;31m";
                    #endif
                }
                std::cerr
                    << "[CRITICAL]: {" << ptr->m_name << "} - " << message;
                #ifdef ARC_OS_UNIX
                    if(g_std_output_use_colour)
                    {
                        std::cerr << "\033[00m";
                    }
                #endif
                std::cerr << std::endl;
                break;
            }
            case OMI_REPORT_LoggerVerbosity_kError:
            {
                if(g_std_output_use_colour)
                {
                    #ifdef ARC_OS_WINDOWS
                        SetConsoleTextAttribute(
                            console_handle,
                            FOREGROUND_RED |
                            FOREGROUND_INTENSITY
                        );
                    #elif defined(ARC_OS_UNIX)
                        std::cerr << "\033[00;31m";
                    #endif
                }
                std::cerr
                    << "[ERROR]:    {" << ptr->m_name << "} - " << message;
                #ifdef ARC_OS_UNIX
                    if(g_std_output_use_colour)
                    {
                        std::cerr << "\033[00m";
                    }
                #endif
                std::cerr << std::endl;
                break;
            }
            case OMI_REPORT_LoggerVerbosity_kWarning:
            {
                if(g_std_output_use_colour)
                {
                    #ifdef ARC_OS_WINDOWS
                        SetConsoleTextAttribute(
                            console_handle,
                            FOREGROUND_GREEN |
                            FOREGROUND_RED   |
                            FOREGROUND_INTENSITY
                        );
                    #elif defined(ARC_OS_UNIX)
                        std::cerr << "\033[00;93m";
                    #endif
                }
                std::cerr
                    << "[WARNING]:  {" << ptr->m_name << "} - " << message;
                #ifdef ARC_OS_UNIX
                    if(g_std_output_use_colour)
                    {
                        std::cerr << "\033[00m";
                    }
                #endif
                std::cerr << std::endl;
                break;
            }
            case OMI_REPORT_LoggerVerbosity_kDebug:
            {
                if(g_std_output_use_colour)
                {
                    #ifdef ARC_OS_WINDOWS
                        SetConsoleTextAttribute(
                            console_handle,
                            FOREGROUND_BLUE  |
                            FOREGROUND_GREEN |
                            FOREGROUND_INTENSITY
                        );
                    #elif defined(ARC_OS_UNIX)
                        std::cerr << "\033[00;36m";
                    #endif
                }
                std::cout
                    << "[DEBUG]:    {" << ptr->m_name << "} - " << message;
                #ifdef ARC_OS_UNIX
                    if(g_std_output_use_colour)
                    {
                        std::cerr << "\033[00m";
                    }
                #endif
                std::cerr << std::endl;
                break;
            }
            default:
            {
                std::cout
                    << "[INFO]:     {" << ptr->m_name << "} - " << message
                    << std::endl;
                break;
            }
        }

        if(g_std_output_use_colour)
        {
            #ifdef ARC_OS_WINDOWS
                SetConsoleTextAttribute(console_handle, original_colour);
            #endif
        }
    }
}

} // namespace anonymous
} // namespace report
} // namespace omi

#include "omicron/api/report/_codegen/LoggerBinding.inl"
