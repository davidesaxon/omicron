/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <algorithm>
#include <iostream>
#include <sstream>
#include <unordered_map>

#include <arcanecore/base/str/StringOperations.hpp>

#include "omicron/api/report/StatsOperations.hpp"


namespace omi
{
namespace report
{

namespace
{

// structure for sorting the names alphabetically and hierarchically
struct StatOrderer
{
    std::unordered_map<std::string, StatOrderer*> children;

    ~StatOrderer()
    {
        for(auto child : children)
        {
            delete child.second;
        }
    }
};

//------------------------------------------------------------------------------
//                               PRIVATE FUNCTIONS
//------------------------------------------------------------------------------

// Recursively sorts the list of hierarchal statistic names into a hierarchal
// data structure
static void sort_stats_hierarchically(
        std::vector<std::string> const& components,
        std::size_t offset,
        std::unordered_map<std::string, StatOrderer*>& out_sorted,
        std::size_t& max_indent)
{
    // get the component to sort
    std::string name = components[offset];
    // update max indent?
    std::size_t indent = (offset * 4) + name.length() + 4;
    if(indent < 78 && indent > max_indent)
    {
        max_indent = indent;
    }
    // is it in the sorted map already?
    auto f_sorted = out_sorted.find(name);
    if(f_sorted == out_sorted.end())
    {
        // StatOrderer
        out_sorted.insert(std::make_pair(name, new StatOrderer()));
        f_sorted = out_sorted.find(name);
    }
    // recurse?
    ++offset;
    if(offset < components.size())
    {
        sort_stats_hierarchically(
            components,
            offset,
            f_sorted->second->children,
            max_indent
        );
    }
}

// Recursively writes the given hierarchal data structure to a string
static void hierarchy_to_string(
        StatsDatabase::QueryResult const& query_result,
        std::unordered_map<std::string, StatOrderer*> const& roots,
        std::size_t indent,
        std::size_t value_indent,
        std::string const& hierarchy_name,
        std::stringstream& out_ss)
{
    // alphabetically order the current names
    std::vector<std::string> sorted;
    sorted.reserve(roots.size());
    for(auto entry : roots)
    {
        sorted.push_back(entry.first);
    }
    std::sort(sorted.begin(), sorted.end());

    // create the indentation string
    std::string const indent_str = arc::str::repeat(" ", indent);

    // write to the string and recurse
    for(std::string const& name : sorted)
    {
        // build the hierarchy name
        std::string next_name = hierarchy_name;
        if(!next_name.empty())
        {
            next_name += ".";
        }
        next_name += name;

        // print the stat name
        out_ss << "\t" << indent_str;
        StatOrderer* p = roots.at(name);
        if(p->children.empty())
        {
            out_ss << "-";
        }
        else
        {
            out_ss << ">";
        }
        out_ss << " " << name;

        // print the stat value
        auto f_stat = query_result.find(next_name);
        if(f_stat != query_result.end())
        {
            // how much to indent by
            std::size_t current_indent = indent + 3 + name.length();
            std::size_t indent_by = 1;
            if(current_indent < value_indent)
            {
                indent_by = value_indent - current_indent;
            }
            // build the string
            std::string const value_indent_str =
                arc::str::repeat(" ", indent_by);
            // print the value
            out_ss << ":" << value_indent_str << f_stat->second;
        }
        out_ss << "\n";
        // print children?
        if(!p->children.empty())
        {
            hierarchy_to_string(
                query_result,
                p->children,
                indent + 4,
                value_indent,
                next_name,
                out_ss
            );
        }
    }
}

} // namespace anonymous

void print_stats_query(
        StatsDatabase::QueryResult const& query_result,
        std::ostream& out_stream,
        std::string const& title)
{
    std::size_t value_indent = 0;
    // sort the stats hierarchically
    std::unordered_map<std::string, StatOrderer*> roots;
    for(auto stat : query_result)
    {
        std::vector<std::string> components = arc::str::split(stat.first, ".");
        sort_stats_hierarchically(components, 0, roots, value_indent);
    }

    // recurse the hierarchy and print in alphabetical order
    std::stringstream content;
    hierarchy_to_string(query_result, roots, 0, value_indent, "", content);

    // clean up
    for(auto root : roots)
    {
        delete root.second;
    }
    roots.clear();

    // build the header string
    std::string header = arc::str::repeat("-", 80);

    // print
    out_stream << "\t" << header << "\n";
    if(!title.empty())
    {
        out_stream << "\t" << title << "\n\t" << header << "\n";
    }
    out_stream << content.str();
    out_stream << "\t" << header << std::endl;
}

} // namespace report
} // namespace omi
