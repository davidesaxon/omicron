/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_REPORT_LOGGER_HPP_
#define OMICRON_API_REPORT_LOGGER_HPP_

#include <arcanecore/base/lang/Restrictors.hpp>

#include <fmt/format.h>

#include "omicron/api/API.h"
#include "omicron/api/report/LoggerCTypes.h"
#include "omicron/api/report/_codegen/LoggerInterface.hpp"


namespace omi
{
namespace report
{

class Logger
    : private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    Logger(std::string const& name);

    Logger(Logger const& other);

    Logger(Logger&& other);

    Logger(OMI_REPORT_LoggerPtr ptr, bool increase_ref=true);


    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    virtual ~Logger();

    //--------------------------------------------------------------------------
    //                                 OPERATORS
    //--------------------------------------------------------------------------

    Logger& operator=(const Logger& other);

    Logger& operator=(Logger&& other);

    //--------------------------------------------------------------------------
    //                          PUBLIC STATIC FUNCTIONS
    //--------------------------------------------------------------------------

    static LoggerInterface& get_interface();

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    OMI_REPORT_LoggerPtr get_ptr() const;

    template<typename ... Args>
    void critical(char const* format, Args const& ... args)
    {
        std::string const message =
            fmt::vformat(format, fmt::make_format_args(args...));
        get_interface().log(
            m_ptr,
            OMI_REPORT_LoggerVerbosity_kCritical,
            message.c_str()
        );
    }

    template<typename ... Args>
    void error(char const* format, Args const& ... args)
    {
        std::string const message =
            fmt::vformat(format, fmt::make_format_args(args...));
        get_interface().log(
            m_ptr,
            OMI_REPORT_LoggerVerbosity_kError,
            message.c_str()
        );
    }

    template<typename ... Args>
    void warning(char const* format, Args const& ... args)
    {
        std::string const message =
            fmt::vformat(format, fmt::make_format_args(args...));
        get_interface().log(
            m_ptr,
            OMI_REPORT_LoggerVerbosity_kWarning,
            message.c_str()
        );
    }

    template<typename ... Args>
    void info(char const* format, Args const& ... args)
    {
        std::string const message =
            fmt::vformat(format, fmt::make_format_args(args...));
        get_interface().log(
            m_ptr,
            OMI_REPORT_LoggerVerbosity_kInfo,
            message.c_str()
        );
    }

    template<typename ... Args>
    void debug(char const* format, Args const& ... args)
    {
        std::string const message =
            fmt::vformat(format, fmt::make_format_args(args...));
        get_interface().log(
            m_ptr,
            OMI_REPORT_LoggerVerbosity_kDebug,
            message.c_str()
        );
    }

private:

    //--------------------------------------------------------------------------
    //                             PRIVATE ATTRIBUTES
    //--------------------------------------------------------------------------

    OMI_REPORT_LoggerPtr m_ptr;
};

} // namespace report
} // namespace omi

#endif
