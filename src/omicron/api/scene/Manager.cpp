/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/scene/Manager.hpp"


namespace omi
{
namespace scene
{

//------------------------------------------------------------------------------
//                            PUBLIC STATIC FUNCTIONS
//------------------------------------------------------------------------------

Manager& Manager::instance()
{
    static Manager inst;
    return inst;
}

SceneInterface& Manager::get_interface()
{
    static SceneInterface inst("omicron_api");
    return inst;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

void Manager::set_current_scene(std::string const& name)
{
    OMI_SCENE_Error ec =
        get_interface().Manager_set_current_scene(name.c_str());
    if(ec == OMI_SCENE_Error_kKey)
    {
        throw arc::ex::KeyError(get_interface().get_error_message());
    }
    if(ec == OMI_SCENE_Error_kParse)
    {
        throw arc::ex::ParseError(get_interface().get_error_message());
    }
}

EntityId Manager::create_entity(
        std::string const& type_name,
        std::string const& name,
        Attribute attributes)
{
    EntityId id = 0;
    OMI_SCENE_Error ec = get_interface().Manager_create_entity(
        type_name.c_str(),
        name.c_str(),
        attributes.get_ptr(),
        &id
    );

    if(ec == OMI_SCENE_Error_kKey)
    {
        throw arc::ex::KeyError(get_interface().get_error_message());
    }
    if(ec != OMI_SCENE_Error_kNone)
    {
        throw arc::ex::RuntimeError(get_interface().get_error_message());
    }

    return id;
}

void Manager::destroy_entity(EntityId id)
{
    get_interface().Manager_destroy_entity(id);
}

comp::Camera Manager::get_active_camera() const
{
    return comp::Camera(get_interface().Manager_get_active_camera(), true);
}

void Manager::set_active_camera(comp::Camera const& camera)
{
    get_interface().Manager_set_active_camera(camera.get_ptr());
}

//------------------------------------------------------------------------------
//                              PRIVATE CONSTRUCTOR
//------------------------------------------------------------------------------

Manager::Manager()
{
}

//------------------------------------------------------------------------------
//                               PRIVATE DESTRUCTOR
//------------------------------------------------------------------------------

Manager::~Manager()
{
}

} // namespace scene
} // namespace omi
