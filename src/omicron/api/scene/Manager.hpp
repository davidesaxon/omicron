/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_SCENE_MANAGER_HPP_
#define OMICRON_SCENE_MANAGER_HPP_

#include <string>

#include <arcanecore/base/lang/Restrictors.hpp>

#include "omicron/api/API.h"
#include "omicron/api/common/attribute/Attribute.hpp"
#include "omicron/api/comp/Component.hpp"
#include "omicron/api/comp/renderable/Camera.hpp"
#include "omicron/api/report/LoggerCTypes.h"
#include "omicron/api/scene/SceneCTypes.h"
#include "omicron/api/scene/_codegen/SceneInterface.hpp"


namespace omi
{
namespace scene
{

typedef OMI_SCENE_EntityId EntityId;

class Manager
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                          PUBLIC STATIC FUNCTIONS
    //--------------------------------------------------------------------------

    static Manager& instance();

    static SceneInterface& get_interface();

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    // TODO: this might need be a deffered version
    void set_current_scene(std::string const& name);

    EntityId create_entity(
            std::string const& type_name,
            std::string const& name,
            Attribute attributes);

    void destroy_entity(EntityId id);

    comp::Camera get_active_camera() const;

    void set_active_camera(comp::Camera const& camera);

    // TODO: async
    // void load_group(std::string const& name);

    // TODO: async
    // void release_grouo(std::string const& name);

    // TODO: async
    // void preload_scene(std::string const& name);

    // TODO: async
    // void preload_scene(
    //         std::string const& scene_name,
    //         std::vector<std::string> const& group_names);

    // TODO: async
    // void release_preloading();


    // void load_entity_definition(std::string const& name);

    // // TODO: async

    // void release_entity_definition(std::string const& name);

    // void load_entity_group(std::string const& name);

    // // TODO: async

    // void release_entity_group(std::string const& name);

    // void load_all();

    // // TODO: async

    // void release_all();

private:

    //--------------------------------------------------------------------------
    //                            PRIVATE CONSTRUCTOR
    //--------------------------------------------------------------------------

    Manager();

    //--------------------------------------------------------------------------
    //                             PRIVATE DESTRUCTOR
    //--------------------------------------------------------------------------

    virtual ~Manager();

};

} // namespace scene
} // namespace omi

#endif
