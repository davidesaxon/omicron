/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/scene/EntityScript.hpp"


namespace omi
{
namespace scene
{

//------------------------------------------------------------------------------
//                                  CONSTRUCTOR
//------------------------------------------------------------------------------

EntityScript::EntityScript(
        EntityId id,
        std::string const& name,
        Attribute const& data)
    : m_ptr(Manager::get_interface().EntityScript_constructor(
        id,
        this,
        &on_update_forwarder
    ))
{
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

EntityScript::~EntityScript()
{
    Manager::get_interface().EntityScript_destructor(m_ptr);
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

OMI_SCENE_EntityScriptPtr EntityScript::get_ptr() const
{
    return m_ptr;
}

EntityId EntityScript::get_id() const
{
    return static_cast<EntityId>(
        Manager::get_interface().EntityScript_get_id(m_ptr)
    );
}

comp::Component EntityScript::get_component(std::string const& name) const
{
    OMI_COMP_ComponentPtr component = nullptr;
    OMI_SCENE_Error ec = Manager::get_interface().EntityScript_get_component(
        m_ptr,
        name.c_str(),
        &component
    );
    if(ec != OMI_SCENE_Error_kNone || component == nullptr)
    {
        throw arc::ex::KeyError(
            "Entity does not have a component with the name \"" + name + "\""
        );
    }

    return comp::Component(component, true);
}

void EntityScript::destroy()
{
    Manager::get_interface().EntityScript_destroy(m_ptr);
}

//------------------------------------------------------------------------------
//                           PROTECTED MEMBER FUNCTIONS
//------------------------------------------------------------------------------

void EntityScript::on_update()
{
    // do nothing by default
}

//------------------------------------------------------------------------------
//                            PRIVATE STATIC FUNCTIONS
//------------------------------------------------------------------------------

OMI_SCENE_Error EntityScript::on_update_forwarder(void* self)
{
    try
    {
        static_cast<EntityScript*>(self)->on_update();
    }
    catch(std::exception const& exc)
    {
       Manager::get_interface().set_error_message(exc.what());
       return OMI_SCENE_Error_kRuntime;
    }
    return OMI_SCENE_Error_kNone;
}

} // namespace scene
} // namespace omi
