/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstddef>
#include <cstring>
#include <fstream>
#include <memory>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <json/json.h>

#include <arcanecore/base/lang/Restrictors.hpp>
#include <arcanecore/base/fsys/FileSystemOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>
#include <arcanecore/base/str/StringOperations.hpp>

#include "omicron/api/API.h"
#include "omicron/api/common/Attributes.hpp"
#include "omicron/api/common/MapBuilder.hpp"
#include "omicron/api/comp/Component.hpp"
#include "omicron/api/comp/ComponentImpl.hpp"
#include "omicron/api/comp/ComponentSchema.hpp"
#include "omicron/api/comp/renderable/Camera.hpp"
#include "omicron/api/comp/script/Script.hpp"
#include "omicron/api/report/Logger.hpp"
#include "omicron/api/res/Storage.hpp"
#include "omicron/api/scene/SceneCTypes.h"
#include "omicron/api/scene/private/AttributesFromJsonParameters.hpp"
#include "omicron/api/scene/private/ComponentsFromAttributes.hpp"


//------------------------------------------------------------------------------
//                              ENTITY SCRIPT STRUCT
//------------------------------------------------------------------------------

struct OMI_SCENE_EntityScriptStruct
{
    //---------------------------A T T R I B U T E S----------------------------

    OMI_SCENE_EntityId m_entity_id;
    void* m_self;
    OMI_SCENE_EntityScript_OnUpdateFunc* m_on_update_func;

    //--------------------------C O N S T R U C T O R---------------------------

    OMI_SCENE_EntityScriptStruct(
            OMI_SCENE_EntityId entity_id,
            void* self,
            OMI_SCENE_EntityScript_OnUpdateFunc* on_update_func)
        : m_entity_id     (entity_id)
        , m_self          (self)
        , m_on_update_func(on_update_func)
    {
    }
};


OMI_CODEGEN_NAMESPACE namespace omi
{
OMI_CODEGEN_NAMESPACE namespace scene
{
namespace
{

//------------------------------------------------------------------------------
//                                    CLASSES
//------------------------------------------------------------------------------

class EntityDefinition
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //---------------------------A T T R I B U T E S----------------------------

    arc::fsys::Path m_path;
    std::string m_name;
    std::string m_tag;
    std::vector<Attribute> m_component_defs;
    std::vector<omi::res::ResourceId> m_resource_ids;

    //-------------------------C O N S T R U C T O R S--------------------------

    EntityDefinition(arc::fsys::Path const& path)
        : m_path(path)
    {
        // ensure the file exists
        if(!arc::fsys::is_file(path, true))
        {
            throw arc::ex::IOError(
                "Entity definition file either does not exist or is not a "
                "file " + path.to_native()
            );
        }

        // read the file
        std::ifstream input(m_path.to_native());
        std::string contents;
        input.seekg(0, std::ios::end);
        contents.reserve(input.tellg());
        input.seekg(0, std::ios::beg);
        contents.assign(
            (std::istreambuf_iterator<char>(input)),
            std::istreambuf_iterator<char>()
        );
        input.close();

        Json::Value root;
        Json::Reader reader;
        bool parse_sucess = reader.parse(contents, root);
        if(!parse_sucess)
        {
            throw arc::ex::ParseError(
                "Failed to parse entity definition from file \"" +
                m_path.to_native() + "\" with error: " +
                reader.getFormattedErrorMessages()
            );
        }

        // ensure root is an object
        if(!root.isObject())
        {
            throw arc::ex::ParseError(
                "Failed to parse entity definition file \"" +
                m_path.to_native() + "\" because the root value is not a map"
            );
        }

        // get the name
        if(!root.isMember("name"))
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse entity definition from file \"" +
                m_path.to_native() + "\" root value is missing name field"
            );
        }
        Json::Value name_field = root["name"];
        if(!name_field.isString())
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse entity definition from file \"" +
                m_path.to_native() + "\" as name field is not of type "
                "string: " + writer.write(name_field)
            );
        }
        m_name = name_field.asString();

        // get the tag
        if(root.isMember("tag"))
        {
            Json::Value tag_field = root["tag"];
            if(!tag_field.isString())
            {
                Json::FastWriter writer;
                throw arc::ex::ParseError(
                    "Failed to parse entity definition from file \"" +
                    m_path.to_native() + "\" as tag field is not of type "
                    "string: " + writer.write(tag_field)
                );
            }
            m_tag = tag_field.asString();
        }

        // parse the components
        if(!root.isMember("components"))
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse entity definition from file \"" +
                m_path.to_native() + "\" root value is missing components field"
            );
        }
        Json::Value components_field = root["components"];
        if(!components_field.isArray())
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse entity definition from file \"" +
                m_path.to_native() + "\" as components field is not of type "
                "array: " + writer.write(components_field)
            );
        }

        // parse each component
        for(std::size_t i = 0; i < components_field.size(); ++i)
        {
            Json::Value component_value =
                components_field[static_cast<Json::ArrayIndex>(i)];
            if(!component_value.isObject())
            {
                Json::FastWriter writer;
                throw arc::ex::ParseError(
                    "Failed to parse entity definition from file \"" +
                    m_path.to_native() + "\" as components entry is not of "
                    "type object: " + writer.write(component_value)
                );
            }

            // parse the component
            scene_::attributes_from_json_parameters(
                m_path,
                component_value,
                m_component_defs,
                m_resource_ids
            );
        }
    }
};

class EntityDefinitionGroup
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //---------------------------A T T R I B U T E S----------------------------

    arc::fsys::Path m_path;
    std::string m_name;
    std::unordered_map<
        std::string,
        std::unique_ptr<EntityDefinition>
    > m_entity_definitions;
    std::unordered_map<
        std::string,
        std::unique_ptr<EntityDefinitionGroup>
    > m_entity_groups;

    //-------------------------C O N S T R U C T O R S--------------------------

    EntityDefinitionGroup()
    {
    }

    EntityDefinitionGroup(arc::fsys::Path const& path, Json::Value& root)
        : m_path(path)
    {
        // get the name
        if(!root.isMember("name"))
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse scene definition from file \"" +
                m_path.to_native() + "\" entity definition group is missing "
                "name field: " + writer.write(root)
            );
        }
        Json::Value name_field = root["name"];
        if(!name_field.isString())
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse scene definition from file \"" +
                m_path.to_native() + "\" entity definition group name field "
                "is not of type string: " + writer.write(root)
            );
        }

        // get entity definitions
        if(root.isMember("entity_definitions"))
        {
            Json::Value entity_list = root["entity_definitions"];
            if(!entity_list.isArray())
            {
                Json::FastWriter writer;
                throw arc::ex::ParseError(
                    "Failed to parse scene definition from file \"" +
                    m_path.to_native() + "\" as entity_definitions field is "
                    "not an array: " + writer.write(root)
                );
            }

            for(std::size_t i = 0; i < entity_list.size(); ++i)
            {
                Json::Value definition_path_value =
                    entity_list[static_cast<Json::ArrayIndex>(i)];
                if(!definition_path_value.isArray())
                {
                    Json::FastWriter writer;
                    throw arc::ex::ParseError(
                        "Failed to parse scene definition from file \"" +
                        m_path.to_native() + "\" as entity definition path is "
                        "not an array: " + writer.write(definition_path_value)
                    );
                }

                arc::fsys::Path definition_path;
                for(std::size_t j = 0;
                    j < definition_path_value.size();
                    ++j)
                {
                    Json::Value path_value = definition_path_value[
                        static_cast<Json::ArrayIndex>(j)
                    ];
                    if(!path_value.isString())
                    {
                        Json::FastWriter writer;
                        throw arc::ex::ParseError(
                            "Failed to parse scene definition from "
                            "file \"" + m_path.to_native() + "\" as entity "
                            "definition path contains non-string value: " +
                            writer.write(definition_path_value)
                        );
                    }

                    definition_path << path_value.asString();
                }

                // load the definition
                std::unique_ptr<EntityDefinition> entity_def(
                    new EntityDefinition(definition_path)
                );
                m_entity_definitions[entity_def->m_name] =
                    std::move(entity_def);
            }
        }

        // get entity groups
        if(root.isMember("entity_groups"))
        {
            Json::Value group_list = root["entity_groups"];
            if(!group_list.isArray())
            {
                Json::FastWriter writer;
                throw arc::ex::ParseError(
                    "Failed to parse scene definition from file \"" +
                    m_path.to_native() + "\" as entity_groups field is not an "
                    "array: " + writer.write(root)
                );
            }

            for(std::size_t i = 0; i < group_list.size(); ++i)
            {
                Json::Value group_value =
                    group_list[static_cast<Json::ArrayIndex>(i)];
                if(!group_value.isObject())
                {
                    Json::FastWriter writer;
                    throw arc::ex::ParseError(
                        "Failed to parse scene definition from file \"" +
                        m_path.to_native() + "\" as entity_groups contains a "
                        "non-object entry: " + writer.write(group_value)
                    );
                }

                std::unique_ptr<EntityDefinitionGroup> group_def(
                    new EntityDefinitionGroup(m_path, group_value)
                );
                m_entity_groups[group_def->m_name] = std::move(group_def);
            }
        }
    }

    //---------------------------D E S T R U C T O R----------------------------

    virtual ~EntityDefinitionGroup()
    {
    }

    //-------------P U B L I C    M E M B E R    F U N C T I O N S--------------

    EntityDefinition& get_definition(std::string const& definition_path) const
    {
        return get_definition(
            definition_path,
            arc::str::split(definition_path, "."),
            0
        );
    }

    EntityDefinition& get_definition(
            std::string const& definition_path,
            std::vector<std::string> const& levels,
            std::size_t offset) const
    {
        if(levels.size() - offset <= 1)
        {
            auto f_def = m_entity_definitions.find(levels[offset]);
            if(f_def == m_entity_definitions.end())
            {
                throw arc::ex::KeyError(
                    "No entity definition in scene with name: \"" +
                    definition_path + "\""
                );
            }
            return *f_def->second.get();
        }

        auto f_group = m_entity_groups.find(levels[offset]);
        if(f_group == m_entity_groups.end())
        {
            throw arc::ex::KeyError(
                "No entity definition in scene with name: \"" +
                definition_path + "\""
            );
        }
        return f_group->second->get_definition(
            definition_path,
            levels,
            offset + 1
        );
    }
};

class Entity
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //---------------------------A T T R I B U T E S----------------------------

    OMI_SCENE_EntityId const m_id;
    std::string const m_name;
    std::vector<omi::comp::Component> const m_components;
    std::unordered_map<std::string, std::size_t> const m_name_mapping;
    std::vector<omi::comp::Script> m_scripts;

    //--------------------------C O N S T R U C T O R---------------------------

    Entity(
            OMI_SCENE_EntityId id,
            std::string const& name,
            std::vector<omi::comp::Component> const& components,
            std::unordered_map<std::string, std::size_t> const& name_mapping)
        : m_id          (id)
        , m_name        (name)
        , m_components  (components)
        , m_name_mapping(name_mapping)
    {
        // store all scripts
        for(auto const& component : m_components)
        {
            if(component.get_component_type() ==
               omi::comp::ComponentType::kScript)
            {
                m_scripts.push_back(component);
            }
        }
    }

    //---------------------------D E S T R U C T O R----------------------------

    virtual ~Entity()
    {
    }
};

class QueuedEntity
{
public:

    OMI_SCENE_EntityId const m_id;
    std::string const m_name;
    std::string const m_tag;
    omi::Attribute m_data;
    std::vector<Attribute> const m_component_defs;
    std::vector<omi::res::ResourceId> const m_resource_ids;

    QueuedEntity(
            OMI_SCENE_EntityId id,
            std::string const& name,
            std::string const& tag,
            omi::Attribute const& data,
            std::vector<Attribute> const& component_defs,
            std::vector<omi::res::ResourceId> const& resource_ids)
        : m_id            (id)
        , m_name          (name)
        , m_tag           (tag)
        , m_data          (data)
        , m_component_defs(component_defs)
        , m_resource_ids  (resource_ids)
    {
    }
};

//------------------------------------------------------------------------------
//                                    GLOBALS
//------------------------------------------------------------------------------

static std::string g_error_message;

static std::unordered_map<std::string, arc::fsys::Path> g_scenes;

static std::string g_active_scene_name;
static std::unique_ptr<EntityDefinitionGroup> g_scene_definition(
    new EntityDefinitionGroup()
);

static OMI_SCENE_EntityId g_entity_id = 0;

static std::unordered_map<
    OMI_SCENE_EntityId,
    std::unique_ptr<Entity>
> g_entities;

static OMI_COMP_ComponentPtr g_active_camera = nullptr;

static std::size_t const GLOBAL_LISTS_RESERVE_SIZE = 1000;

std::vector<QueuedEntity> g_queued_entities;
std::unordered_set<OMI_SCENE_EntityId> g_marked_for_destruction;

static std::vector<OMI_COMP_ComponentPtr> g_new_components;
static std::vector<OMI_COMP_ComponentPtr> g_removed_components;

// TODO: TAGS

//------------------------------------------------------------------------------
//                                   FUNCTIONS
//------------------------------------------------------------------------------

static void create_entity_from_json(
        arc::fsys::Path const& path,
        Json::Value root)
{
    // get the definition to use
    if(!root.isMember("definition"))
    {
        Json::FastWriter writer;
        throw arc::ex::ParseError(
            "Failed to parse scene definition from file \"" +
            path.to_native() + "\" initial entity definition is missing "
            "definition field: " + writer.write(root)
        );
    }
    Json::Value definition_field = root["definition"];
    if(!definition_field.isString())
    {
        Json::FastWriter writer;
        throw arc::ex::ParseError(
            "Failed to parse scene definition from file \"" +
            path.to_native() + "\" initial entity definition definition field "
            "is not of type string: " + writer.write(root)
        );
    }
    // get the definition
    EntityDefinition const& definition = g_scene_definition->get_definition(
        definition_field.asString()
    );

    // get the name
    std::string name;
    if(root.isMember("name"))
    {
        Json::Value name_field = root["name"];
        if(!name_field.isString())
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse scene definition from file \"" +
                path.to_native() + "\" initial entity definition name field "
                "is not of type string: " + writer.write(root)
            );
        }
        name = name_field.asString();
    }

    // build the list of component attributes and resources that need loading
    std::vector<omi::Attribute> component_attrs;
    std::vector<omi::res::ResourceId> resource_ids;
    if(root.isMember("components"))
    {
        Json::Value components_field = root["components"];
        if(!components_field.isArray())
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse scene definition from file \"" +
                path.to_native() + "\" as initial entity \"" + name +
                "\" components field is not of type array: " +
                writer.write(components_field)
            );
        }

        // parse each component
        for(std::size_t i = 0; i < components_field.size(); ++i)
        {
            Json::Value component_value =
                components_field[static_cast<Json::ArrayIndex>(i)];
            if(!component_value.isObject())
            {
                Json::FastWriter writer;
                throw arc::ex::ParseError(
                    "Failed to parse scene definition from file \"" +
                    path.to_native() + "\" as initial entity \"" + name +
                    "\" components entry is not of type object: " +
                    writer.write(component_value)
                );
            }

            // parse the component
            scene_::attributes_from_json_parameters(
                path,
                component_value,
                component_attrs,
                resource_ids
            );
        }
    }
    else
    {
        component_attrs = definition.m_component_defs;
        resource_ids = definition.m_resource_ids;
    }

    // read parameter overrides
    if(root.isMember("parameter_overrides"))
    {
        Json::Value parameter_overrides = root["parameter_overrides"];
        if(!parameter_overrides.isObject())
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse scene definition from file \"" +
                path.to_native() + "\" as initial entity \"" + name +
                "\" parameter_overrides is not of type object: " +
                writer.write(parameter_overrides)
            );
        }

        // break down the existing components into builders and schemas
        std::vector<std::string> ordered_names;
        ordered_names.reserve(component_attrs.size());
        std::unordered_map<std::string, omi::MapBuilder> builders;
        std::unordered_map<std::string, omi::comp::ComponentSchema> schemas;
        for(omi::Attribute const& parameter : component_attrs)
        {
            omi::MapAttribute map_attr = parameter;
            // extract the name
            omi::StringAttribute name_attr = map_attr.at("name");
            std::string const component_name = name_attr.get_value();
            ordered_names.push_back(component_name);

            // insert all map entries into the builder
            for(char const* key : map_attr.get_keys())
            {
                builders[component_name].insert(key, map_attr.at(key));
            }

            // get the schema
            omi::StringAttribute type_attr = map_attr.at("type");
            schemas.insert(std::make_pair(
                component_name,
                omi::comp::ComponentSchema::get_schema(type_attr.get_value())
            ));
        }

        // parse overrides
        for(std::string const& member : parameter_overrides.getMemberNames())
        {
            // find the component name
            std::size_t p = member.find("::");
            if(p == std::string::npos)
            {
                Json::FastWriter writer;
                throw arc::ex::ParseError(
                    "Failed to parse scene definition from file \"" +
                    path.to_native() + "\" as initial entity \"" + name +
                    "\" parameter_overrides has field \"" + member +
                    "\" which does not include a component name: " +
                    writer.write(parameter_overrides)
                );
            }

            // split
            std::string const component_name = member.substr(0, p);
            std::string const parameter_name =
                member.substr(p + 2, member.length());

            // ensure there is a component with this name
            auto f_builder = builders.find(component_name);
            auto f_schema = schemas.find(component_name);
            if(f_builder == builders.end() || f_schema == schemas.end())
            {
                Json::FastWriter writer;
                throw arc::ex::ParseError(
                    "Failed to parse scene definition from file \"" +
                    path.to_native() + "\" as initial entity \"" + name +
                    "\" parameter_overrides has field \"" + member +
                    "\" which refers to component that doesn't exist: " +
                    writer.write(parameter_overrides)
                );
            }

            omi::comp::ComponentSchema& schema = f_schema->second;
            // ensure the parameter is exists
            if(!schema.has_parameter(parameter_name))
            {
                Json::FastWriter writer;
                throw arc::ex::ParseError(
                    "Failed to parse scene definition from file \"" +
                    path.to_native() + "\" as initial entity \"" + name +
                    "\" parameter_overrides has field \"" + member +
                    "\" which refers to component parameter that doesn't "
                    "exist: " + writer.write(parameter_overrides)
                );
            }

            // TODO: need to check parameter value is array or not!!!!

            omi::comp::ComponentParameter parameter =
                schema.get_parameter(parameter_name);

            // get the parameter JSON value
            Json::Value parameter_value = parameter_overrides[member];
            // ensure parameter size is valid
            if(parameter.get_size() != 1)
            {
                if(!parameter_value.isArray())
                {
                    Json::FastWriter writer;
                    throw arc::ex::ParseError(
                        "Failed to parse scene definition from file \"" +
                        path.to_native() + "\" as initial entity \"" + name +
                        "\" parameter_overrides has field \"" + member +
                        "\" is not of array when multiple values are expected "
                        "by the schema: " + writer.write(parameter_overrides)
                    );
                }
                // check size
                if(parameter.get_size() != comp::ComponentParameter::NULL_SIZE
                   &&
                   parameter_value.size() != parameter.get_size())
                {
                    Json::FastWriter writer;
                    throw arc::ex::ParseError(
                        "Failed to parse scene definition from file \"" +
                        path.to_native() + "\" as initial entity \"" + name +
                        "\" parameter_overrides has field \"" + member +
                        "\" does not have the required number of values "
                        "specified by the schema: " +
                        writer.write(parameter_overrides)
                    );
                }
            }

            // TODO: FIX ME: resource ids that get overridden currently still
            //               get loaded
            // get the type of the parameter
            omi::Attribute attr;
            omi::scene_::parse_attribute_value_from_json(
                path,
                member,
                std::string(parameter.get_type()),
                parameter.get_size(),
                parameter_value,
                attr,
                resource_ids
            );

            f_builder->second.insert(parameter_name, attr);
        }

        // rebuild
        component_attrs.clear();
        component_attrs.reserve(builders.size());
        for(std::string const& component_name : ordered_names)
        {
            component_attrs.push_back(builders[component_name].build());
        }
    }

    // TODO: should be thread safe
    ++g_entity_id;
    g_queued_entities.emplace_back(
        g_entity_id,
        name,
        definition.m_tag,
        omi::Attribute(),
        component_attrs,
        resource_ids
    );
}

//------------------------------------------------------------------------------
//                               GET ERROR MESSAGE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static char const* get_error_message()
{
    return g_error_message.c_str();
}

//------------------------------------------------------------------------------
//                               SET ERROR MESSAGE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void set_error_message(char const* message)
{
    g_error_message = message;
}

//------------------------------------------------------------------------------
//                                    STARTUP
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool startup(OMI_REPORT_LoggerPtr logger_ptr)
{
    report::Logger logger(logger_ptr, true);

    g_queued_entities.reserve(GLOBAL_LISTS_RESERVE_SIZE);
    g_new_components.reserve(GLOBAL_LISTS_RESERVE_SIZE);
    g_removed_components.reserve(GLOBAL_LISTS_RESERVE_SIZE);

    // build the path to scenes directory
    arc::fsys::Path const scenes_dir("scenes");
    if(!arc::fsys::is_directory(scenes_dir))
    {
        logger.critical(
            "Scenes path either does not exist or is not a directory: {0}",
            scenes_dir.to_native()
        );
        return OMI_False;
    };

    for(arc::fsys::Path const& subpath : arc::fsys::list(scenes_dir))
    {
        if(subpath.get_extension() != "json")
        {
            continue;
        }

        // read the file
        std::ifstream input(subpath.to_native());
        std::string contents;
        input.seekg(0, std::ios::end);
        contents.reserve(input.tellg());
        input.seekg(0, std::ios::beg);
        contents.assign(
            (std::istreambuf_iterator<char>(input)),
            std::istreambuf_iterator<char>()
        );
        input.close();

        Json::Value root;
        Json::Reader reader;
        bool parse_sucess = reader.parse(contents, root);
        if(!parse_sucess)
        {
            logger.critical(
                "Failed to parse scene definition from file \"{0}\" with "
                "error: {1}",
                subpath.to_native(),
                reader.getFormattedErrorMessages()
            );
            return OMI_False;
        }

        // ensure root is an object
        if(!root.isObject())
        {
            logger.critical(
                "Failed to parse scene definition file \"{0}\" because the "
                "root value is not a map ",
                subpath.to_native()
            );
            return OMI_False;
        }

        // get the name
        if(!root.isMember("name"))
        {
            logger.critical(
                "Failed to parse scene definition file \"{0}\" because the "
                "root value is missing the name field",
                subpath.to_native()
            );
            return OMI_False;
        }
        Json::Value name_field = root["name"];
        if(!name_field.isString())
        {
            Json::FastWriter writer;
            logger.critical(
                "Failed to parse scene definition file \"{0}\" as name field "
                "is not of type string: {1}",
                subpath.to_native(),
                writer.write(name_field)
            );
            return OMI_False;
        }

        // store in the global mapping
        std::string const scene_name = name_field.asString();
        auto f_scene = g_scenes.find(scene_name);
        if(f_scene != g_scenes.end())
        {
            logger.critical(
                "Failed to load scene definition file \"{0}\" as its name "
                "conflicts with an existing scene definition: \"{1}\"",
                subpath.to_native(),
                scene_name
            );
        }
        g_scenes[scene_name] = subpath;

        logger.debug(
            "Found scene definition \"{0}\" from file: {1}, ",
            scene_name,
            subpath.to_native()
        );
    }


    return OMI_True;
};

//------------------------------------------------------------------------------
//                                    CLEANUP
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void cleanup(OMI_REPORT_LoggerPtr logger_ptr)
{
    report::Logger logger(logger_ptr, true);

    logger.debug("Scene management clean up");

    // destroy entities marked for destruction
    for(auto& entity : g_entities)
    {
        // add components to the removed components list
        for(omi::comp::Component const& component : entity.second->m_components)
        {
            // note we have to take an extra reference count of the components
            // since the objects attached to the entity are about to be
            // destroyed
            omi::comp::Component_increase_reference(component.get_ptr());
            g_removed_components.push_back(component.get_ptr());
        }
    }
    g_entities.clear();
}

//------------------------------------------------------------------------------
//                                    SHUTDOWN
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Bool shutdown()
{
    // clear global lists
    g_entities.clear();
    g_scene_definition.reset(nullptr);
    g_active_scene_name = "";
    g_scenes.clear();

    return OMI_True;
};

//------------------------------------------------------------------------------
//                                     UPDATE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void update(OMI_REPORT_LoggerPtr logger_ptr)
{
    report::Logger logger(logger_ptr, true);

    // destroy entities marked for destruction
    for(OMI_SCENE_EntityId id : g_marked_for_destruction)
    {
        auto f_entity = g_entities.find(id);
        if(f_entity == g_entities.end())
        {
            continue;
        }

        // add components to the removed components list
        for(omi::comp::Component const& component :
            f_entity->second->m_components)
        {
            // note we have to take an extra reference count of the components
            // since the objects attached to the entity are about to be
            // destroyed
            omi::comp::Component_increase_reference(component.get_ptr());
            g_removed_components.push_back(component.get_ptr());
        }

        // remove the entity
        g_entities.erase(f_entity);
    }

    // create queued entities
    for(QueuedEntity const& queued_entity : g_queued_entities)
    {
        try
        {
            std::vector<omi::res::ResourceId> const& resource_ids =
                queued_entity.m_resource_ids;

            // check if the resource ids needed are loaded
            if(!res::Storage::instance().is_loaded(resource_ids))
            {
                res::Storage::instance().load_blocking(resource_ids);
                std::string const warning_messages =
                    res::Storage::instance().get_warning_messages();
                if(!warning_messages.empty())
                {
                    logger.warning(
                        "Encoutered warnings while attempting to load "
                        "resources for newly created entities:\n{0}",
                        warning_messages
                    );
                }
            }

            OMI_SCENE_EntityId const id = queued_entity.m_id;
            std::string entity_name = queued_entity.m_name;

            // build components
            std::vector<omi::comp::Component> components;
            std::unordered_map<std::string, std::size_t> name_mapping;
            scene_::components_from_attributes(
                queued_entity.m_name,
                queued_entity.m_component_defs,
                components,
                name_mapping
            );

            // add the components to the new list
            for(auto const& component : components)
            {
                g_new_components.push_back(component.get_ptr());
            }

            // if no name was provided use the entity id
            if(entity_name.empty())
            {
                entity_name = std::to_string(id);
            }
            // TODO: should also check for duplicate names

            std::unique_ptr<Entity> new_entity(new Entity(
                id,
                entity_name,
                components,
                name_mapping
            ));
            g_entities[id]  = std::move(new_entity);
            // fire script constructors
            for(omi::comp::Script& script : g_entities[id]->m_scripts)
            {
                script.call_constructor(
                    id,
                    entity_name.c_str(),
                    queued_entity.m_data.get_ptr()
                );
            }
        }
        catch(std::exception const& exc)
        {
            logger.error(
                "Failed to create entity {0}:{1} with error: {2}",
                queued_entity.m_id,
                queued_entity.m_name,
                exc.what()
            );
            continue;
        }
    }
}

//------------------------------------------------------------------------------
//                               GET NEW COMPONENTS
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size get_new_components(OMI_COMP_ComponentPtr** out_components)
{
    *out_components = &g_new_components[0];
    return g_new_components.size();
}

//------------------------------------------------------------------------------
//                             GET REMOVED COMPONENTS
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_Size get_removed_components(OMI_COMP_ComponentPtr** out_components)
{
    *out_components = &g_removed_components[0];
    return g_removed_components.size();
}

//------------------------------------------------------------------------------
//                               CLEAR GLOBAL LISTS
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void clear_global_lists()
{
    g_queued_entities.clear();
    g_marked_for_destruction.clear();
    g_new_components.clear();
    for(OMI_COMP_ComponentPtr component : g_removed_components)
    {
        omi::comp::Component_decrease_reference(component);
    }
    g_removed_components.clear();

    g_queued_entities.reserve(GLOBAL_LISTS_RESERVE_SIZE);
    g_new_components.reserve(GLOBAL_LISTS_RESERVE_SIZE);
    g_removed_components.reserve(GLOBAL_LISTS_RESERVE_SIZE);
}

//------------------------------------------------------------------------------
//                           MANAGER SET CURRENT SCENE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_SCENE_Error Manager_set_current_scene(char const* name)
{
    auto f_scene = g_scenes.find(name);
    if(f_scene == g_scenes.end())
    {
        return OMI_SCENE_Error_kKey;
    }

    // TODO: clean up the current scene entities and components and active
    //       camera

    g_active_scene_name = name;

    // read the file
    std::ifstream input(f_scene->second.to_native());
    std::string contents;
    input.seekg(0, std::ios::end);
    contents.reserve(input.tellg());
    input.seekg(0, std::ios::beg);
    contents.assign(
        (std::istreambuf_iterator<char>(input)),
        std::istreambuf_iterator<char>()
    );
    input.close();

    Json::Value root;
    Json::Reader reader;
    bool parse_sucess = reader.parse(contents, root);
    if(!parse_sucess)
    {
        g_error_message =
            "Failed to parse scene definition from file \"" +
            f_scene->second.to_native() + "\" with error: " +
            reader.getFormattedErrorMessages();
        return OMI_SCENE_Error_kParse;
    }

    try
    {
        g_scene_definition.reset(new EntityDefinitionGroup(
            f_scene->second,
            root
        ));
    }
    catch(std::exception const& exc)
    {
        g_error_message = exc.what();
        return OMI_SCENE_Error_kParse;
    }

    // construct the initial entities
    if(!root.isMember("initial_entities"))
    {
        g_error_message =
            "Failed to parse scene definition from file \"" +
            f_scene->second.to_native() + "\" as the initial_entities field is "
            "missing";
        return OMI_SCENE_Error_kParse;
    }
    Json::Value initial_entities_field = root["initial_entities"];
    if(!initial_entities_field.isArray())
    {
        Json::FastWriter writer;
        g_error_message =
            "Failed to parse scene definition from file \"" +
            f_scene->second.to_native() + "\" as the initial_entities field is "
            "not of array type: " + writer.write(initial_entities_field);
        return OMI_SCENE_Error_kParse;
    }
    for(std::size_t i = 0; i < initial_entities_field.size(); ++i)
    {
        try
        {
            create_entity_from_json(
                f_scene->second,
                initial_entities_field[static_cast<Json::ArrayIndex>(i)]
            );
        }
        catch(std::exception const& exc)
        {
            g_error_message = exc.what();
            return OMI_SCENE_Error_kParse;
        }
    }

    return OMI_SCENE_Error_kNone;
};

//------------------------------------------------------------------------------
//                             MANAGER CREATE ENTITY
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_SCENE_Error Manager_create_entity(
        char const* type_name,
        char const* name,
        OMI_AttributePtr data,
        OMI_SCENE_EntityId* out_id)
{
    // get the definition
    EntityDefinition const* definition = nullptr;
    try
    {
        definition = &g_scene_definition->get_definition(type_name);
    }
    catch(...)
    {
        g_error_message =
            "Cannot create entity as no definition with the name \"" +
            std::string(type_name) + "\" exists";
        return OMI_SCENE_Error_kKey;
    }

    // TODO: should be thread safe
    ++g_entity_id;
    g_queued_entities.emplace_back(
        g_entity_id,
        name,
        // TODO: tag support
        "",
        data,
        definition->m_component_defs,
        definition->m_resource_ids
    );

    return OMI_SCENE_Error_kNone;
}

//------------------------------------------------------------------------------
//                             MANAGER DESTROY ENTITY
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Manager_destroy_entity(OMI_SCENE_EntityId id)
{
    g_marked_for_destruction.insert(id);
}

//------------------------------------------------------------------------------
//                           MANAGER GET ACTIVE CAMERA
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_COMP_ComponentPtr Manager_get_active_camera()
{
    return g_active_camera;
}

//------------------------------------------------------------------------------
//                           MANAGER SET ACTIVE CAMERA
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void Manager_set_active_camera(OMI_COMP_ComponentPtr camera)
{
    // clean up the current camera
    if(g_active_camera != nullptr)
    {
        comp::Component_decrease_reference(g_active_camera);
    }
    g_active_camera = camera;
    if(g_active_camera != nullptr)
    {
        comp::Component_increase_reference(g_active_camera);
    }
}

//------------------------------------------------------------------------------
//                           ENTITY SCRIPT CONSTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_SCENE_EntityScriptPtr EntityScript_constructor(
        OMI_SCENE_EntityId id,
        void* self,
        OMI_SCENE_EntityScript_OnUpdateFunc* on_update_func)
{
    return new OMI_SCENE_EntityScriptStruct(id, self, on_update_func);
}

//------------------------------------------------------------------------------
//                            ENTITY SCRIPT DESTRUCTOR
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void EntityScript_destructor(OMI_SCENE_EntityScriptPtr ptr)
{
    if(ptr == nullptr)
    {
        return;
    }

    delete ptr;
}

//------------------------------------------------------------------------------
//                              ENTITY SCRIPT GET ID
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_SCENE_EntityId EntityScript_get_id(
        OMI_SCENE_EntityScriptPtrConst ptr)
{
    return ptr->m_entity_id;
}

//------------------------------------------------------------------------------
//                          ENTITY SCRIPT GET COMPONENT
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_SCENE_Error EntityScript_get_component(
        OMI_SCENE_EntityScriptPtrConst ptr,
        char const* component_name,
        OMI_COMP_ComponentPtr* out_component)
{
    auto f_entity = g_entities.find(ptr->m_entity_id);
    if(f_entity == g_entities.end())
    {
        return OMI_SCENE_Error_kKey;
    }

    auto f_comp = f_entity->second->m_name_mapping.find(component_name);
    if(f_comp == f_entity->second->m_name_mapping.end())
    {
        return OMI_SCENE_Error_kKey;
    }

    *out_component = f_entity->second->m_components[f_comp->second].get_ptr();
    return OMI_SCENE_Error_kNone;
}

//------------------------------------------------------------------------------
//                             ENTITY SCRIPT DESTROY
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static void EntityScript_destroy(OMI_SCENE_EntityScriptPtrConst ptr)
{
    Manager_destroy_entity(ptr->m_entity_id);
}

//------------------------------------------------------------------------------
//                              ENTITY SCRIPT UPDATE
//------------------------------------------------------------------------------

OMI_CODEGEN_FUNCTION
static OMI_SCENE_Error EntityScript_on_update(
        OMI_SCENE_EntityScriptPtrConst ptr)
{
    return ptr->m_on_update_func(ptr->m_self);
}

} // namespace anonymous
} // namespace scene
} // namespace omi

#include "omicron/api/scene/_codegen/SceneBinding.inl"
