/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#include <cstring>

#include "omicron/api/API.h"

#include "omicron/api/scene/_codegen/SceneCSymbols.h"


#ifdef __cplusplus
extern "C" {
#endif

OMI_API_EXPORT int OMI_SCENE_Scene_BINDING_IMPL(char const* func_def, void** func_ptr)
{
    if(strcmp(func_def, OMI_SCENE_Scene_get_error_message_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::scene::get_error_message;
        return 0;
    }
    if(strcmp(func_def, OMI_SCENE_Scene_set_error_message_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::scene::set_error_message;
        return 0;
    }
    if(strcmp(func_def, OMI_SCENE_Scene_startup_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::scene::startup;
        return 0;
    }
    if(strcmp(func_def, OMI_SCENE_Scene_cleanup_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::scene::cleanup;
        return 0;
    }
    if(strcmp(func_def, OMI_SCENE_Scene_shutdown_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::scene::shutdown;
        return 0;
    }
    if(strcmp(func_def, OMI_SCENE_Scene_update_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::scene::update;
        return 0;
    }
    if(strcmp(func_def, OMI_SCENE_Scene_get_new_components_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::scene::get_new_components;
        return 0;
    }
    if(strcmp(func_def, OMI_SCENE_Scene_get_removed_components_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::scene::get_removed_components;
        return 0;
    }
    if(strcmp(func_def, OMI_SCENE_Scene_clear_global_lists_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::scene::clear_global_lists;
        return 0;
    }
    if(strcmp(func_def, OMI_SCENE_Scene_Manager_set_current_scene_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::scene::Manager_set_current_scene;
        return 0;
    }
    if(strcmp(func_def, OMI_SCENE_Scene_Manager_create_entity_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::scene::Manager_create_entity;
        return 0;
    }
    if(strcmp(func_def, OMI_SCENE_Scene_Manager_destroy_entity_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::scene::Manager_destroy_entity;
        return 0;
    }
    if(strcmp(func_def, OMI_SCENE_Scene_Manager_get_active_camera_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::scene::Manager_get_active_camera;
        return 0;
    }
    if(strcmp(func_def, OMI_SCENE_Scene_Manager_set_active_camera_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::scene::Manager_set_active_camera;
        return 0;
    }
    if(strcmp(func_def, OMI_SCENE_Scene_EntityScript_constructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::scene::EntityScript_constructor;
        return 0;
    }
    if(strcmp(func_def, OMI_SCENE_Scene_EntityScript_destructor_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::scene::EntityScript_destructor;
        return 0;
    }
    if(strcmp(func_def, OMI_SCENE_Scene_EntityScript_get_id_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::scene::EntityScript_get_id;
        return 0;
    }
    if(strcmp(func_def, OMI_SCENE_Scene_EntityScript_get_component_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::scene::EntityScript_get_component;
        return 0;
    }
    if(strcmp(func_def, OMI_SCENE_Scene_EntityScript_destroy_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::scene::EntityScript_destroy;
        return 0;
    }
    if(strcmp(func_def, OMI_SCENE_Scene_EntityScript_on_update_SYMBOL) == 0)
    {
        *func_ptr = (void*) &omi::scene::EntityScript_on_update;
        return 0;
    }

    return 1;
}

#ifdef __cplusplus
} // extern "C"
#endif

