/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_SCENE_CODEGEN_SCENECSMYBOLS_H_
#define OMI_SCENE_CODEGEN_SCENECSMYBOLS_H_


#ifdef __cplusplus
extern "C" {
#endif

// OMI_SCENE_get_error_message
static char const* OMI_SCENE_Scene_get_error_message_SYMBOL = "char const* OMI_SCENE_Scene_get_error_message()";

// OMI_SCENE_set_error_message
static char const* OMI_SCENE_Scene_set_error_message_SYMBOL = "void OMI_SCENE_Scene_set_error_message(char const*)";

// OMI_SCENE_startup
static char const* OMI_SCENE_Scene_startup_SYMBOL = "OMI_Bool OMI_SCENE_Scene_startup(OMI_REPORT_LoggerPtr)";

// OMI_SCENE_cleanup
static char const* OMI_SCENE_Scene_cleanup_SYMBOL = "void OMI_SCENE_Scene_cleanup(OMI_REPORT_LoggerPtr)";

// OMI_SCENE_shutdown
static char const* OMI_SCENE_Scene_shutdown_SYMBOL = "OMI_Bool OMI_SCENE_Scene_shutdown()";

// OMI_SCENE_update
static char const* OMI_SCENE_Scene_update_SYMBOL = "void OMI_SCENE_Scene_update(OMI_REPORT_LoggerPtr)";

// OMI_SCENE_get_new_components
static char const* OMI_SCENE_Scene_get_new_components_SYMBOL = "OMI_Size OMI_SCENE_Scene_get_new_components(OMI_COMP_ComponentPtr**)";

// OMI_SCENE_get_removed_components
static char const* OMI_SCENE_Scene_get_removed_components_SYMBOL = "OMI_Size OMI_SCENE_Scene_get_removed_components(OMI_COMP_ComponentPtr**)";

// OMI_SCENE_clear_global_lists
static char const* OMI_SCENE_Scene_clear_global_lists_SYMBOL = "void OMI_SCENE_Scene_clear_global_lists()";

// OMI_SCENE_Manager_set_current_scene
static char const* OMI_SCENE_Scene_Manager_set_current_scene_SYMBOL = "OMI_SCENE_Error OMI_SCENE_Scene_Manager_set_current_scene(char const*)";

// OMI_SCENE_Manager_create_entity
static char const* OMI_SCENE_Scene_Manager_create_entity_SYMBOL = "OMI_SCENE_Error OMI_SCENE_Scene_Manager_create_entity(char const*, char const*, OMI_AttributePtr, OMI_SCENE_EntityId*)";

// OMI_SCENE_Manager_destroy_entity
static char const* OMI_SCENE_Scene_Manager_destroy_entity_SYMBOL = "void OMI_SCENE_Scene_Manager_destroy_entity(OMI_SCENE_EntityId)";

// OMI_SCENE_Manager_get_active_camera
static char const* OMI_SCENE_Scene_Manager_get_active_camera_SYMBOL = "OMI_COMP_ComponentPtr OMI_SCENE_Scene_Manager_get_active_camera()";

// OMI_SCENE_Manager_set_active_camera
static char const* OMI_SCENE_Scene_Manager_set_active_camera_SYMBOL = "void OMI_SCENE_Scene_Manager_set_active_camera(OMI_COMP_ComponentPtr)";

// OMI_SCENE_EntityScript_constructor
static char const* OMI_SCENE_Scene_EntityScript_constructor_SYMBOL = "OMI_SCENE_EntityScriptPtr OMI_SCENE_Scene_EntityScript_constructor(OMI_SCENE_EntityId, void*, OMI_SCENE_EntityScript_OnUpdateFunc*)";

// OMI_SCENE_EntityScript_destructor
static char const* OMI_SCENE_Scene_EntityScript_destructor_SYMBOL = "void OMI_SCENE_Scene_EntityScript_destructor(OMI_SCENE_EntityScriptPtr)";

// OMI_SCENE_EntityScript_get_id
static char const* OMI_SCENE_Scene_EntityScript_get_id_SYMBOL = "OMI_SCENE_EntityId OMI_SCENE_Scene_EntityScript_get_id(OMI_SCENE_EntityScriptPtrConst)";

// OMI_SCENE_EntityScript_get_component
static char const* OMI_SCENE_Scene_EntityScript_get_component_SYMBOL = "OMI_SCENE_Error OMI_SCENE_Scene_EntityScript_get_component(OMI_SCENE_EntityScriptPtrConst, char const*, OMI_COMP_ComponentPtr*)";

// OMI_SCENE_EntityScript_destroy
static char const* OMI_SCENE_Scene_EntityScript_destroy_SYMBOL = "void OMI_SCENE_Scene_EntityScript_destroy(OMI_SCENE_EntityScriptPtrConst)";

// OMI_SCENE_EntityScript_on_update
static char const* OMI_SCENE_Scene_EntityScript_on_update_SYMBOL = "OMI_SCENE_Error OMI_SCENE_Scene_EntityScript_on_update(OMI_SCENE_EntityScriptPtrConst)";

#ifdef __cplusplus
} // extern "C"
#endif

#endif