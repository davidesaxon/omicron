/*!
 * \file
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 * \note This file was automatically generated.
 */
#ifndef OMI_SCENE_CODEGEN_SCENEINTERFACE_HPP_
#define OMI_SCENE_CODEGEN_SCENEINTERFACE_HPP_

#include <cassert>
#include <string>

#include <arcanecore/base/dl/DLOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>
#include <arcanecore/base/lang/Restrictors.hpp>

#include "omicron/api/scene/_codegen/SceneCSymbols.h"


namespace omi
{
namespace scene
{

class SceneInterface
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                              TYPE DEFINITIONS
    //--------------------------------------------------------------------------

    typedef OMI_Bool (BindingFunc)(char const*, void**);

    //--------------------------------------------------------------------------
    //                             PUBLIC ATTRIBUTES
    //--------------------------------------------------------------------------

    // get_error_message
    char const* (*get_error_message)();
    // set_error_message
    void (*set_error_message)(char const*);
    // startup
    OMI_Bool (*startup)(OMI_REPORT_LoggerPtr);
    // cleanup
    void (*cleanup)(OMI_REPORT_LoggerPtr);
    // shutdown
    OMI_Bool (*shutdown)();
    // update
    void (*update)(OMI_REPORT_LoggerPtr);
    // get_new_components
    OMI_Size (*get_new_components)(OMI_COMP_ComponentPtr**);
    // get_removed_components
    OMI_Size (*get_removed_components)(OMI_COMP_ComponentPtr**);
    // clear_global_lists
    void (*clear_global_lists)();
    // Manager_set_current_scene
    OMI_SCENE_Error (*Manager_set_current_scene)(char const*);
    // Manager_create_entity
    OMI_SCENE_Error (*Manager_create_entity)(char const*, char const*, OMI_AttributePtr, OMI_SCENE_EntityId*);
    // Manager_destroy_entity
    void (*Manager_destroy_entity)(OMI_SCENE_EntityId);
    // Manager_get_active_camera
    OMI_COMP_ComponentPtr (*Manager_get_active_camera)();
    // Manager_set_active_camera
    void (*Manager_set_active_camera)(OMI_COMP_ComponentPtr);
    // EntityScript_constructor
    OMI_SCENE_EntityScriptPtr (*EntityScript_constructor)(OMI_SCENE_EntityId, void*, OMI_SCENE_EntityScript_OnUpdateFunc*);
    // EntityScript_destructor
    void (*EntityScript_destructor)(OMI_SCENE_EntityScriptPtr);
    // EntityScript_get_id
    OMI_SCENE_EntityId (*EntityScript_get_id)(OMI_SCENE_EntityScriptPtrConst);
    // EntityScript_get_component
    OMI_SCENE_Error (*EntityScript_get_component)(OMI_SCENE_EntityScriptPtrConst, char const*, OMI_COMP_ComponentPtr*);
    // EntityScript_destroy
    void (*EntityScript_destroy)(OMI_SCENE_EntityScriptPtrConst);
    // EntityScript_on_update
    OMI_SCENE_Error (*EntityScript_on_update)(OMI_SCENE_EntityScriptPtrConst);

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    SceneInterface(std::string const& libname)
        : get_error_message(nullptr)
        , set_error_message(nullptr)
        , startup(nullptr)
        , cleanup(nullptr)
        , shutdown(nullptr)
        , update(nullptr)
        , get_new_components(nullptr)
        , get_removed_components(nullptr)
        , clear_global_lists(nullptr)
        , Manager_set_current_scene(nullptr)
        , Manager_create_entity(nullptr)
        , Manager_destroy_entity(nullptr)
        , Manager_get_active_camera(nullptr)
        , Manager_set_active_camera(nullptr)
        , EntityScript_constructor(nullptr)
        , EntityScript_destructor(nullptr)
        , EntityScript_get_id(nullptr)
        , EntityScript_get_component(nullptr)
        , EntityScript_destroy(nullptr)
        , EntityScript_on_update(nullptr)
    {
        #ifdef ARC_OS_WINDOWS
            arc::fsys::Path libpath({
                "build",
                "windows",
                "Release",
                libname + "_impl.dll"
            });
        #elif defined(ARC_OS_UNIX)
            arc::fsys::Path libpath({
                "build",
                "linux",
                "lib" + libname + "_impl.so"
            });
        #else
            assert(false);
        #endif

        arc::dl::Handle lib_handle = arc::dl::open_library(libpath);
        BindingFunc* binding_func = arc::dl::bind_symbol<BindingFunc>(lib_handle, "OMI_SCENE_Scene_BINDING_IMPL");

        if(binding_func(OMI_SCENE_Scene_get_error_message_SYMBOL, (void**) &get_error_message) != 0)
        {
        }
        if(binding_func(OMI_SCENE_Scene_set_error_message_SYMBOL, (void**) &set_error_message) != 0)
        {
        }
        if(binding_func(OMI_SCENE_Scene_startup_SYMBOL, (void**) &startup) != 0)
        {
        }
        if(binding_func(OMI_SCENE_Scene_cleanup_SYMBOL, (void**) &cleanup) != 0)
        {
        }
        if(binding_func(OMI_SCENE_Scene_shutdown_SYMBOL, (void**) &shutdown) != 0)
        {
        }
        if(binding_func(OMI_SCENE_Scene_update_SYMBOL, (void**) &update) != 0)
        {
        }
        if(binding_func(OMI_SCENE_Scene_get_new_components_SYMBOL, (void**) &get_new_components) != 0)
        {
        }
        if(binding_func(OMI_SCENE_Scene_get_removed_components_SYMBOL, (void**) &get_removed_components) != 0)
        {
        }
        if(binding_func(OMI_SCENE_Scene_clear_global_lists_SYMBOL, (void**) &clear_global_lists) != 0)
        {
        }
        if(binding_func(OMI_SCENE_Scene_Manager_set_current_scene_SYMBOL, (void**) &Manager_set_current_scene) != 0)
        {
        }
        if(binding_func(OMI_SCENE_Scene_Manager_create_entity_SYMBOL, (void**) &Manager_create_entity) != 0)
        {
        }
        if(binding_func(OMI_SCENE_Scene_Manager_destroy_entity_SYMBOL, (void**) &Manager_destroy_entity) != 0)
        {
        }
        if(binding_func(OMI_SCENE_Scene_Manager_get_active_camera_SYMBOL, (void**) &Manager_get_active_camera) != 0)
        {
        }
        if(binding_func(OMI_SCENE_Scene_Manager_set_active_camera_SYMBOL, (void**) &Manager_set_active_camera) != 0)
        {
        }
        if(binding_func(OMI_SCENE_Scene_EntityScript_constructor_SYMBOL, (void**) &EntityScript_constructor) != 0)
        {
        }
        if(binding_func(OMI_SCENE_Scene_EntityScript_destructor_SYMBOL, (void**) &EntityScript_destructor) != 0)
        {
        }
        if(binding_func(OMI_SCENE_Scene_EntityScript_get_id_SYMBOL, (void**) &EntityScript_get_id) != 0)
        {
        }
        if(binding_func(OMI_SCENE_Scene_EntityScript_get_component_SYMBOL, (void**) &EntityScript_get_component) != 0)
        {
        }
        if(binding_func(OMI_SCENE_Scene_EntityScript_destroy_SYMBOL, (void**) &EntityScript_destroy) != 0)
        {
        }
        if(binding_func(OMI_SCENE_Scene_EntityScript_on_update_SYMBOL, (void**) &EntityScript_on_update) != 0)
        {
        }
    }
};

} // namespace omi
} // namespace scene


#endif