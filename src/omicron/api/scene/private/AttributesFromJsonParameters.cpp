/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <arcanecore/base/str/StringOperations.hpp>

#include "omicron/api/common/MapBuilder.hpp"
#include "omicron/api/comp/ComponentSchema.hpp"
#include "omicron/api/scene/private/AttributesFromJsonParameters.hpp"


namespace omi
{
namespace scene_
{

void attributes_from_json_parameters(
        arc::fsys::Path const& path,
        Json::Value& root,
        std::vector<Attribute>& out_attrs,
        std::vector<omi::res::ResourceId>& out_resource_ids)
{
    // TODO: need to collect resource ids here

    // get the type
    if(!root.isMember("type"))
    {
        Json::FastWriter writer;
        throw arc::ex::ParseError(
            "Failed to parse entity definition from file \"" +
            path.to_native() + "\" component definition is missing type "
            " field " + writer.write(root)
        );
    }
    Json::Value type_field = root["type"];
    if(!type_field.isString())
    {
        Json::FastWriter writer;
        throw arc::ex::ParseError(
            "Failed to parse entity definition from file \"" +
            path.to_native() + "\" as component definition type field is "
            "not of type string: " + writer.write(root)
        );
    }
    std::string const type = type_field.asString();

    // get the schema for the name
    comp::ComponentSchema schema = comp::ComponentSchema::get_schema(type);

    // start building attributes
    omi::MapBuilder builder;
    builder.insert("type", omi::StringAttribute(type));

    // optional name?
    if(root.isMember("name"))
    {
        Json::Value name_field = root["name"];
        if(!name_field.isString())
        {
            Json::FastWriter writer;
            throw arc::ex::ParseError(
                "Failed to parse entity definition from file \"" +
                path.to_native() + "\" as component definition name "
                "field is not of type string: " + writer.write(root)
            );
        }

        builder.insert("name", omi::StringAttribute(name_field.asString()));
    }

    // parse parameters
    for(std::string const& member : root.getMemberNames())
    {
        // skip fields we've already parsed
        if(member == "type" || member == "name")
        {
            continue;
        }

        // otherwise try get the member name as a parameter from the schema
        if(!schema.has_parameter(member))
        {
            Json::FastWriter writer;
            throw arc::ex::KeyError(
                "Entity definition from file \"" + path.to_native() +
                "\" defines component parameter \"" + member + "\" that "
                "does not exist within component schema: " +
                writer.write(root)
            );
        }
        comp::ComponentParameter parameter = schema.get_parameter(member);

        // get the parameter json value
        Json::Value parameter_value = root[member];
        // type check based on required size
        if(parameter.get_size() != 1)
        {
            if(!parameter_value.isArray())
            {
                Json::FastWriter writer;
                throw arc::ex::ParseError(
                    "Failed to parse entity definition from file \"" +
                    path.to_native() + "\" as component parameter \"" +
                    member + "\" is not of array type when multiple values "
                    "are expected by the schema: " + writer.write(root)
                );
            }
            // check size
            if(parameter.get_size() != comp::ComponentParameter::NULL_SIZE
               &&
               parameter_value.size() != parameter.get_size())
            {
                Json::FastWriter writer;
                throw arc::ex::ParseError(
                    "Failed to parse entity definition from file \"" +
                    path.to_native() + "\" as component parameter \"" +
                    member + "\" does not have the required number of values "
                    "specified by the schema: " + writer.write(root)
                );
            }
        }

        // get the type of the parameter
        omi::Attribute attr;
        parse_attribute_value_from_json(
            path,
            member,
            std::string(parameter.get_type()),
            parameter.get_size(),
            parameter_value,
            attr,
            out_resource_ids
        );

        builder.insert(member, attr);
    }

    out_attrs.push_back(builder.build());
}

void parse_attribute_value_from_json(
        arc::fsys::Path const& path,
        std::string const& name,
        std::string const& type,
        std::size_t size,
        Json::Value& root,
        omi::Attribute& out_attr,
        std::vector<omi::res::ResourceId>& out_resource_ids)
{
    if(type == "bool")
    {
        if(size == 1)
        {
            if(!root.isBool())
            {
                Json::FastWriter writer;
                throw arc::ex::ParseError(
                    "Failed to parse entity definition from file \"" +
                    path.to_native() + "\" as component "
                    "parameter \"" + name + "\" value does not match "
                    "bool type as specified by the schema: " +
                    writer.write(root)
                );
            }

            out_attr = omi::ByteAttribute(
                static_cast<omi::ByteAttribute::DataType>(
                    root.asBool()
                )
            );
        }
        else
        {
            std::vector<omi::ByteAttribute::DataType> values;
            values.reserve(root.size());
            for(std::size_t i = 0; i < root.size(); ++i)
            {
                Json::Value sub_value =
                    root[static_cast<Json::ArrayIndex>(i)];

                if(!sub_value.isBool())
                {
                    Json::FastWriter writer;
                    throw arc::ex::ParseError(
                        "Failed to parse entity definition from "
                        "file \"" + path.to_native() + "\" as "
                        "component parameter \"" + name + "\" value "
                        "does not match bool type as specified by the "
                        "schema: " + writer.write(root)
                    );
                }

                values.push_back(
                    static_cast<omi::ByteAttribute::DataType>(
                        sub_value.asBool()
                    )
                );
            }

            out_attr = omi::ByteAttribute(&values[0], values.size());
        }
    }
    else if(type == "int16")
    {
        if(size == 1)
        {
            if(!root.isIntegral())
            {
                Json::FastWriter writer;
                throw arc::ex::ParseError(
                    "Failed to parse entity definition from file \"" +
                    path.to_native() + "\" as component "
                    "parameter \"" + name + "\" value does not match "
                    "int16 type as specified by the schema: " +
                    writer.write(root)
                );
            }

            out_attr = omi::Int16Attribute(
                static_cast<omi::Int16Attribute::DataType>(
                    root.asInt()
                )
            );
        }
        else
        {
            std::vector<omi::Int16Attribute::DataType> values;
            values.reserve(root.size());
            for(std::size_t i = 0; i < root.size(); ++i)
            {
                Json::Value sub_value =
                    root[static_cast<Json::ArrayIndex>(i)];

                if(!sub_value.isIntegral())
                {
                    Json::FastWriter writer;
                    throw arc::ex::ParseError(
                        "Failed to parse entity definition from "
                        "file \"" + path.to_native() + "\" as "
                        "component parameter \"" + name + "\" value "
                        "does not match int16 type as specified by the "
                        "schema: " + writer.write(root)
                    );
                }

                values.push_back(
                    static_cast<omi::Int16Attribute::DataType>(
                        sub_value.asInt()
                    )
                );
            }

            out_attr = omi::Int16Attribute(&values[0], values.size());
        }
    }
    else if(type == "int32")
    {
        if(size == 1)
        {
            if(!root.isIntegral())
            {
                Json::FastWriter writer;
                throw arc::ex::ParseError(
                    "Failed to parse entity definition from file \"" +
                    path.to_native() + "\" as component "
                    "parameter \"" + name + "\" value does not match "
                    "int32 type as specified by the schema: " +
                    writer.write(root)
                );
            }

            out_attr = omi::Int32Attribute(
                static_cast<omi::Int32Attribute::DataType>(
                    root.asInt()
                )
            );
        }
        else
        {
            std::vector<omi::Int32Attribute::DataType> values;
            values.reserve(root.size());
            for(std::size_t i = 0; i < root.size(); ++i)
            {
                Json::Value sub_value =
                    root[static_cast<Json::ArrayIndex>(i)];

                if(!sub_value.isIntegral())
                {
                    Json::FastWriter writer;
                    throw arc::ex::ParseError(
                        "Failed to parse entity definition from "
                        "file \"" + path.to_native() + "\" as "
                        "component parameter \"" + name + "\" value "
                        "does not match int32 type as specified by the "
                        "schema: " + writer.write(root)
                    );
                }

                values.push_back(
                    static_cast<omi::Int32Attribute::DataType>(
                        sub_value.asInt()
                    )
                );
            }

            out_attr = omi::Int32Attribute(&values[0], values.size());
        }
    }
    else if(type == "int64")
    {
        if(size == 1)
        {
            if(!root.isIntegral())
            {
                Json::FastWriter writer;
                throw arc::ex::ParseError(
                    "Failed to parse entity definition from file \"" +
                    path.to_native() + "\" as component "
                    "parameter \"" + name + "\" value does not match "
                    "int64 type as specified by the schema: " +
                    writer.write(root)
                );
            }

            out_attr = omi::Int64Attribute(
                static_cast<omi::Int64Attribute::DataType>(
                    root.asInt64()
                )
            );
        }
        else
        {
            std::vector<omi::Int64Attribute::DataType> values;
            values.reserve(root.size());
            for(std::size_t i = 0; i < root.size(); ++i)
            {
                Json::Value sub_value =
                    root[static_cast<Json::ArrayIndex>(i)];

                if(!sub_value.isIntegral())
                {
                    Json::FastWriter writer;
                    throw arc::ex::ParseError(
                        "Failed to parse entity definition from "
                        "file \"" + path.to_native() + "\" as "
                        "component parameter \"" + name + "\" value "
                        "does not match int64 type as specified by the "
                        "schema: " + writer.write(root)
                    );
                }

                values.push_back(
                    static_cast<omi::Int64Attribute::DataType>(
                        sub_value.asInt64()
                    )
                );
            }

            out_attr = omi::Int64Attribute(&values[0], values.size());
        }
    }
    else if(type == "float32")
    {
        if(size == 1)
        {
            if(!root.isDouble())
            {
                Json::FastWriter writer;
                throw arc::ex::ParseError(
                    "Failed to parse entity definition from file \"" +
                    path.to_native() + "\" as component "
                    "parameter \"" + name + "\" value does not match "
                    "float32 type as specified by the schema: " +
                    writer.write(root)
                );
            }

            out_attr = omi::Float32Attribute(
                static_cast<omi::Float32Attribute::DataType>(
                    root.asFloat()
                )
            );
        }
        else
        {
            std::vector<omi::Float32Attribute::DataType> values;
            values.reserve(root.size());
            for(std::size_t i = 0; i < root.size(); ++i)
            {
                Json::Value sub_value =
                    root[static_cast<Json::ArrayIndex>(i)];

                if(!sub_value.isDouble())
                {
                    Json::FastWriter writer;
                    throw arc::ex::ParseError(
                        "Failed to parse entity definition from "
                        "file \"" + path.to_native() + "\" as "
                        "component parameter \"" + name + "\" value "
                        "does not match float32 type as specified by "
                        "the schema: " + writer.write(root)
                    );
                }

                values.push_back(
                    static_cast<omi::Float32Attribute::DataType>(
                        sub_value.asFloat()
                    )
                );
            }

            out_attr = omi::Float32Attribute(&values[0], values.size());
        }
    }
    else if(type == "float64")
    {
        if(size == 1)
        {
            if(!root.isDouble())
            {
                Json::FastWriter writer;
                throw arc::ex::ParseError(
                    "Failed to parse entity definition from file \"" +
                    path.to_native() + "\" as component "
                    "parameter \"" + name + "\" value does not match "
                    "float64 type as specified by the schema: " +
                    writer.write(root)
                );
            }

            out_attr = omi::Float64Attribute(
                static_cast<omi::Float64Attribute::DataType>(
                    root.asDouble()
                )
            );
        }
        else
        {
            std::vector<omi::Float64Attribute::DataType> values;
            values.reserve(root.size());
            for(std::size_t i = 0; i < root.size(); ++i)
            {
                Json::Value sub_value =
                    root[static_cast<Json::ArrayIndex>(i)];

                if(!sub_value.isDouble())
                {
                    Json::FastWriter writer;
                    throw arc::ex::ParseError(
                        "Failed to parse entity definition from "
                        "file \"" + path.to_native() + "\" as "
                        "component parameter \"" + name + "\" value "
                        "does not match float64 type as specified by "
                        "the schema: " + writer.write(root)
                    );
                }

                values.push_back(
                    static_cast<omi::Float64Attribute::DataType>(
                        sub_value.asDouble()
                    )
                );
            }

            out_attr = omi::Float64Attribute(&values[0], values.size());
        }
    }
    else if(type == "resource")
    {
        if(size == 1)
        {
            if(!root.isString())
            {
                Json::FastWriter writer;
                throw arc::ex::ParseError(
                    "Failed to parse entity definition from file \"" +
                    path.to_native() + "\" as component "
                    "parameter \"" + name + "\" value does not match "
                    "resource string type as specified by the schema: " +
                    writer.write(root)
                );
            }

            omi::res::ResourceId id =
                omi::res::get_id(root.asString());

            out_attr = omi::Int64Attribute(
                static_cast<omi::Int64Attribute::DataType>(id)
            );
            out_resource_ids.push_back(id);
        }
        else
        {
            std::vector<omi::Int64Attribute::DataType> values;
            values.reserve(root.size());
            for(std::size_t i = 0; i < root.size(); ++i)
            {
                Json::Value sub_value =
                    root[static_cast<Json::ArrayIndex>(i)];

                if(!sub_value.isString())
                {
                    Json::FastWriter writer;
                    throw arc::ex::ParseError(
                        "Failed to parse entity definition from "
                        "file \"" + path.to_native() + "\" as "
                        "component parameter \"" + name + "\" value "
                        "does not match string resourcetype as specified "
                        "by the schema: " + writer.write(root)
                    );
                }

                omi::res::ResourceId id =
                    omi::res::get_id(sub_value.asString());

                values.push_back(
                    static_cast<omi::Int64Attribute::DataType>(id)
                );
                out_resource_ids.push_back(id);
            }

            out_attr = omi::Int64Attribute(&values[0], values.size());
        }
    }
    else if(type == "string" ||
            arc::str::starts_with(type, "component:"))
    {
        if(size == 1)
        {
            if(!root.isString())
            {
                Json::FastWriter writer;
                throw arc::ex::ParseError(
                    "Failed to parse entity definition from file \"" +
                    path.to_native() + "\" as component "
                    "parameter \"" + name + "\" value does not match "
                    "string type as specified by the schema: " +
                    writer.write(root)
                );
            }

            out_attr = omi::StringAttribute(root.asString());
        }
        else
        {
            std::vector<std::string> values;
            values.reserve(root.size());
            for(std::size_t i = 0; i < root.size(); ++i)
            {
                Json::Value sub_value =
                    root[static_cast<Json::ArrayIndex>(i)];

                if(!sub_value.isString())
                {
                    Json::FastWriter writer;
                    throw arc::ex::ParseError(
                        "Failed to parse entity definition from "
                        "file \"" + path.to_native() + "\" as "
                        "component parameter \"" + name + "\" value "
                        "does not match string type as specified by "
                        "the schema: " + writer.write(root)
                    );
                }

                values.push_back(sub_value.asString());
            }

            out_attr = omi::StringAttribute(values);
        }
    }
    else
    {
        Json::FastWriter writer;
        throw arc::ex::NotImplementedError(
            "Failed to parse entity definition from file \"" +
            path.to_native() + "\" as component parameter \"" + name +
            "\" has an unsupported type: \"" + type + "\" " +
            writer.write(root)
        );
    }
}

} // namespace scene_
} // namespace omi
