/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omicron/api/common/Attributes.hpp"
#include "omicron/api/comp/ComponentSchema.hpp"
#include "omicron/api/comp/renderable/Camera.hpp"
#include "omicron/api/comp/renderable/Material.hpp"
#include "omicron/api/comp/renderable/Mesh.hpp"
#include "omicron/api/comp/renderable/Text2D.hpp"
#include "omicron/api/comp/renderable/Texture.hpp"
#include "omicron/api/comp/script/Script.hpp"
#include "omicron/api/comp/transform/AxisAngleTransform.hpp"
#include "omicron/api/comp/transform/MatrixTransform.hpp"
#include "omicron/api/comp/transform/QuaternionTransform.hpp"
#include "omicron/api/comp/transform/Scale3Transform.hpp"
#include "omicron/api/comp/transform/ScaleTransform.hpp"
#include "omicron/api/comp/transform/TranslateTransform.hpp"
#include "omicron/api/scene/private/ComponentsFromAttributes.hpp"


namespace omi
{
namespace scene_
{

template <typename AttributeT>
AttributeT get_parameter_value(
        std::string const& name,
        omi::MapAttribute const& data,
        comp::ComponentSchema const& schema)
{
    AttributeT value = data.at(name);
    if(value.is_valid())
    {
        return value;
    }
    comp::ComponentParameter parameter = schema.get_parameter(name);
    return parameter.get_default(name);
}

void components_from_attributes(
        std::string const& entity_name,
        std::vector<omi::Attribute> const& attr_list,
        std::vector<omi::comp::Component>& out_components,
        std::unordered_map<std::string, std::size_t>& out_name_mapping)
{
    std::size_t uid = 1;
    for(omi::Attribute const& attrs : attr_list)
    {
        // get the component type
        omi::MapAttribute map_attr = attrs;
        omi::StringAttribute type_attr = map_attr.at("type");
        std::string const type = type_attr.get_value();

        // get the schema for the name
        comp::ComponentSchema schema = comp::ComponentSchema::get_schema(type);

        // get or generate the component name
        std::string component_name;
        omi::StringAttribute name_attr = map_attr.at("name");
        if(name_attr.is_valid())
        {
            component_name = name_attr.get_value();
        }
        else
        {
            component_name = type + std::to_string(uid);
            ++uid;
        }
        // ensure the name hasn't been used
        auto f_comp = out_name_mapping.find(component_name);
        if(f_comp != out_name_mapping.end())
        {
            throw arc::ex::RuntimeError(
                "Entity \"" + entity_name + "\" has multiple components with "
                "the same name: \"" + component_name + "\": "
            );
        }
        // TODO: this should be done either with template magic or code gen
        if(type == "Script")
        {
            omi::StringAttribute script_name_attr =
                map_attr.at("script_name");

            out_components.push_back(
                omi::comp::Script(script_name_attr.get_value())
            );
        }
        else if(type == "MatrixTransform")
        {
            // get the transform component
            OMI_COMP_ComponentPtr constrained_to = nullptr;
            auto f_constrainted_to = out_name_mapping.find(
                get_parameter_value<omi::StringAttribute>(
                    "constrained_to",
                    map_attr,
                    schema
                ).get_value()
            );
            if(f_constrainted_to != out_name_mapping.end())
            {
                constrained_to =
                    out_components[f_constrainted_to->second].get_ptr();
            }

            out_components.push_back(
                omi::comp::MatrixTransform(
                    *reinterpret_cast<arclx::Matrix44f const*>(
                        &get_parameter_value<omi::Float32Attribute>(
                            "matrix",
                            map_attr,
                            schema
                        ).get_values()[0]
                    ),
                    static_cast<omi::comp::TransformConstraint>(
                        get_parameter_value<omi::Int32Attribute>(
                            "constraint",
                            map_attr,
                            schema
                        ).get_value()
                    ),
                    constrained_to
                )
            );
        }
        else if(type == "TranslateTransform")
        {
            // get the transform component
            OMI_COMP_ComponentPtr constrained_to = nullptr;
            auto f_constrainted_to = out_name_mapping.find(
                get_parameter_value<omi::StringAttribute>(
                    "constrained_to",
                    map_attr,
                    schema
                ).get_value()
            );
            if(f_constrainted_to != out_name_mapping.end())
            {
                constrained_to =
                    out_components[f_constrainted_to->second].get_ptr();
            }

            out_components.push_back(
                omi::comp::TranslateTransform(
                    *reinterpret_cast<arclx::Vector3f const*>(
                        &get_parameter_value<omi::Float32Attribute>(
                            "translation",
                            map_attr,
                            schema
                        ).get_values()[0]
                    ),
                    static_cast<omi::comp::TransformConstraint>(
                        get_parameter_value<omi::Int32Attribute>(
                            "constraint",
                            map_attr,
                            schema
                        ).get_value()
                    ),
                    constrained_to
                )
            );
        }
        else if(type == "AxisAngleTransform")
        {
            // get the transform component
            OMI_COMP_ComponentPtr constrained_to = nullptr;
            auto f_constrainted_to = out_name_mapping.find(
                get_parameter_value<omi::StringAttribute>(
                    "constrained_to",
                    map_attr,
                    schema
                ).get_value()
            );
            if(f_constrainted_to != out_name_mapping.end())
            {
                constrained_to =
                    out_components[f_constrainted_to->second].get_ptr();
            }

            out_components.push_back(
                omi::comp::AxisAngleTransform(
                    get_parameter_value<omi::Float32Attribute>(
                        "angle",
                        map_attr,
                        schema
                    ).get_value(),
                    *reinterpret_cast<arclx::Vector3f const*>(
                        &get_parameter_value<omi::Float32Attribute>(
                            "axis",
                            map_attr,
                            schema
                        ).get_values()[0]
                    ),
                    static_cast<omi::comp::TransformConstraint>(
                        get_parameter_value<omi::Int32Attribute>(
                            "constraint",
                            map_attr,
                            schema
                        ).get_value()
                    ),
                    constrained_to
                )
            );
        }
        else if(type == "QuaternionTransform")
        {
            omi::Float32Attribute::ArrayType quaternion_values =
                get_parameter_value<omi::Float32Attribute>(
                    "quaternion",
                    map_attr,
                    schema
                ).get_values();

            // get the transform component
            OMI_COMP_ComponentPtr constrained_to = nullptr;
            auto f_constrainted_to = out_name_mapping.find(
                get_parameter_value<omi::StringAttribute>(
                    "constrained_to",
                    map_attr,
                    schema
                ).get_value()
            );
            if(f_constrainted_to != out_name_mapping.end())
            {
                constrained_to =
                    out_components[f_constrainted_to->second].get_ptr();
            }

            out_components.push_back(
                omi::comp::QuaternionTransform(
                    arclx::Quaternionf(
                        quaternion_values[0],
                        quaternion_values[1],
                        quaternion_values[2],
                        quaternion_values[3]
                    ),
                    static_cast<omi::comp::TransformConstraint>(
                        get_parameter_value<omi::Int32Attribute>(
                            "constraint",
                            map_attr,
                            schema
                        ).get_value()
                    ),
                    constrained_to
                )
            );
        }
        else if(type == "ScaleTransform")
        {
            // get the transform component
            OMI_COMP_ComponentPtr constrained_to = nullptr;
            auto f_constrainted_to = out_name_mapping.find(
                get_parameter_value<omi::StringAttribute>(
                    "constrained_to",
                    map_attr,
                    schema
                ).get_value()
            );
            if(f_constrainted_to != out_name_mapping.end())
            {
                constrained_to =
                    out_components[f_constrainted_to->second].get_ptr();
            }

            out_components.push_back(
                omi::comp::ScaleTransform(
                    get_parameter_value<omi::Float32Attribute>(
                        "scale",
                        map_attr,
                        schema
                    ).get_value(),
                    static_cast<omi::comp::TransformConstraint>(
                        get_parameter_value<omi::Int32Attribute>(
                            "constraint",
                            map_attr,
                            schema
                        ).get_value()
                    ),
                    constrained_to
                )
            );
        }
        else if(type == "Scale3Transform")
        {
            // get the transform component
            OMI_COMP_ComponentPtr constrained_to = nullptr;
            auto f_constrainted_to = out_name_mapping.find(
                get_parameter_value<omi::StringAttribute>(
                    "constrained_to",
                    map_attr,
                    schema
                ).get_value()
            );
            if(f_constrainted_to != out_name_mapping.end())
            {
                constrained_to =
                    out_components[f_constrainted_to->second].get_ptr();
            }

            out_components.push_back(
                omi::comp::Scale3Transform(
                    *reinterpret_cast<arclx::Vector3f const*>(
                        &get_parameter_value<omi::Float32Attribute>(
                            "scale",
                            map_attr,
                            schema
                        ).get_values()[0]
                    ),
                    static_cast<omi::comp::TransformConstraint>(
                        get_parameter_value<omi::Int32Attribute>(
                            "constraint",
                            map_attr,
                            schema
                        ).get_value()
                    ),
                    constrained_to
                )
            );
        }
        else if(type == "Camera")
        {
            // get the transform component
            OMI_COMP_ComponentPtr transform = nullptr;
            auto f_transform = out_name_mapping.find(
                get_parameter_value<omi::StringAttribute>(
                    "transform",
                    map_attr,
                    schema
                ).get_value()
            );
            if(f_transform != out_name_mapping.end())
            {
                transform = out_components[f_transform->second].get_ptr();
            }

            out_components.push_back(
                omi::comp::Camera(
                    get_parameter_value<omi::Float32Attribute>(
                        "focal_length",
                        map_attr,
                        schema
                    ).get_value(),
                    *reinterpret_cast<arclx::Vector2f const*>(
                        &get_parameter_value<omi::Float32Attribute>(
                            "sensor_size",
                            map_attr,
                            schema
                        ).get_values()[0]
                    ),
                    *reinterpret_cast<arclx::Vector2f const*>(
                        &get_parameter_value<omi::Float32Attribute>(
                            "sensor_offset",
                            map_attr,
                            schema
                        ).get_values()[0]
                    ),
                    get_parameter_value<omi::Float32Attribute>(
                        "near_clip",
                        map_attr,
                        schema
                    ).get_value(),
                    get_parameter_value<omi::Float32Attribute>(
                        "far_clip",
                        map_attr,
                        schema
                    ).get_value(),
                    transform
                )
            );
        }
        else if(type == "Texture")
        {
            out_components.push_back(
                omi::comp::Texture(
                    static_cast<omi::res::ResourceId>(
                        get_parameter_value<omi::Int64Attribute>(
                            "resource_path",
                            map_attr,
                            schema
                        ).get_value()
                    ),
                    get_parameter_value<omi::StringAttribute>(
                        "resource_name",
                        map_attr,
                        schema
                    ).get_value()
                )
            );
        }
        else if(type == "Material")
        {
            // get the texture component
            OMI_COMP_ComponentPtr texture = nullptr;
            auto f_texture = out_name_mapping.find(
                get_parameter_value<omi::StringAttribute>(
                    "texture",
                    map_attr,
                    schema
                ).get_value()
            );
            if(f_texture != out_name_mapping.end())
            {
                texture = out_components[f_texture->second].get_ptr();
            }

            out_components.push_back(
                omi::comp::Material(
                    *reinterpret_cast<arclx::Vector3f const*>(
                        &get_parameter_value<omi::Float32Attribute>(
                            "albedo",
                            map_attr,
                            schema
                        ).get_values()[0]
                    ),
                    get_parameter_value<omi::Float32Attribute>(
                        "opacity",
                        map_attr,
                        schema
                    ).get_value(),
                    texture
                )
            );
        }
        else if(type == "Mesh")
        {
            // get the transform component
            OMI_COMP_ComponentPtr transform = nullptr;
            auto f_transform = out_name_mapping.find(
                get_parameter_value<omi::StringAttribute>(
                    "transform",
                    map_attr,
                    schema
                ).get_value()
            );
            if(f_transform != out_name_mapping.end())
            {
                transform = out_components[f_transform->second].get_ptr();
            }

            // get the material component
            OMI_COMP_ComponentPtr material = nullptr;
            auto f_material = out_name_mapping.find(
                get_parameter_value<omi::StringAttribute>(
                    "material",
                    map_attr,
                    schema
                ).get_value()
            );
            if(f_material != out_name_mapping.end())
            {
                material = out_components[f_material->second].get_ptr();
            }

            out_components.push_back(
                omi::comp::Mesh(
                    static_cast<omi::res::ResourceId>(
                        get_parameter_value<omi::Int64Attribute>(
                            "resource_path",
                            map_attr,
                            schema
                        ).get_value()
                    ),
                    get_parameter_value<omi::StringAttribute>(
                        "resource_name",
                        map_attr,
                        schema
                    ).get_value(),
                    transform,
                    material,
                    get_parameter_value<omi::ByteAttribute>(
                        "cull_backfaces",
                        map_attr,
                        schema
                    ).get_value() != OMI_False
                )
            );
        }
        else if(type == "Text2D")
        {
            out_components.push_back(
                omi::comp::Text2D(
                    get_parameter_value<omi::StringAttribute>(
                        "text",
                        map_attr,
                        schema
                    ).get_value(),
                    static_cast<omi::res::ResourceId>(
                        get_parameter_value<omi::Int64Attribute>(
                            "font",
                            map_attr,
                            schema
                        ).get_value()
                    ),
                    *reinterpret_cast<arclx::Vector4f const*>(
                        &get_parameter_value<omi::Float32Attribute>(
                            "colour",
                            map_attr,
                            schema
                        ).get_values()[0]
                    ),
                    get_parameter_value<omi::Float32Attribute>(
                        "size",
                        map_attr,
                        schema
                    ).get_value(),
                    *reinterpret_cast<arclx::Vector2f const*>(
                        &get_parameter_value<omi::Float32Attribute>(
                            "position",
                            map_attr,
                            schema
                        ).get_values()[0]
                    ),
                    static_cast<omi::comp::Text2D::HorizontalOrigin>(
                        get_parameter_value<omi::Int32Attribute>(
                            "horizontal_origin",
                            map_attr,
                            schema
                        ).get_value()
                    ),
                    static_cast<omi::comp::Text2D::VerticalOrigin>(
                        get_parameter_value<omi::Int32Attribute>(
                            "vertical_origin",
                            map_attr,
                            schema
                        ).get_value()
                    )
                )
            );
        }
        else
        {
            throw arc::ex::NotImplementedError(
                "Entity \"" + entity_name + "\" has component defined with "
                "unsupported type : \"" + type + "\""
            );
        }

        out_name_mapping[component_name] = out_components.size() - 1;
    }}

} // namespace scene_
} // namespace omi
