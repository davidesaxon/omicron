/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_GAMEINTERFACE_H_
#define OMICRON_API_GAMEINTERFACE_H_

#include <stddef.h>
#include <stdint.h>
#include <unordered_map>
#include <vector>

#include "omicron/api/common/attribute/Attribute.hpp"
#include "omicron/api/scene/SceneCTypes.h"


//------------------------------------------------------------------------------
//                                     MACROS
//------------------------------------------------------------------------------

/*!
 * \brief Macros that must be defined once and only once within a game's dso.
 *
 * \param name The name of the game.
 * \param version The version number string of the game.
 */
#define OMI_GAME_DEFINE(name, version)                                         \
    typedef OMI_SCENE_EntityScriptPtr (OMI_GAME_EntityScriptCreateFunc)(       \
            OMI_SCENE_EntityId,                                                \
            char const*, OMI_AttributePtr);                                    \
    typedef void (OMI_GAME_EntityScriptDestroyFunc)(OMI_SCENE_EntityScriptPtr);\
    std::unordered_map<                                                        \
        std::string,                                                           \
        std::pair<                                                             \
            OMI_GAME_EntityScriptCreateFunc*,                                  \
            OMI_GAME_EntityScriptDestroyFunc*                                  \
        >                                                                      \
    > OMI_GAME_entity_script_factory;                                          \
    extern "C"                                                                 \
    {                                                                          \
    OMI_GAME_EXPORT char const* OMI_GAME_get_name()                            \
    {                                                                          \
        return name;                                                           \
    }                                                                          \
    OMI_GAME_EXPORT char const* OMI_GAME_get_version()                         \
    {                                                                          \
        return version;                                                        \
    }                                                                          \
    OMI_GAME_EXPORT OMI_SCENE_EntityScriptPtr OMI_GAME_create_entity_script(   \
            char const* script_id,                                             \
            OMI_SCENE_EntityId entity_id,                                      \
            char const* entity_name,                                           \
            OMI_AttributePtr data,                                             \
            OMI_GAME_EntityScriptDestroyFunc** out_destory_func)               \
    {                                                                          \
        auto f_factory = OMI_GAME_entity_script_factory.find(script_id);       \
        if(f_factory == OMI_GAME_entity_script_factory.end())                  \
        {                                                                      \
            return nullptr;                                                    \
        }                                                                      \
        *out_destory_func = f_factory->second.second;                          \
        return f_factory->second.first(entity_id, entity_name, data);          \
    }                                                                          \
    }


#define OMI_GAME_DEFINE_ENTITY_SCRIPT(ScriptType, id)                          \
    namespace                                                                  \
    {                                                                          \
    static std::unordered_map<                                                 \
        OMI_SCENE_EntityScriptPtr,                                             \
        ScriptType*                                                            \
    > OMI_GAME_script_instances;                                               \
    }                                                                          \
    static OMI_SCENE_EntityScriptPtr id##_create(                              \
            OMI_SCENE_EntityId entity_id,                                      \
            char const* entity_name,                                           \
            OMI_AttributePtr data)                                             \
    {                                                                          \
        ScriptType* new_script = new ScriptType(entity_id, entity_name, data); \
        OMI_GAME_script_instances[new_script->get_ptr()] = new_script;         \
        return new_script->get_ptr();                                          \
    }                                                                          \
    static void id##_destroy(OMI_SCENE_EntityScriptPtr script)                 \
    {                                                                          \
        auto f_script = OMI_GAME_script_instances.find(script);                \
        if(f_script != OMI_GAME_script_instances.end())                        \
        {                                                                      \
            delete f_script->second;                                           \
        }                                                                      \
    }                                                                          \
    typedef OMI_SCENE_EntityScriptPtr (OMI_GAME_EntityScriptCreateFunc)(       \
            OMI_SCENE_EntityId,                                                \
            char const*,                                                       \
            OMI_AttributePtr);                                                 \
    typedef void (OMI_GAME_EntityScriptDestroyFunc)(OMI_SCENE_EntityScriptPtr);\
    extern std::unordered_map<                                                 \
        std::string,                                                           \
        std::pair<                                                             \
            OMI_GAME_EntityScriptCreateFunc*,                                  \
            OMI_GAME_EntityScriptDestroyFunc*                                  \
        >                                                                      \
    > OMI_GAME_entity_script_factory;                                          \
    namespace                                                                  \
    {                                                                          \
    class id##Register                                                         \
    {                                                                          \
    public:                                                                    \
        id##Register()                                                         \
        {                                                                      \
            OMI_GAME_entity_script_factory[#id] = std::make_pair(              \
                &id##_create,                                                  \
                &id##_destroy                                                  \
            );                                                                 \
        }                                                                      \
    };                                                                         \
    static id##Register id##_register;                                         \
    }

#endif
