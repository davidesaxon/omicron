/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_API_HPP_
#define OMICRON_API_HPP_

#include <stddef.h>
#include <stdint.h>

#include <arcanecore/base/Preproc.hpp>


// TODO: REMOVE ME: when there's a proper build system
#define OMI_API_MODE_DEBUG

//-------------------------------COMPILATION MODE-------------------------------
#if !defined(OMI_API_MODE_DEBUG) && !defined(OMI_API_MODE_DEVELOPER)
#define OMI_API_MODE_PRODUCTION
#endif
//------------------------------------------------------------------------------

//----------------------------API EXPORT DEFINITION-----------------------------
#ifdef ARC_OS_WINDOWS
    #ifdef omicron_api_impl_EXPORTS
        #define OMI_API_EXPORT __declspec(dllexport)
    #else
        #define OMI_API_EXPORT __declspec(dllimport)
    #endif
#else
    #define OMI_API_EXPORT
#endif
//------------------------------------------------------------------------------

// TODO:
//---------------------------PLUGIN EXPORT DEFINITION---------------------------
#ifdef ARC_OS_WINDOWS
    #ifdef OMI_PLUGIN_ENABLE_EXPORT
        #define OMI_PLUGIN_EXPORT __declspec(dllexport)
    #else
        #define OMI_PLUGIN_EXPORT __declspec(dllimport)
    #endif
#else
    #define OMI_PLUGIN_EXPORT
#endif
//------------------------------------------------------------------------------

//---------------------------------CODEGEN TAGS---------------------------------
#define OMI_CODEGEN_NAMESPACE
#define OMI_CODEGEN_FUNCTION
//------------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif

//-----------------------------------C TYPES------------------------------------
typedef int8_t OMI_Bool;
#define OMI_False 0
#define OMI_True  1

typedef size_t OMI_Size;

typedef OMI_Bool (OMI_BindingFunc)(char const*, void**);
//------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif

#endif
