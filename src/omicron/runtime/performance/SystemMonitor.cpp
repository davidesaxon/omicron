/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <arcanecore/base/data/DataConstants.hpp>
#include <arcanecore/base/os/OSOperations.hpp>

#include <omicron/api/common/Attributes.hpp>
#include <omicron/api/report/StatsDatabase.hpp>

#include "omicron/runtime/performance/SystemMonitor.hpp"


namespace omi
{
namespace runtime
{
namespace perf
{

//------------------------------------------------------------------------------
//                                 IMPLEMENTATION
//------------------------------------------------------------------------------

class SystemMonitor::SystemMonitorImpl
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
private:

    //-------------------P R I V A T E    A T T R I B U T E S-------------------

    omi::StringAttribute m_os_name;
    omi::StringAttribute m_os_distro;
    omi::StringAttribute m_cpu_model;
    omi::Int64Attribute m_cpu_physical_cores;
    omi::Int64Attribute m_cpu_logical_processors;
    omi::Float32Attribute m_cpu_clock_rate;
    omi::Int64Attribute m_primary_processor;
    omi::Float64Attribute m_total_ram;
    omi::Float64Attribute m_free_ram;
    omi::Float64Attribute m_total_virtual_memory;
    omi::Float64Attribute m_free_virtual_memory;
    omi::Float64Attribute m_current_rss;
    omi::Float64Attribute m_peak_rss;

public:

    //--------------------------C O N S T R U C T O R---------------------------

    SystemMonitorImpl()
        : m_os_name               ("")
        , m_os_distro             ("")
        , m_cpu_model             ("")
        , m_cpu_physical_cores    (0)
        , m_cpu_logical_processors(0)
        , m_cpu_clock_rate        (0.0F)
        , m_primary_processor     (0)
        , m_total_ram             (0.0)
        , m_free_ram              (0.0)
        , m_total_virtual_memory  (0.0)
        , m_free_virtual_memory   (0.0)
        , m_current_rss           (0.0)
        , m_peak_rss              (0.0)
    {
    }

    //---------------------------D E S T R U C T O R----------------------------

    virtual ~SystemMonitorImpl()
    {
    }

    //-------------P U B L I C    M E M B E R    F U N C T I O N S--------------

    void startup()
    {
        define_statistics();

        // set the one time stats
        m_os_name.set_value(arc::os::get_os_name());
        m_os_distro.set_value(arc::os::get_distro_name());
        m_cpu_model.set_value(arc::os::get_cpu_model());
        m_cpu_physical_cores.set_value(arc::os::get_cpu_physical_cores());
        m_cpu_logical_processors.set_value(
            arc::os::get_cpu_logical_processors()
        );
        m_cpu_clock_rate.set_value(arc::os::get_cpu_clock_rate());
        m_primary_processor.set_value(arc::os::get_thread_processor());
        m_total_ram.set_value(
            static_cast<double>(arc::os::get_total_ram()) /
            static_cast<double>(arc::data::BYTE_TO_MEGABYTE)
        );
        m_total_virtual_memory.set_value(
            static_cast<double>(arc::os::get_total_virtual_memory()) /
            static_cast<double>(arc::data::BYTE_TO_MEGABYTE)
        );
    }

    void shutdown()
    {
    }

    void update(bool force)
    {
        // TODO: stochastic sampling

        m_free_ram.set_value(
            static_cast<double>(arc::os::get_free_ram()) /
            static_cast<double>(arc::data::BYTE_TO_MEGABYTE)
        );
        m_free_virtual_memory.set_value(
            static_cast<double>(arc::os::get_free_virtual_memory()) /
            static_cast<double>(arc::data::BYTE_TO_MEGABYTE)
        );
        m_current_rss.set_value(
            static_cast<double>(arc::os::get_rss()) /
            static_cast<double>(arc::data::BYTE_TO_MEGABYTE)
        );
        m_peak_rss.set_value(
            static_cast<double>(arc::os::get_peak_rss()) /
            static_cast<double>(arc::data::BYTE_TO_MEGABYTE)
        );
    }

private:

    //--------------------------------------------------------------------------
    //                             PRIVATE FUNCTIONS
    //--------------------------------------------------------------------------

    void define_statistics()
    {
        report::StatsDatabase::instance().define_entry(
            "System.OS.Name",
            m_os_name,
            "The model name of this machine's operating system."
        );
        report::StatsDatabase::instance().define_entry(
            "System.OS.Distro",
            m_os_distro,
            "The distribution name/version of this machine's operating system."
        );
        report::StatsDatabase::instance().define_entry(
            "System.CPU.Model",
            m_cpu_model,
            "The model name of this machine's CPU."
        );
        report::StatsDatabase::instance().define_entry(
            "System.CPU.Physical Cores",
            m_cpu_physical_cores,
            "The number of physical cores this machine's CPU has."
        );
        report::StatsDatabase::instance().define_entry(
            "System.CPU.Logical Processing Units",
            m_cpu_logical_processors,
            "The number of logical processing units this machine's CPU has."
        );
        report::StatsDatabase::instance().define_entry(
            "System.CPU.Clock Rate (MHz)",
            m_cpu_clock_rate,
            "The estimated clock rate of this machine's CPU."
        );
        report::StatsDatabase::instance().define_entry(
            "System.CPU.Primary Processor",
            m_primary_processor,
            "The id of the logical processor the primary thread of the engine "
            "is running on."
        );
        report::StatsDatabase::instance().define_entry(
            "System.Memory.Total RAM (mb)",
            m_total_ram,
            "The total amount of RAM on this system."
        );
        report::StatsDatabase::instance().define_entry(
            "System.Memory.Free RAM (mb)",
            m_free_ram,
            "The total amount of RAM free on this system."
        );
        report::StatsDatabase::instance().define_entry(
            "System.Memory.Total Virtual Memory (mb)",
            m_total_virtual_memory,
            "The total amount of virtual memory on this system."
        );
        report::StatsDatabase::instance().define_entry(
            "System.Memory.Free Virtual Memory (mb)",
            m_free_virtual_memory,
            "The total amount of virtual memory free on this system."
        );
        report::StatsDatabase::instance().define_entry(
            "System.Memory.RSS (mb)",
            m_current_rss,
            "The last sampled size of the engine's Resident Set Size (RSS) "
            "i.e. RAM usage."
        );
        report::StatsDatabase::instance().define_entry(
            "System.Memory.Peak RSS (mb)",
            m_peak_rss,
            "The peak sampled value that the engine's Resident Set Size "
            "reached."
        );
    }
};

//------------------------------------------------------------------------------
//                            PUBLIC STATIC FUNCTIONS
//------------------------------------------------------------------------------

SystemMonitor& SystemMonitor::instance()
{
    static SystemMonitor inst;
    return inst;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

void SystemMonitor::startup()
{
    m_impl->startup();
}

void SystemMonitor::shutdown()
{
    m_impl->shutdown();
}

void SystemMonitor::update(bool force)
{
    m_impl->update(force);
}

//------------------------------------------------------------------------------
//                              PRIVATE CONSTRUCTOR
//------------------------------------------------------------------------------

SystemMonitor::SystemMonitor()
    : m_impl(new SystemMonitorImpl())
{
}

//------------------------------------------------------------------------------
//                               PRIVATE DESTRUCTOR
//------------------------------------------------------------------------------

SystemMonitor::~SystemMonitor()
{
    delete m_impl;
}

} // namespace perf
} // namespace runtime
} // namespace omi
