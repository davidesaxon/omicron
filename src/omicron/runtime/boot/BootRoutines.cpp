/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <iostream>

#include <arcanecore/base/Preproc.hpp>
#include <arcanecore/base/clock/ClockOperations.hpp>

#ifdef ARC_OS_WINDOWS

    #include <windows.h>

#endif

#include <omicron/api/common/Attributes.hpp>
#include <omicron/api/comp/ComponentSchema.hpp>
#include <omicron/api/config/Registry.hpp>
#include <omicron/api/host/Host.hpp>
#include <omicron/api/physics/Physics.hpp>
#include <omicron/api/renderer/Renderer.hpp>
#include <omicron/api/report/StatsDatabase.hpp>
#include <omicron/api/report/StatsQuery.hpp>
#include <omicron/api/report/StatsOperations.hpp>
#include <omicron/api/res/Storage.hpp>
#include <omicron/api/scene/Manager.hpp>
#include <omicron/api/sys/Timing.hpp>

#include "omicron/runtime/RuntimeGlobals.hpp"
#include "omicron/runtime/boot/BootRoutines.hpp"
#include "omicron/runtime/game/GameBinding.hpp"
#include "omicron/runtime/performance/SystemMonitor.hpp"


namespace omi
{
namespace runtime
{
namespace boot
{

//------------------------------------------------------------------------------
//                                    GLOBALS
//------------------------------------------------------------------------------

namespace
{

static bool g_initialised = false;

// the time in milliseconds since epoch that Omicron started
static arc::clock::TimeInt g_start_time;

//------------------------------------STATS-------------------------------------
static omi::StringAttribute g_stat_start_at           ("");
static omi::StringAttribute g_stat_end_at             ("");
static omi::Int64Attribute  g_stat_config_load_time   (0);
static omi::Int64Attribute  g_stat_base_startup_time  (0);
static omi::Int64Attribute  g_stat_time_to_first_frame(0);
static omi::Int64Attribute  g_stat_active_time        (0);
static omi::Int64Attribute  g_stat_shutdown_time      (0);
//------------------------------------------------------------------------------

} // namespace anonymous

//------------------------------------------------------------------------------
//                                   FUNCTIONS
//------------------------------------------------------------------------------

static void define_statistics()
{
    report::StatsDatabase::instance().define_entry(
        "Lifecycle.Start At",
        g_stat_start_at,
        "The time at which this instance of Omicron was started."
    );
    report::StatsDatabase::instance().define_entry(
        "Lifecycle.End At",
        g_stat_end_at,
        "The time at which this instance of Omicron ended."
    );
    report::StatsDatabase::instance().define_entry(
        "Lifecycle.Startup.Config Load Time (ms)",
        g_stat_config_load_time,
        "The time taken during startup to load configuration data."
    );
    report::StatsDatabase::instance().define_entry(
        "Lifecycle.Startup.Base Time (ms)",
        g_stat_base_startup_time,
        "The time taken by startup of the base engine (including "
        "subsystem loading and startup)."
    );
    report::StatsDatabase::instance().define_entry(
        "Lifecycle.Startup.Time to First Frame (ms)",
        g_stat_time_to_first_frame,
        "The time taken until rendering of the first frame of Omicron "
        "begins (This time includes initial loading and setup requiring a "
        "GL context)."
    );
    report::StatsDatabase::instance().define_entry(
        "Lifecycle.Active Time (ms)",
        g_stat_active_time,
        "The amount of time Omicron has been active for."
    );
    report::StatsDatabase::instance().define_entry(
        "Lifecycle.Shutdown.Time (ms)",
        g_stat_shutdown_time,
        "The time taken by shutdown of the engine."
    );
}

static void os_startup_routine()
{
    global::logger.debug(
        "Initialising operating system specific functionality."
    );

    #ifdef ARC_OS_WINDOWS
        SetErrorMode(SEM_FAILCRITICALERRORS);
    #endif
}

static void print_stats(
        arc::fsys::Path const& query_path,
        std::string const& title)
{
    // build the query
    report::StatsQuery query(query_path);
    // execute
    report::StatsDatabase::QueryResult result =
        report::StatsDatabase::instance().execute_query(query);
    // dump the results
    report::print_stats_query(
        result,
        std::cout,
        title
    );
}

//------------------------------------------------------------------------------
//                                STARTUP ROUTINE
//------------------------------------------------------------------------------

bool startup_routine()
{
    // warn and do nothing if Omicron has already been initialised
    if(g_initialised)
    {
        global::logger.warning(
            "Attempted to run engine startup routines after the engine has "
            "already successfully started."
        );
        return true;
    }

    // record the time the engine started
    g_start_time = arc::clock::get_current_time();

    define_statistics();

    // stat the time the engine started
    g_stat_start_at.set_value(
        arc::clock::get_timestamp(g_start_time, "%Y/%m/%d - %H:%M:%S")
    );

    // load config
    global::logger.debug("Loading configuration data");
    arc::clock::TimeInt config_load_time = arc::clock::get_current_time();
    OMI_CONFIG_Registry_Error config_ec =
        config::Registry::get_interface().Registry_load();
    g_stat_config_load_time.set_value(
        arc::clock::get_current_time() - config_load_time
    );
    // check for errors
    if(config_ec != OMI_CONFIG_Registry_Error_kNone)
    {
        global::logger.critical(
            "Failed to load configuration data with error: {0}",
            config::Registry::get_interface().get_error_message()
        );
        return false;
    }
    // check for warnings
    std::string const config_warnings =
        config::Registry::get_interface().get_warnings();
    if(!config_warnings.empty())
    {
        global::logger.warning(
            "Configuration data loaded with warnings:\n{0}",
            config_warnings
        );
    }
    else
    {
        global::logger.debug("Configuration data successfully loaded");
    }

    // startup logging
    if(report::Logger::get_interface().startup() !=
       OMI_REPORT_Logger_Error_kNone)
    {
        // TODO: get error message
        global::logger.critical("Failed to setup logger");
        return false;
    }
    global::logger.debug("Logging initialised");

    os_startup_routine();

    // startup system monitor
    try
    {
        perf::SystemMonitor::instance().startup();
    }
    catch(std::exception const& exc)
    {
        global::logger.critical(
            "Failed to setup SystemMonitor with exception: {0}",
            exc.what()
        );
        return false;
    }
    global::logger.debug("SystemMonitor initialised");

    // start up system
    if(!sys::Timing::get_interface().startup(global::logger.get_ptr()))
    {
        global::logger.critical("Failed to setup Engine Systems Management");
        return false;
    }
    global::logger.debug("Engine Systems Management initialised");

    // load component schemas
    if(!comp::ComponentSchema::get_interface().load(
            global::logger.get_ptr()))
    {
        global::logger.critical("Failed to load component schemas");
        return false;
    }
    global::logger.debug("Loaded component schemas");

    // startup the resource storage
    if(!res::Storage::get_interface().startup(global::logger.get_ptr()))
    {
        global::logger.critical("Failed to setup the Resource Storage");
        return false;
    }
    global::logger.debug("Resource Storage initialised");

    // start up host subsystem
    try
    {
        if(!host::Host::get_interface().startup(global::logger.get_ptr()))
        {
            global::logger.critical("Failed to setup host subsystem");
            return false;
        }
    }
    catch(std::exception const& exc)
    {
        global::logger.critical(
            "Failed to setup host subsystem with exception: {0}",
            exc.what()
        );
        return false;
    }
    global::logger.debug("Host subsystem initialised");

    // start up physics subsystem
    try
    {
        if(!physics::Physics::get_interface().startup(global::logger.get_ptr()))
        {
            global::logger.critical("Failed to setup physics subsystem");
            return false;
        }
    }
    catch(std::exception const& exc)
    {
        global::logger.critical(
            "Failed to setup host subsystem with exception: {0}",
            exc.what()
        );
        return false;
    }
    global::logger.debug("Physics subsystem initialised");

    // start up the renderer subsystem
    try
    {
        if(!renderer::Renderer::get_interface().startup(
            global::logger.get_ptr()))
        {
            global::logger.critical("Failed to setup renderer subsystem");
            return false;
        }
    }
    catch(std::exception const& exc)
    {
        global::logger.critical(
            "Failed to setup renderer subsystem with exception: {0}",
            exc.what()
        );
        return false;
    }
    global::logger.debug("Renderer subsystem initialised");

    // startup the scene manager
    if(!scene::Manager::get_interface().startup(global::logger.get_ptr()))
    {
        global::logger.critical("Failed to setup the Scene Manager");
        return false;
    }
    global::logger.debug("Scene Manager initialised");

    // bind and startup the game
    try
    {
        if(!game::GameBinding::instance().startup())
        {
            global::logger.critical("Failed to setup game binding");
            return false;
        }
    }
    catch(std::exception const& exc)
    {
        global::logger.critical(
            "Failed to setup game binding with exception: {0}",
            exc.what()
        );
        return false;
    }
    global::logger.debug("Game initialised");

    // open surface
    try
    {
        if(!host::Host::get_interface().open_surface(global::logger.get_ptr()))
        {
            global::logger.critical("Failed to open surface");
            return false;
        }
    }
    catch(std::exception const& exc)
    {
        global::logger.critical(
            "Failed to open surface with exception: {0}",
            exc.what()
        );
        return false;
    }
    global::logger.debug("Surface opened");

    // stat the time of core startup
    g_stat_base_startup_time.set_value(
        arc::clock::get_current_time() - g_start_time
    );

    return true;
}

bool firstframe_routine()
{
    // TODO: this should be move
    g_stat_active_time.set_value(
        arc::clock::get_current_time() - g_start_time
    );

    // renderer subsystem first frame setup
    try
    {
        if(!renderer::Renderer::get_interface().firstframe(
            global::logger.get_ptr()))
        {
            global::logger.critical(
                "Failed to perform renderer subsystem firstframe setup"
            );
            return false;
        }
    }
    catch(std::exception const& exc)
    {
        global::logger.critical(
            "Failed to perform renderer subsystem firstframe setup with "
            "exception: {0}",
            exc.what()
        );
        return false;
    }
    global::logger.debug("Renderer subsystem firstframe setup complete");

    // game first frame setup
    try
    {
        if(!game::GameBinding::instance().firstframe())
        {
            global::logger.critical(
                "Failed to perform game firstframe setup"
            );
            return false;
        }
    }
    catch(std::exception const& exc)
    {
        global::logger.critical(
            "Failed to perform game firstframe setup with exception: {0}",
            exc.what()
        );
        return false;
    }
    global::logger.debug("Game firstframe setup complete");

    // force update the system monitor so we can get up-to-date stats
    perf::SystemMonitor::instance().update(true);

    // stat the time to first frame
    g_stat_time_to_first_frame.set_value(
        arc::clock::get_current_time() - g_start_time
    );
    // TODO: active time should be moved
    // also use for the current active time
    g_stat_active_time.set_value(g_stat_time_to_first_frame.get_value());

    // perform reports?
    ByteAttribute print_stats_attr = config::Registry::instance().get_attr(
        "omicron.startup.print_stats.enable"
    );
    if(print_stats_attr.get_value(0) != 0)
    {
        // get the path of the query file to use
        StringAttribute path_attr = config::Registry::instance().get_attr(
            "omicron.startup.print_stats.query_path"
        );

        arc::fsys::Path query_path;
        for(StringAttribute::DataType const& c : path_attr.get_values())
        {
            query_path << c;
        }

        // TODO: should check query path is valid
        print_stats(query_path, "Startup Statistics");
    }

    return true;
}

bool shutdown_routine()
{
    // time shutdown starts
    arc::clock::TimeInt shutdown_start_time = arc::clock::get_current_time();

    bool success = true;

    // shutdown the Scene Manager
    if(scene::Manager::get_interface().shutdown())
    {
        global::logger.debug("Scene Manager successfully shutdown");
    }
    else
    {
        global::logger.critical("Failed to shutdown Scene Manager");
        success = false;
    }

    // shutdown the game
    try
    {
        if(game::GameBinding::instance().shutdown())
        {
            global::logger.debug("Game successfully shutdown");
        }
        else
        {
            global::logger.critical("Failed to shutdown game");
            success = false;
        }
    }
    catch(std::exception const& exc)
    {
        global::logger.critical(
            "Failed to shutdown game with exception: {0}",
            exc.what()
        );
        success = false;
    }

    // shutdown renderer subsystem
    try
    {
        if(renderer::Renderer::get_interface().shutdown())
        {
            global::logger.debug("Renderer subsystem successfully shutdown");
        }
        else
        {
            global::logger.critical("Failed to shutdown renderer subsystem");
            success = false;
        }
    }
    catch(std::exception const& exc)
    {
        global::logger.critical(
            "Failed to shutdown renderer subsystem with exception: {0}",
            exc.what()
        );
        success = false;
    }

    // shutdown physics subsystem
    try
    {
        if(physics::Physics::get_interface().shutdown())
        {
            global::logger.debug("Physics subsystem successfully shutdown");
        }
        else
        {
            global::logger.critical("Failed to shutdown physics subsystem");
            success = false;
        }
    }
    catch(std::exception const& exc)
    {
        global::logger.critical(
            "Failed to shutdown host subsystem with exception: {0}",
            exc.what()
        );
        success = false;
    }

    // shutdown host subsystem
    try
    {
        if(host::Host::get_interface().shutdown())
        {
            global::logger.debug("Host subsystem successfully shutdown");
        }
        else
        {
            global::logger.critical("Failed to shutdown host subsystem");
            success = false;
        }
    }
    catch(std::exception const& exc)
    {
        global::logger.critical(
            "Failed to shutdown host subsystem with exception: {0}",
            exc.what()
        );
        success = false;
    }

    // shutdown the Resource Storage
    if(res::Storage::get_interface().shutdown())
    {
        global::logger.debug("Resource Storage successfully shutdown");
    }
    else
    {
        global::logger.critical("Failed to shutdown Resource Storage");
        success = false;
    }

    // unload component schemas
    if(comp::ComponentSchema::get_interface().unload())
    {
        global::logger.debug("Component schemas successfully unloaded");
    }
    else
    {
        global::logger.critical("Failed to unload component schemas");
        success = false;
    }

    // force update the system monitor before we shut it down so we can get
    // up-to-date stats
    perf::SystemMonitor::instance().update(true);

    // shutdown the Engine System Management
    if(sys::Timing::get_interface().shutdown())
    {
        global::logger.debug("Engine System Management successfully shutdown");
    }
    else
    {
        global::logger.critical("Failed to shutdown Engine System Management");
        success = false;
    }

    // shutdown system monitor
    try
    {
        perf::SystemMonitor::instance().shutdown();
    }
    catch(std::exception const& exc)
    {
        global::logger.critical(
            "Failed to shutdown SystemMonitor with exception: {0}",
            exc.what()
        );
        success = false;
    }

    // get shutdown config before shutting down the config manager
    ByteAttribute print_stats_attr = config::Registry::instance().get_attr(
        "omicron.shutdown.print_stats.enable"
    );
    StringAttribute query_path_attr =
        config::Registry::instance().get_attr(
            "omicron.shutdown.print_stats.query_path"
        );

    // shutdown configuration manager
    global::logger.debug("Destroying configuration data");
    config::Registry::get_interface().Registry_destroy();

    // get the time of shutdown
    arc::clock::TimeInt end_time = arc::clock::get_current_time();
    // stat
    g_stat_end_at.set_value(
        arc::clock::get_timestamp(end_time, "%Y/%m/%d - %H:%M:%S")
    );
    g_stat_active_time.set_value(end_time - g_start_time);
    g_stat_shutdown_time.set_value(end_time - shutdown_start_time);

    // perform reports?
    if(print_stats_attr.get_value(0) != 0)
    {
        // build the path
        arc::fsys::Path query_path;
        for(StringAttribute::DataType const& c : query_path_attr.get_values())
        {
            query_path << c;
        }

        // TODO: should check query path is valid
        print_stats(query_path, "Shutdown Statistics");
    }

    return success;
}

} // namespace boot
} // namespace runtime
} // namespace omi
