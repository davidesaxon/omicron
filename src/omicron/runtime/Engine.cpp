/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <omicron/api/comp/Component.hpp>
#include <omicron/api/event/Listener.hpp>
#include <omicron/api/host/Host.hpp>
#include <omicron/api/renderer/Renderer.hpp>
#include <omicron/api/res/Storage.hpp>
#include <omicron/api/scene/Manager.hpp>
#include <omicron/api/sys/Timing.hpp>

#include "omicron/runtime/Engine.hpp"
#include "omicron/runtime/RuntimeGlobals.hpp"
#include "omicron/runtime/boot/BootRoutines.hpp"
#include "omicron/runtime/game/ScriptManager.hpp"


namespace omi
{
namespace runtime
{

//------------------------------------------------------------------------------
//                                 IMPLEMENTATION
//------------------------------------------------------------------------------

class Engine::EngineImpl
    : public omi::event::Listener
    , private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
private:

    //-----------P R I V A T E    M E M B E R    A T T R I B U T E S------------

    // signals that the engine should exit
    bool m_should_exit;
    // first frame completed?
    bool m_first_frame;

public:

    //--------------------------C O N S T R U C T O R---------------------------

    EngineImpl()
        : m_should_exit(false)
        , m_first_frame(false)
    {
    }

    //---------------------------D E S T R U C T O R----------------------------

    ~EngineImpl()
    {
    }

    //-------------P U B L I C    M E M B E R    F U N C T I O N S--------------

    int execute()
    {
        if(!boot::startup_routine())
        {
            global::logger.critical("Engine startup failed. Aborting.");
            return -1;
        }

        // subscribe to events
        subscribe_to_event(omi::event::kEngineShutdown);

        // start the main loop
        global::logger.info("Starting main loop");
        host::Host::get_interface().main_loop(
            &cycle_static,
            global::logger.get_ptr()
        );

        // before shuttingdown systems we need to remove all components and
        // unload all resources
        cleanup();

        // unsubscribe from events
        unsubsribe_from_all_events();

        if(!boot::shutdown_routine())
        {
            global::logger.critical("Engine shutdown failed");
            return -1;
        }

        return 0;
    }

protected:

    //----------P R O T E C T E D    M E M B E R    F U N C T I O N S-----------

    virtual void on_event(omi::event::Event const& event) override
    {
        if(event.get_type() == omi::event::kEngineShutdown)
        {
            m_should_exit = true;
        }
    }

private:

    //------------P R I V A T E    S T A T I C    F U N C T I O N S-------------

    // callback function to execute a single cycle of the Omicron engine
    static OMI_Bool cycle_static()
    {
        return Engine::instance().m_impl->cycle();
    }

    //------------P R I V A T E    M E M B E R    F U N C T I O N S-------------

    // performs a single cycle of the Omicron engine
    OMI_Bool cycle()
    {
        if(!m_first_frame)
        {
            m_first_frame = true;
            // first frame setup
            if(!boot::firstframe_routine())
            {
                global::logger.critical(
                    "First frame routine failed. Aborting."
                );
                return false;
            }
        }

        // exiting?
        if(m_should_exit)
        {
            return false;
        }

        // update engine timing
        sys::Timing::get_interface().update();

        // // TODO: don't update more than 60fps (config based)

        // update scene management
        scene::Manager::get_interface().update(global::logger.get_ptr());
        remove_components();
        add_components();

        // clear the lists of new/removed components
        scene::Manager::get_interface().clear_global_lists();

        // update scripts
        game::ScriptManager::instance().update();

        // render the frame
        renderer::Renderer::get_interface().render();

        // TODO: delay (if frame cap)

        return OMI_True;
    }

    void cleanup()
    {
        // clean up scene management
        scene::Manager::get_interface().cleanup(global::logger.get_ptr());
        remove_components();

        // clean up subs-systems that may have omponents
        sys::Timing::get_interface().cleanup(global::logger.get_ptr());

        // clear global lists
        scene::Manager::get_interface().clear_global_lists();

        // clean up resources
        res::Storage::get_interface().cleanup(global::logger.get_ptr());
    }

    void add_components()
    {
        // get the new components that have been added
        OMI_COMP_ComponentPtr* new_components = nullptr;
        OMI_Size new_size =
            scene::Manager::get_interface().get_new_components(
                &new_components
            );
        for(OMI_Size i = 0; i < new_size; ++i)
        {
            comp::Component component(new_components[i], true);
            if(!component.is_valid())
            {
                continue;
            }

            // handle by type
            switch(component.get_component_type())
            {
                case comp::ComponentType::kScript:
                {
                    game::ScriptManager::instance().add_component(
                        new_components[i]
                    );
                    break;
                }
                case comp::ComponentType::kRenderable:
                {
                    renderer::Renderer::get_interface().add_component(
                        new_components[i]
                    );
                    break;
                }
                default:
                {
                    // component not owned by any subsystems
                    break;
                }
            }
        }
    }

    void remove_components()
    {
        OMI_COMP_ComponentPtr* removed_components = nullptr;
        OMI_Size removed_size =
            scene::Manager::get_interface().get_removed_components(
                &removed_components
            );
        for(OMI_Size i = 0; i < removed_size; ++i)
        {
            comp::Component component(removed_components[i], true);
            if(!component.is_valid())
            {
                continue;
            }

            // handle by type
            switch(component.get_component_type())
            {
                case comp::ComponentType::kScript:
                {
                    game::ScriptManager::instance().remove_component(
                        removed_components[i]
                    );
                    break;
                }
                case comp::ComponentType::kRenderable:
                {
                    renderer::Renderer::get_interface().remove_component(
                        removed_components[i]
                    );
                    break;
                }
                default:
                {
                    // component not owned by any subsystems
                    break;
                }
            }
        }
    }
};

//------------------------------------------------------------------------------
//                            PUBLIC STATIC FUNCTIONS
//------------------------------------------------------------------------------

Engine& Engine::instance()
{
    static Engine inst;
    return inst;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

int Engine::execute()
{
    return m_impl->execute();
}

//------------------------------------------------------------------------------
//                              PRIVATE CONSTRUCTOR
//------------------------------------------------------------------------------

Engine::Engine()
    : m_impl(new EngineImpl())
{
}

//------------------------------------------------------------------------------
//                               PRIVATE DESTRUCTOR
//------------------------------------------------------------------------------

Engine::~Engine()
{
    delete m_impl;
}

} // namespace runtime
} // namespace omi
