/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <omicron/api/comp/script/Script.hpp>
#include <omicron/api/scene/Manager.hpp>

#include "omicron/runtime/RuntimeGlobals.hpp"
#include "omicron/runtime/game/ScriptManager.hpp"


namespace omi
{
namespace runtime
{
namespace game
{

//------------------------------------------------------------------------------
//                            PUBLIC STATIC FUNCTIONS
//------------------------------------------------------------------------------

ScriptManager& ScriptManager::instance()
{
    static ScriptManager inst;
    return inst;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

void ScriptManager::add_component(OMI_COMP_ComponentPtr component)
{
    m_scripts.insert(
        comp::Script::get_script_interface().Script_get_entity_script_ptr(
            component
        )
    );
}

void ScriptManager::remove_component(OMI_COMP_ComponentPtr component)
{
    OMI_SCENE_EntityScriptPtr script_ptr =
        comp::Script::get_script_interface().Script_get_entity_script_ptr(
            component
        );

    auto f_script = m_scripts.find(script_ptr);
    if(f_script != m_scripts.end())
    {
        m_scripts.erase(f_script);
    }
}


void ScriptManager::update()
{
    // call for each script
    for(OMI_SCENE_EntityScriptPtr script : m_scripts)
    {
        OMI_SCENE_Error ec =
            scene::Manager::get_interface().EntityScript_on_update(script);
        if(ec != OMI_SCENE_Error_kNone)
        {
            // TODO: better error - get entity and component data
            global::logger.error(
                "Entity script failed with message: {0}",
                scene::Manager::get_interface().get_error_message()
            );
        }
    }
}

//------------------------------------------------------------------------------
//                              PRIVATE CONSTRUCTOR
//------------------------------------------------------------------------------

ScriptManager::ScriptManager()
{
    m_scripts.clear();
}

//------------------------------------------------------------------------------
//                               PRIVATE DESTRUCTOR
//------------------------------------------------------------------------------

ScriptManager::~ScriptManager()
{
}

} // namespace game
} // namespace runtime
} // namespace omi
