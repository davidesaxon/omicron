/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <arcanecore/base/Preproc.hpp>
#include <arcanecore/base/fsys/FileSystemOperations.hpp>
#include <arcanecore/base/fsys/Path.hpp>

#include <omicron/api/common/Attributes.hpp>
#include <omicron/api/comp/script/Script.hpp>
#include <omicron/api/config/Registry.hpp>

#include "omicron/runtime/RuntimeGlobals.hpp"
#include "omicron/runtime/game/GameBinding.hpp"


namespace omi
{
namespace runtime
{
namespace game
{

//------------------------------------------------------------------------------
//                            PUBLIC STATIC FUNCTIONS
//------------------------------------------------------------------------------

GameBinding& GameBinding::instance()
{
    static GameBinding inst;
    return inst;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

bool GameBinding::startup()
{
    if(m_dl_handle != nullptr)
    {
        throw arc::ex::StateError(
            "GameBinding::startup called after GameBinding already initialised"
        );
    }

    // get the path to game to bind
    StringAttribute path_attr = config::Registry::instance().get_attr(
        #ifdef ARC_OS_WINDOWS
            "omicron.game.bind.windows_path"
        #else
            "omicron.game.bind.linux_path"
        #endif
    );

    arc::fsys::Path game_path;
    for(StringAttribute::DataType const& c : path_attr.get_values())
    {
        game_path << c;
    }
    global::logger.debug(
        "Loading game from: {0}",
        game_path.to_native()
    );

    // check that the file exists
    if(!arc::fsys::is_file(game_path))
    {
        throw arc::ex::IOError(
            "Game dynamic library path either does not exist or is not a "
            "file: " + game_path.to_native()
        );
    }

    // open as dynamic library
    m_dl_handle = arc::dl::open_library(game_path);

    // bind symbols
    GetNameFunc* get_name_func = arc::dl::bind_symbol<GetNameFunc>(
        m_dl_handle,
        "OMI_GAME_get_name"
    );
    GetVersionFunc* get_version_func = arc::dl::bind_symbol<GetVersionFunc>(
        m_dl_handle,
        "OMI_GAME_get_version"
    );
    StartupFunc* startup_func = arc::dl::bind_symbol<StartupFunc>(
        m_dl_handle,
        "OMI_GAME_startup_routine"
    );
    m_firstframe_func = arc::dl::bind_symbol<FirstframeFunc>(
        m_dl_handle,
        "OMI_GAME_firstframe_routine"
    );
    m_shutdown_func = arc::dl::bind_symbol<ShutdownFunc>(
        m_dl_handle,
        "OMI_GAME_shutdown_routine"
    );
    m_create_entity_script_func = arc::dl::bind_symbol<CreateEntityScriptFunc>(
        m_dl_handle,
        "OMI_GAME_create_entity_script"
    );

    // pass the script create function to the component interface
    omi::comp::Script::get_script_interface().set_create_entity_script_func(
        m_create_entity_script_func
    );

    m_name = get_name_func();
    m_version = get_version_func();

    global::logger.info(
        "Bound Game: {0}-{1}",
        m_name,
        m_version
    );

    return startup_func() != OMI_False;
}

bool GameBinding::firstframe()
{
    // nothing to do
    if(m_dl_handle == nullptr)
    {
        return true;
    }

    return m_firstframe_func() != OMI_False;
}

bool GameBinding::shutdown()
{
    // nothing to do
    if(m_dl_handle == nullptr)
    {
        return true;
    }

    bool success = m_shutdown_func() != OMI_False;

    m_shutdown_func = nullptr;
    m_firstframe_func = nullptr;
    m_create_entity_script_func = nullptr;
    if(m_dl_handle != nullptr)
    {
        arc::dl::close_library(m_dl_handle);
    }
    m_dl_handle = nullptr;

    return success;
}

//------------------------------------------------------------------------------
//                              PRIVATE CONSTRUCTOR
//------------------------------------------------------------------------------

GameBinding::GameBinding()
    : m_dl_handle                (nullptr)
    , m_firstframe_func          (nullptr)
    , m_shutdown_func            (nullptr)
    , m_create_entity_script_func(nullptr)
{
}

//------------------------------------------------------------------------------
//                               PRIVATE DESTRUCTOR
//------------------------------------------------------------------------------

GameBinding::~GameBinding()
{
    m_shutdown_func = nullptr;
    m_firstframe_func = nullptr;
    m_create_entity_script_func = nullptr;
    if(m_dl_handle != nullptr)
    {
        arc::dl::close_library(m_dl_handle);
    }
    m_dl_handle = nullptr;
}

} // namespace game
} // namespace runtime
} // namespace omi
