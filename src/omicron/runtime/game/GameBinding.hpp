/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMICRON_RUNTIME_GAME_GAMEBINDING_HPP_
#define OMICRON_RUNTIME_GAME_GAMEBINDING_HPP_

#include <arcanecore/base/dl/DLOperations.hpp>
#include <arcanecore/base/lang/Restrictors.hpp>

#include <omicron/api/scene/SceneCTypes.h>


namespace omi
{
namespace runtime
{
namespace game
{

class GameBinding
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                              TYPE DEFINITIONS
    //--------------------------------------------------------------------------

    typedef char const* (GetNameFunc)();
    typedef char const* (GetVersionFunc)();
    typedef OMI_Bool (StartupFunc)();
    typedef OMI_Bool (FirstframeFunc)();
    typedef OMI_Bool (ShutdownFunc)();
    typedef void (DestroyEntityScriptFunc)(OMI_SCENE_EntityScriptPtr);
    typedef OMI_SCENE_EntityScriptPtr (CreateEntityScriptFunc)(
            char const*,
            OMI_SCENE_EntityId,
            char const*,
            OMI_AttributePtr,
            DestroyEntityScriptFunc**);

    //--------------------------------------------------------------------------
    //                          PUBLIC STATIC FUNCTIONS
    //--------------------------------------------------------------------------

    static GameBinding& instance();

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    bool startup();

    bool firstframe();

    bool shutdown();

private:

    //--------------------------------------------------------------------------
    //                             PRIVATE ATTRIBUTES
    //--------------------------------------------------------------------------

    std::string m_name;
    std::string m_version;

    arc::dl::Handle m_dl_handle;

    FirstframeFunc* m_firstframe_func;
    ShutdownFunc* m_shutdown_func;
    CreateEntityScriptFunc* m_create_entity_script_func;

    //--------------------------------------------------------------------------
    //                            PRIVATE CONSTRUCTOR
    //--------------------------------------------------------------------------

    GameBinding();

    //--------------------------------------------------------------------------
    //                             PRIVATE DESTRUCTOR
    //--------------------------------------------------------------------------

    virtual ~GameBinding();
};

} // namespace game
} // namespace runtime
} // namespace omi

#endif
