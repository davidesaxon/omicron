/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <omicron/api/common/MapBuilder.hpp>
#include <omicron/api/event/Service.hpp>

#include "omi_glfw/Input.hpp"
#include "omi_glfw/Surface.hpp"


namespace omi_glfw
{
namespace input
{

void surface_resize_callback(GLFWwindow* window, int width, int height)
{
    Surface::instance().update_size(arclx::Vector2u(
        static_cast<uint32_t>(width),
        static_cast<uint32_t>(height)
    ));

    // build event data
    omi::Int32Attribute::DataType size[2] = {
        static_cast<omi::Int32Attribute::DataType>(width),
        static_cast<omi::Int32Attribute::DataType>(height)
    };
    omi::event::Service::instance().broadcast(omi::event::Event(
        omi::event::kSurfaceResize,
        omi::Int32Attribute(size, 2)
    ));
}

void surface_move_callback(GLFWwindow* window, int x, int y)
{
    Surface::instance().update_position(arclx::Vector2i(
        static_cast<int32_t>(x),
        static_cast<int32_t>(y)
    ));

    // build event data
    omi::Int32Attribute::DataType position[2] = {
        static_cast<omi::Int32Attribute::DataType>(x),
        static_cast<omi::Int32Attribute::DataType>(y)
    };
    omi::event::Service::instance().broadcast(omi::event::Event(
        omi::event::kSurfaceMove,
        omi::Int32Attribute(position, 2)
    ));
}

void mouse_move_callback(GLFWwindow* window, double x, double y)
{
    // build event data
    omi::Int32Attribute::DataType mouse_position[2] = {
        static_cast<omi::Int32Attribute::DataType>(x),
        static_cast<omi::Int32Attribute::DataType>(y)
    };
    omi::event::Service::instance().broadcast(omi::event::Event(
        omi::event::kMouseMove,
        omi::Int32Attribute(mouse_position, 2)
    ));
}

void mouse_button_callback(
        GLFWwindow* window,
        int button,
        int action,
        int mods)
{
    // determine the event type
    std::string type;
    switch(action)
    {
        case GLFW_PRESS:
            type = omi::event::kMouseButtonPress;
            break;
        case GLFW_RELEASE:
            type = omi::event::kMouseButtonRelease;
            break;
        default:
            // not interested in this event
            return;
    }

    // construct data
    omi::MapBuilder builder;
    builder.insert(
        omi::event::kDataMouseButton,
        omi::Int32Attribute(button)
    );
    builder.insert(
        omi::event::kDataModifiers,
        omi::Int32Attribute(mods)
    );

    omi::event::Service::instance().broadcast(omi::event::Event(
        type,
        builder.build()
    ));
}

void mouse_scroll_callback(
        GLFWwindow* window,
        double amount_x,
        double amount_y)
{
    // construct data
    omi::MapBuilder builder;
    builder.insert(
        omi::event::kDataMouseScrollX,
        omi::Int32Attribute(
            static_cast<omi::Int32Attribute::DataType>(amount_x)
        )
    );
    builder.insert(
        omi::event::kDataMouseScrollY,
        omi::Int32Attribute(
            static_cast<omi::Int32Attribute::DataType>(amount_y)
        )
    );

    omi::event::Service::instance().broadcast(omi::event::Event(
        omi::event::kMouseScroll,
        builder.build()
    ));
}

void mouse_enter_callback(GLFWwindow* window, int entered)
{
    if(entered)
    {
        omi::event::Service::instance().broadcast(omi::event::Event(
            omi::event::kMouseEnter,
            omi::Attribute()
        ));
    }
    else
    {
        omi::event::Service::instance().broadcast(omi::event::Event(
            omi::event::kMouseExit,
            omi::Attribute()
        ));
    }
}

void key_callback(
        GLFWwindow* window,
        int key,
        int scancode,
        int action,
        int mods)
{
    // determine the event type
    std::string type;
    switch(action)
    {
        case GLFW_PRESS:
            type = omi::event::kKeyPress;
            break;
        case GLFW_RELEASE:
            type = omi::event::kKeyRelease;
            break;
        default:
            // not interested in this event
            return;
    }

    // construct data
    omi::MapBuilder builder;
    builder.insert(
        omi::event::kDataKeyCode,
        omi::Int32Attribute(key)
    );
    builder.insert(
        omi::event::kDataModifiers,
        omi::Int32Attribute(mods)
    );

    omi::event::Service::instance().broadcast(omi::event::Event(
        type,
        builder.build()
    ));
}

} // namespace input
} // namespace omi_glfw
