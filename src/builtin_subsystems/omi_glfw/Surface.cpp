/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omi_glfw/Surface.hpp"

#include "omi_glfw/Globals.hpp"
#include "omi_glfw/Input.hpp"


namespace omi_glfw
{

//------------------------------------------------------------------------------
//                            PUBLIC STATIC FUNCTIONS
//------------------------------------------------------------------------------

Surface& Surface::instance()
{
    static Surface inst;
    return inst;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

bool Surface::open()
{
    close();

    global::logger.debug("opening GLFW window.");

    // create based on fullscreen mode
    if(m_fullscreen)
    {
        GLFWmonitor* monitor = glfwGetPrimaryMonitor();
        GLFWvidmode const* mode = glfwGetVideoMode(monitor);

        // update size with new fullscreen size
        m_size = arclx::Vector2u(
            static_cast<uint32_t>(mode->width),
            static_cast<uint32_t>(mode->height)
        );

        m_glfw_window = glfwCreateWindow(
            static_cast<int>(m_size(0)),
            static_cast<int>(m_size(1)),
            m_title.c_str(),
            monitor,
            nullptr
        );
    }
    else
    {
        m_glfw_window = glfwCreateWindow(
            static_cast<int>(m_size(0)),
            static_cast<int>(m_size(1)),
            m_title.c_str(),
            nullptr,
            nullptr
        );
    }

    if(m_glfw_window == nullptr)
    {
        global::logger.critical(
            "glfwCreateWindow failed: {0}",
            omi_glfw::global::error_description
        );
        return false;
    }

    glfwMakeContextCurrent(m_glfw_window);

    // apply initial settings
    set_position(m_position);
    set_cursor_hidden(m_hide_cursor);

    // connect callbacks
    glfwSetWindowSizeCallback(
        m_glfw_window,
        &omi_glfw::input::surface_resize_callback
    );
    glfwSetWindowPosCallback(
        m_glfw_window,
        &omi_glfw::input::surface_move_callback
    );
    glfwSetCursorPosCallback(
        m_glfw_window,
        &omi_glfw::input::mouse_move_callback
    );
    glfwSetMouseButtonCallback(
        m_glfw_window,
        &omi_glfw::input::mouse_button_callback
    );
    glfwSetScrollCallback(
        m_glfw_window,
        &omi_glfw::input::mouse_scroll_callback
    );
    glfwSetCursorEnterCallback(
        m_glfw_window,
        &omi_glfw::input::mouse_enter_callback
    );
    glfwSetKeyCallback(
        m_glfw_window,
        &omi_glfw::input::key_callback
    );

    return true;
}

void Surface::close()
{
    if(m_glfw_window == nullptr)
    {
        return;
    }

    glfwDestroyWindow(m_glfw_window);
    m_glfw_window = nullptr;
}

std::string const& Surface::get_title() const
{
    return m_title;
}

void Surface::set_title(std::string const& title)
{
    m_title = title;
    if(m_glfw_window != nullptr)
    {
        // rely on callback to set m_title
        glfwSetWindowTitle(m_glfw_window, title.c_str());
    }
}

arclx::Vector2u const& Surface::get_size() const
{
    return m_size;
}

void Surface::set_size(arclx::Vector2u const& size)
{
    m_size = size;
    if(m_glfw_window != nullptr)
    {
        glfwSetWindowSize(
            m_glfw_window,
            static_cast<int>(m_size(0)),
            static_cast<int>(m_size(1))
        );
    }
}

void Surface::update_size(arclx::Vector2u const& size)
{
    m_size = size;
}

arclx::Vector2i const& Surface::get_position() const
{
    return m_position;
}

void Surface::set_position(arclx::Vector2i const& position)
{
    m_position = position;
    if(m_glfw_window != nullptr)
    {
        glfwSetWindowPos(
            m_glfw_window,
            static_cast<int>(m_position(0)),
            static_cast<int>(m_position(1))
        );
    }
}

void Surface::update_position(arclx::Vector2i const& position)
{
    m_position = position;
}

bool Surface::is_fullscreen() const
{
    return m_fullscreen;
}

void Surface::set_fullscreen(bool state)
{
    m_fullscreen = state;
    if(m_glfw_window != nullptr)
    {
        // TODO: add support for choosing monitors
        // get monitor information
        GLFWmonitor* monitor = glfwGetPrimaryMonitor();
        GLFWvidmode const* mode = glfwGetVideoMode(monitor);

        // update size with monitor size
        m_size = arclx::Vector2u(
            static_cast<uint32_t>(mode->width),
            static_cast<uint32_t>(mode->height)
        );

        if(m_fullscreen)
        {
            glfwSetWindowMonitor(
                m_glfw_window,
                monitor,
                0,
                0,
                static_cast<int>(m_size(0)),
                static_cast<int>(m_size(1)),
                mode->refreshRate
            );
        }
        else
        {
            glfwSetWindowMonitor(
                m_glfw_window,
                nullptr,
                0,
                0,
                static_cast<int>(m_size(0)),
                static_cast<int>(m_size(1)),
                mode->refreshRate
            );
        }

        // TODO: need to trigger resize?
    }
}

bool Surface::is_cursor_hidden() const
{
    return m_hide_cursor;
}

void Surface::set_cursor_hidden(bool state)
{
    m_hide_cursor = state;
    if(m_glfw_window != nullptr)
    {
        if(m_hide_cursor)
        {
            glfwSetInputMode(m_glfw_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        }
        else
        {
            glfwSetInputMode(m_glfw_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        }
    }
}

bool Surface::should_close() const
{
    return glfwWindowShouldClose(m_glfw_window);
}

void Surface::swap_buffers()
{
    glfwSwapBuffers(m_glfw_window);
}

//------------------------------------------------------------------------------
//                              PRIVATE CONSTRUCTORS
//------------------------------------------------------------------------------

Surface::Surface()
    : m_glfw_window(nullptr)
    , m_title      ("omicron")
    , m_size       (640, 480)
    , m_position   (0, 0)
    , m_fullscreen (false)
    , m_hide_cursor(false)
{
}

//------------------------------------------------------------------------------
//                               PRIVATE DESTRUCTOR
//------------------------------------------------------------------------------

Surface::~Surface()
{
}

} // namespace omi_glfw
