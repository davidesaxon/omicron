/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMI_GLFW_GLFWHOSTSUBSYSTEM_HPP_
#define OMI_GLFW_GLFWHOSTSUBSYSTEM_HPP_

#include <GLFW/glfw3.h>

#include <arcanecore/base/Preproc.hpp>
#include <arcanecore/lx/Vector.hpp>

#include <omicron/api/host/Host.hpp>

#include "omi_glfw/Globals.hpp"
#include "omi_glfw/Input.hpp"
#include "omi_glfw/Surface.hpp"


//------------------------------------------------------------------------------
//                                   API EXPORT
//------------------------------------------------------------------------------

#ifdef ARC_OS_WINDOWS
    #ifdef glfw_host_EXPORTS
        #define GLFW_EXPORT __declspec(dllexport)
    #else
        #define GLFW_EXPORT __declspec(dllimport)
    #endif
#else
    #define GLFW_EXPORT
#endif


//------------------------------------------------------------------------------
//                                PLUGIN FUNCTIONS
//------------------------------------------------------------------------------

extern "C"
{

GLFW_EXPORT char const* OMI_HOST_get_plugin_name()
{
    return "GLFW";
}

GLFW_EXPORT char const* OMI_HOST_get_plugin_version()
{
    return "0.0.1";
}

GLFW_EXPORT OMI_Bool OMI_HOST_startup()
{
    // set the error callback function
    glfwSetErrorCallback(&omi_glfw::global::GLFW_error_callback);

    if(!glfwInit())
    {
        omi_glfw::global::logger.critical(
            "glfwInit failed: {0}",
            omi_glfw::global::error_description
        );
        return OMI_False;
    }

    return OMI_True;
}

GLFW_EXPORT OMI_Bool OMI_HOST_shutdown()
{
    omi_glfw::Surface::instance().close();

    glfwTerminate();

    // remove the error callback function
    glfwSetErrorCallback(nullptr);

    return OMI_True;
}

GLFW_EXPORT char const* OMI_HOST_get_surface_title()
{
    return omi_glfw::Surface::instance().get_title().c_str();
}

GLFW_EXPORT void OMI_HOST_set_surface_title(char const* title)
{
    omi_glfw::Surface::instance().set_title(title);
}

GLFW_EXPORT void OMI_HOST_get_surface_size(
        uint32_t* out_width,
        uint32_t* out_height)
{
    arclx::Vector2u const& size = omi_glfw::Surface::instance().get_size();
    *out_width  = size(0);
    *out_height = size(1);
}

GLFW_EXPORT void OMI_HOST_set_surface_size(uint32_t width, uint32_t height)
{
    omi_glfw::Surface::instance().set_size(
        arclx::Vector2u(width, height)
    );
}

GLFW_EXPORT void OMI_HOST_get_surface_position(int32_t* out_x, int32_t* out_y)
{
    arclx::Vector2i const& position =
        omi_glfw::Surface::instance().get_position();
    *out_x = position(0);
    *out_y = position(1);
}

GLFW_EXPORT void OMI_HOST_set_surface_position(int32_t x, int32_t y)
{
    omi_glfw::Surface::instance().set_position(arclx::Vector2i(x, y));
}

GLFW_EXPORT OMI_Bool OMI_HOST_is_fullscreen()
{
    return omi_glfw::Surface::instance().is_fullscreen();
}

GLFW_EXPORT void OMI_HOST_set_fullscreen(OMI_Bool state)
{
    omi_glfw::Surface::instance().set_fullscreen(state != OMI_False);
}

GLFW_EXPORT OMI_Bool OMI_HOST_is_cursor_hidden()
{
    return omi_glfw::Surface::instance().is_cursor_hidden();
}

GLFW_EXPORT void OMI_HOST_set_cursor_hidden(OMI_Bool state)
{
    omi_glfw::Surface::instance().set_cursor_hidden(state != OMI_False);
}

GLFW_EXPORT OMI_Bool OMI_HOST_open_surface()
{
    return omi_glfw::Surface::instance().open();
}

GLFW_EXPORT void OMI_HOST_main_loop(OMI_HOST_EngineCycleFunc* engine_cycle_func)
{
    while(!omi_glfw::Surface::instance().should_close())
    {
        // TODO: stat frame time

        if(engine_cycle_func() == OMI_False)
        {
            break;
        }

        omi_glfw::Surface::instance().swap_buffers();

        // let the window manager know we're still here
        glfwPollEvents();

        // TODO: mouse locking
    }
}

} // extern C

#endif
