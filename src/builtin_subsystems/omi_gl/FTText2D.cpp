/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omi_gl/FTText2D.hpp"


namespace omi_gl
{

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

FTText2D::FTText2D(omi::comp::Text2D const& text2d_component)
    : m_text2d_component(text2d_component)
    , m_font_resource_id(0)
{
    omi::MapAttribute font_resource = m_text2d_component.get_font_resource();
    omi::Int64Attribute resource_id_attr = font_resource.at("id");
    m_font_resource_id =
        static_cast<omi::res::ResourceId>(resource_id_attr.get_value());
}

FTText2D::FTText2D(FTText2D const& other)
    : m_text2d_component(other.m_text2d_component)
    , m_font_resource_id(other.m_font_resource_id)
{
}

FTText2D::FTText2D(FTText2D&& other)
    : m_text2d_component(std::move(other.m_text2d_component))
    , m_font_resource_id(std::move(other.m_font_resource_id))
{
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

FTText2D::~FTText2D()
{
    // TODO: need static text shutdown
}

//------------------------------------------------------------------------------
//                                   OPERATORS
//------------------------------------------------------------------------------

FTText2D& FTText2D::operator=(FTText2D const& other)
{
    m_text2d_component = other.m_text2d_component;
    m_font_resource_id = other.m_font_resource_id;

    return *this;
}

FTText2D& FTText2D::operator=(FTText2D&& other)
{
    m_text2d_component = std::move(other.m_text2d_component);
    m_font_resource_id = std::move(other.m_font_resource_id);

    return *this;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

std::string_view FTText2D::get_text() const
{
    return m_text2d_component.get_text();
}

omi::res::ResourceId FTText2D::get_font_resource_id() const
{
    return m_font_resource_id;
}

arclx::Vector4f const& FTText2D::get_colour() const
{
    return m_text2d_component.get_colour();
}

float FTText2D::get_size() const
{
    return m_text2d_component.get_size();
}

arclx::Vector2f const& FTText2D::get_position() const
{
    return m_text2d_component.get_position();
}

omi::comp::Text2D::HorizontalOrigin FTText2D::get_horizontal_origin() const
{
    return m_text2d_component.get_horizontal_origin();
}

omi::comp::Text2D::VerticalOrigin FTText2D::get_vertical_origin() const
{
    return m_text2d_component.get_vertical_origin();
}

} // namespace omi_gl
