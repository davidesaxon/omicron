/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <unordered_map>

#include <arcanecore/base/str/StringOperations.hpp>

#include <arcanecore/lx/Vector.hpp>

#include <arcanecore/gl/ShaderProgram.hpp>

#include <omicron/api/common/Attributes.hpp>
#include <omicron/api/common/MapBuilder.hpp>
#include <omicron/api/event/Listener.hpp>
#include <omicron/api/host/Surface.hpp>
#include <omicron/api/res/ResourceId.hpp>
#include <omicron/api/res/Storage.hpp>

#include "omi_gl/FTText2D.hpp"
#include "omi_gl/FontRenderer.hpp"
#include "omi_gl/Globals.hpp"


namespace omi_gl
{

//------------------------------------------------------------------------------
//                                 IMPLEMENTATION
//------------------------------------------------------------------------------

class FontRenderer::FontRendererImpl
    : public omi::event::Listener
    , private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
private:

    //---------------------P R I  V A T E    S T R U C T S----------------------

    struct CachedGlyph
    {
        GLuint texture_id;
        arclx::Vector2i size;
        arclx::Vector2i bearing;
        int64_t advance;
    };

    struct CachedSize
    {
        bool used;
        std::unordered_map<char, CachedGlyph> glyphs;
    };

    //-----------P R I V A T E    M E M B E R    A T T R I B U T E S------------

    FT_Library m_ft;

    std::unordered_map<omi::res::ResourceId, FT_Face> m_faces;
    std::unordered_map<omi::res::ResourceId, std::string> m_font_names;

    std::unordered_map<omi::comp::Text2D, FTText2D> m_text2d;

    std::unordered_map<
        omi::res::ResourceId,
        std::unordered_map<std::size_t, CachedSize>
    > m_font_cache;

    GLuint g_text2d_vao;
    GLuint g_text2d_vbo;
    arcgl::ShaderProgram g_text2d_shader;

    arclx::Vector2u m_surface_size;

public:

    #include <arcanecore/lx/Alignment.hpp>

    //--------------------------C O N S T R U C T O R---------------------------

    FontRendererImpl()
        : g_text2d_vao(0)
        , g_text2d_vbo(0)
    {
    }

    //---------------------------D E S T R U C T O R----------------------------

    ~FontRendererImpl()
    {
    }

    //-------------P U B L I C    M E M B E R    F U N C T I O N S--------------

    bool startup()
    {
        // initialise freetype
        if(FT_Init_FreeType(&m_ft) != 0)
        {
            global::logger.critical("Failed to initialise FreeType");
            return false;
        }

        // define loader funcs
        omi::res::Storage::instance().set_loader_func("ttf", &load_font);
        omi::res::Storage::instance().set_loader_func("otf", &load_font);

        // build the text2d vao and vbo
        glGenVertexArrays(1, &g_text2d_vao);
        glBindVertexArray(g_text2d_vao);
        glGenBuffers(1, &g_text2d_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, g_text2d_vbo);
        glBufferData(
            GL_ARRAY_BUFFER,
            24 * sizeof(float),
            nullptr,
            GL_DYNAMIC_DRAW
        );
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(
            0,                    // attribute
            4,                    // size
            GL_FLOAT,             // type
            GL_FALSE,             // normalized?
            0,                    // stride
            static_cast<void*>(0) // array buffer offset
        );
        glBindVertexArray(0);

        // load and compile the text2d shader
        g_text2d_shader.attach_shader(
            GL_VERTEX_SHADER,
            arc::fsys::Path(
                {"res", "builtin", "shaders", "text2d.vertex.glsl"}
            )
        );
        g_text2d_shader.attach_shader(
            GL_FRAGMENT_SHADER,
            arc::fsys::Path(
                {"res", "builtin", "shaders", "text2d.fragment.glsl"}
            )
        );
        g_text2d_shader.set_attribute_location(0, "v_pos_uv");
        g_text2d_shader.link();

        m_surface_size = omi::host::Surface::instance().get_size();

        // subscribe to events
        subscribe_to_event(omi::event::kSurfaceResize);

        return true;
    }

    bool shutdown()
    {
        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        g_text2d_shader.release();
        glDeleteBuffers(1, &g_text2d_vbo);
        glDeleteVertexArrays(1, &g_text2d_vao);

        // release font caches
        for(auto& font : m_font_cache)
        {
            for(auto& font_size : font.second)
            {
                for(auto & glyph : font_size.second.glyphs)
                {
                    glDeleteTextures(1, &glyph.second.texture_id);
                }
            }
        }

        // release faces
        for(auto& face : m_faces)
        {
            FT_Done_Face(face.second);
        }

        FT_Done_FreeType(m_ft);
        return true;
    }

    void add_text2d(omi::comp::Text2D const& text2d)
    {
        m_text2d.insert(std::make_pair(text2d, FTText2D(text2d)));
    }

    void remove_text2d(omi::comp::Text2D const& text2d)
    {
        auto f_text2d = m_text2d.find(text2d);
        if(f_text2d != m_text2d.end())
        {
            m_text2d.erase(f_text2d);
        }
    }

    void render2d(arclx::Matrix44f const& ortho_matrix)
    {
        // TODO: need a better way of managing GL state
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        // compute aspect ration
        float const aspect_ratio =
            static_cast<float>(m_surface_size(0)) /
            static_cast<float>(m_surface_size(1));

        // render 2d text
        for(auto& text2d : m_text2d)
        {
            render_text2d(
                text2d.second,
                aspect_ratio,
                ortho_matrix
            );
        }

        // clean up any unused caches and reset state
        for(auto& font : m_font_cache)
        {
            for(auto it = font.second.begin(); it != font.second.end();)
            {
                CachedSize& cached_size = it->second;
                if(cached_size.used)
                {
                    cached_size.used = false;
                    ++it;
                }
                else
                {
                    // delete
                    for(auto& glyph : cached_size.glyphs)
                    {
                        glDeleteTextures(1, &glyph.second.texture_id);
                    }
                    it = font.second.erase(it);
                }
            }
        }
    }

protected:

    //----------P R O T E C T E D    M E M B E R    F U N C T I O N S-----------

    virtual void on_event(omi::event::Event const& event) override
    {
        if(event.get_type() == omi::event::kSurfaceResize)
        {
            omi::Int32Attribute size_attr = event.get_data();
            omi::Int32Attribute::ArrayType size_values = size_attr.get_values();
            m_surface_size(0) = size_values[0];
            m_surface_size(1) = size_values[1];
        }
    }

private:

    //------------P R I V A T E    S T A T I C    F U N C T I O N S-------------

    static OMI_RES_Error load_font(
            OMI_RES_ResourceId id,
            char const* path,
            int64_t begin,
            int64_t end,
            OMI_AttributePtr* out_data,
            OMI_RES_RELEASE_Func** out_release_func)
    {
        // TODO: support offsets?
        FT_Face face;
        FT_Error error = FT_New_Face(
            FontRenderer::instance().m_impl->m_ft,
            path,
            0,
            &face
        );
        if(error != 0)
        {
            omi::res::Storage::instance().append_warning(
                "FreeType loader failed to load font \"" + std::string(path) +
                "\""
            );
            return OMI_RES_Error_kParse;
        }

        // store the face
        FontRenderer::instance().m_impl->m_faces[id]      = face;
        FontRenderer::instance().m_impl->m_font_names[id] = path;

        // build data
        omi::MapBuilder builder;
        builder.insert("type", omi::StringAttribute("Font"));
        builder.insert(
            "id",
            omi::Int64Attribute(
                static_cast<omi::Int64Attribute::DataType>(id)
            )
        );
        builder.insert("name", omi::StringAttribute(path));

        omi::MapAttribute output = builder.build();
        omi::Attribute::get_interface().increase_reference(output.get_ptr());
        *out_data = output.get_ptr();
        *out_release_func = &release_font;

        return OMI_RES_Error_kNone;
    }

    static OMI_RES_Error release_font(
            OMI_RES_ResourceId id,
            OMI_AttributePtr data)
    {
        // get the face for the resource id
        auto f_face = FontRenderer::instance().m_impl->m_faces.find(id);
        if(f_face == FontRenderer::instance().m_impl->m_faces.end())
        {
            omi::res::Storage::instance().append_warning(
                "GLRenderer could not release font resource " +
                std::to_string(id) + " as no face is currently stored for " +
                "that id."
            );
            return OMI_RES_Error_kRuntime;
        }

        // free the face
        FT_Done_Face(f_face->second);

        FontRenderer::instance().m_impl->m_faces.erase(f_face);

        return OMI_RES_Error_kNone;
    }

    //------------P R I V A T E    M E M B E R    F U N C T I O N S-------------

    CachedGlyph* get_cached_glyph(
            omi::res::ResourceId font_resource_id,
            FT_Face face,
            char c,
            std::unordered_map<char, CachedGlyph>& glyph_map)
    {
        // get or load the glyph data
        auto f_glyph = glyph_map.find(c);
        // load data?
        if(f_glyph == glyph_map.end())
        {
            // disable byte-alignment restriction
            glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

            // load character glyph
            if(FT_Load_Char(face, c, FT_LOAD_RENDER))
            {
                // TODO: should only report this once?
                global::logger.warning(
                    "FreeType failed to load glyph {0} from font "
                    "\"{1}\"",
                    c,
                    m_font_names[font_resource_id]
                );
                return nullptr;
            }

            // generate texture
            GLuint texture_id = 0;
            glGenTextures(1, &texture_id);
            glBindTexture(GL_TEXTURE_2D, texture_id);
            glTexImage2D(
                GL_TEXTURE_2D,
                0,
                GL_RED,
                face->glyph->bitmap.width,
                face->glyph->bitmap.rows,
                0,
                GL_RED,
                GL_UNSIGNED_BYTE,
                face->glyph->bitmap.buffer
            );
            // set texture options
            glTexParameteri(
                GL_TEXTURE_2D,
                GL_TEXTURE_WRAP_S,
                GL_CLAMP_TO_EDGE
            );
            glTexParameteri(
                GL_TEXTURE_2D,
                GL_TEXTURE_WRAP_T,
                GL_CLAMP_TO_EDGE
            );
            glTexParameteri(
                GL_TEXTURE_2D,
                GL_TEXTURE_MIN_FILTER,
                GL_LINEAR
            );
            glTexParameteri(
                GL_TEXTURE_2D,
                GL_TEXTURE_MAG_FILTER,
                GL_LINEAR
            );
            // unbind
            glBindTexture(GL_TEXTURE_2D, 0);

            // store
            glyph_map[c] = {
                texture_id,
                arclx::Vector2i(
                    face->glyph->bitmap.width,
                    face->glyph->bitmap.rows
                ),
                arclx::Vector2i(
                    face->glyph->bitmap_left,
                    face->glyph->bitmap_top
                ),
                face->glyph->advance.x
            };
            return &glyph_map[c];
        }

        return &f_glyph->second;
    }

    void render_text2d(
            FTText2D const& text2d,
            float aspect_ratio,
            arclx::Matrix44f const& ortho_matrix)
    {
        // determine the pixel height of the text
        float const size = text2d.get_size();
        uint32_t font_size = static_cast<uint32_t>(
            (size * 0.5F) * m_surface_size(1)
        );
        float const pixel_scale = size / static_cast<float>(font_size);

        omi::comp::Text2D::HorizontalOrigin const horizontal_origin =
            text2d.get_horizontal_origin();
        omi::comp::Text2D::VerticalOrigin const vertical_origin =
            text2d.get_vertical_origin();

        // TODO: offset by origin
        float const x_base = text2d.get_position()(0) * aspect_ratio;
        float const y_base = text2d.get_position()(1);

        omi::res::ResourceId const font_resource_id =
            text2d.get_font_resource_id();
        // get the face
        auto f_face = m_faces.find(font_resource_id);
        if(f_face == m_faces.end())
        {
            // TODO: shouldn't be repeated every frame
            global::logger.warning(
                "No FT Face for resource id: {0}",
                font_resource_id
            );
            return;
        }

        // get cached data for this text
        auto f_font = m_font_cache.find(font_resource_id);
        if(f_font == m_font_cache.end())
        {
            // make new entry for this font
            m_font_cache[font_resource_id] =
                std::unordered_map<std::size_t, CachedSize>();
            f_font = m_font_cache.find(font_resource_id);
        }

        // get the cached size data
        auto f_cached_size = f_font->second.find(font_size);
        if(f_cached_size == f_font->second.end())
        {
            // make a new entry for this size
            f_font->second[font_size] = {
                false,
                std::unordered_map<char, CachedGlyph>()
            };
            f_cached_size = f_font->second.find(font_size);
        }
        // mark this font as used
        f_cached_size->second.used = true;

        // get the glyph map
        std::unordered_map<char, CachedGlyph>& glyph_map =
            f_cached_size->second.glyphs;

        FT_Set_Pixel_Sizes(f_face->second, 0, font_size);
        uint32_t const line_height =
            (f_face->second->size->metrics.ascender -
            f_face->second->size->metrics.descender) >> 6;

        // TODO: this can be cached and we can use a hash to check whether the
        //       string has changed
        // split into lines
        std::vector<std::string> const lines =
            arc::str::split(text2d.get_text(), "\n");

        // determine y start position
        float y = y_base;
        if(vertical_origin == omi::comp::Text2D::VerticalOrigin::kCentre)
        {
            y += size * (lines.size() - 1) * 0.5F;
        }
        else if(vertical_origin == omi::comp::Text2D::VerticalOrigin::kBottom)
        {
            y += size * (lines.size() - 1);
        }

        // render each line
        for(std::string const& line : lines)
        {
            float x = x_base;
            // apply offset?
            if(horizontal_origin ==
               omi::comp::Text2D::HorizontalOrigin::kCentre
               ||
               horizontal_origin == omi::comp::Text2D::HorizontalOrigin::kRight)
            {
                float line_size = 0;
                for(char c : line)
                {
                    // get or load the glyph data
                    CachedGlyph* glyph = get_cached_glyph(
                        font_resource_id,
                        f_face->second,
                        c,
                        glyph_map
                    );

                    line_size += (glyph->advance >> 6) * pixel_scale;
                }

                if(horizontal_origin ==
                   omi::comp::Text2D::HorizontalOrigin::kCentre)
                {
                    x -= line_size * 0.5F;
                }
                else if(horizontal_origin ==
                   omi::comp::Text2D::HorizontalOrigin::kRight)
                {
                    x -= line_size;
                }
            }

            // TODO: support unicode
            // iterate over each character
            for(char c : line)
            {
                // get or load the glyph data
                CachedGlyph* glyph = get_cached_glyph(
                    font_resource_id,
                    f_face->second,
                    c,
                    glyph_map
                );

                GLfloat const width  = glyph->size(0) * pixel_scale;
                GLfloat const height = glyph->size(1) * pixel_scale;

                // TODO: there should be a faster of doing this than rebuilding
                //       each time
                // build vertex data
                GLfloat x_min = x + (glyph->bearing(0) * pixel_scale);
                GLfloat x_max = x_min + width;
                GLfloat y_min = 0.0F;
                GLfloat y_max = 0.0F;
                switch(vertical_origin)
                {
                    case omi::comp::Text2D::VerticalOrigin::kTop:
                    {
                        y_max =
                            y -
                            ((line_height - glyph->bearing(1)) * pixel_scale);
                        y_min = y_max - height;
                        break;
                    }
                    case omi::comp::Text2D::VerticalOrigin::kCentre:
                    {
                        y_max =
                            y -
                            ((
                                (line_height * 0.5F) - glyph->bearing(1)
                            ) * pixel_scale);
                        y_min = y_max - height;
                        break;
                    }
                    case omi::comp::Text2D::VerticalOrigin::kBottom:
                    {
                        y_min =
                            y -
                            ((
                                glyph->size(1) - glyph->bearing(1)
                            ) * pixel_scale);
                        y_max = y_min + height;
                        break;
                    }
                }
                GLfloat vertices[] = {
                    x_min, y_max,   0.0, 0.0,
                    x_min, y_min,   0.0, 1.0,
                    x_max, y_min,   1.0, 1.0,

                    x_min, y_max,   0.0, 0.0,
                    x_max, y_min,   1.0, 1.0,
                    x_max, y_max,   1.0, 0.0
                };

                // render
                glBindVertexArray(g_text2d_vao);
                glBindBuffer(GL_ARRAY_BUFFER, g_text2d_vbo);
                glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
                glBindBuffer(GL_ARRAY_BUFFER, 0);
                g_text2d_shader.bind();
                g_text2d_shader.set_uniform_44f(
                    "u_projection_matrix",
                    ortho_matrix
                );
                glBindTexture(GL_TEXTURE_2D, glyph->texture_id);
                g_text2d_shader.set_uniform_1u("u_texture", glyph->texture_id);
                // TODO: get colour
                g_text2d_shader.set_uniform_4f(
                    "u_colour",
                    text2d.get_colour()
                );
                glDrawArrays(GL_TRIANGLES, 0, 6);
                glBindTexture(GL_TEXTURE_2D, 0);
                g_text2d_shader.unbind();
                glBindVertexArray(0);

                // advance position
                x += (glyph->advance >> 6) * pixel_scale;
            }

            y -= size;
        }
    }
};

//------------------------------------------------------------------------------
//                            PUBLIC STATIC FUNCTIONS
//------------------------------------------------------------------------------

FontRenderer& FontRenderer::instance()
{
    static FontRenderer inst;
    return inst;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

bool FontRenderer::startup()
{
    return m_impl->startup();
}

bool FontRenderer::shutdown()
{
    return m_impl->shutdown();
}

void FontRenderer::add_text2d(omi::comp::Text2D const& text2d)
{
    m_impl->add_text2d(text2d);
}

void FontRenderer::remove_text2d(omi::comp::Text2D const& text2d)
{
    m_impl->remove_text2d(text2d);
}

void FontRenderer::render2d(arclx::Matrix44f const& ortho_matrix)
{
    m_impl->render2d(ortho_matrix);
}

//------------------------------------------------------------------------------
//                              PRIVATE CONSTRUCTOR
//------------------------------------------------------------------------------

FontRenderer::FontRenderer()
    : m_impl(new FontRendererImpl())
{
}

//------------------------------------------------------------------------------
//                               PRIVATE DESTRUCTOR
//------------------------------------------------------------------------------

FontRenderer::~FontRenderer()
{
    delete m_impl;
}

} // namespace omi_gl
