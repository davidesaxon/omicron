/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OMI_GL_GLMESH_HPP_
#define OMI_GL_GLMESH_HPP_

#include <GL/glew.h>

#include <arcanecore/base/lang/Restrictors.hpp>

#include <arcanecore/lx/Matrix.hpp>

#include <omicron/api/comp/renderable/Material.hpp>
#include <omicron/api/comp/renderable/Mesh.hpp>
#include <omicron/api/comp/transform/Transform.hpp>

#include "omi_gl/GLTexture.hpp"


namespace omi_gl
{

class GLMesh
    : private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                                CONSTRUCTORS
    //--------------------------------------------------------------------------

    GLMesh(omi::comp::Mesh const& mesh_component);

    GLMesh(GLMesh const& other);

    GLMesh(GLMesh&& other);

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    virtual ~GLMesh();

    //--------------------------------------------------------------------------
    //                                 OPERATORS
    //--------------------------------------------------------------------------

    GLMesh& operator=(GLMesh const& other);

    GLMesh& operator=(GLMesh&& other);

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    GLuint get_vao() const;

    GLuint get_index_buffer() const;

    GLsizei get_size() const;

    omi::comp::Transform const& get_transform() const;

    omi::comp::Material const& get_material() const;

    GLTexture const& get_texture() const;

private:

    //--------------------------------------------------------------------------
    //                             PRIVATE ATTRIBUTES
    //--------------------------------------------------------------------------

    GLuint m_vao;
    GLuint m_index_buffer;
    GLsizei m_size;
    omi::comp::Transform m_transform;
    omi::comp::Material m_material;
    GLTexture m_texture;
};

} // namespace omi_gl

#endif
