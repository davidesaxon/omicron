/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omi_gl/GLTexture.hpp"

// TODO: REMOVE ME
#include <iostream>


namespace omi_gl
{

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

GLTexture::GLTexture()
    : m_texture_id(0)
{
}

GLTexture::GLTexture(omi::comp::Texture const& texture_component)
    : m_texture_id(0)
{
    omi::MapAttribute data = texture_component.get_data();

    omi::Int64Attribute id_attr = data.at("texture_id");
    m_texture_id = static_cast<GLuint>(id_attr.get_value());
}

GLTexture::GLTexture(GLTexture const& other)
    : m_texture_id(other.m_texture_id)
{
}

GLTexture::GLTexture(GLTexture&& other)
    : m_texture_id(other.m_texture_id)
{
    other.m_texture_id = 0;
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

GLTexture::~GLTexture()
{
}

//------------------------------------------------------------------------------
//                                   OPERATORS
//------------------------------------------------------------------------------

GLTexture& GLTexture::operator=(GLTexture const& other)
{
    m_texture_id = other.m_texture_id;
    return *this;
}

GLTexture& GLTexture::operator=(GLTexture&& other)
{
    m_texture_id = other.m_texture_id;
    other.m_texture_id = 0;
    return *this;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

GLuint GLTexture::get_texture_id() const
{
    return m_texture_id;
}

} // namespace omi_gl
