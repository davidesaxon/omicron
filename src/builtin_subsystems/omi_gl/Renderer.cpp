/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <arcanecore/lx/MatrixMath44f.hpp>

#include <omicron/api/comp/renderable/Camera.hpp>
#include <omicron/api/host/Surface.hpp>
#include <omicron/api/scene/Manager.hpp>

#include "omi_gl/FontRenderer.hpp"
#include "omi_gl/Globals.hpp"
#include "omi_gl/Renderer.hpp"



namespace omi_gl
{

//------------------------------------------------------------------------------
//                            PUBLIC STATIC FUNCTIONS
//------------------------------------------------------------------------------

Renderer& Renderer::instance()
{
    static Renderer inst;
    return inst;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

bool Renderer::startup()
{
    // initialise GLEW
    GLenum glew_error = glewInit();
    if(glew_error != GLEW_OK)
    {
        global::logger.critical(
            "Failed to initialise GLEW: {0}",
            glewGetErrorString(glew_error)
        );
        return false;
    }

    // startup the font renderer
    if(!FontRenderer::instance().startup())
    {
        return false;
    }

    // TODO: source this
    // set gl state
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glClearColor(0.55F, 0.8F, 1.0F, 1.0F);

    // get the surface size and set the viewport
    m_surface_size = omi::host::Surface::instance().get_size();
    glViewport(0, 0, m_surface_size(0), m_surface_size(1));

    // subscribe to events
    subscribe_to_event(omi::event::kSurfaceResize);

    // TODO: TESTING
    //--------------------------------------------------------------------------
    // load and compile the shader
    m_shader.attach_shader(
        GL_VERTEX_SHADER,
        arc::fsys::Path({"res", "dev", "shaders", "texture.vertex.glsl"})
    );
    m_shader.attach_shader(
        GL_FRAGMENT_SHADER,
        arc::fsys::Path({"res", "dev", "shaders", "texture.fragment.glsl"})
    );
    // TODO:
    m_shader.set_attribute_location(0, "v_position");
    m_shader.set_attribute_location(1, "v_uv");
    m_shader.set_attribute_location(2, "v_normal");
    m_shader.link();
    //--------------------------------------------------------------------------

    return true;
}

bool Renderer::shutdown()
{
    bool success = true;

    // shutdown the font renderer
    success = FontRenderer::instance().shutdown();

    unsubsribe_from_all_events();

    return success;
}

void Renderer::render()
{
    // clear
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // TODO: camera testing
    //--------------------------------------------------------------------------
    omi::comp::Camera camera =
        omi::scene::Manager::instance().get_active_camera();
    arclx::Matrix44f const projection_matrix = arclx::perspective_44f(
        camera.get_horizontal_fov(),
        camera.get_aspect_ratio(),
        camera.get_near_clip(),
        camera.get_far_clip()
    );
    arclx::Matrix44f const view_matrix = camera.get_transform().eval();
    //--------------------------------------------------------------------------

    // TODO: testing
    //--------------------------------------------------------------------------

    arclx::Matrix44f const pv_matrix = projection_matrix * view_matrix;
    for(auto const& mesh : m_meshes)
    {
        // get the material
        omi::comp::Material material = mesh.first.get_material();

        // cull backfaces?
        if(mesh.first.get_cull_backfaces())
        {
            glEnable(GL_CULL_FACE);
        }
        else
        {
            glDisable(GL_CULL_FACE);
        }

        glBindVertexArray(mesh.second.get_vao());
        m_shader.bind();
        omi::comp::Transform const& mesh_transform =
            mesh.second.get_transform();
        arclx::Matrix44f const mvp_matrix = pv_matrix * mesh_transform.eval();
        m_shader.set_uniform_44f(
            "u_mvp_matrix",
            mvp_matrix
        );
        m_shader.set_uniform_3f("u_albedo", material.get_albedo());
        if(material.has_texture())
        {
            GLuint texture_id = mesh.second.get_texture().get_texture_id();
            glBindTexture(GL_TEXTURE_2D, texture_id);
            m_shader.set_uniform_1u("u_has_texture", 1U);
            m_shader.set_uniform_1u("u_texture", texture_id);
        }
        else
        {
            m_shader.set_uniform_1u("u_has_texture", 0U);
        }
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh.second.get_index_buffer());
        glDrawElements(
            GL_TRIANGLES,
            mesh.second.get_size(),
            GL_UNSIGNED_INT,
            static_cast<void*>(0)
        );
        glBindTexture(GL_TEXTURE_2D, 0);
        m_shader.unbind();
        glBindVertexArray(0);
    }

    // glBindVertexArray(m_vao);
    // m_shader.bind();
    // // TODO:
    // m_shader.set_uniform_3f(
    //     "colour",
    //     arclx::Vector3f(0.85F, 0.4F, 0.5F)
    // );
    // // TODO: need to compute camera matrix
    // m_shader.set_uniform_44f(
    //     "u_mvp_matrix",
    //     projection_matrix * view_matrix
    // );
    // glDrawArrays(GL_TRIANGLES, 0, m_number_of_points);
    // m_shader.unbind();
    // glBindVertexArray(0);
    //--------------------------------------------------------------------------

    // build orthographic matrix for 2d rendering
    float const half_aspect = camera.get_aspect_ratio() / 2.0F;
    arclx::Matrix44f ortho_matrix = arclx::orthographic_44f(
        -half_aspect,
         half_aspect,
        -0.5F,
         0.5,
         0.0F,
         10000.0F
    );

    FontRenderer::instance().render2d(ortho_matrix);
}

void Renderer::add_renderable(omi::comp::Renderable const& renderable)
{
    omi::comp::RenderableType type = renderable.get_renderable_type();
    switch(type)
    {
        case omi::comp::RenderableType::kTexture:
        {
            omi::comp::Texture texture = renderable;
            m_textures.insert(std::make_pair(texture, GLTexture(texture)));
            break;
        }
        case omi::comp::RenderableType::kMesh:
        {
            omi::comp::Mesh mesh = renderable;
            m_meshes.insert(std::make_pair(mesh, GLMesh(mesh)));
            break;
        }
        case omi::comp::RenderableType::kText2D:
        {
            omi::comp::Text2D text2d = renderable;
            FontRenderer::instance().add_text2d(text2d);
            break;
        }
        default:
        {
            // no need to store this renderable
            break;
        }
    }
}

void Renderer::remove_renderable(omi::comp::Renderable const& renderable)
{
    omi::comp::RenderableType type = renderable.get_renderable_type();
    switch(type)
    {
        case omi::comp::RenderableType::kTexture:
        {
            // TODO: this seems like overkill?
            omi::comp::Texture texture = renderable;
            auto f_texture = m_textures.find(texture);
            if(f_texture != m_textures.end())
            {
                m_textures.erase(f_texture);
            }
            break;
        }
        case omi::comp::RenderableType::kMesh:
        {
            omi::comp::Mesh mesh = renderable;
            auto f_mesh = m_meshes.find(mesh);
            if(f_mesh != m_meshes.end())
            {
                m_meshes.erase(f_mesh);
            }
            break;
        }
        case omi::comp::RenderableType::kText2D:
        {
            omi::comp::Text2D text2d = renderable;
            FontRenderer::instance().remove_text2d(text2d);
            break;
        }
        default:
        {
            // no need to store this renderable
            break;
        }
    }
}

GLTexture const& Renderer::get_gl_texture(
        omi::comp::Texture const& texture_component) const
{
    auto f_texture = m_textures.find(texture_component);
    if(f_texture == m_textures.end())
    {
        throw arc::ex::KeyError(
            "No GL texture for given Omicron texture component: " +
            std::to_string(texture_component.get_id())
        );
    }
    return f_texture->second;
}

//------------------------------------------------------------------------------
//                           PROTECTED MEMBER FUNCTIONS
//------------------------------------------------------------------------------

void Renderer::on_event(omi::event::Event const& event)
{
    if(event.get_type() == omi::event::kSurfaceResize)
    {
        omi::Int32Attribute size_attr = event.get_data();
        omi::Int32Attribute::ArrayType size_values = size_attr.get_values();
        m_surface_size(0) = size_values[0];
        m_surface_size(1) = size_values[1];

        // TODO: this should be delayed until we come to render
        glViewport(0, 0, m_surface_size(0), m_surface_size(1));
    }
}

//------------------------------------------------------------------------------
//                              PRIVATE CONSTRUCTOR
//------------------------------------------------------------------------------

Renderer::Renderer()
    : m_surface_size(640, 480)
{
}

//------------------------------------------------------------------------------
//                               PRIVATE DESTRUCTOR
//------------------------------------------------------------------------------

Renderer::~Renderer()
{
}

} // namespace omi_gl
