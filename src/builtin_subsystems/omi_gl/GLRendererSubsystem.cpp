/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <arcanecore/base/Preproc.hpp>

#include <GL/glew.h>

#include <omicron/api/comp/Component.hpp>
#include <omicron/api/comp/renderable/Renderable.hpp>
#include <omicron/api/res/ResourceCTypes.h>

#include "omi_gl/Globals.hpp"
#include "omi_gl/Renderer.hpp"
#include "omi_gl/Resource.hpp"


//------------------------------------------------------------------------------
//                                   API EXPORT
//------------------------------------------------------------------------------

#ifdef ARC_OS_WINDOWS
    #ifdef gl_renderer_EXPORTS
        #define GL_EXPORT __declspec(dllexport)
    #else
        #define GL_EXPORT __declspec(dllimport)
    #endif
#else
    #define GL_EXPORT
#endif

//------------------------------------------------------------------------------
//                                PLUGIN FUNCTIONS
//------------------------------------------------------------------------------

extern "C"
{

GL_EXPORT char const* OMI_RENDERER_get_plugin_name()
{
    return "GL";
}

GL_EXPORT char const* OMI_RENDERER_get_plugin_version()
{
    return "0.0.1";
}

GL_EXPORT OMI_Bool OMI_RENDERER_startup()
{
    return OMI_True;
}

GL_EXPORT OMI_Bool OMI_RENDERER_firstframe()
{
    return omi_gl::Renderer::instance().startup();
}

GL_EXPORT OMI_Bool OMI_RENDERER_shutdown()
{
    return omi_gl::Renderer::instance().shutdown();
}

GL_EXPORT void OMI_RENDERER_add_component(OMI_COMP_ComponentPtr component)
{
    omi::comp::Renderable renderable(component);
    if(!renderable.is_valid())
    {
        return;
    }

    omi_gl::Renderer::instance().add_renderable(renderable);
}

GL_EXPORT void OMI_RENDERER_remove_component(OMI_COMP_ComponentPtr component)
{
    omi::comp::Renderable renderable(component);
    if(!renderable.is_valid())
    {
        return;
    }

    omi_gl::Renderer::instance().remove_renderable(renderable);
}

GL_EXPORT OMI_Bool OMI_RENDERER_load_resource(
        OMI_RES_ResourceId id,
        OMI_AttributePtr data,
        OMI_AttributePtr* out_resource,
        OMI_RES_RELEASE_Func** out_release_func)
{
    omi::MapAttribute res;
    bool success = omi_gl::resource::load(
        id,
        omi::MapAttribute(data),
        res,
        out_release_func
    );

    if(!success)
    {
        return OMI_False;
    }

    omi::Attribute::get_interface().increase_reference(res.get_ptr());
    *out_resource = res.get_ptr();

    return OMI_True;
}

GL_EXPORT void OMI_RENDERER_render()
{
    omi_gl::Renderer::instance().render();
}

} // extern C
