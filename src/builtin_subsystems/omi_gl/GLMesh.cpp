/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "omi_gl/GLMesh.hpp"
#include "omi_gl/Renderer.hpp"

// TODO: REMOVE ME
#include <iostream>


namespace omi_gl
{

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

GLMesh::GLMesh(omi::comp::Mesh const& mesh_component)
    : m_vao         (0)
    , m_index_buffer(0)
    , m_size        (0)
    , m_transform   (mesh_component.get_transform())
    , m_material    (mesh_component.get_material())
{
    omi::MapAttribute data = mesh_component.get_data();

    omi::Int64Attribute vao_attr = data.at("vao");
    m_vao = static_cast<GLuint>(vao_attr.get_value());

    omi::Int64Attribute index_buffer_attr = data.at("index_buffer");
    m_index_buffer = static_cast<GLuint>(index_buffer_attr.get_value());

    omi::Int64Attribute size_attr = data.at("size");
    m_size = static_cast<GLsizei>(size_attr.get_value());

    if(m_material.has_texture())
    {
        m_texture =
            Renderer::instance().get_gl_texture(m_material.get_texture());
    }
}

GLMesh::GLMesh(GLMesh const& other)
    : m_vao         (other.m_vao)
    , m_index_buffer(other.m_index_buffer)
    , m_size        (other.m_size)
    , m_transform   (other.m_transform)
    , m_material    (other.m_material)
    , m_texture     (other.m_texture)
{
}

GLMesh::GLMesh(GLMesh&& other)
    : m_vao         (other.m_vao)
    , m_index_buffer(other.m_index_buffer)
    , m_size        (other.m_size)
    , m_transform   (std::move(other.m_transform))
    , m_material    (std::move(other.m_material))
    , m_texture     (std::move(other.m_texture))
{
    other.m_vao = 0;
    other.m_index_buffer = 0;
    other.m_size = 0;
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

GLMesh::~GLMesh()
{
}

//------------------------------------------------------------------------------
//                                   OPERATORS
//------------------------------------------------------------------------------

GLMesh& GLMesh::operator=(GLMesh const& other)
{
    m_vao = other.m_vao;
    m_index_buffer = other.m_index_buffer;
    m_size = other.m_size;
    m_transform = other.m_transform;
    m_material = other.m_material;
    m_texture = other.m_texture;
    return *this;
}

GLMesh& GLMesh::operator=(GLMesh&& other)
{
    m_vao = other.m_vao;
    other.m_vao = 0;
    m_index_buffer = other.m_index_buffer;
    other.m_index_buffer = 0;
    m_size = other.m_size;
    other.m_size = 0;
    m_transform = std::move(other.m_transform);
    m_material = std::move(other.m_material);
    m_texture = std::move(other.m_texture);
    return *this;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

GLuint GLMesh::get_vao() const
{
    return m_vao;
}

GLuint GLMesh::get_index_buffer() const
{
    return m_index_buffer;
}

GLsizei GLMesh::get_size() const
{
    return m_size;
}

omi::comp::Transform const& GLMesh::get_transform() const
{
    return m_transform;
}

omi::comp::Material const& GLMesh::get_material() const
{
    return m_material;
}

GLTexture const& GLMesh::get_texture() const
{
    return m_texture;
}

} // namespace omi_gl
