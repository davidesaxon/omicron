/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <GL/glew.h>

#include <omicron/api/common/MapBuilder.hpp>
#include <omicron/api/res/Storage.hpp>

#include "omi_gl/Resource.hpp"


namespace omi_gl
{
namespace resource
{

static OMI_RES_Error release_texture(
        OMI_RES_ResourceId id,
        OMI_AttributePtr data)
{
    if(data == nullptr)
    {
        omi::res::Storage::instance().append_warning(
            "GLRenderer could not release texture resource " +
            std::to_string(id) + " as null data was supplied."
        );
        return OMI_RES_Error_kRuntime;
    }

    omi::MapAttribute map_data(data, true);
    if(!map_data.is_valid())
    {
        omi::res::Storage::instance().append_warning(
            "GLRenderer could not release texture resource " +
            std::to_string(id) + " as invalid data was supplied."
        );
        return OMI_RES_Error_kRuntime;
    }

    omi::Int64Attribute id_attr = map_data.at("texture_id");
    if(!id_attr.is_valid())
    {
        omi::res::Storage::instance().append_warning(
            "GLRenderer could not release texture resource " +
            std::to_string(id) + " as invalid data was supplied."
        );
        return OMI_RES_Error_kRuntime;
    }

    GLuint texture = static_cast<GLuint>(id_attr.get_value());
    glDeleteTextures(1, &texture);

    return OMI_RES_Error_kNone;
}

static OMI_RES_Error release_mesh(
        OMI_RES_ResourceId id,
        OMI_AttributePtr data)
{
    if(data == nullptr)
    {
        omi::res::Storage::instance().append_warning(
            "GLRenderer could not release mesh resource " +
            std::to_string(id) + " as null data was supplied."
        );
        return OMI_RES_Error_kRuntime;
    }

    omi::MapAttribute map_data(data, true);
    if(!map_data.is_valid())
    {
        omi::res::Storage::instance().append_warning(
            "GLRenderer could not release mesh resource " +
            std::to_string(id) + " as invalid data was supplied."
        );
        return OMI_RES_Error_kRuntime;
    }

    omi::Int64Attribute index_buffer_attr = map_data.at("index_buffer");
    if(!index_buffer_attr.is_valid())
    {
        omi::res::Storage::instance().append_warning(
            "GLRenderer could not release texture resource " +
            std::to_string(id) + " as invalid data was supplied."
        );
        return OMI_RES_Error_kRuntime;
    }

    omi::Int64Attribute vao_attr = map_data.at("index_buffer");
    if(!vao_attr.is_valid())
    {
        omi::res::Storage::instance().append_warning(
            "GLRenderer could not release texture resource " +
            std::to_string(id) + " as invalid data was supplied."
        );
        return OMI_RES_Error_kRuntime;
    }

    GLuint index_buffer = static_cast<GLuint>(index_buffer_attr.get_value());
    glDeleteBuffers(1, &index_buffer);
    GLuint vao = static_cast<GLuint>(vao_attr.get_value());
    glDeleteVertexArrays(1, &vao);

    return OMI_RES_Error_kNone;
}

bool load(
        omi::res::ResourceId id,
        omi::MapAttribute const& data,
        omi::MapAttribute& out_resource,
        OMI_RES_RELEASE_Func** out_release_func)
{

    // handle by type
    omi::StringAttribute type_attr = data.at("type");
    std::string const type = type_attr.get_value("");
    if(type == "Texture")
    {
        // get width
        omi::Int32Attribute width_attr = data.at("width");
        if(!width_attr.is_valid())
        {
            omi::res::Storage::instance().append_warning(
                "GLRenderer could not load texture resource as \"width\" is "
                "not a valid int32 attribute"
            );
            return false;
        }
        GLsizei const width = static_cast<GLsizei>(width_attr.get_value());
        // get height
        omi::Int32Attribute height_attr = data.at("height");
        if(!height_attr.is_valid())
        {
            omi::res::Storage::instance().append_warning(
                "GLRenderer could not load texture resource as \"height\" is "
                "not a valid int32 attribute"
            );
            return false;
        }
        GLsizei const height = static_cast<GLsizei>(height_attr.get_value());

        // get channels
        omi::StringAttribute channels_attr = data.at("channels");
        if(!channels_attr.is_valid())
        {
            omi::res::Storage::instance().append_warning(
                "GLRenderer could not load texture resource as \"channels\" is "
                "not a valid string attribute"
            );
            return false;
        }
        GLint format = GL_RGB;
        if(channels_attr.size() == 4)
        {
            format = GL_RGBA;
        }
        else if(channels_attr.size() != 3)
        {
            omi::res::Storage::instance().append_warning(
                "GLRenderer could not load texture resource as image data "
                "contains an unsupported number of channels: " +
                std::to_string(channels_attr.size())
            );
            return false;
        }

        // get data type
        omi::StringAttribute data_type_attr = data.at("data_type");
        if(!data_type_attr.is_valid())
        {
            omi::res::Storage::instance().append_warning(
                "GLRenderer could not load texture resource as \"data_type\" "
                "is not a valid string attribute"
            );
            return false;
        }
        // TODO: support other data types?
        std::string const data_type = data_type_attr.get_value();
        if(data_type != "unsigned_byte")
        {
            omi::res::Storage::instance().append_warning(
                "GLRenderer could not load texture resource as image data is "
                "using an unsupported data type: \"" + data_type + "\""
            );
            return false;
        }

        // get image data
        omi::ByteAttribute data_attr = data.at("data");
        if(!data_attr.is_valid())
        {
            omi::res::Storage::instance().append_warning(
                "GLRenderer could not load texture resource as \"data\" is not "
                "a valid byte attribute"
            );
            return false;
        }

        GLuint texture = 0;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);

        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            format,
            width,
            height,
            0,
            format,
            GL_UNSIGNED_BYTE,
            &data_attr.get_values()[0]
        );
        glGenerateMipmap(GL_TEXTURE_2D);

        glBindTexture(GL_TEXTURE_2D, 0);

        omi::MapBuilder builder;
        builder.insert("type", omi::StringAttribute("Texture"));
        builder.insert("width", width_attr);
        builder.insert("height", height_attr);
        builder.insert("channels", channels_attr);
        builder.insert("data_type", data_type_attr);
        builder.insert(
            "texture_id",
            omi::Int64Attribute(
                static_cast<omi::Int64Attribute::DataType>(texture)
            )
        );
        out_resource = builder.build();
        *out_release_func = &release_texture;
    }
    else if(type == "Mesh")
    {
        // get positions
        omi::Float32Attribute p_attr = data.at("geometry.vertex.p");
        if(!p_attr.is_valid())
        {
            omi::res::Storage::instance().append_warning(
                "GLRenderer could not load mesh resource as "
                "\"geometry.vertex.p\" is not a valid float32 attribute"
            );
            return false;
        }
        omi::Float32Attribute::ArrayType positions = p_attr.get_values();
        if((positions.size()) % 3 != 0)
        {
            omi::res::Storage::instance().append_warning(
                "GLRenderer could not load mesh resource as number of "
                "\"geometry.vertex.p\" is not divisible by 3"
            );
            return false;
        }

        // has uvs?
        omi::Float32Attribute t_attr = data.at("geometry.vertex.t");
        omi::Float32Attribute::ArrayType uvs;
        if(t_attr.is_valid())
        {
            uvs = t_attr.get_values();
            if((uvs.size()) % 2 != 0)
            {
                omi::res::Storage::instance().append_warning(
                    "GLRenderer could not load mesh resource as number of "
                    "\"geometry.vertex.t\" is not divisible by 2"
                );
                return false;
            }
        }

        // has normals?
        omi::Float32Attribute n_attr = data.at("geometry.vertex.n");
        omi::Float32Attribute::ArrayType normals;
        if(n_attr.is_valid())
        {
            normals = n_attr.get_values();
            if((normals.size()) % 3 != 0)
            {
                omi::res::Storage::instance().append_warning(
                    "GLRenderer could not load mesh resource as number of "
                    "\"geometry.vertex.n\" is not divisible by 3"
                );
                return false;
            }
        }

        // get indices
        omi::Int32Attribute index_attr = data.at("geometry.vertex.index");
        if(!index_attr.is_valid())
        {
            omi::res::Storage::instance().append_warning(
                "GLRenderer could not load mesh resource as "
                "\"geometry.vertex.index\" is not a valid int32 attribute"
            );
            return false;
        }
        omi::Int32Attribute::ArrayType indices = index_attr.get_values();
        if((indices.size()) % 3 != 0)
        {
            omi::res::Storage::instance().append_warning(
                "GLRenderer could not load mesh resource as number of "
                "\"geometry.vertex.index\" is not divisible by 3"
            );
            return false;
        }

        // // compute number of points
        // GLsizei const number_of_points =
        //     static_cast<GLsizei>(vertex_positions.size() / 3);

        // set up the VAO
        GLuint vao = 0;
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);

        GLuint current_attr = 0;

        // build position buffer
        GLuint position_buffer = 0;
        glGenBuffers(1, &position_buffer);
        glBindBuffer(GL_ARRAY_BUFFER, position_buffer);
        glBufferData(
            GL_ARRAY_BUFFER,
            positions.size() * sizeof(float),
            &positions[0],
            GL_STATIC_DRAW
        );
        // set up position attribute
        glEnableVertexAttribArray(current_attr);
        glVertexAttribPointer(
            current_attr,         // attribute
            3,                    // size
            GL_FLOAT,             // type
            GL_FALSE,             // normalized?
            0,                    // stride
            static_cast<void*>(0) // array buffer offset
        );
        ++current_attr;

        // uv buffer?
        GLuint uv_buffer = 0;
        if(!uvs.empty())
        {
            // build buffer
            glGenBuffers(1, &uv_buffer);
            glBindBuffer(GL_ARRAY_BUFFER, uv_buffer);
            glBufferData(
                GL_ARRAY_BUFFER,
                uvs.size() * sizeof(float),
                &uvs[0],
                GL_STATIC_DRAW
            );
            // set up attribute
            glEnableVertexAttribArray(current_attr);
            glVertexAttribPointer(
                current_attr,         // attribute
                2,                    // size
                GL_FLOAT,             // type
                GL_FALSE,             // normalized?
                0,                    // stride
                static_cast<void*>(0) // array buffer offset
            );
            ++current_attr;
        }

        // normal buffer?
        GLuint normal_buffer = 0;
        if(!normals.empty())
        {
            // build buffer
            glGenBuffers(1, &normal_buffer);
            glBindBuffer(GL_ARRAY_BUFFER, normal_buffer);
            glBufferData(
                GL_ARRAY_BUFFER,
                normals.size() * sizeof(float),
                &normals[0],
                GL_STATIC_DRAW
            );
            // set up attribute
            glEnableVertexAttribArray(current_attr);
            glVertexAttribPointer(
                current_attr,         // attribute
                3,                    // size
                GL_FLOAT,             // type
                GL_FALSE,             // normalized?
                0,                    // stride
                static_cast<void*>(0) // array buffer offset
            );
            ++current_attr;
        }

        // clean up
        glBindVertexArray(0);
        glDeleteBuffers(1, &position_buffer);
        if(uv_buffer != 0)
        {
            glDeleteBuffers(1, &uv_buffer);
        }
        if(normal_buffer != 0)
        {
            glDeleteBuffers(1, &normal_buffer);
        }
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // set up index buffer
        GLuint index_buffer = 0;
        glGenBuffers(1, &index_buffer);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
        glBufferData(
            GL_ELEMENT_ARRAY_BUFFER,
            indices.size() * sizeof(unsigned int),
            &indices[0],
            GL_STATIC_DRAW
        );
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        omi::MapBuilder builder;
        builder.insert("type", omi::StringAttribute("Mesh"));
        builder.insert(
            "vao",
            omi::Int64Attribute(static_cast<int64_t>(vao))
        );
        builder.insert(
            "index_buffer",
            omi::Int64Attribute(static_cast<int64_t>(index_buffer))
        );
        builder.insert(
            "size",
            omi::Int64Attribute(static_cast<int64_t>(indices.size()))
        );
        out_resource = builder.build();
        *out_release_func = &release_mesh;
    }
    else
    {
        omi::res::Storage::instance().append_warning(
            "GLRenderer could not load resource with unsupported type: \"" +
            type + "\""
        );
        return false;
    }

    return true;
}

} // namespace resource
} // namespace omi_gl
