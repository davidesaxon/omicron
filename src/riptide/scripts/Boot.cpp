/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <arcanecore/base/Preproc.hpp>

#include <omicron/api/res/Storage.hpp>
#include <omicron/api/scene/Manager.hpp>

#include "riptide/RiptideGlobals.hpp"


//------------------------------------------------------------------------------
//                                   API EXPORT
//------------------------------------------------------------------------------

#ifdef ARC_OS_WINDOWS
 #ifdef riptide_EXPORTS
    #define OMI_GAME_EXPORT __declspec(dllexport)
 #else
    #define OMI_GAME_EXPORT __declspec(dllimport)
 #endif
#else
    #define OMI_GAME_EXPORT
#endif

//------------------------------------------------------------------------------
//                                   FUNCTIONS
//------------------------------------------------------------------------------

extern "C"
{

OMI_GAME_EXPORT OMI_Bool OMI_GAME_startup_routine()
{
    riptide::global::logger.debug("Riptide startup");

    // TODO: this should be triggered by a config file instead?
    omi::scene::Manager::instance().set_current_scene("DevScene");

    return OMI_True;
}

OMI_GAME_EXPORT OMI_Bool OMI_GAME_firstframe_routine()
{
    riptide::global::logger.debug("Riptide firstframe startup");

    return OMI_True;
}

OMI_GAME_EXPORT OMI_Bool OMI_GAME_shutdown_routine()
{
    riptide::global::logger.debug("Riptide shutdown");

    return OMI_True;
}

} // extern "C"
