/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <omicron/api/event/Listener.hpp>
#include <omicron/api/event/Service.hpp>
#include <omicron/api/scene/EntityScript.hpp>

#include "riptide/RiptideGlobals.hpp"


namespace riptide
{

class GameManager
    : public omi::scene::EntityScript
    , public omi::event::Listener
{
public:

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    GameManager(
            omi::scene::EntityId id,
            std::string const& name,
            omi::Attribute const& data)
        : omi::scene::EntityScript(id, name, data)
    {
        // subscribe to events
        subscribe_to_event(omi::event::kKeyPress);
    }

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    virtual ~GameManager()
    {
    }

protected:

    //--------------------------------------------------------------------------
    //                         PROTECTED MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    virtual void on_event(omi::event::Event const& event) override
    {
        if(event.get_type() == omi::event::kKeyPress)
        {
            omi::MapAttribute key_data = event.get_data();
            omi::Int32Attribute key_code_attr =
                key_data.at(omi::event::kDataKeyCode);
            omi::event::KeyCode key_code =
                static_cast<omi::event::KeyCode>(key_code_attr.get_value());

            if(key_code == omi::event::KeyCode::kEscape)
            {
                omi::event::Service::instance().broadcast(omi::event::Event(
                    omi::event::kEngineShutdown
                ));
            }
        }
    }
};

} // namespace riptide

OMI_GAME_DEFINE_ENTITY_SCRIPT(
    riptide::GameManager,
    riptide_dev_GameManager
);
