/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <arcanecore/lx/Alignment.hpp>
#include <arcanecore/lx/MatrixMath44f.hpp>
#include <arcanecore/lx/Vector.hpp>

#include <omicron/api/comp/transform/TranslateTransform.hpp>
#include <omicron/api/event/Listener.hpp>
#include <omicron/api/scene/EntityScript.hpp>
#include <omicron/api/sys/Timing.hpp>

#include "riptide/RiptideGlobals.hpp"


namespace riptide
{

//------------------------------------------------------------------------------
//                                    GLOBALS
//------------------------------------------------------------------------------

namespace
{

static float const MOVE_SPEED = 0.1F;

} // namespace anonymous

class Player
    : public omi::scene::EntityScript
    , public omi::event::Listener
{
private:

    //--------------------------------------------------------------------------
    //                             PRIVATE ATTRIBUTES
    //--------------------------------------------------------------------------

    omi::comp::TranslateTransform m_transform;

    arclx::Vector3f m_translation;

    bool m_forwards;
    bool m_backwards;
    bool m_left;
    bool m_right;

public:

    ARCLX_ALIGNED_NEW;

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    Player(
            omi::scene::EntityId id,
            std::string const& name,
            omi::Attribute const& data)
        : omi::scene::EntityScript(id, name, data)
        , m_transform             (get_component("transform"))
        , m_forwards              (false)
        , m_backwards             (false)
        , m_left                  (false)
        , m_right                 (false)
    {
        // subscribe to events
        subscribe_to_event(omi::event::kKeyPress);
        subscribe_to_event(omi::event::kKeyRelease);
    }

    virtual ~Player()
    {
    }

protected:

    //--------------------------------------------------------------------------
    //                         PROTECTED MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    virtual void on_update() override
    {
        // no movement
        if(!m_forwards && !m_backwards && !m_left && !m_right)
        {
            return;
        }

        // determine movement direction
        arclx::Vector3f move_direction(0.0F, 0.0F, 0.0F);
        if(m_forwards)
        {
            move_direction(2) += 1.0F;
        }
        if(m_backwards)
        {
            move_direction(2) -= 1.0F;
        }
        if(m_left)
        {
            move_direction(0) += 1.0F;
        }
        if(m_right)
        {
            move_direction(0) -= 1.0F;
        }
        move_direction.normalize();

        m_transform.translation() +=
            move_direction *
            MOVE_SPEED *
            omi::sys::Timing::instance().get_scale();
    }

    virtual void on_event(omi::event::Event const& event) override
    {
        if(event.get_type() == omi::event::kKeyPress)
        {
            omi::MapAttribute key_data = event.get_data();
            omi::Int32Attribute key_code_attr =
                key_data.at(omi::event::kDataKeyCode);
            omi::event::KeyCode key_code =
                static_cast<omi::event::KeyCode>(key_code_attr.get_value());

            switch(key_code)
            {
                case omi::event::KeyCode::kW:
                    m_forwards = true;
                    break;
                case omi::event::KeyCode::kS:
                    m_backwards = true;
                    break;
                case omi::event::KeyCode::kA:
                    m_left = true;
                    break;
                case omi::event::KeyCode::kD:
                    m_right = true;
                    break;
                case omi::event::KeyCode::kSpace:
                    omi::scene::Manager::instance().create_entity(
                        "CannonBall",
                        "",
                        omi::Attribute()
                    );
                    break;
                default:
                    break;
            }
        }
        else if(event.get_type() == omi::event::kKeyRelease)
        {
            omi::MapAttribute key_data = event.get_data();
            omi::Int32Attribute key_code_attr =
                key_data.at(omi::event::kDataKeyCode);
            omi::event::KeyCode key_code =
                static_cast<omi::event::KeyCode>(key_code_attr.get_value());

            switch(key_code)
            {
                case omi::event::KeyCode::kW:
                    m_forwards = false;
                    break;
                case omi::event::KeyCode::kS:
                    m_backwards = false;
                    break;
                case omi::event::KeyCode::kA:
                    m_left = false;
                    break;
                case omi::event::KeyCode::kD:
                    m_right = false;
                    break;
                default:
                    break;
            }
        }
    }
};

} // namespace riptide

OMI_GAME_DEFINE_ENTITY_SCRIPT(
    riptide::Player,
    riptide_dev_Player
);
