/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <arcanecore/base/math/MathOperations.hpp>

#include <arcanecore/lx/Alignment.hpp>
#include <arcanecore/lx/MatrixMath44f.hpp>
#include <arcanecore/lx/Quaternion.hpp>
#include <arcanecore/lx/Vector.hpp>

#include <omicron/api/comp/renderable/Camera.hpp>
#include <omicron/api/comp/transform/MatrixTransform.hpp>
#include <omicron/api/event/Listener.hpp>
#include <omicron/api/host/Surface.hpp>
#include <omicron/api/scene/EntityScript.hpp>
#include <omicron/api/scene/Manager.hpp>

#include "riptide/RiptideGlobals.hpp"


namespace riptide
{

//------------------------------------------------------------------------------
//                                    GLOBALS
//------------------------------------------------------------------------------

namespace
{

// TODO: get from config?
static arclx::Vector2f const DEFAULT_SENSOR_SIZE(28.2478F, 14.9273F);
static float const ROTATE_SENSITIVITY = 0.0075F;

} // namespace anonymous

class OrbitalCamera
    : public omi::scene::EntityScript
    , public omi::event::Listener
{
private:

    //--------------------------------------------------------------------------
    //                             PRIVATE ATTRIBUTES
    //--------------------------------------------------------------------------

    omi::comp::MatrixTransform m_transform;
    omi::comp::Camera m_camera;

    arclx::Vector3f m_translation;
    arclx::Quaternionf m_rotation;
    float m_center_of_interest;

    arclx::Vector2u m_surface_size;

    // the last mouse position
    bool m_last_mouse_unset;
    arclx::Vector2f m_last_mouse_pos;

public:

    ARCLX_ALIGNED_NEW;

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    OrbitalCamera(
            omi::scene::EntityId id,
            std::string const& name,
            omi::Attribute const& data)
        : omi::scene::EntityScript(id, name, data)
        , m_transform             (get_component("transform"))
        , m_camera                (get_component("camera"))
        , m_translation           (26.8199F, -51.0542F, -16.5578F)
        , m_rotation              (
            arclx::AxisAnglef(45.0F, arclx::Vector3f::UnitX()) *
            arclx::AxisAnglef(45.0F, arclx::Vector3f::UnitY()) *
            arclx::AxisAnglef( 0.0F, arclx::Vector3f::UnitZ())
        )
        , m_center_of_interest    (60.0F)
        , m_last_mouse_unset      (true)
    {
        omi::scene::Manager::instance().set_active_camera(m_camera);

        m_surface_size = omi::host::Surface::instance().get_size();
        update_sensor_size();

        // subscribe to events
        subscribe_to_event(omi::event::kMouseMove);
        subscribe_to_event(omi::event::kKeyPress);
        subscribe_to_event(omi::event::kSurfaceResize);
    }

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    virtual ~OrbitalCamera()
    {
    }

protected:

    //--------------------------------------------------------------------------
    //                         PROTECTED MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    virtual void on_update() override
    {
    }

    virtual void on_event(omi::event::Event const& event) override
    {
        if(event.get_type() == omi::event::kMouseMove)
        {
            omi::Int32Attribute position_attr = event.get_data();
            omi::Int32Attribute::ArrayType position_values =
                position_attr.get_values();
            arclx::Vector2f position(
                static_cast<float>(position_values[0]),
                static_cast<float>(position_values[1])
            );

            // first time?
            if(m_last_mouse_unset)
            {
                m_last_mouse_pos = position;
                m_last_mouse_unset = false;
                return;
            }

            // get the distance the mouse has moved
            arclx::Vector2f mouse_distance(
                position(0) - m_last_mouse_pos(0),
                position(1) - m_last_mouse_pos(1)
            );
            m_last_mouse_pos = position;

            arclx::Vector3f const poi = get_point_of_interest();

            arclx::Quaternionf x_rot = arclx::Quaternionf(
                arclx::AxisAnglef(
                    mouse_distance(1) * ROTATE_SENSITIVITY,
                    arclx::Vector3f::UnitX()
                )
            );
            arclx::Quaternionf y_rot = arclx::Quaternionf(
                arclx::AxisAnglef(
                    mouse_distance(0) * ROTATE_SENSITIVITY,
                    arclx::Vector3f::UnitY()
                )
            );
            m_rotation = x_rot * m_rotation * y_rot;

            recompute_translation(poi);

            // apply to camera matrix
            arclx::Matrix44f rot_matrix = arclx::Matrix44f::Identity();
            rot_matrix.block<3, 3>(0, 0) = m_rotation.toRotationMatrix();
            m_transform.matrix() =
                rot_matrix * arclx::translate_44f(m_translation);
        }
        else if(event.get_type() == omi::event::kSurfaceResize)
        {
            omi::Int32Attribute size_attr = event.get_data();
            omi::Int32Attribute::ArrayType size_values = size_attr.get_values();
            m_surface_size(0) = size_values[0];
            m_surface_size(1) = size_values[1];
            update_sensor_size();
        }
    }

private:

    //--------------------------------------------------------------------------
    //                          PRIVATE MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    arclx::Vector3f get_point_of_interest() const
    {
        arclx::Vector3f const coi_v3(0.0F, 0.0F, m_center_of_interest);
        arclx::Matrix33f const rot_inv_m3 = m_rotation.matrix().inverse();
        return m_translation + (rot_inv_m3 * coi_v3);
    }

    void recompute_translation(arclx::Vector3f const& poi)
    {
        arclx::Vector3f const coi_v3(0.0F, 0.0F, -m_center_of_interest);
        arclx::Matrix33f const rot_inv_m3 = m_rotation.matrix().inverse();
        m_translation = poi + (rot_inv_m3 * coi_v3);
    }

    void update_sensor_size()
    {
        static float const sensor_aspect_ratio =
            DEFAULT_SENSOR_SIZE(1) / DEFAULT_SENSOR_SIZE(0);
        float const aspect_ratio =
            static_cast<float>(m_surface_size(1)) /
            static_cast<float>(m_surface_size(0));

        m_camera.set_sensor_size(arclx::Vector2f(
            DEFAULT_SENSOR_SIZE(0),
            DEFAULT_SENSOR_SIZE(1) * (aspect_ratio / sensor_aspect_ratio)
        ));
    }
};

} // namespace riptide

OMI_GAME_DEFINE_ENTITY_SCRIPT(
    riptide::OrbitalCamera,
    riptide_dev_OrbitalCamera
);
