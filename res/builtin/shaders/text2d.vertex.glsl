#version 130

in vec4 v_pos_uv;

out vec2 f_uv;

uniform mat4 u_projection_matrix;
uniform vec2 u_position;

void main()
{
    gl_Position = u_projection_matrix * vec4(v_pos_uv.xy, -1.0, 1.0);
    f_uv = v_pos_uv.zw;
}
