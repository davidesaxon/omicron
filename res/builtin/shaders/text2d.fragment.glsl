#version 130

in vec2 f_uv;

uniform sampler2D u_texture;
uniform vec4 u_colour;

out vec4 out_colour;


void main()
{
    vec4 glyph_mask = vec4(1.0, 1.0, 1.0, texture(u_texture, f_uv).r);
    out_colour = u_colour * glyph_mask;
}
