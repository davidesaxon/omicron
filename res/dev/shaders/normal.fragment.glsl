#version 130

in vec3 f_normal;

out vec4 out_colour;


void main()
{
    out_colour = vec4(abs(f_normal), 1.0);
}
