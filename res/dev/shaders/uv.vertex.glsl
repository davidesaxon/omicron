#version 130

in vec3 v_position;
in vec2 v_uv;
in vec3 v_normal;

uniform mat4 u_mvp_matrix;

out vec2 f_uv;


void main()
{
    gl_Position = u_mvp_matrix * vec4(v_position, 1.0);
    f_uv = v_uv;
}
