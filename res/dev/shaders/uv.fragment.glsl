#version 130

in vec2 f_uv;

out vec4 out_colour;


void main()
{
    out_colour = vec4(f_uv, 0.0, 1.0);
}
