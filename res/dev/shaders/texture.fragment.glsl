#version 130

in vec2 f_uv;
in vec3 f_normal;

uniform vec3 u_albedo;
uniform uint u_has_texture;
uniform sampler2D u_texture;

out vec4 out_colour;


void main()
{
    if(u_has_texture != 0U)
    {
        out_colour = texture(u_texture, f_uv);
    }
    else
    {
        out_colour = vec4(u_albedo, 1.0);
    }
}
