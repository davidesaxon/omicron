#version 130

flat in uint v_side;
flat in float v_tone;

out vec4 out_colour;

uniform uint id_pass;
uniform vec3 colour;
uniform uint highlighted;

void main()
{
    vec3 final_colour = colour;

    if(v_side <= 1U)
    {
        // do nothing
    }
    else if(v_side <= 3U)
    {
        if(final_colour.r >= 0.05)
        {
            final_colour.r = final_colour.r - 0.05;
        }
        else
        {
            final_colour.r = final_colour.r + 0.05;
        }
        if(final_colour.g >= 0.05)
        {
            final_colour.g = final_colour.g - 0.05;
        }
        else
        {
            final_colour.g = final_colour.g + 0.05;
        }
        if(final_colour.b >= 0.05)
        {
            final_colour.b = final_colour.b - 0.05;
        }
        else
        {
            final_colour.b = final_colour.b + 0.05;
        }
    }
    else
    {
        if(final_colour.r >= 0.025)
        {
            final_colour.r = final_colour.r - 0.025;
        }
        else
        {
            final_colour.r = final_colour.r + 0.025;
        }
        if(final_colour.g >= 0.025)
        {
            final_colour.g = final_colour.g - 0.025;
        }
        else
        {
            final_colour.g = final_colour.g + 0.025;
        }
        if(final_colour.b >= 0.025)
        {
            final_colour.b = final_colour.b - 0.025;
        }
        else
        {
            final_colour.b = final_colour.b + 0.025;
        }
    }

    out_colour = vec4(final_colour, 1.0);
}
