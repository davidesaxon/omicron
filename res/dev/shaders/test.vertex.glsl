#version 130

in vec3 vertex_position;
in uint vertex_side;
in float vertex_tone;

flat out uint v_side;
flat out float v_tone;

uniform mat4 u_mvp_matrix;

void main()
{
    gl_Position = u_mvp_matrix * vec4(vertex_position, 1.0);
    v_side = vertex_side;
    v_tone = vertex_tone;
}
