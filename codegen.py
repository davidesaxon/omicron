#!/usr/bin/env python
# -*- coding: utf-8 -*-

# enforce python 3 compatibility
from __future__ import absolute_import, division, print_function

import os
import sys


#-------------------------------------------------------------------------------
#                                     GLOBALS
#-------------------------------------------------------------------------------

NAMESPACE_TAG = 'OMI_CODEGEN_NAMESPACE'
FUNCTION_TAG = 'OMI_CODEGEN_FUNCTION'


#-------------------------------------------------------------------------------
#                                     CLASSES
#-------------------------------------------------------------------------------

class CFunction:

    #---------------------------C O N S T R U C T O R---------------------------

    def __init__(self, ret_type, name, parameters):
        self.ret_type = ret_type
        self.name = name
        self.parameters = parameters


#-------------------------------------------------------------------------------
#                                    FUNCTIONS
#-------------------------------------------------------------------------------

def run_codgen(paths):
    # search for implementation files
    impl_files = paths
    if not impl_files:
        for p in os.walk('src'):
            dir_path = p[0]
            for f in p[2]:
                if f.endswith('Impl.cpp'):
                    impl_files.append(dir_path + '/' + f)

    # process each implementation file
    for impl_file in impl_files:
        # namsepace parsing
        parsing_namespace = False
        cpp_namespace = []
        c_namespace = ''
        # function parsing
        parsing_function = 0
        function_preamble = []
        function_ret_type = ''
        function_name = ''
        current_parameter = []
        function_parameters = []
        functions = []
        #-----------------------------------------------------------------------
        with open(impl_file) as in_file:
            # get the root directory of the file
            root_dir = '/'.join(impl_file.split('/')[:-1])
            # get the base name of the file
            base_file_name = impl_file.split('/')[-1][:-8]
            # process each line of the file
            line_num = 0
            for line in in_file:
                line_num += 1
                if line.endswith('\n'):
                    line = line[:-1]
                # start codegening a namespace?
                if line == NAMESPACE_TAG or \
                   line.startswith(NAMESPACE_TAG + ' '):
                    parsing_namespace = True
                # start codegeging a function?
                elif line == FUNCTION_TAG or \
                     line.startswith(FUNCTION_TAG + ' '):
                    parsing_function = 1
                # parsing namespace?
                if parsing_namespace:
                    line_words = line.split(' ')
                    for word in line_words:
                        word = word.strip()
                        if word == NAMESPACE_TAG or word == 'namespace':
                            continue
                        # TODO: support multi-line comments
                        elif word == '//':
                            break
                        elif word == '{':
                            parsing_namespace = False
                            break
                        cpp_namespace.append(word)
                        c_namespace += '{0}_'.format(word.upper())
                # TODO: support namespace ends
                # parsing function?
                if parsing_function != 0:
                    line_words = line.split(' ')
                    for word in line_words:
                        word = word.strip()
                        if not word:
                            continue
                        # parsing preamble?
                        if parsing_function == 1:
                            # TODO: check for any other decorators?
                            if word == FUNCTION_TAG or word == 'static':
                                continue
                            # set name, and move onto the next parsing state
                            if '(' in word:
                                function_ret_type = ' '.join(function_preamble)
                                comps = word.split('(')
                                function_name = comps[0]
                                parsing_function += 1
                                word = comps[1]
                                if not word:
                                    continue
                            elif word == '(':
                                if len(function_preamble) < 2:
                                    raise RuntimeError(
                                        ('PARSE_FAIL:\n\tfile: {0}\n\tline:' +
                                         '{0}\n\t\tFunction with no return ' +
                                         'type and/or name: ').format(
                                            impl_file,
                                            line_num
                                        )
                                    )
                                function_ret_type = \
                                    ' '.join(function_preamble[:-1])
                                function_name = function_preamble[-1]
                                parsing_function += 1
                                continue
                            else:
                                function_preamble.append(word)
                                continue
                        # parsing parameters?
                        if parsing_function == 2:
                            param_done = False
                            func_done = False
                            if word == ','or word == ')':
                                param_done = True
                                if word == ')':
                                    func_done = True
                            elif word.endswith(',') or word.endswith(')'):
                                current_parameter.append(word[:-1])
                                param_done = True
                                if word.endswith(')'):
                                    func_done = True
                            else:
                                current_parameter.append(word)
                            # finished a parameter?
                            if param_done:
                                # TODO: unnamed parameters with decorators
                                if len(current_parameter) == 1:
                                    function_parameters.append(
                                        ' '.join(current_parameter)
                                    )
                                elif len(current_parameter) > 1:
                                    function_parameters.append(
                                        ' '.join(current_parameter[:-1])
                                    )
                                current_parameter = []
                            # finished the entire function?
                            if func_done:
                                # complete parsing
                                functions.append(CFunction(
                                    function_ret_type,
                                    function_name,
                                    function_parameters
                                ))
                                parsing_function = 0
                                function_preamble = []
                                function_ret_type = ''
                                function_name = ''
                                current_parameter = []
                                function_parameters = []
        #-----------------------------------------------------------------------
        # any work to do
        if not functions:
            continue
        # check if we need to make the codegen directory
        codgen_dir = '{0}/_codegen'.format(root_dir)
        if not os.path.exists(codgen_dir):
            os.makedirs(codgen_dir)
        # write the contents of the symbols file
        #-----------------------------------------------------------------------
        symbols_content = ''
        # header
        symbols_content += '/*!\n'
        symbols_content += ' * \\file\n'
        symbols_content += ' * \copyright Copyright (c) 2018, David Saxon\n'
        symbols_content += ' *            All rights reserved.\n'
        symbols_content += ' * \\note This file was automatically generated.\n'
        symbols_content += ' */\n'
        # include guards
        include_guard = '{0}CODEGEN_{1}CSMYBOLS_H_'.format(
            c_namespace,
            base_file_name.upper()
        )
        symbols_content += '#ifndef {0}\n'.format(include_guard)
        symbols_content += '#define {0}\n'.format(include_guard)
        symbols_content += '\n\n'
        # no mangle begin
        symbols_content += '#ifdef __cplusplus\n'
        symbols_content += 'extern "C" {\n'
        symbols_content += '#endif\n'
        symbols_content += '\n'
        # functions
        for function in functions:
            symbols_content += '// {0}{1}\n'.format(
                c_namespace,
                function.name
            )
            symbols_content += \
                ('static char const* {0}{1}_{2}_SYMBOL = ' +
                 '"{3} {0}{1}_{2}({4})";\n\n').format(
                    c_namespace,
                    base_file_name,
                    function.name,
                    function.ret_type,
                    ', '.join(function.parameters)
                )
        # no mangle end
        symbols_content += '#ifdef __cplusplus\n'
        symbols_content += '} // extern "C"\n'
        symbols_content += '#endif\n'
        symbols_content += '\n'
        # include guard end
        symbols_content += '#endif'

        # write the interface file
        symbols_path = codgen_dir + '/{0}CSymbols.h'.format(base_file_name)
        with open(symbols_path, 'w') as symbols_file:
            symbols_file.write(symbols_content)
        #-----------------------------------------------------------------------
        # write the contents of the interface file
        #-----------------------------------------------------------------------
        interface_content = ''
        # header
        interface_content += '/*!\n'
        interface_content += ' * \\file\n'
        interface_content += ' * \copyright Copyright (c) 2018, David Saxon\n'
        interface_content += ' *            All rights reserved.\n'
        interface_content += ' * \\note This file was automatically generated.\n'
        interface_content += ' */\n'
        # include guards
        include_guard = '{0}CODEGEN_{1}INTERFACE_HPP_'.format(
            c_namespace,
            base_file_name.upper()
        )
        interface_content += '#ifndef {0}\n'.format(include_guard)
        interface_content += '#define {0}\n'.format(include_guard)
        interface_content += '\n'
        # includes
        interface_content += '#include <cassert>\n'
        interface_content += '#include <string>\n\n'
        interface_content += '#include <arcanecore/base/dl/DLOperations.hpp>\n'
        interface_content += '#include <arcanecore/base/fsys/Path.hpp>\n'
        interface_content += \
            '#include <arcanecore/base/lang/Restrictors.hpp>\n\n'
        interface_content += '#include "{0}/{1}CSymbols.h"\n'.format(
            codgen_dir[4:],
            base_file_name
        )
        interface_content += '\n\n'
        # namespace starts
        for namespace in cpp_namespace:
            interface_content += 'namespace {0}\n'.format(namespace)
            interface_content += '{\n'
        interface_content += '\n'
        # class start
        interface_content += 'class {0}Interface\n'.format(base_file_name)
        interface_content += \
            '    : private arc::lang::Noncopyable\n'
        interface_content += \
            '    , private arc::lang::Nonmovable\n'
        interface_content += \
            '    , private arc::lang::Noncomparable\n'
        interface_content += '{\n'
        interface_content += 'public:\n'
        interface_content += '\n'
        # type definitions
        interface_content += \
            '    //--------------------------------------------------------' + \
            '------------------\n'
        interface_content += \
            '    //                              TYPE DEFINITIONS\n'
        interface_content += \
            '    //--------------------------------------------------------' + \
            '------------------\n'
        interface_content += '\n'
        interface_content += \
            '    typedef OMI_Bool (BindingFunc)(char const*, void**);\n'
        interface_content += '\n'
        # public attributes
        interface_content += \
            '    //--------------------------------------------------------' + \
            '------------------\n'
        interface_content += \
            '    //                             PUBLIC ATTRIBUTES\n'
        interface_content += \
            '    //--------------------------------------------------------' + \
            '------------------\n'
        interface_content += '\n'
        # function pointers
        for function in functions:
            interface_content += '    // {0}\n'.format(function.name)
            interface_content += '    {0} (*{1})({2});\n'.format(
                function.ret_type,
                function.name,
                ', '.join(function.parameters)
            )
        interface_content += '\n'
        # constructor
        interface_content += \
            '    //--------------------------------------------------------' + \
            '------------------\n'
        interface_content += \
            '    //                                CONSTRUCTOR\n'
        interface_content += \
            '    //--------------------------------------------------------' + \
            '------------------\n'
        interface_content += '\n'
        interface_content += \
            '    {0}Interface(std::string const& libname)\n'.format(
                base_file_name
            )
        first_init_list = True
        for function in functions:
            if first_init_list:
                interface_content += "        : "
                first_init_list = False
            else:
                interface_content += "        , "
            interface_content += '{0}(nullptr)\n'.format(function.name)
        interface_content += '    {\n'
        # TODO: FIX ME
        # dl open
        interface_content += '        #ifdef ARC_OS_WINDOWS\n'
        interface_content += '            arc::fsys::Path libpath({\n'
        interface_content += '                "build",\n'
        interface_content += '                "windows",\n'
        interface_content += '                "Release",\n'
        interface_content += '                libname + "_impl.dll"\n'
        interface_content += '            });\n'
        interface_content += '        #elif defined(ARC_OS_UNIX)\n'
        interface_content += '            arc::fsys::Path libpath({\n'
        interface_content += '                "build",\n'
        interface_content += '                "linux",\n'
        interface_content += '                "lib" + libname + "_impl.so"\n'
        interface_content += '            });\n'
        interface_content += '        #else\n'
        interface_content += '            assert(false);\n'
        interface_content += '        #endif\n'
        interface_content += '\n'
        interface_content += \
            ('        arc::dl::Handle lib_handle = arc::dl::' +
             'open_library(libpath);\n')
        interface_content += \
            ('        BindingFunc* binding_func = arc::dl::bind_symbol' +
             '<BindingFunc>(lib_handle, "{0}{1}_BINDING_IMPL");\n').format(
                c_namespace,
                base_file_name
            )
        interface_content += '\n'
        # bind functions
        for function in functions:
            interface_content += \
                ('        if(binding_func({0}{1}_{2}_SYMBOL, ' +
                 '(void**) &{2}) != 0)\n').format(
                    c_namespace,
                    base_file_name,
                    function.name
                )
            interface_content += '        {\n'
            # TODO: report failed bindings somehow
            interface_content += '        }\n'
        interface_content += '    }\n'
        # class end
        interface_content += '};\n'
        interface_content += '\n'
        # namespace ends
        for namespace in cpp_namespace:
            interface_content += '}} // namespace {0}\n'.format(namespace)
        interface_content += '\n\n'
        # include guard end
        interface_content += '#endif'

        # write the interface file
        interface_path = codgen_dir + '/{0}Interface.hpp'.format(base_file_name)
        with open(interface_path, 'w') as interface_file:
            interface_file.write(interface_content)
        #-----------------------------------------------------------------------
        # write the contents of the binding file
        #-----------------------------------------------------------------------
        binding_content = ''
        # header
        binding_content += '/*!\n'
        binding_content += ' * \\file\n'
        binding_content += ' * \copyright Copyright (c) 2018, David Saxon\n'
        binding_content += ' *            All rights reserved.\n'
        binding_content += ' * \\note This file was automatically generated.\n'
        binding_content += ' */\n'
        # includes
        binding_content += '#include <cstring>\n\n'
        binding_content += '#include "omicron/api/API.h"\n\n'
        binding_content += '#include "{0}/{1}CSymbols.h"\n\n\n'.format(
            codgen_dir[4:],
            base_file_name
        )
        # no mangle begin
        binding_content += '#ifdef __cplusplus\n'
        binding_content += 'extern "C" {\n'
        binding_content += '#endif\n'
        binding_content += '\n'
        # binding function
        binding_content += \
            ('OMI_API_EXPORT int {0}{1}_BINDING_IMPL(char const* func_def, ' +
             'void** func_ptr)\n').format(
                c_namespace,
                base_file_name
            )
        binding_content += '{\n'
        for function in functions:
            binding_content += \
                '    if(strcmp(func_def, {0}{1}_{2}_SYMBOL) == 0)\n'.format(
                    c_namespace,
                    base_file_name,
                    function.name
                )
            binding_content += '    {\n'
            binding_content += \
                '        *func_ptr = (void*) &{0}::{1};\n'.format(
                    '::'.join(cpp_namespace),
                    function.name
                )
            binding_content += '        return 0;\n'
            binding_content += '    }\n'
        binding_content += '\n'
        binding_content += '    return 1;\n'
        binding_content += '}\n'
        binding_content += '\n'
        # no mangle end
        binding_content += '#ifdef __cplusplus\n'
        binding_content += '} // extern "C"\n'
        binding_content += '#endif\n'
        binding_content += '\n'

        # write the binding file
        binding_path = codgen_dir + '/{0}Binding.inl'.format(base_file_name)
        with open(binding_path, 'w') as binding_file:
            binding_file.write(binding_content)
        #-----------------------------------------------------------------------

#-------------------------------------------------------------------------------
#                                      SCRIPT
#-------------------------------------------------------------------------------

paths=[]
if len(sys.argv) > 1:
    paths = sys.argv[1:]

run_codgen(paths=paths)
